<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class SendPartnerMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->data = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'helloa@vivocarat.com';
        $name = 'Vivocarat Support';
        $subject = 'Vivocarat - New partner bro';
        
//        return $this->view('email.contactus')
//                    ->with(['name'=>$this->data['name'],
//                    'email'=>$this->data['email'],
//                    'phone'=>$this->data['phone'],
//                    'message'=>$this->data['message']])
//                    ->from($address,$name)
//                    ->replyTo($address,$name)
//                    ->subject($subject);
        return $this->view('email.partner')
                    ->with([
                        'name'=>$this->data['name']
                           ])
                    ->with([
                        'email'=>$this->data['email']
                           ])
                    ->with([
                        'phone'=>$this->data['phone']
                           ])
                    ->with([
                        'address'=>$this->data['address']
                           ])
                    ->with([
                        'brand'=>$this->data['brand']
                           ])
                    ->from($address,$name)
                    ->replyTo($address,$name)
                    ->subject($subject);

    }
}