<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class SendCustomizeMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct($request)
     {
         $this->data = $request;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $address = 'hello@vivocarat.com';
      $name = 'Vivocarat Support';
      $subject = 'Vivocarat - Customize';

      // $mail = $this->view('email.customize')
      //             ->with([
      //                 'name'=>$this->data['name']
      //                    ])
      //             ->with([
      //                 'email'=>$this->data['email']
      //                    ])
      //             ->with([
      //                 'phone'=>$this->data['phone']
      //                    ])
      //             ->with([
      //                 'comment'=>$this->data['comment']
      //                    ])
      //             ->attach($this->data['image_1'])
      //             ->attach($this->data['image_2'])
      //             ->attach($this->data['image_3'])
      //             ->from($address,$name)
      //             ->replyTo($address,$name)
      //             ->subject($subject);

      $mail = $this->view('email.customize')
                  ->with([
                      'name'=>$this->data['name']
                         ])
                  ->with([
                      'email'=>$this->data['email']
                         ])
                  ->with([
                      'phone'=>$this->data['phone']
                         ])
                  ->with([
                      'comment'=>$this->data['comment']
                         ])
                  ->from($address,$name)
                  ->replyTo($address,$name)
                  ->subject($subject);

          if($this->data['image_1'] != NULL || $this->data['image_1'] != '')
          {
            if (file_exists('uploads' . DIRECTORY_SEPARATOR . $this->data['image_1']))
            {
              $mail = $mail->attach('uploads' . DIRECTORY_SEPARATOR . $this->data['image_1']);
            }
          }

          if($this->data['image_2'] != NULL || $this->data['image_2'] != '')
          {
            if (file_exists('uploads' . DIRECTORY_SEPARATOR . $this->data['image_2']))
            {
              $mail = $mail->attach('uploads' . DIRECTORY_SEPARATOR . $this->data['image_2']);
            }
          }

          if($this->data['image_3'] != NULL || $this->data['image_3'] != '')
          {
            if (file_exists('uploads' . DIRECTORY_SEPARATOR . $this->data['image_3']))
            {
              $mail = $mail->attach('uploads' . DIRECTORY_SEPARATOR . $this->data['image_3']);
            }
          }
      return $mail;
    }
}
