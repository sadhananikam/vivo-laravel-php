<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class SendSellerLocationEnquiryAdminMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->data = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'hello@vivocarat.com';
        $name = 'Vivocarat Support';
        $subject = 'Vivocarat - Seller Location Enquiry';
    
        return $this->view('email.sellerlocationenquiry')
                    ->with([
                        'name'=>$this->data['name']
                           ])
                    ->with([
                        'email'=>$this->data['email']
                           ])
                    ->with([
                        'phone'=>$this->data['phone']
                           ])
                    ->with([
                        'location'=>$this->data['location']
                           ])
                    ->from($address,$name)
                    ->replyTo($address,$name)
                    ->subject($subject);
    }
}
