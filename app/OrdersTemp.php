<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersTemp extends Model
{
    protected $table = 'orders_temp';
}
