<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alerts extends Model
{
    protected $table = 'alerts';
    protected $fillable = array('id','email','phone','pid','size');
}
