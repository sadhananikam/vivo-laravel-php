<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JewellerInformation extends Model
{
    protected $table = 'jeweller_information';
}
