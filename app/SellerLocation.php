<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerLocation extends Model
{
    protected $table = 'seller_location';
    protected $fillable = array('id','name','address','pincode','phone');
}
