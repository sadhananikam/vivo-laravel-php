<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersAuth extends Model
{
    protected $table = 'customers_auth';
    protected $fillable = array('uid','name','email','phone','gender','dob','password','token');
}
