<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\JewellerInformation;

class Store extends Controller
{
    public function getStoreCategories(){
        
        $store = $_REQUEST['store'];
        
        $categories = DB::table('products')
                    ->select('category')
                    ->distinct()
                    ->where('supplier_name',$store)
                    ->get();
                
        return json_encode($categories);
    }
    
    public function getJewellerInformationByName(){
        
        $store = $_REQUEST['store'];
        
        $jeweller_info = DB::table('jeweller_information')
                    ->where('name',$store)
                    ->get();
                
        return json_encode($jeweller_info);
    }
    
    public function getStoreProducts(){
        
        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $page = $_REQUEST['page'];

        if($parent=='q')
        {
//            $sql2="SELECT * from products where title like '%".$c."%' GROUP BY VC_SKU limit 8 offset ".(intval($page)*8);
            
//            $products = DB::table('products')
//                    ->where('title','like','%'.$c.'%')
//                    ->groupBy('VC_SKU')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where title like '%".$c."%' GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
        }
        else if($parent=='w')
        {
//            $sql2="SELECT * from products where category='".$parent."' and supplier_name='".$c."' GROUP BY VC_SKU ORDER BY id desc limit 8 offset ".(intval($page)*8);
            
//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('supplier_name',$c)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' and supplier_name='".$c."' GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
        }
        else if($c=='All')
        {
//            $sql2="SELECT * from products where category='".$parent."' GROUP BY VC_SKU ORDER BY id desc limit 8 offset ".(intval($page)*8);
            
//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
        }
        else if($parent=='All')
        {
//            $sql2="SELECT * from products where supplier_name='".$c."' GROUP BY VC_SKU ORDER BY id desc limit 8 offset ".(intval($page)*8);
            
//            $products = DB::table('products')
//                    ->where('supplier_name',$c)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where supplier_name='".$c."' GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
        }
        else
        {
//            $sql2="SELECT * from products where category='".$parent."' and supplier_name='".$c."' GROUP BY VC_SKU ORDER BY id desc limit 8 offset ".(intval($page)*8);
            
//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('supplier_name',$c)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' and supplier_name='".$c."' GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
        }
                    
        return json_encode($products);
    }
    
    public function getStoreProductsCount(){
        
        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];

        if($parent=='q')
        {
//            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where title like '%".$c."%'";
            
            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('title','like','%'.$c.'%')
                    ->get();
        }
        else if($parent=='w')
        {
//            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where category='".$parent."' and supplier_name='".$c."'";
            
            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('supplier_name',$c)
                    ->get();
        }
        else if($c=='All')
        {
//            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where category='".$parent."'";
            
            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->get();
        }
        else if($parent=='All')
        {
//            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where supplier_name='".$c."'";
            
            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('supplier_name',$c)
                    ->get();
        }
        else
        {
//            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where category='".$parent."' and supplier_name='".$c."'";
            
            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('supplier_name',$c)
                    ->get();
        }
        
        return json_encode($products_count);
    }
    
    public function filterStorePrice(){
        
        $tag = isset($_REQUEST['tags'])? $_REQUEST['tags'] : "";
        
        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $page = $_REQUEST['page'];

        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];
        
        $isRoseGold = isset($_REQUEST['isRoseGold'])? $_REQUEST['isRoseGold'] : false;
        
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];
        //$isVS = $_REQUEST['isVS']; //$isVVS = $_REQUEST['isVVS']; //$isDEdu = $_REQUEST['isDEdu']; //$isMale = $_REQUEST['isMale']; //$isFemale= $_REQUEST['isFemale']; //$isUniSex= $_REQUEST['isUniSex'];

        $sql2="SELECT * from products ";
        if($parent!="All"){
           $sql2=$sql2."where category='".$parent."'";

        }
        
        if($tag!="" && $tag!=null )
        { 
            $sql2=$sql2."AND secondary_tags like'%".$tag."%'"; 
        }
        if($c!="All")
        {
            if($parent!="All"){
            $sql2=$sql2." AND ";
            }
            if($parent=="All"){
            $sql2=$sql2." where ";
            }
           $sql2=$sql2." supplier_name='".$c."'";

        }
        //***********************************
        if($isGold=="true" || $isWhiteGold=="true"|| $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true")
        {
            $sql2=$sql2."AND (";
        }

        if($isGold=="true")
        {
            $sql2=$sql2."metal like '%Yellow Gold%'";
        }

        if($isWhiteGold=="true")
        {
            if($isGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" )
        {
             if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true")
        {
              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isRoseGold=="true")
        {

              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true" ||  $isSilver=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2."metal like '%Rose Gold%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true")
        {
            $sql2=$sql2.")";
        }


        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true")
        {
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }
        
        if($is18=="true")
        {
             if($is22=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2." purity= '18 KT'";
        }
        
        if($is14=="true")
        {
             if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true")
        {
              if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true")
        {
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }


        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true")
        {
            if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true")
        {
            if($isCasual=="true" || $isBridal=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }
        
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        if($s==null){
        $s=0;
        }
        if($e==null){
        $e=200000;
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e." GROUP BY VC_SKU limit 8 offset ".(intval($page)*8);
        
        $filter = DB::select(DB::raw($sql2));
        return json_encode($filter);
    }
    
    public function filterStorePriceCount(){
        
        $tag = isset($_REQUEST['tags']) ? $_REQUEST['tags'] : "";
        
        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];

        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];
        
        $isRoseGold = isset($_REQUEST['isRoseGold']) ? $_REQUEST['isRoseGold'] : false;
        
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];
        //$isVS = $_REQUEST['isVS']; //$isVVS = $_REQUEST['isVVS']; //$isDEdu = $_REQUEST['isDEdu']; //$isMale = $_REQUEST['isMale']; //$isFemale= $_REQUEST['isFemale']; //$isUniSex= $_REQUEST['isUniSex'];

        $sql2="SELECT count(DISTINCT (VC_SKU)) as count  from products ";
        if($parent!="All"){
           $sql2=$sql2."where category='".$parent."'";
        }
        
        if($tag!="" && $tag!=null )
        {
            $sql2=$sql2."AND secondary_tags like'%".$tag."%'"; 
        }
        
        if($c!="All")
        {
            if($parent!="All"){
                $sql2=$sql2." AND ";
            }
            if($parent=="All"){
                $sql2=$sql2." where ";
            }
           $sql2=$sql2." supplier_name='".$c."'";
        }
        //***********************************
        if($isGold=="true" || $isWhiteGold=="true"|| $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Yellow Gold%'";
        }

        if($isWhiteGold=="true")
        {
            if($isGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" )
        {
            if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true")
        {
            if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isRoseGold=="true")
        {
              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true" ||  $isSilver=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Rose Gold%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }
        
        if($is18=="true")
        {
            if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }
        
        if($is14=="true")
        {
            if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true")
        {
            if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }

        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true")
        {
            if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true")
        {
            if($isCasual=="true" || $isBridal=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }
        
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        if($s==null){
            $s=0;
        }
        if($e==null){
            $e=200000;
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e;

        $filter_count = DB::select(DB::raw($sql2));
        return json_encode($filter_count);
    }
    
    public function sortStoreList(){
        
        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $param=$_REQUEST['p'];
        if($param == "" || $param == null)
        {
            $param = "-sortorder DESC,id desc";
        }
        if($param == "id desc")
        {
            $param = "-sortorder DESC,id desc";
        }
        $page = $_REQUEST['page'];
        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];
        //$isVS = $_REQUEST['isVS']; //$isVVS = $_REQUEST['isVVS']; //$isDEdu = $_REQUEST['isDEdu']; //$isMale = $_REQUEST['isMale']; //$isFemale= $_REQUEST['isFemale']; //$isUniSex= $_REQUEST['isUniSex'];

        $sql2="SELECT * from products ";
        
        if($parent!="All"){
           $sql2=$sql2."where category='".$parent."'";

        }
        
        if($c!="All")
        {
            if($parent!="All"){
                $sql2=$sql2." AND ";
            }
            if($parent=="All"){
                $sql2=$sql2." where ";
            }
            $sql2=$sql2." supplier_name='".$c."'";
        }

        //***********************************
        if($isGold=="true" || $isWhiteGold=="true" || $isSilver=="true" || $isSilver=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Yellow Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){
             if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
                }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){
              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isSilver=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }
        
        if($is18=="true"){
             if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }
        
        if($is14=="true"){
             if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
              if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }


        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
              if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
              if($isCasual=="true" || $isBridal=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }
        
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e." GROUP BY VC_SKU order by ".$param." limit 8 offset ".(intval($page)*8);

        $sortstorelist = DB::select(DB::raw($sql2));
        return json_encode($sortstorelist);
    }
    
    public function sortStoreListCount(){
        
        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $param=$_REQUEST['p'];
        if($param == "" || $param == null)
        {
            $param = "-sortorder DESC,id desc";
        }
        if($param == "id desc")
        {
            $param = "-sortorder DESC,id desc";
        }
        $page = $_REQUEST['page'];
        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];
        //$isVS = $_REQUEST['isVS']; //$isVVS = $_REQUEST['isVVS']; //$isDEdu = $_REQUEST['isDEdu']; //$isMale = $_REQUEST['isMale']; //$isFemale= $_REQUEST['isFemale']; //$isUniSex= $_REQUEST['isUniSex'];

        $sql2="SELECT count(DISTINCT (VC_SKU)) as count  from products ";
        
        if($parent!="All"){
           $sql2=$sql2."where category='".$parent."'";

        }
        
        if($c!="All")
        {
            if($parent!="All"){
                $sql2=$sql2." AND ";
            }
            if($parent=="All"){
                $sql2=$sql2." where ";
            }
            $sql2=$sql2." supplier_name='".$c."'";
        }

        //***********************************
        if($isGold=="true" || $isWhiteGold=="true" || $isSilver=="true" || $isSilver=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Yellow Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){
             if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
                }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){
              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isSilver=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }
        
        if($is18=="true"){
             if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }
        
        if($is14=="true"){
             if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
              if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }


        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
              if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
              if($isCasual=="true" || $isBridal=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }
        
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e;

        $sortstorelistcount = DB::select(DB::raw($sql2));
        return json_encode($sortstorelistcount);
    }
}