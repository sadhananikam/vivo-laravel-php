<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\Cart;
use App\Categories;

class Category extends Controller
{
    public function getAllJewellers(){
        $jewells = DB::table('jeweller_information')
                    ->select('id','name')
                    ->get();
        return json_encode($jewells);
    }

    public function getcategorydescription(){
        $category = $_REQUEST['category'];
        $parent_category = $_REQUEST['parent_category'];

        $description = DB::table('categories')
                    ->where('parent_category',$parent_category)
                    ->where('category',$category)
                    ->first();
        return json_encode($description);
    }

    public function getProducts(){

        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $page = $_REQUEST['page'];
        
        if (strpos($c, 'less_') !== FALSE)
        {
            $str = str_replace("less_","",$c);
        }
        if (strpos($c, 'greater_') !== FALSE)
        {
            $str = str_replace("greater_","",$c);
        }
        if (strpos($c, 'purity_') !== FALSE)
        {
            $purity = str_replace("purity_","",$c);
            $parr = explode('_', $purity);
        }
        if (strpos($c, 'wt_') !== FALSE)
        {
            $wt = str_replace("wt_","",$c);
            if($wt == "1")
            {
                $wtparam = "gram";
            }
            else
            {
                $wtparam = "grams";
            }
        }
        if (strpos($c, 'weight_lt_') !== FALSE)
        {
            $wt_lt_param = "grams";
            $wt_lt = str_replace("weight_lt_","",$c);
            if($wt_lt == '1')
            {
                $wt_lt_param = "mgrams";
            }
        }
        if (strpos($c, 'weight_gt_') !== FALSE)
        {
            $wt_gt = str_replace("weight_gt_","",$c);
            $num = (int)$wt_gt;
            $inarr = array();
            $inarr[0] = "weight NOT LIKE '%1 gram%'";
            for ($i = 2; $i <= $num; ++$i) 
            {
                $instr = "weight NOT LIKE '%".$i." grams%'";
                array_push($inarr , $instr);
            }
            array_push($inarr , "weight NOT LIKE '%mgrams%'");
            //print_r($inarr);
        }
        if (strpos($c, 'gender_') !== FALSE)
        {
            $g = str_replace("gender_","",$c);
            $garr = array();
            if($g == 'male')
            {
                array_push($garr , "male");
                array_push($garr , "unisex");
            }
            elseif($g == 'female')
            {
                array_push($garr , "female");
                array_push($garr , "unisex");
            }
            elseif($g == 'unisex')
            {
                array_push($garr , $g);
            }
            elseif($g == 'kids')
            {
                array_push($garr , $g);
            }
            //print_r($garr);
        }
        
        if (strpos($parent, ',') !== FALSE)
        {
            $arr = explode(',', $parent);
            $param1 = "OR";
            //print_r($arr);
            
            $sql2="SELECT * from products where category REGEXP '";
            
            for($i=0;$i<count($arr);$i++){
                if($arr[$i]!=null)
                {
                    if($i == count($arr)-1)
                    {
                        $sql2=$sql2.$arr[$i];
                    }
                    else
                    {
                        $sql2=$sql2.$arr[$i]."|";
                    }
                }
            }
            $sql2=$sql2."'";
            
            if($c != 'All')
            {
                $sql2=$sql2."AND parent_category like'".$c."' ";
            }
            
            $sql2=$sql2."GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            //print_r($sql2);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);
        }
        else if($parent=='q'){
            //$sql2="SELECT * from products where title like '%".$c."%' GROUP BY VC_SKU limit 8 offset ".(intval($page)*8);

//            $products = DB::table('products')
//                    ->where('title','like','%'.$c.'%')
//                    ->groupBy('VC_SKU')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where title like '%".$c."%' GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);
        }
        else if($parent=='w'){
            //$sql2="SELECT * from products where category='".$parent."' and parent_category='".$c."' GROUP BY VC_SKU ORDER BY id desc limit 8 offset ".(intval($page)*8);

//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('parent_category',$c)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' and parent_category='".$c."' GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);

        }else if($c=='All'){
            //$sql2="SELECT * from products where category='".$parent."' GROUP BY VC_SKU ORDER BY id desc limit 8 offset ".(intval($page)*8);

//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);

        }else if($c=='1'){
            //$sql2="SELECT * from products where category='".$parent."' GROUP BY VC_SKU ORDER BY id desc limit 8 offset ".(intval($page)*8);

//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('is_gem_present',1)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' and is_gem_present=1 GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);

        }else if(strpos($c, 'less_') !== FALSE){

//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('price_after_discount','<',$str)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' and price_after_discount<".$str." GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);

        }else if(strpos($c, 'greater_') !== FALSE){

//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('price_after_discount','>',$str)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' and price_after_discount>".$str." GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);

        }else if(strpos($c, 'purity_') !== FALSE){

//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('purity','like','%'.$parr[0].'%')
//                    ->where('purity','like','%'.$parr[1].'%')
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' and  purity like '%".$parr[0]."%' and  purity like '%".$parr[1]."%' GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);

        }else if(strpos($c, 'wt_') !== FALSE){

//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('weight','like',$wt." ".$wtparam)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' and  weight like '".$wt." ".$wtparam."' GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);

        }else if(strpos($c, 'weight_lt_') !== FALSE){

//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('weight','like','%'.$wt_lt_param.'%')
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where category='".$parent."' and  weight like '%".$wt_lt_param."%' GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);

        }if (strpos($c, 'weight_gt_') !== FALSE)
        {
            
            $sql2="SELECT * from products where category like'".$parent."' AND ";
            
            $sql2 = $sql2.implode(" AND ", $inarr);
            
            $sql2=$sql2."GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            //print_r($sql2);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);
            
        }else if (strpos($c, 'gender_') !== FALSE){
            $gs = join("','",$garr);
            $sql2="SELECT * from products where ";
            
            $sql2=$sql2."gender IN ('$gs')";
            $sql2=$sql2."AND category like'".$parent."' ";
            
            $sql2=$sql2."GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            //print_r($sql2);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);

        }else if($parent=='All'){
            //$sql2="SELECT * from products where parent_category='".$c."' GROUP BY VC_SKU ORDER BY id desc limit 8 offset ".(intval($page)*8);

//            $products = DB::table('products')
//                    ->where('parent_category',$c)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where parent_category='".$c."' GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);
        }
        else{
            //$sql2="SELECT * from products where category='".$parent."' and parent_category='".$c."' GROUP BY VC_SKU ORDER BY id desc limit 8 offset ".(intval($page)*8);

//            $products = DB::table('products')
//                    ->where('category',$parent)
//                    ->where('parent_category',$c)
//                    ->groupBy('VC_SKU')
//                    ->orderBy('sortorder','desc')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            
            $sql2="SELECT * from products where category='".$parent."' and parent_category='".$c."' GROUP BY VC_SKU ORDER BY -sortorder DESC,id desc limit 8 offset ".(intval($page)*8);
            $products = DB::select(DB::raw($sql2));
            return json_encode($products);
        }

//        return json_encode($products);
    }

    public function getProductsCount(){

        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        
        if (strpos($c, 'less_') !== FALSE)
        {
            $str = str_replace("less_","",$c);
        }
        if (strpos($c, 'greater_') !== FALSE)
        {
            $str = str_replace("greater_","",$c);
        }
        if (strpos($c, 'purity_') !== FALSE)
        {
            $purity = str_replace("purity_","",$c);
            $parr = explode('_', $purity);
        }
        if (strpos($c, 'wt_') !== FALSE)
        {
            $wt = str_replace("wt_","",$c);
            if($wt == "1")
            {
                $wtparam = "gram";
            }
            else
            {
                $wtparam = "grams";
            }
        }
        if (strpos($c, 'weight_lt_') !== FALSE)
        {
            $wt_lt_param = "grams";
            $wt_lt = str_replace("weight_lt_","",$c);
            if($wt_lt == '1')
            {
                $wt_lt_param = "mgrams";
            }
        }
        if (strpos($c, 'weight_gt_') !== FALSE)
        {
            $wt_gt = str_replace("weight_gt_","",$c);
            $num = (int)$wt_gt;
            $inarr = array();
            $inarr[0] = "weight NOT LIKE '%1 gram%'";
            for ($i = 2; $i <= $num; ++$i) 
            {
                $instr = "weight NOT LIKE '%".$i." grams%'";
                array_push($inarr , $instr);
            }
            array_push($inarr , "weight NOT LIKE '%mgrams%'");
            //print_r($inarr);
        }
        if (strpos($c, 'gender_') !== FALSE)
        {
            $g = str_replace("gender_","",$c);
            $garr = array();
            if($g == 'male')
            {
                array_push($garr , "male");
                array_push($garr , "unisex");
            }
            elseif($g == 'female')
            {
                array_push($garr , "female");
                array_push($garr , "unisex");
            }
            elseif($g == 'unisex')
            {
                array_push($garr , $g);
            }
            elseif($g == 'kids')
            {
                array_push($garr , $g);
            }
            //print_r($garr);
        }

        if (strpos($parent, ',') !== FALSE)
        {
            $arr = explode(',', $parent);
            $param1 = "OR";
            //print_r($arr);
            
            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where category REGEXP '";
            
            for($i=0;$i<count($arr);$i++){
                if($arr[$i]!=null)
                {
//                    if($i>0){
//                     $sql2=$sql2."OR ";
//                    }
//                    $sql2=$sql2."category like'".$arr[$i]."' ";
                    if($i == count($arr)-1)
                    {
                        $sql2=$sql2.$arr[$i];
                    }
                    else
                    {
                        $sql2=$sql2.$arr[$i]."|";
                    }
                }
            }
            $sql2=$sql2."'";
            
            if($c != 'All')
            {
                $sql2=$sql2."AND parent_category like'".$c."' ";
            }
            
            $products_count = DB::select(DB::raw($sql2));
            return json_encode($products_count);
        }
        else if($parent=='q'){
            //$sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where title like '%".$c."%'";

            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('title','like','%'.$c.'%')
                    ->get();
            return json_encode($products_count);

        }else if($parent=='w'){
            //$sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where category='".$parent."' and parent_category='".$c."'  ORDER BY id desc ";

            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('parent_category',$c)
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);

        }else if($c=='All'){
            //$sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where category='".$parent."'  ORDER BY id desc ";

            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);

        }else if($c=='1'){
            //$sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where category='".$parent."'  ORDER BY id desc ";

            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('is_gem_present',1)
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);

        }else if(strpos($c, 'less_') !== FALSE){
            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('price_after_discount','<',$str)
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);

        }else if(strpos($c, 'greater_') !== FALSE){

            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('price_after_discount','>',$str)
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);

        }else if(strpos($c, 'purity_') !== FALSE){
            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('purity','like','%'.$parr[0].'%')
                    ->where('purity','like','%'.$parr[1].'%')
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);

        }else if(strpos($c, 'wt_') !== FALSE){
            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('weight','like',$wt." ".$wtparam)
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);

        }else if(strpos($c, 'weight_lt_') !== FALSE){
            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('weight','like','%'.$wt_lt_param.'%')
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);

        }else if (strpos($c, 'weight_gt_') !== FALSE)
        {       
            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where category like'".$parent."' AND ";
            
            $sql2 = $sql2.implode(" AND ", $inarr);
            
            $products_count = DB::select(DB::raw($sql2));
            return json_encode($products_count);
            
        }else if (strpos($c, 'gender_') !== FALSE){
            $gs = join("','",$garr);
            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where ";
            
            $sql2=$sql2."gender IN ('$gs')";
            
            $sql2=$sql2."AND category like'".$parent."' ";
            
            $products_count = DB::select(DB::raw($sql2));
            return json_encode($products_count);

        }else if($parent=='All'){
            //$sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where parent_category='".$c."'  ORDER BY id desc";

            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('parent_category',$c)
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);
        }
        else{
            //$sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where category='".$parent."' and parent_category='".$c."'  ORDER BY id desc ";

            $products_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('category',$parent)
                    ->where('parent_category',$c)
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($products_count);
        }

//        return json_encode($products_count);
    }

    public function filterPrice(){

        $tag = isset($_REQUEST['tags']) ? $_REQUEST['tags'] : "";

        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $page = $_REQUEST['page'];

        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];

        $isRoseGold = isset($_REQUEST['isRoseGold']) ? $_REQUEST['isRoseGold'] : false;

        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];
        //$isVS = $_REQUEST['isVS']; //$isVVS = $_REQUEST['isVVS']; //$isDEdu = $_REQUEST['isDEdu']; //$isMale = $_REQUEST['isMale']; //$isFemale= $_REQUEST['isFemale']; //$isUniSex= $_REQUEST['isUniSex'];
        $str = null;
        $purity = null;
        $wt = null;
        $wt_lt_param = null;
        $wt_gt = null;
        $arr = array();
        $parr =array();
        $garr =array();
        if (strpos($c, 'less_') !== FALSE)
        {
            $str = str_replace("less_","",$c);
            $e=$str;
        }
        if (strpos($c, 'greater_') !== FALSE)
        {
            $str = str_replace("greater_","",$c);
            $e=$str;
        }
        if (strpos($parent, ',') !== FALSE)
        {
            $arr = explode(',', $parent);
        }
        if (strpos($c, 'purity_') !== FALSE)
        {
            $purity = str_replace("purity_","",$c);
            $parr = explode('_', $purity);
        }
        if (strpos($c, 'wt_') !== FALSE)
        {
            $wt = str_replace("wt_","",$c);
            if($wt == "1")
            {
                $wtparam = "gram";
            }
            else
            {
                $wtparam = "grams";
            }
        }
        if (strpos($c, 'weight_lt_') !== FALSE)
        {
            $wt_lt_param = "grams";
            $wt_lt = str_replace("weight_lt_","",$c);
            if($wt_lt == '1')
            {
                $wt_lt_param = "mgrams";
            }
        }
        if (strpos($c, 'weight_gt_') !== FALSE)
        {
            $wt_gt = str_replace("weight_gt_","",$c);
            $num = (int)$wt_gt;
            $inarr = array();
            $inarr[0] = "weight NOT LIKE '%1 gram%'";
            for ($i = 2; $i <= $num; ++$i) 
            {
                $instr = "weight NOT LIKE '%".$i." grams%'";
                array_push($inarr , $instr);
            }
            array_push($inarr , "weight NOT LIKE '%mgrams%'");
            //print_r($inarr);
        }
        if (strpos($c, 'gender_') !== FALSE)
        {
            $g = str_replace("gender_","",$c);
            $garr = array();
            if($g == 'male')
            {
                array_push($garr , "male");
                array_push($garr , "unisex");
            }
            elseif($g == 'female')
            {
                array_push($garr , "female");
                array_push($garr , "unisex");
            }
            elseif($g == 'unisex')
            {
                array_push($garr , $g);
            }
            elseif($g == 'kids')
            {
                array_push($garr , $g);
            }
            //print_r($garr);
        }

        $sql2="SELECT * from products ";

        if($parent!="All" && count($arr)<=0 && count($garr)>=0){
           $sql2=$sql2."where category='".$parent."'";
        }
        else
        {
            $sql2=$sql2."where category REGEXP '";
            
            for($i=0;$i<count($arr);$i++){
                if($arr[$i]!=null)
                {
                    if($i == count($arr)-1)
                    {
                        $sql2=$sql2.$arr[$i];
                    }
                    else
                    {
                        $sql2=$sql2.$arr[$i]."|";
                    }
                }
            }
            $sql2=$sql2."'";
        }
        
        if(count($parr)>0){
            $sql2=$sql2."AND ";
            for($i=0;$i<count($parr);$i++){
                if($parr[$i]!=null)
                {
                    if($i>0){
                        $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2." purity like '%".$parr[$i]."%'";
                }
            }
        }
        
        if(count($garr)>0){
            $gs = join("','",$garr);
            $sql2=$sql2."AND ";
            $sql2=$sql2."gender IN ('$gs')";
        }
        
        if($wt!="" && $wt!=null )
        {
            $sql2=$sql2."AND weight like'".$wt." ".$wtparam."'";
        }
        
        if($wt_lt_param!="" && $wt_lt_param!=null )
        {
            $sql2=$sql2."AND weight like'%".$wt_lt_param."%'";
        }
        if($wt_gt!="" && $wt_gt!=null )
        {
            $sql2 = $sql2."AND ".implode(" AND ", $inarr);
        }

        if($tag!="" && $tag!=null )
        {
            $sql2=$sql2."AND secondary_tags like'%".$tag."%'";
        }
        if($c!="All" && $c!='1' && $str == null && $purity == null && $wt == null && $wt_lt_param == null && $wt_gt == null && count($garr)<=0){
            if($parent!="All"){
            $sql2=$sql2." AND ";
            }
             if($parent=="All"){
            $sql2=$sql2." where ";
            }
           $sql2=$sql2." parent_category='".$c."'";
        }
        if($c=='1'){
            $sql2=$sql2."AND is_gem_present='1'";
        }

        //***********************************
        if($isGold=="true" || $isWhiteGold=="true"|| $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Yellow Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
            $sql2=$sql2." OR ";
            }

            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){
            if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){
            if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isRoseGold=="true"){
            if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true" ||  $isSilver=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Rose Gold%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }

        if($is18=="true"){
             if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }

        if($is14=="true"){
             if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
            if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }


        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
            if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
             if($isCasual=="true" || $isFashion=="true"){
                 $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }

        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        $jewel=json_decode($jewellers);
        if(count($jewel)>0)
        {
          $sql2=$sql2."AND (";

            for($i=0;$i<count($jewel);$i++){
                if($jewel[$i]!=null){
                if($i>0){
                    $sql2=$sql2."OR ";
                }
                $sql2=$sql2." supplier_name = '".$jewel[$i]."'";
            }
            }

          $sql2=$sql2.")";
        }


        if($s==null){
            $s=0;
        }
        if($e==null){
            $e=1000000;
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e." GROUP BY VC_SKU limit 8 offset ".(intval($page)*8);

        //print_r($sql2);
        $filterprice = DB::select(DB::raw($sql2));
        return json_encode($filterprice);
    }

    public function filterPriceCount(){

        $tag = isset($_REQUEST['tags']) ? $_REQUEST['tags'] : "";

        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];

        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];

        $isRoseGold = isset($_REQUEST['isRoseGold']) ? $_REQUEST['isRoseGold'] : false;

        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];
        //$isVS = $_REQUEST['isVS']; //$isVVS = $_REQUEST['isVVS']; //$isDEdu = $_REQUEST['isDEdu']; //$isMale = $_REQUEST['isMale']; //$isFemale= $_REQUEST['isFemale']; //$isUniSex= $_REQUEST['isUniSex'];
        $str = null;
        $purity = null;
        $wt = null;
        $wt_lt_param = null;
        $wt_gt = null;
        $arr = array();
        $parr =array();
        $garr =array();
        if (strpos($c, 'less_') !== FALSE)
        {
            $str = str_replace("less_","",$c);
            $e=$str;
        }
        if (strpos($c, 'greater_') !== FALSE)
        {
            $str = str_replace("greater_","",$c);
            $e=$str;
        }
        if (strpos($parent, ',') !== FALSE)
        {
            $arr = explode(',', $parent);
        }
        if (strpos($c, 'purity_') !== FALSE)
        {
            $purity = str_replace("purity_","",$c);
            $parr = explode('_', $purity);
        }
        if (strpos($c, 'wt_') !== FALSE)
        {
            $wt = str_replace("wt_","",$c);
            if($wt == "1")
            {
                $wtparam = "gram";
            }
            else
            {
                $wtparam = "grams";
            }
        }
        if (strpos($c, 'weight_lt_') !== FALSE)
        {
            $wt_lt_param = "grams";
            $wt_lt = str_replace("weight_lt_","",$c);
            if($wt_lt == '1')
            {
                $wt_lt_param = "mgrams";
            }
        }
        if (strpos($c, 'weight_gt_') !== FALSE)
        {
            $wt_gt = str_replace("weight_gt_","",$c);
            $num = (int)$wt_gt;
            $inarr = array();
            $inarr[0] = "weight NOT LIKE '%1 gram%'";
            for ($i = 2; $i <= $num; ++$i) 
            {
                $instr = "weight NOT LIKE '%".$i." grams%'";
                array_push($inarr , $instr);
            }
            array_push($inarr , "weight NOT LIKE '%mgrams%'");
            //print_r($inarr);
        }
        if (strpos($c, 'gender_') !== FALSE)
        {
            $g = str_replace("gender_","",$c);
            $garr = array();
            if($g == 'male')
            {
                array_push($garr , "male");
                array_push($garr , "unisex");
            }
            elseif($g == 'female')
            {
                array_push($garr , "female");
                array_push($garr , "unisex");
            }
            elseif($g == 'unisex')
            {
                array_push($garr , $g);
            }
            elseif($g == 'kids')
            {
                array_push($garr , $g);
            }
            //print_r($garr);
        }

        $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products ";
        if($parent!="All" && count($arr)<=0 && count($garr)>=0){
           $sql2=$sql2."where category='".$parent."'";
        }
        else
        {
            $sql2=$sql2."where category REGEXP '";
            
            for($i=0;$i<count($arr);$i++){
                if($arr[$i]!=null)
                {
                    if($i == count($arr)-1)
                    {
                        $sql2=$sql2.$arr[$i];
                    }
                    else
                    {
                        $sql2=$sql2.$arr[$i]."|";
                    }
                }
            }
            $sql2=$sql2."'";
        }
        
        if(count($parr)>0){
            $sql2=$sql2."AND ";
            for($i=0;$i<count($parr);$i++){
                if($parr[$i]!=null)
                {
                    if($i>0){
                        $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2." purity like '%".$parr[$i]."%'";
                }
            }
        }
        
        if(count($garr)>0){
            $gs = join("','",$garr);
            $sql2=$sql2."AND ";
            $sql2=$sql2."gender IN ('$gs')";
        }
        
        if($wt!="" && $wt!=null )
        {
            $sql2=$sql2."AND weight like'".$wt." ".$wtparam."'";
        }
        if($wt_lt_param!="" && $wt_lt_param!=null )
        {
            $sql2=$sql2."AND weight like'%".$wt_lt_param."%'";
        }
        if($wt_gt!="" && $wt_gt!=null )
        {
            $sql2 = $sql2."AND ".implode(" AND ", $inarr);
        }

        if($tag!="" && $tag!=null )
        {
            $sql2=$sql2."AND secondary_tags like'%".$tag."%'";
        }

        if($c!="All" && $c!='1' && $str == null && $purity == null && $wt == null && $wt_lt_param == null && $wt_gt == null && count($garr)<=0){
            if($parent!="All"){
            $sql2=$sql2." AND ";
            }
             if($parent=="All"){
            $sql2=$sql2." where ";
            }
           $sql2=$sql2." parent_category='".$c."'";
        }
        if($c=='1'){
            $sql2=$sql2."AND is_gem_present='1'";
        }
        //***********************************
        if($isGold=="true" || $isWhiteGold=="true"|| $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Yellow Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
            $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){
             if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){

              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                    $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isRoseGold=="true"){

              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true" ||  $isSilver=="true"){
                    $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Rose Gold%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }
        if($is18=="true"){
             if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }

        if($is14=="true"){
             if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
              if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }


        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
              if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
            if($isCasual=="true" || $isFashion=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }

        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        $jewel=json_decode($jewellers);
        if(count($jewel)>0)
        {
          $sql2=$sql2."AND (";

            for($i=0;$i<count($jewel);$i++){
                if($jewel[$i]!=null){
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2." supplier_name = '".$jewel[$i]."'";
                }
            }

          $sql2=$sql2.")";
        }

        if($s==null){
            $s=0;
        }
        if($e==null){
            $e=1000000;
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e;


        $filter_count = DB::select(DB::raw($sql2));
        return json_encode($filter_count);
    }

    public function sortList(){

        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $param=$_REQUEST['p'];
        if($param == "" || $param == null)
        {
            $param = "-sortorder DESC,id desc";
        }
        if($param == "id desc")
        {
            $param = "-sortorder DESC,id desc";
        }
        $page = isset($_REQUEST['page'])?$_REQUEST['page']:'';
        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];

        $isRoseGold = isset($_REQUEST['isRoseGold']) ? $_REQUEST['isRoseGold'] : false;
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];
        //$isVS = $_REQUEST['isVS']; //$isVVS = $_REQUEST['isVVS']; //$isDEdu = $_REQUEST['isDEdu']; //$isMale = $_REQUEST['isMale']; //$isFemale= $_REQUEST['isFemale']; //$isUniSex= $_REQUEST['isUniSex'];
        $str = null;
        $purity = null;
        $wt = null;
        $wt_lt_param = null;
        $wt_gt = null;
        $arr = array();
        $parr =array();
        $garr =array();
        if (strpos($c, 'less_') !== FALSE)
        {
            $str = str_replace("less_","",$c);
            $e=$str;
        }
        if (strpos($c, 'greater_') !== FALSE)
        {
            $str = str_replace("greater_","",$c);
            $e=$str;
        }
        if (strpos($parent, ',') !== FALSE)
        {
            $arr = explode(',', $parent);
        }
        if (strpos($c, 'purity_') !== FALSE)
        {
            $purity = str_replace("purity_","",$c);
            $parr = explode('_', $purity);
        }
        if (strpos($c, 'wt_') !== FALSE)
        {
            $wt = str_replace("wt_","",$c);
            if($wt == "1")
            {
                $wtparam = "gram";
            }
            else
            {
                $wtparam = "grams";
            }
        }
        if (strpos($c, 'weight_lt_') !== FALSE)
        {
            $wt_lt_param = "grams";
            $wt_lt = str_replace("weight_lt_","",$c);
            if($wt_lt == '1')
            {
                $wt_lt_param = "mgrams";
            }
        }
        if (strpos($c, 'weight_gt_') !== FALSE)
        {
            $wt_gt = str_replace("weight_gt_","",$c);
            $num = (int)$wt_gt;
            $inarr = array();
            $inarr[0] = "weight NOT LIKE '%1 gram%'";
            for ($i = 2; $i <= $num; ++$i) 
            {
                $instr = "weight NOT LIKE '%".$i." grams%'";
                array_push($inarr , $instr);
            }
            array_push($inarr , "weight NOT LIKE '%mgrams%'");
            //print_r($inarr);
        }
        if (strpos($c, 'gender_') !== FALSE)
        {
            $g = str_replace("gender_","",$c);
            $garr = array();
            if($g == 'male')
            {
                array_push($garr , "male");
                array_push($garr , "unisex");
            }
            elseif($g == 'female')
            {
                array_push($garr , "female");
                array_push($garr , "unisex");
            }
            elseif($g == 'unisex')
            {
                array_push($garr , $g);
            }
            elseif($g == 'kids')
            {
                array_push($garr , $g);
            }
            //print_r($garr);
        }

        $sql2="SELECT * from products ";
        if($parent!="All" && count($arr)<=0 && count($garr)>=0){
           $sql2=$sql2."where category='".$parent."'";
        }
        else
        {
            $sql2=$sql2."where category REGEXP '";
            
            for($i=0;$i<count($arr);$i++){
                if($arr[$i]!=null)
                {
                    if($i == count($arr)-1)
                    {
                        $sql2=$sql2.$arr[$i];
                    }
                    else
                    {
                        $sql2=$sql2.$arr[$i]."|";
                    }
                }
            }
            $sql2=$sql2."'";
        }
        
        if(count($parr)>0){
            $sql2=$sql2."AND ";
            for($i=0;$i<count($parr);$i++){
                if($parr[$i]!=null)
                {
                    if($i>0){
                        $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2." purity like '%".$parr[$i]."%'";
                }
            }
        }
        
        if(count($garr)>0){
            $gs = join("','",$garr);
            $sql2=$sql2."AND ";
            $sql2=$sql2."gender IN ('$gs')";
        }
        
        if($wt!="" && $wt!=null )
        {
            $sql2=$sql2."AND weight like'".$wt." ".$wtparam."'";
        }
        if($wt_lt_param!="" && $wt_lt_param!=null )
        {
            $sql2=$sql2."AND weight like'%".$wt_lt_param."%'";
        }
        if($wt_gt!="" && $wt_gt!=null )
        {
            $sql2 = $sql2."AND ".implode(" AND ", $inarr);
        }
        
        if($c!="All" && $c!='1' && $str == null && $purity == null && $wt == null && $wt_lt_param == null && $wt_gt == null && count($garr)<=0){
            if($parent!="All"){
            $sql2=$sql2." AND ";
            }
             if($parent=="All"){
            $sql2=$sql2." where ";
            }
           $sql2=$sql2." parent_category='".$c."'";
        }
        
        if($c=='1'){
            $sql2=$sql2."AND is_gem_present='1'";
        }

        //***********************************
        if($isGold=="true" || $isWhiteGold=="true"|| $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Yellow Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
            $sql2=$sql2." OR ";
            }

            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){

            if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){
            if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }
        
        if($isRoseGold=="true"){
            if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true" ||  $isSilver=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Rose Gold%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }

        if($is18=="true"){
             if($is22=="true"){
            $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }

        if($is14=="true"){
            if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
            if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }

        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
             if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
            if($isCasual=="true" || $isFashion=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }

        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        $jewel=json_decode($jewellers);
        if(count($jewel)>0)
        {
          $sql2=$sql2."AND (";

            for($i=0;$i<count($jewel);$i++){
                if($jewel[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2." supplier_name = '".$jewel[$i]."'";
                }
            }

          $sql2=$sql2.")";
        }
        
        if($s==null){
            $s=0;
        }
        if($e==null){
            $e=1000000;
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e." GROUP BY VC_SKU order by ".$param." limit 8 offset ".(intval($page)*8);

        $sortlist = DB::select(DB::raw($sql2));
        return json_encode($sortlist);
    }

    public function sortListCount(){

        $c = $_REQUEST['c'];
        $parent = $_REQUEST['type'];
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $param=$_REQUEST['p'];
        if($param == "" || $param == null)
        {
            $param = "-sortorder DESC,id desc";
        }
        if($param == "id desc")
        {
            $param = "-sortorder DESC,id desc";
        }
        $page = isset($_REQUEST['page'])?$_REQUEST['page']:'';
        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];

        $isRoseGold = isset($_REQUEST['isRoseGold']) ? $_REQUEST['isRoseGold'] : false;
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];
        //$isVS = $_REQUEST['isVS']; //$isVVS = $_REQUEST['isVVS']; //$isDEdu = $_REQUEST['isDEdu']; //$isMale = $_REQUEST['isMale']; //$isFemale= $_REQUEST['isFemale']; //$isUniSex= $_REQUEST['isUniSex'];
        $str = null;
        $purity = null;
        $wt = null;
        $wt_lt_param = null;
        $wt_gt = null;
        $arr = array();
        $parr =array();
        $garr =array();
        if (strpos($c, 'less_') !== FALSE)
        {
            $str = str_replace("less_","",$c);
            $e=$str;
        }
        if (strpos($c, 'greater_') !== FALSE)
        {
            $str = str_replace("greater_","",$c);
            $e=$str;
        }
        if (strpos($parent, ',') !== FALSE)
        {
            $arr = explode(',', $parent);
        }
        if (strpos($c, 'purity_') !== FALSE)
        {
            $purity = str_replace("purity_","",$c);
            $parr = explode('_', $purity);
        }
        if (strpos($c, 'wt_') !== FALSE)
        {
            $wt = str_replace("wt_","",$c);
            if($wt == "1")
            {
                $wtparam = "gram";
            }
            else
            {
                $wtparam = "grams";
            }
        }
        if (strpos($c, 'weight_lt_') !== FALSE)
        {
            $wt_lt_param = "grams";
            $wt_lt = str_replace("weight_lt_","",$c);
            if($wt_lt == '1')
            {
                $wt_lt_param = "mgrams";
            }
        }
        if (strpos($c, 'weight_gt_') !== FALSE)
        {
            $wt_gt = str_replace("weight_gt_","",$c);
            $num = (int)$wt_gt;
            $inarr = array();
            $inarr[0] = "weight NOT LIKE '%1 gram%'";
            for ($i = 2; $i <= $num; ++$i) 
            {
                $instr = "weight NOT LIKE '%".$i." grams%'";
                array_push($inarr , $instr);
            }
            array_push($inarr , "weight NOT LIKE '%mgrams%'");
            //print_r($inarr);
        }
        if (strpos($c, 'gender_') !== FALSE)
        {
            $g = str_replace("gender_","",$c);
            $garr = array();
            if($g == 'male')
            {
                array_push($garr , "male");
                array_push($garr , "unisex");
            }
            elseif($g == 'female')
            {
                array_push($garr , "female");
                array_push($garr , "unisex");
            }
            elseif($g == 'unisex')
            {
                array_push($garr , $g);
            }
            elseif($g == 'kids')
            {
                array_push($garr , $g);
            }
            //print_r($garr);
        }

        $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products ";

        if($parent!="All" && count($arr)<=0 && count($garr)>=0){
           $sql2=$sql2."where category='".$parent."'";
        }
        else
        {
            $sql2=$sql2."where category REGEXP '";
            
            for($i=0;$i<count($arr);$i++){
                if($arr[$i]!=null)
                {
                    if($i == count($arr)-1)
                    {
                        $sql2=$sql2.$arr[$i];
                    }
                    else
                    {
                        $sql2=$sql2.$arr[$i]."|";
                    }
                }
            }
            $sql2=$sql2."'";
        }
        
        if(count($parr)>0){
            $sql2=$sql2."AND ";
            for($i=0;$i<count($parr);$i++){
                if($parr[$i]!=null)
                {
                    if($i>0){
                        $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2." purity like '%".$parr[$i]."%'";
                }
            }
        }
        
        if(count($garr)>0){
            $gs = join("','",$garr);
            $sql2=$sql2."AND ";
            $sql2=$sql2."gender IN ('$gs')";
        }
        
        if($wt!="" && $wt!=null )
        {
            $sql2=$sql2."AND weight like'".$wt." ".$wtparam."'";
        }
        if($wt_lt_param!="" && $wt_lt_param!=null )
        {
            $sql2=$sql2."AND weight like'%".$wt_lt_param."%'";
        }
        if($wt_gt!="" && $wt_gt!=null )
        {
            $sql2 = $sql2."AND ".implode(" AND ", $inarr);
        }

        if($c!="All" && $c!='1' && $str == null && $purity == null && $wt == null && $wt_lt_param == null && $wt_gt == null && count($garr)<=0){
            if($parent!="All"){
            $sql2=$sql2." AND ";
            }
             if($parent=="All"){
            $sql2=$sql2." where ";
            }
           $sql2=$sql2." parent_category='".$c."'";

        }
        if($c=='1'){
            $sql2=$sql2."AND is_gem_present='1'";
        }

        //***********************************
        if($isGold=="true" || $isWhiteGold=="true"|| $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Yellow Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){

             if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){
            if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }
        
        if($isRoseGold=="true"){

              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true" ||  $isSilver=="true"){
                    $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Rose Gold%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }

        if($is18=="true"){
            if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }

        if($is14=="true"){
            if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
            if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }


        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
            if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
            if($isCasual=="true" || $isFashion=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }

        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        $jewel=json_decode($jewellers);
        if(count($jewel)>0)
        {
          $sql2=$sql2."AND (";

            for($i=0;$i<count($jewel);$i++){
                if($jewel[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2." supplier_name = '".$jewel[$i]."'";
                }
            }

          $sql2=$sql2.")";
        }
        
        if($s==null){
            $s=0;
        }
        if($e==null){
            $e=1000000;
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e." order by ".$param;

        $sortlist_count = DB::select(DB::raw($sql2));
        return json_encode($sortlist_count);
    }


    public function savetocart(Request $request){

//        $products = json_decode($_REQUEST['items']);
//        $uid = $_REQUEST['uid'];
        $products = json_decode($request->input('items'));
        $uid = $request->input('uid');

        $checkcart = DB::table('cart')
                    ->select('cart_ref_id')
                    ->where('user_id',$uid)
                    ->where('is_active','1')
                    ->first();

        if($checkcart != null)
        {
            $cart_ref_id = $checkcart->cart_ref_id;

            if(!empty($products) && !empty($uid))
            {
                $items = $products;
                for ($i = 0; $i < count($items); $i++)
                {
                    $p = $items[$i]->item;

                    $product_id = $p->id;

                $checkp = DB::table('cart')
                    ->where('user_id',$uid)
                    ->where('cart_ref_id',$cart_ref_id)
                    ->where('product_id',$product_id)
                    ->where('is_active','1')
                    ->first();

                    if($checkp != null)
                    {

                        DB::table('cart')
                        ->where('user_id',$uid)
                        ->where('cart_ref_id',$cart_ref_id)
                        ->where('product_id',$product_id)
                        ->where('is_active','1')
                        ->update(['quantity' => $items[$i]->quantity,'ring_size' => $p->ring_size,'bangle_size' => $p->bangle_size,'bracelet_size' => $p->bracelet_size,'updated_at' => date("Y-m-d H:i:s")]);

                    }
                    else
                    {
                        $cart = new Cart;

                        $cart->cart_ref_id = $cart_ref_id;
                        $cart->user_id = $uid;
                        $cart->product_id = $p->id;
                        $cart->ring_size = $p->ring_size;
                        $cart->bangle_size = $p->bangle_size;
                        $cart->bracelet_size = $p->bracelet_size;
                        $cart->quantity = $items[$i]->quantity;
                        $cart->is_active = '1';
                        $rs = $cart->save();
                    }
                }
            }
        }
        else
        {
            $query = "SHOW TABLE STATUS WHERE name='cart'";
            $result = DB::select(DB::raw($query));
            if($result != null){
                $r = json_decode(json_encode($result), True);
                $nextid = $r[0]['Auto_increment'];
            }

            if(!empty($products) && !empty($uid))
            {
                $items = $products;
                for ($i = 0; $i < count($items); $i++) {
                    $p = $items[$i]->item;

                    $cart = new Cart;

                    $cart->cart_ref_id = $uid.'-'.$nextid;
                    $cart->user_id = $uid;
                    $cart->product_id = $p->id;
                    $cart->ring_size = $p->ring_size;
                    $cart->bangle_size = $p->bangle_size;
                    $cart->bracelet_size = $p->bracelet_size;
                    $cart->quantity = $items[$i]->quantity;
                    $cart->is_active = '1';
                    $rs = $cart->save();
                }
            }
        }
    }
}
