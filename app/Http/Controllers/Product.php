<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\Alerts;
use App\Review;
use App\Watchlist;
use App\JewellerInformation;
use App\Mail\SendAlertMail;
use App\Customize;
use App\Mail\SendCustomizeMail;
use App\Mail\SendPriceDropAlertMail;
use App\RatingReview;
use App\PriceDropAlert;

class Product extends Controller
{
    public function getProductDetail(){
        
        $id = $_REQUEST['id'];

        //$sql2="SELECT * from products where id='".$id."'";
        
        $product = DB::table('products')
                    ->where('id',$id)
                    ->get();
        
        return json_encode($product);
    }
    
    public function getJewellerInformation(){
       
        $c = $_REQUEST['s'];

        //mysqli_set_charset($conn,"utf8");

        //$sql2="SELECT * from jeweller_information where id=".$c;

        $j_info = DB::table('jeweller_information')
                    ->where('id',$c)
                    ->get();
        
        return json_encode($j_info);
    }
    
    public function getJewellerRating(){

        $c = $_REQUEST['s'];
        /*
        $total = DB::table('products')
                    ->select(DB::raw('count(VC_SKU) as count'))
                    ->get();
        
        $t = $total[0]->count;

        $pcount = DB::table('products')
                    ->select(DB::raw('count(VC_SKU) as count'))
                    ->where('jeweller_id',$c)
                    ->get();
        $j = $pcount[0]->count;
        
        $s1 = $t / 5;
        $s2 = $j / $s1;
        $jpcocunt = round($s2,2);

        return $jpcocunt;
        */
        $sql2="SELECT AVG(score) as score from rating_review where pid in (SELECT id FROM products WHERE jeweller_id = ".$c.")";
        
        $jpcocunt = DB::select(DB::raw($sql2));
        return json_encode($jpcocunt);
    }
    
    public function getSimilarJewellerProducts(){
        
        $c = $_REQUEST['s'];

        //$sql2="SELECT * from products where supplier_name='".$c."' LIMIT 0,12";
        
        $products = DB::table('products')
                    ->where('supplier_name',$c)
                    ->offset(0)
                    ->limit(12)
                    ->get();
        
        return json_encode($products);
    }
    
    public function getSimilarProductsByCat(){
        
        $c = $_REQUEST['c'];

        //$sql2="SELECT * from products where category='".$c."'  GROUP BY VC_SKU LIMIT 0,12";
        
        $products = DB::table('products')
                    ->where('category',$c)
                    ->groupBy('VC_SKU')
                    ->offset(0)
                    ->limit(12)
                    ->get();
        
        return json_encode($products);
    }
    
    public function getProductSize(){
        $sku = $_REQUEST['sku'];
        $category = $_REQUEST['category'];
        $sizetype = $_REQUEST['sizetype'];

        $psize = DB::table('products')
                    ->select($sizetype)
                    ->where('VC_SKU',$sku)
                    ->where('category',$category)
                    ->orderBy($sizetype, 'asc')
                    ->get();
        
        return json_encode($psize);
    }
    
    public function getProductVariant(){
        
        $sku = $_REQUEST['sku'];
        $v = $_REQUEST['v'];
        $type = $_REQUEST['type'];

        //$sql2="SELECT * from products where VC_SKU='".$sku."' and ".$type." ='".$v."'";

        $pvariant = DB::table('products')
                    ->where('VC_SKU',$sku)
                    ->where($type,$v)
                    ->get();
        
        return json_encode($pvariant);
    }
    
    public function setAlert(){
        $email = $_REQUEST['email'];
        $phone = $_REQUEST['phone'];
        $pid = $_REQUEST['id'];
        $size = $_REQUEST['size'];

        //$sql2="INSERT INTO alerts (id,email,phone,pid,size,created_at) VALUES (0,'".$email."','".$phone."',".$pid.",'".$size."',NOW())";
        
        $alert = new Alerts;
        
        $alert->email = $email;
        $alert->phone = $phone;
        $alert->pid = $pid;
        $alert->size = $size;
        
        $alert->save();
        
        $data = array(
                'pid' => $pid,
                'email' => $email,
                'phone' => $phone
            );
        
        $emailto1 = "a@vivocarat.com";
        $emailto2 = "r@vivocarat.com";
        \Mail::to($emailto1)->send(new SendAlertMail($data));
        \Mail::to($emailto2)->send(new SendAlertMail($data));
        
    }
    
    public function sendPriceDropAlert(){
        $email = $_REQUEST['email'];
        $phone = $_REQUEST['phone'];
        $pid = $_REQUEST['id'];

        $alert = new PriceDropAlert;

        $alert->email = $email;
        $alert->phone = $phone;
        $alert->pid = $pid;

        $alert->save();

        $data = array(
                'pid' => $pid,
                'email' => $email,
                'phone' => $phone
            );
        
        $emailto1 = "a@vivocarat.com";
        $emailto2 = "r@vivocarat.com";
        \Mail::to($emailto1)->send(new SendPriceDropAlertMail($data));
        \Mail::to($emailto2)->send(new SendPriceDropAlertMail($data));
    }
    
    public function getReviewList(){
        
        $pid = $_REQUEST['pid'];

        //$sql2="SELECT * from review where pid=".$pid." order by id desc";
        $reviewlist = DB::table('review')
                    ->where('pid',$pid)
                    ->orderBy('id','desc')
                    ->get();
        
        return json_encode($reviewlist);
    }
    
    public function getCumulativeReview(){
        
        $pid = $_REQUEST['pid'];

        //$sql2="SELECT AVG(quality) as q,AVG(price) as p,AVG(value) as v from review where pid=".$pid."";

        $cum_review = DB::table('review')
                    ->select(DB::raw('AVG(quality) as q, AVG(price) as p, AVG(value) as v'))
                    ->where('pid',$pid)
                    ->get();
        
        return json_encode($cum_review);
    }
    
    public function addReview(){
        $postdata = $_REQUEST['review'];
        $request = json_decode($postdata);
        
        //$p=$request->review;
        $p=$request;
        
        $pid = $p->pid;
        $uid= $p->uid;
        $name = $p->name;
        $quality= $p->quality;
        $price = $p->price;
        $value = $p->value;

        $review_text = $p->review;

        //$sql2="INSERT INTO review (id,pid,uid,name,review,quality,price,value,created_at) VALUES (0,".$pid.",".$uid.",'".$name."','".$review."',".$quality.",".$price.",".$value.",NOW())";
        
        $review = new Review;
        
        $review->pid = $pid;
        $review->uid = $uid;
        $review->name = $name;
        $review->review = $review_text;
        $review->quality = $quality;
        $review->price = $price;
        $review->value = $value;
        
        $review->save();
    }
    
    public function addProductReview(){
        $postdata = $_REQUEST['review'];
        $request = json_decode($postdata);

        $p=$request;

        $pid = $p->pid;
        $uid= $p->uid;
        $name = $p->name;
        $title= $p->title;
        $score = $p->score;
        $review_text = $p->review;

        $review = new RatingReview;

        $review->pid = $pid;
        $review->uid = $uid;
        $review->name = $name;
        $review->review = $review_text;
        $review->title = $title;
        $review->score = $score;

        $review->save();
    }
    
    public function getProductReviewList(){

        $pid = $_REQUEST['pid'];

        $reviewlist = DB::table('rating_review')
                    ->where('pid',$pid)
                    ->orderBy('id','desc')
                    ->get();

        return json_encode($reviewlist);
    }
    
    public function getProductCumulativeReview(){

        $pid = $_REQUEST['pid'];

        $cum_review = DB::table('rating_review')
                    ->select(DB::raw('AVG(score) as score'))
                    ->where('pid',$pid)
                    ->get();

        return json_encode($cum_review);
    }
    
    public function getWatchlist(){
        
        $uid = $_REQUEST['uid'];

        //$sql2="SELECT * from watchlist as w left join products as p on w.product_id=p.id where w.uid=".$uid;
        
        $watchlist = DB::table('watchlist')
            ->leftJoin('products', 'watchlist.product_id', '=', 'products.id')
            ->where('uid',$uid)
            ->get();
        
        return json_encode($watchlist);
        
    }
    
    public function addToWatchlist(){
        
        $pid= $_REQUEST['pid'];
        $uid = $_REQUEST['uid'];

        //$sql2="INSERT INTO watchlist (id,uid,product_id) VALUES (0,".$uid.",".$pid.")";
        
        try {
            DB::table('watchlist')->insert(
                ['uid' => $uid, 'product_id' => $pid]
                );
            return "success";
        }
        catch (\Exception $e) {
            return $e;
        }
    }
    
    public function removeFromWatchlist(){
        $pid= $_REQUEST['pid'];
        $uid = $_REQUEST['uid'];

        //$sql2="DELETE FROM watchlist where uid=".$uid." and product_id=".$pid;
        
        try {
            DB::table('watchlist')
                ->where('uid', $uid)
                ->where('product_id',$pid)
                ->delete();
            return "success";
        }
        catch (\Exception $e) {
            return $e;
        }
    }
    
    public function uploadimages(){
      if ( !empty( $_FILES ) ) {
          $date = date("YmdHis");
          $path = $_FILES[ 'file' ][ 'name' ];
          $ext = pathinfo($path, PATHINFO_EXTENSION);
          $name = pathinfo($path, PATHINFO_FILENAME);
          $fname = $name.'_'.$date.'.'.$ext;

          $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
          $uploadPath = 'uploads' . DIRECTORY_SEPARATOR . $fname;

          move_uploaded_file( $tempPath, $uploadPath );

          //$answer = array( 'answer' => 'File transfer completed' );
          $answer = array( 'filename' => $fname );
          $json = json_encode( $answer );

          return $json;

      } else {

          return 'No files';

      }
    }

    public function saveCustomizeformData(){
      $name = $_REQUEST['name'];
      $email = $_REQUEST['email'];
      $phone = $_REQUEST['phone'];
      $comment = isset($_REQUEST['comment'])?$_REQUEST['comment']:NULL;
      $image = json_decode($_REQUEST['imagename']);
      $img1 = NULL;
      $img2 = NULL;
      $img3 = NULL;

      if(count($image)>0){
          $img1 = isset($image[0])?$image[0]:NULL;
          $img2 = isset($image[1])?$image[1]:NULL;
          $img3 = isset($image[2])?$image[2]:NULL;
      }

      $customize = new Customize;
      $customize->name = $name;
      $customize->email = $email;
      $customize->phone = $phone;
      $customize->comment = $comment;
      $customize->image_1 = $img1;
      $customize->image_2 = $img2;
      $customize->image_3 = $img3;

      $data = array(
          'name' => $name,
          'email' => $email,
          'phone' => $phone,
          'comment' => $comment,
          'image_1' => $img1,
          'image_2' => $img2,
          'image_3' => $img3
      );

      try {
          $customize->save();

          $emailto1 = 'r@vivocarat.com';
          $emailto2 = 'a@vivocarat.com';
          \Mail::to($emailto1)->send(new SendCustomizeMail($data));
          \Mail::to($emailto2)->send(new SendCustomizeMail($data));

          return "success";
      }
      catch (\Exception $e) {
          return $e;
      }
    }
}