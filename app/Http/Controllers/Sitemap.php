<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;

class Sitemap extends Controller
{
    public function getSitemap()
    {
        $text='<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
        <url>
            <loc>https://www.vivocarat.com</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/home" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/home" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>1.00</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=Gold&amp;subtype=All</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/Gold/All" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/Gold/All" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.99</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=Diamond&amp;subtype=All</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/Diamond/All" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/Diamond/All" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.99</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=Silver&amp;subtype=All</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/Silver/All" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/Silver/All" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.99</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-lookbook.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/lookbook" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/lookbook" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.99</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=gold&amp;subtype=rings</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/gold/rings" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/gold/rings" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=gold&amp;subtype=earrings</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/gold/earrings" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/gold/earrings" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=gold&amp;subtype=pendants</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/gold/pendants" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/gold/pendants" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=gold&amp;subtype=nosepins</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/gold/nosepins" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/gold/nosepins" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=gold&amp;subtype=bangles</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/gold/bangles" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/gold/bangles" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=gold&amp;subtype=bracelets</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/gold/bracelets" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/gold/bracelets" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=gold&amp;subtype=necklaces</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/gold/necklaces" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/gold/necklaces" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=gold&amp;subtype=goldcoins</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/gold/goldcoins" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/gold/goldcoins" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=diamond&amp;subtype=rings</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/diamond/rings" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/diamond/rings" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=diamond&amp;subtype=earrings</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/diamond/earrings" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/diamond/earrings" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=diamond&amp;subtype=pendants</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/diamond/pendants" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/diamond/pendants" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=diamond&amp;subtype=noepins</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/diamond/noepins" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/diamond/noepins" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=diamond&amp;subtype=Tanmaniya</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/diamond/Tanmaniya" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/diamond/Tanmaniya" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=silver&amp;subtype=rings</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/silver/rings" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/silver/rings" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=silver&amp;subtype=earrings</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/silver/earrings" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/silver/earrings" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=silver&amp;subtype=pendants</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/silver/pendants" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/silver/pendants" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-list.php?type=silver&amp;subtype=accessories</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/silver/accessories" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/silver/accessories" />            
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>

        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Incocu%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Incocu%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Incocu%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Shankaram%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Shankaram%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Shankaram%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Lagu%20Bandhu</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Lagu%20Bandhu" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Lagu%20Bandhu" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Arkina-Diamonds</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Arkina-Diamonds" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Arkina-Diamonds" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=OrnoMart</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/OrnoMart" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/OrnoMart" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Kundan%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Kundan%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Kundan%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Mayura%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Mayura%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Mayura%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Megha%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Megha%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Megha%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Regaalia%20Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Regaalia%20Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Regaalia%20Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Glitter%20Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Glitter%20Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Glitter%20Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=PP-Gold</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/PP-Gold" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/PP-Gold" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=KaratCraft</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/KaratCraft" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/KaratCraft" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=ZKD-Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/ZKD-Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/ZKD-Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=IskiUski</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/IskiUski" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/IskiUski" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Myrah-Silver-Works</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Myrah-Silver-Works" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Myrah-Silver-Works" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Charu-Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Charu-Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Charu-Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Sarvada-Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Sarvada-Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Sarvada-Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-store.php?store=Tsara-Jewellery</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/Tsara-Jewellery" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/Tsara-Jewellery" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-ringguide.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/ringguide" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/ringguide" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-jewelleryeducation.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/jewelleryeducation" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/jewelleryeducation" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-about.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/about" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/about" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-brands.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/brands" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/brands" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-contact.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/contact" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/contact" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-privacypolicy.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/privacypolicy" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/privacypolicy" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-terms.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/terms" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/terms" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-faq.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/faq" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/faq" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-partner.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/partner" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/partner" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/p-affiliate.php</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/affiliate" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/affiliate" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>';
        
        $allstore = DB::table('jeweller_information')
                    ->select('name')
                    ->get();
        
        if($allstore != null)
        {       
            foreach($allstore as $s)
            {
                $categories = DB::table('products')
                            ->select('category')
                            ->distinct()
                            ->where('supplier_name',$s->name)
                            ->get();
                
                if($categories != null)
                {       
                    foreach($categories as $c)
                    {
                
                        $text=$text.'
                        <url>
                            <loc>https://www.vivocarat.com/p-splist.php?store='.$s->name.'&amp;type='.$c->category.'</loc>
                            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/c/store/'.$s->name.'/'.$c->category.'" />
                            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/c/store/'.$s->name.'/'.$c->category.'" />
                            <lastmod>'.date("Y-m-d").'</lastmod>
                            <changefreq>daily</changefreq>
                            <priority>0.90</priority>
                        </url>';
                    }
                }
            }
        }
        
        $lookbookArticle = DB::table('lookbook')
                    ->select('id')
                    ->get();
        
        if($lookbookArticle != null)
        {       
            foreach($lookbookArticle as $l)
            {
                $text=$text.'
                <url>
                    <loc>https://www.vivocarat.com/p-lookbookArticle.php?id='.$l->id.'</loc>
                    <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/lookbookArticle/'.$l->id.'" />
                    <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/lookbookArticle/'.$l->id.'" />
                    <lastmod>'.date("Y-m-d").'</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>0.80</priority>
                </url>';
            }
        }
        
        $products = DB::table('products')
                    ->select('id','slug')
                    ->get();
        
        if($products != null)
        {       
            foreach($products as $p)
            {
                //$p_id = $p->id;
                $text=$text.'
                <url>
                    <loc>https://www.vivocarat.com/p-product.php?id='.$p->id.'&amp;title='.$p->slug.'</loc>
                    <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/p/'.$p->id.'" />
                    <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/p/'.$p->id.'" />
                    <lastmod>'.date("Y-m-d").'</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>0.98</priority>
                </url>';
            }
        }
        
        $searchtag = DB::table('search')
                    ->select('tag')
                    ->get();
        
        if($searchtag != null)
        {       
            foreach($searchtag as $s)
            {
                if($s->tag != 'default')
                {
                    $text=$text.'
                    <url>
                        <loc>https://www.vivocarat.com/p-search.php?text='.$s->tag.'</loc>
                        <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/search/'.$s->tag.'" />
                        <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/search/'.$s->tag.'" />
                        <lastmod>'.date("Y-m-d").'</lastmod>
                        <changefreq>daily</changefreq>
                        <priority>0.98</priority>
                    </url>';
                }
            }
        }
        
        $text=$text. '</urlset>';
        
        file_put_contents('sitemap.xml', $text);
        
        echo "Sitemap generated successfully";
    }
}
