<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\CustomersAuth;
use App\Addresses;
use App\OrdersTemp;
use App\Coupon;
use App\Cart;
use App\Mail\SendOrderConfirmationMail;

class Checkout extends Controller
{
    public function object_to_array($data)
    {
        if (is_array($data) || is_object($data))
        {
            $result = array();
            foreach ($data as $key => $value)
            {
                $result[$key] = $this->object_to_array($value);
            }
            return $result;
        }
        return $data;
    }
    
    public function addOrder(Request $request){
        
//        $o = json_decode($_REQUEST['order']);
        $o = json_decode($request->input('order'));
        
        $uid= $o->uid;
        $total=$o->total;
        $promo=$o->promo;
        $items = $o->items;
        $comment = $o->comment;
        $isGift = $o->isGift;
        
        $orders = DB::table('orders_temp')
                    ->select('id','items')
                    ->where('uid',$uid)
                    ->where('status','NOT_CONFIRMED')
                    ->orderBy('id','desc')
                    ->get();
        
        $checkcart = DB::table('cart')
                    ->select('cart_ref_id')
                    ->where('user_id',$uid)
                    ->where('is_active','1')
                    ->first();
        $cart_ref_id = $checkcart->cart_ref_id;
        
        if($orders != null){
            
            foreach($orders as $order)
            {
                $o_id = $order->id;
                    
                $diff = strcmp($order->items, json_encode($items));
            
                if($diff === 0)
                {
                    DB::table('orders_temp')
                    ->where('id',$o_id)
                    ->update(['cart_ref_id' => $cart_ref_id,'updated_at' => date("Y-m-d H:i:s")]);
                    
                    $arr[0]=$order;
                    return json_encode($arr);
                }
            }
            
        }
        
        try 
        {
            //$sql3="INSERT INTO orders_temp (id,uid,items,total,comment,isGift,promo,created_at) VALUES (0,".$uid.",'".json_encode($items)."',".$total.",'".$comment."',".$isGift.",'".$promo."',NOW());";

            DB::table('orders_temp')->insert(
                ['uid' => $uid, 'items' => json_encode($items), 'total' => $total, 'comment' => $comment, 'isGift' => $isGift, 'promo' => $promo, 'updated_at' => date("Y-m-d H:i:s")]
                );

            try{
                //$sql4="SELECT id from orders_temp where uid=".$uid." ORDER BY id desc LIMIT 1";

                $orderid = DB::table('orders_temp')
                    ->select('id')
                    ->where('uid',$uid)
                    ->orderBy('id','desc')
                    ->limit(1)
                    ->get();
                
                $o_id = $orderid[0]->id;
                DB::table('orders_temp')
                    ->where('id',$o_id)
                    ->update(['cart_ref_id' => $cart_ref_id,'updated_at' => date("Y-m-d H:i:s")]);

                return json_encode($orderid);    
            }
            catch(\Exception $e){
                return $e;
            }
        }
        catch (\Exception $e) {
            return $e;
        }
                
    }
    
    public function getAddress(){
        
        $uid = $_REQUEST['uid'];
        //$sql2="SELECT * from addresses where uid=".$uid." order by address_id desc";
        
        $address = DB::table('addresses')
                    ->where('uid',$uid)
                    ->orderBy('address_id','desc')
                    ->get();
        return json_encode($address);
    }
    
    public function addNewAddress(){
        
        $add=json_decode($_REQUEST['address']);
        $s_add=json_decode($_REQUEST['ship_address']);
        
        $orderId= $add->orderId;

        $name = $add->name;
        $ph= $add->phone;
        $fl = $add->first_line;
        $email = $add->email;
        $uid= $add->uid;

        $ct = $add->city;
        $sta= $add->state;
        $pin = $add->pin;
        $cty = $add->country;


        $s_name = $s_add->name;
        $s_ph= $s_add->phone;
        $s_fl = $s_add->first_line;
        $s_email = $s_add->email;
        $s_uid= $s_add->uid;

        $s_ct = $s_add->city;
        $s_sta= $s_add->state;
        $s_pin = $s_add->pin;
        $s_cty = $s_add->country;


        //$sql2="INSERT INTO addresses (address_id,name,phone,first_line,uid,order_id,city,state,pin,country) VALUES (0,'".$name."','".$ph."','".$fl."',".$uid.",".$orderId.",'".$ct."','".$sta."','".$pin."','".$cty."')";
            
        try{
            DB::table('addresses')->insert(
                ['name' => $name, 'phone' => $ph, 'first_line' => $fl, 'uid' => $uid, 'order_id' => $orderId, 'city' => $ct, 'state' => $sta, 'pin' => $pin, 'country' => $cty]
                );

        }
        catch(\Exception $e){
            return $e;
        }

        
        //$sql_ship="INSERT INTO addresses (address_id,name,phone,first_line,uid,order_id,city,state,pin,country) VALUES (0,'".$s_name."','".$s_ph."','".$s_fl."',".$uid.",".$orderId.",'".$s_ct."','".$s_sta."','".$s_pin."','".$s_cty."')";
        
        try{
            DB::table('addresses')->insert(
                ['name' => $s_name, 'phone' => $s_ph, 'first_line' => $s_fl, 'uid' => $uid, 'order_id' => $orderId, 'city' => $s_ct, 'state' => $s_sta, 'pin' => $s_pin, 'country' => $s_cty]
                );

        }
        catch(\Exception $e){
            return $e;
        }

        
        //$sql_np="UPDATE orders_temp set name='".$name."',email='".$s_email."', phone='".$ph."' where id=".$orderId;

        try{
            DB::table('orders_temp')
                    ->where('id', $orderId)
                    ->update(['name' => $name,'email' => $s_email,'phone' => $s_ph,'updated_at' => date("Y-m-d H:i:s")]);

        }
        catch(\Exception $e){
            return $e;
        }
        
        //$sql4="SELECT address_id from addresses where order_id=".$orderId." order by address_id desc limit 2";
        
        $addressArr = DB::table('addresses')
                    ->select('address_id')
                    ->where('order_id',$orderId)
                    ->orderBy('address_id','desc')
                    ->limit(2)
                    ->get();
        
        if ($addressArr != NULL) {
            $adrs = json_decode(json_encode($addressArr), True);
            
            //$sql3="UPDATE orders_temp set bill_address_id=".$emparray[1]." , ship_address_id=".$emparray[0]." where id=".$orderId;
            
            try{
                DB::table('orders_temp')
                    ->where('id', $orderId)
                    ->update(['bill_address_id' => $adrs[1]['address_id'],'ship_address_id' => $adrs[0]['address_id'],'updated_at' => date("Y-m-d H:i:s")]);
                return 'success';

            }
            catch(\Exception $e){
                return $e;
            }
            
        }
        else
        {
            return 'error';   
        }
    }
    
    public function getCoupons(){
        
        //$sql2="SELECT * from coupon where status='true'";
        
        try{

            $coupon = DB::table('coupon')
                ->where('status','true')
                ->get();

            return json_encode($coupon);  
        }
        catch(\Exception $e){
            return $e;
        }
    }
    
    public function getOrderDetails(){
        
        $id = $_REQUEST['id'];
        $orderdata = array();

        //$sql2="SELECT * from orders_temp left join addresses on orders_temp.bill_address_id=addresses.address_id where id=".$id;
        
        $result1 = DB::table('orders_temp')
            ->leftJoin('addresses', 'orders_temp.bill_address_id', '=', 'addresses.address_id')
            ->where('orders_temp.id',$id)
            ->get();

        array_push($orderdata,$result1[0]);
        
        //$sql3="SELECT * from orders_temp left join addresses on orders_temp.ship_address_id=addresses.address_id where id=".$id;
        
        $result2 = DB::table('orders_temp')
            ->leftJoin('addresses', 'orders_temp.ship_address_id', '=', 'addresses.address_id')
            ->where('orders_temp.id',$id)
            ->get();
        
        array_push($orderdata,$result2[0]);
        
        return json_encode($orderdata);
    }
    
    public function confirmOrder(){
        
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $cust_email = $_REQUEST['email'];
        $isCod = $_REQUEST['isCod'];
		$isGift = $_REQUEST['isGift'];
        $txid = $_REQUEST['txid'];

        //$sql2="UPDATE orders_temp set status='CONFIRMED', isCod='".$isCod."', order_gen_id='".$txid."' where id=".$id;
        
        DB::table('orders_temp')
            ->where('id', $id)
            ->update(['status' => 'CONFIRMED','isCod' => $isCod,'isGift' => $isGift,'order_gen_id' => $txid,'updated_at' => date("Y-m-d H:i:s")]);

        //$sql3="SELECT * from orders_temp left join addresses on orders_temp.bill_address_id=addresses.address_id where id=".$id;
        
        $result = DB::table('orders_temp')
            ->leftJoin('addresses', 'orders_temp.bill_address_id', '=', 'addresses.address_id')
            ->where('orders_temp.id',$id)
            ->get();

        foreach ($result as $row) {

            $order=$row->items;
            $total=$row->total;
            $isGift=$row->isGift;
            $promo=$row->promo;
            $name=$row->name;
            $phone=$row->phone;
            
            $data = array(
                    'id' => $id,
                    'name' => $name,
                    'order' => $order,
                    'total' => $total,
                    'promo' => $promo
                );
                
            \Mail::to($cust_email)->send(new SendOrderConfirmationMail($data));

        }
        
        $cartrefid = DB::table('orders_temp')
                    ->select('uid','cart_ref_id')
                    ->where('id',$id)
                    ->first();
        
        $cart_ref_id = $cartrefid->cart_ref_id;
        $uid = $cartrefid->uid;
        
        DB::table('cart')
            ->where('user_id',$uid)
            ->where('cart_ref_id',$cart_ref_id)
            ->update(['is_active' => '0','updated_at' => date("Y-m-d H:i:s")]);
        
        return 'success';
    }
    
    public function removeFromCart(){
        $uid = $_REQUEST['uid'];
        $item = json_decode($_REQUEST['item']);
        $product_id = $item->id;
        
        $checkp = DB::table('cart')
                    ->where('user_id',$uid)
                    ->where('product_id',$product_id)
                    ->where('is_active','1')
                    ->first();
        
        if($checkp != null){
            DB::table('cart')
                ->where('user_id',$uid)
                ->where('product_id',$product_id)
                ->where('is_active','1')
                ->delete();
        }   
    }
}