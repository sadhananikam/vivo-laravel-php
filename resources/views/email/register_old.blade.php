<html>
 <head>
  <style>
        @font-face {
            font-family: 'leela';
            src: url('https://www.vivocarat.com/fonts/LEELAWADEE.TTF') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        
        /* For Outlook email client compatibility*/
        
        @media screen {
            .webfont {
                font-family: 'Leelawadee', Roboto, Arial, sans-serif !important;
            }
        }
        /*End of compatibility css*/
        
        body {
            font-family: 'Leelawadee', Roboto, sans-serif;
            font-size: 15px;
            margin-left: auto;
            margin-right: auto;
            background-color: #f3f3f3;

        }
</style>

    <!--Web font-->
    <link href='https://www.vivocarat.com/fonts/LEELAWADEE.TTF' rel='stylesheet' type='text/css'>
    
    
</head>

<body align='center' style="width:600px;text-align:center;font-family: 'Leelawadee', Roboto, sans-serif;font-size: 15px;margin-left: auto;margin-right: auto;background-color: #f3f3f3;">

<table align='center' style='padding-top: 10px;padding-bottom: 10px;'>
 <tr>
  <td align='center'>
   <img src='https://www.vivocarat.com/images/emailers/rounded-logo.png' alt='logo'>
  </td>
 </tr>
</table>

<div style='margin-bottom: 5px;'>

        <div style="font-family: 'Leelawadee';text-align:left;font-size:16px;font-weight: 100;">


            <a href='https://play.google.com/store/apps/details?id=com.vivocarat.catalogapp&hl=en' style='text-decoration:none;'>
                <img src='https://www.vivocarat.com/images/emailers/vivocarat-app.jpg' alt='Vivocarat android app'>
            </a>
        </div>
</div>
            
                

<div style='text-align:left;background-color:white;padding-left:10px;padding-right:10px;padding-top: 20px;padding-bottom: 20px;width: 600px;'>
            
  <p style='margin-left:20px;margin-right:20px;'>Hello {{ $name }}, thank you for registering with us!</p>

  <p style='margin-left:20px;margin-right:20px;'>Vivocarat.com is your one stop source for purchasing affordable jewellery online!</p>

  <p style='margin:0px;margin-left:20px;margin-right:20px;padding-bottom: 10px;'>We welcome you to our paradise, explore the gold and diamond products crafted by the choicest of jewellers in your city. Take a look and scroll through our wide array of certified diamond and gold ornaments crafted to perfection.</p>

  <p style='margin:0px;margin-left:20px;margin-right:20px;padding-bottom: 10px;'>Apart from our impeccable designs, what's more exciting is, we also provide customization of jewellery to suit your personal style.</p>
                    
  <p style='margin:0px;margin-left:20px;margin-right:20px;padding-bottom: 10px;'>Finally, with our easy EMI payment option, you can now splurge on that diamond piece you have been long dreaming of and add a luxurious touch to your lifestyle.</p> 
                    
  <p style='margin:0px;margin-left:20px;margin-right:20px;padding-bottom: 40px;'>Happy Shopping!</p>

                    
                    
    <img src='https://www.vivocarat.com/images/emailers/usp.png' style='padding-bottom: 30px;'>
            
    <a href='https://www.vivocarat.com/' style='text-decoration: none;'>
      <img src='https://www.vivocarat.com/images/emailers/coupon.jpg' alt='vivocarat coupon VC500'>
                
      <div style='text-align:center;background-color:#e62739;color:white;margin-left: 36%;margin-right: 39%;padding: 10px 20px;margin-top: 30px;margin-bottom: 30px;'>
                Get Started
      </div>
    </a>
        
     
     <a href='https://www.vivocarat.com/#/i/lookbook' style='text-decoration: none;'>
      <img src='https://www.vivocarat.com/images/emailers/lookbook.jpg' alt='lookbook'>
      
      <div style='text-align:center;background-color:#e62739;color:white;margin-left: 36%;margin-right: 39%;padding: 10px 20px;margin-top: 30px;margin-bottom: 20px;'>
                Explore
      </div>
     </a>
                
</div>


<table style='height: 50px;width: 600px;background-color:#f4f4f4;border-right: solid 1px #d4d4d4;border-left: solid 1px #d4d4d4;border-top: solid 1px #d4d4d4;' align='center'>
        <tr>

            <td align='center' style='width:25%'></td>
            <td align='center'>
                <a href='https://www.facebook.com/VivoCarat/' title='Facebook' target='_blank'><img src='https://www.vivocarat.com/images/emailers/facebook-icon.png' style='padding-top: 5px;padding-bottom: 5px;'></a>
            </td>
            <td align='center'>
                <a href='https://twitter.com/vivocarat' title='Twitter' target='_blank'><img src='https://www.vivocarat.com/images/emailers/twitter-icon.png'></a>
            </td>
            <td align='center'>
                <a href='https://www.pinterest.com/vivocarat/' title='Pinterest' target='_blank'><img src='https://www.vivocarat.com/images/emailers/pinterest-icon.png'></a>
            </td>
            <td align='center'>
                <a href='https://www.instagram.com/vivocarat/' title='Instagram' target='_blank'><img src='https://www.vivocarat.com/images/emailers/instagram-icon.png'></a>
            </td>
            <td align='center' style='width:25%'></td>

        </tr>
</table>


<table style='background-color:#E62739;width: 600px;'>
        <tr>
            <td align='center' style='font-size:15px;color:white;text-align: center;padding:7px;color:white;'>CALL US: +91 9167 645 314</td>
            <td align='center' style='width:15%'></td>
            <td align='center' style='font-size:15px;color:white;text-align: center;padding:7px;color:white;'>EMAIL US: <a href='mailto:hello@vivocarat.com' style='text-decoration:none;color:white;'>hello@vivocarat.com</a></td>
        </tr>
</table>


 <div style='height: 7%;padding-top: 20px;padding-bottom: 20px;text-align: center;border: 1px solid #d4d4d4;font-weight: 500;background-color: white;width: 600px;'>
        <p style='font-size:12px; margin:0px;'>Privacy Policy | Terms & Conditions</p>
        <p style='font-size:12px; margin:0px;'>&copy;&nbsp;2016 VivoCarat Retail Pvt. Ltd.All Rights Reserved.</p>
  </div>
  
  
</body>

</html>