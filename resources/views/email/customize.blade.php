<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Customize Mail</title>
</head>

<body>
    <table width="550" border="0" cellpadding="0" cellspacing="0" style="color:#0a7b89; font-family:Arial, Helvetica, sans-serif; font-size:13px;">

    <tbody>
    <tr>
    <td style="border:solid 1px #bababa; padding-left:20px;">
        <p><a href="#"><img src="https://www.vivocarat.com/images/emailers/rounded-logo.png" style="border:none;"></a></p>
        <p>&nbsp;</p>
        <p><strong>Hey Ritesh,</strong>,</p>
        <p>        New customize mail </p>
        <p>       Name ----> {{ $name }}</p>
        <p>       Email ----> {{ $email }}</p>
        <p>       Phone ----> {{ $phone }}</p>
        <p>       Comment ----> {{ $comment }}</p>

        <p>        Regards,<br>Manuel Fernandes
        </p>
        <p>&nbsp;</p>
        <p><br>
        </p>
    </td>
    </tr>

    </tbody>
</table>
</body>
</html>
