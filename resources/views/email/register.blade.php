<html>
 <head>
  <style>
   @font-face {
            font-family: 'Leelawadee';
            src: url('https://www.vivocarat.com/fonts/Leelawadee.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        
        body {
            font-family: 'Leelawadee',sans-serif;
            font-size: 15px;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
        }
</style>

<link href='https://www.vivocarat.com/fonts/Leelawadee.ttf' rel='stylesheet' type='text/css'>
    
    
</head>

<body align='center' style="max-width:600px;text-align:center;font-family: 'Leelawadee';font-size: 15px;margin:0 auto;background-color: #f3f3f3;">
    
<table cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>

 <tr>
  <td>
   <table cellpadding='0' cellspacing='0' align='center' style='padding-top: 10px;padding-bottom: 10px;border-collapse: collapse;'> 
    <tr>
     <td align='center'>
      <img src='https://www.vivocarat.com/images/emailers/rounded-logo.png' alt='Vivocarat logo' title='Vivocarat logo'>  
     </td>
    </tr>
   </table>
  </td>
 </tr>

 <tr>
  <td align='center'>
   <a href='https://play.google.com/store/apps/details?id=com.vivocarat.catalogapp&hl=en' style='text-decoration:none;'>
    <img src='https://www.vivocarat.com/images/emailers/vivocarat-app.jpg' alt='Vivocarat Android app' title='Vivocarat Android app' style="width:100%;display:block;">   
   </a>
  </td>  
 </tr>
            
 <tr>
  <table cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>
   <tr>
    <td style='text-align:left;background-color:white;padding-left:10px;padding-right:10px;padding-top: 20px;padding-bottom: 20px;width:100%;'>
     <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        Hello {{ $name }}, thank you for registering with us!
     </p>

     <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        Vivocarat.com is your one stop source for purchasing affordable jewellery online!
     </p>

     <p style='margin:0px;margin-left:20px;margin-right:20px;padding-bottom: 10px;font-family: Leelawadee;'>
        We welcome you to our paradise, explore the gold and diamond products crafted by the choicest of jewellers in your city. Take a look and scroll through our wide array of certified diamond and gold ornaments crafted to perfection.
     </p>

     <p style='margin:0px;margin-left:20px;margin-right:20px;padding-bottom: 10px;font-family: Leelawadee;'>
        Apart from our impeccable designs, what's more exciting is, we also provide customization of jewellery to suit your personal style.
     </p>
                    
     <p style='margin:0px;margin-left:20px;margin-right:20px;padding-bottom: 10px;font-family: Leelawadee;'>
        Finally, with our easy EMI payment option, you can now splurge on that diamond piece you have been long dreaming of and add a luxurious touch to your lifestyle.
     </p> 
                    
     <p style='margin:0px;margin-left:20px;margin-right:20px;padding-bottom: 40px;font-family: Leelawadee;'>
         Happy Shopping!
     </p>

    </td>  
   </tr>               
   
   <tr>
    <td>
     <img src='https://www.vivocarat.com/images/emailers/usp.png' style='padding-bottom: 30px;display:block;' alt='Vivocarat USP'>
            
      <a href='https://www.vivocarat.com/' style='text-decoration: none;'>
       <img src='https://www.vivocarat.com/images/emailers/coupon.jpg' alt='vivocarat coupon VC500'>
                
        <div style='text-align:center;background-color:#e62739;color:white;margin-left: 36%;margin-right: 39%;padding: 10px 20px;margin-top: 30px;margin-bottom: 30px;'>
                Get Started
        </div>
     </a>
    </td>  
   </tr>
      
   <tr>
    <td>
     <a href='https://www.vivocarat.com/#/i/lookbook' style='text-decoration: none;'>
      <img src='https://www.vivocarat.com/images/emailers/lookbook.jpg' alt='Lookbook'>
      
       <div style='text-align:center;background-color:#e62739;color:white;margin-left: 36%;margin-right: 39%;padding: 10px 20px;margin-top: 30px;margin-bottom: 20px;font-family: Leelawadee;'>
                Explore
       </div>
     </a>  
    </td>  
   </tr>
     
  </table>  
 </tr>               
 


<table style='height: 50px;width:100%;background-color:#f4f4f4;border-right: solid 1px #d4d4d4;border-left: solid 1px #d4d4d4;border-top: solid 1px #d4d4d4;' align='center'>
 <tr>
            <td align='center'>
                <a href='https://www.facebook.com/VivoCarat/' title='Facebook' target='_blank'><img src='https://www.vivocarat.com/images/emailers/facebook-icon.png' style='padding-top: 5px;padding-bottom: 5px;' alt='Facebook'></a>
            </td>
            <td align='center'>
                <a href='https://twitter.com/vivocarat' title='Twitter' target='_blank'><img src='https://www.vivocarat.com/images/emailers/twitter-icon.png' alt='Twitter'></a>
            </td>
            <td align='center'>
                <a href='https://www.pinterest.com/vivocarat/' title='Pinterest' target='_blank'><img src='https://www.vivocarat.com/images/emailers/pinterest-icon.png' alt='Pinterest'></a>
            </td>
            <td align='center'>
                <a href='https://www.instagram.com/vivocarat/' title='Instagram' target='_blank'><img src='https://www.vivocarat.com/images/emailers/instagram-icon.png' alt='Instagram'></a>
            </td>

        </tr>
</table>


<table align='center' style='background-color:#E62739;width: 100%;'>
 <tr>
            <td style='font-size:13px;color:white;text-align: left;padding:7px;color:white;font-family: Leelawadee;'>+91 9167 645 314</td>
            <td style='font-size:13px;color:white;text-align: right;padding:7px;color:white;font-family: Leelawadee;'><a href='mailto:hello@vivocarat.com' style='text-decoration:none;color:white;'>hello@vivocarat.com</a></td>
        </tr>
</table>


 <table style='height: 7%;width: 100%;padding-top: 20px;padding-bottom: 20px;text-align: center;border: 1px solid #d4d4d4;font-weight: 500;'>
 <tr>
      <td>
       <span style='font-size:12px;font-family: Leelawadee;'>
           Privacy Policy | Terms &amp; Conditions
       </span>  
      </td>  
     </tr>
     
 <tr>
      <td>
       <span style='font-size:12px;font-family: Leelawadee;'>
         &copy;&nbsp;2016 VivoCarat Retail Pvt. Ltd.All Rights Reserved.
       </span> 
      </td>   
     </tr>
</table>
  
 </table> 
</body>

</html>