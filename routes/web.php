<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    //return view('welcome');
//    return view('index');
//});

Route::get('/', function () {
   return File::get(public_path() . '/default.php');
});

Route::post('/api/v1/newsletter','Home@newsletter');
Route::get('/api/v1/getbanners','Home@getbanners');

Route::get('/api/v1/getlookbook/{id?}','Home@getlookbook');

Route::get('/api/v1/getLookbookDetail','Home@getLookbookDetail');
Route::get('/api/v1/getLookbookList','Home@getLookbookList');
Route::get('/api/v1/getRelatedBlog','Home@getRelatedBlog');
Route::get('/api/v1/getSimilarProducts','Home@getSimilarProducts');

Route::get('/api/v1/getfeaturedproducts','Home@getfeaturedproducts');

Route::post('/api/v1/contactus','Home@saveContactusform');
Route::post('/api/v1/partner','Home@savePartnerform');
Route::get('/api/v1/getsellerlocation','Home@getsellerlocation');
Route::post('/api/v1/getSellerDetailBySearch','Home@getSellerDetailBySearch');
Route::post('/api/v1/saveSellerLocationEnquiry','Home@saveSellerLocationEnquiry');

Route::get('/api/v1/getStoreCategories','Store@getStoreCategories');
Route::get('/api/v1/getJewellerInformationByName','Store@getJewellerInformationByName');
Route::get('/api/v1/getStoreProducts','Store@getStoreProducts');
Route::get('/api/v1/getStoreProductsCount','Store@getStoreProductsCount');
Route::get('/api/v1/filterStorePrice','Store@filterStorePrice');
Route::get('/api/v1/filterStorePriceCount','Store@filterStorePriceCount');
Route::get('/api/v1/sortStoreList','Store@sortStoreList');
Route::get('/api/v1/sortStoreListCount','Store@sortStoreListCount');

Route::get('/api/v1/getProducts','Category@getProducts');
Route::get('/api/v1/getProductsCount','Category@getProductsCount');
Route::get('/api/v1/filterPrice','Category@filterPrice');
Route::get('/api/v1/filterPriceCount','Category@filterPriceCount');
Route::get('/api/v1/sortList','Category@sortList');
Route::get('/api/v1/sortListCount','Category@sortListCount');
Route::post('/api/v1/savetocart','Category@savetocart');
Route::get('/api/v1/getAllJewellers','Category@getAllJewellers');
Route::get('/api/v1/getcategorydescription','Category@getcategorydescription');

Route::get('/api/v1/getProductsTag','Search@getProductsTag');
Route::get('/api/v1/getProductsTagCount','Search@getProductsTagCount');
Route::get('/api/v1/filterPriceTag','Search@filterPriceTag');
Route::get('/api/v1/filterPriceTagCount','Search@filterPriceTagCount');
Route::get('/api/v1/sortListTag','Search@sortListTag');
Route::get('/api/v1/sortListTagCount','Search@sortListTagCount');
Route::get('/api/v1/gettagdescription','Search@gettagdescription');

Route::get('/api/v1/getProductDetail','Product@getProductDetail');
Route::get('/api/v1/getJewellerInformation','Product@getJewellerInformation');
Route::get('/api/v1/getSimilarJewellerProducts','Product@getSimilarJewellerProducts');
Route::get('/api/v1/getSimilarProductsByCat','Product@getSimilarProductsByCat');
Route::get('/api/v1/getProductVariant','Product@getProductVariant');
Route::get('/api/v1/setAlert','Product@setAlert');
Route::post('/api/v1/sendPriceDropAlert','Product@sendPriceDropAlert');
Route::get('/api/v1/getReviewList','Product@getReviewList');
Route::get('/api/v1/getCumulativeReview','Product@getCumulativeReview');
Route::post('/api/v1/addReview','Product@addReview');
Route::get('/api/v1/getWatchlist','Product@getWatchlist');
Route::get('/api/v1/addToWatchlist','Product@addToWatchlist');
Route::get('/api/v1/removeFromWatchlist','Product@removeFromWatchlist');
Route::get('/api/v1/getProductSize','Product@getProductSize');
Route::post('/api/v1/uploadimages','Product@uploadimages');
Route::post('/api/v1/saveCustomizeformData','Product@saveCustomizeformData');
Route::get('/api/v1/getJewellerRating','Product@getJewellerRating');
Route::post('/api/v1/addProductReview','Product@addProductReview');
Route::get('/api/v1/getProductReviewList','Product@getProductReviewList');
Route::get('/api/v1/getProductCumulativeReview','Product@getProductCumulativeReview');

Route::get('/api/v1/session','Customer@session');
Route::post('/api/v1/login','Customer@login');
Route::post('/api/v1/signUp','Customer@signUp');
Route::get('/api/v1/logout','Customer@logout');
Route::post('/api/v1/forgotpassword','Customer@forgotpassword');
Route::get('/api/v1/getAccountDetails','Customer@getAccountDetails');
Route::post('/api/v1/updateAccountDetails','Customer@updateAccountDetails');
Route::get('/api/v1/checkUrl','Customer@checkUrl');
Route::post('/api/v1/resetPassword','Customer@resetPassword');
Route::post('/api/v1/oauth_login','Customer@oauth_login');
Route::post('/api/v1/oauth_login_google','Customer@oauth_login_google');
Route::post('/api/v1/oauth_login_googleplus','Customer@oauth_login_googleplus');


Route::post('/api/v1/addOrder','Checkout@addOrder');
Route::get('/api/v1/getAddress','Checkout@getAddress');
Route::post('/api/v1/addNewAddress','Checkout@addNewAddress');
Route::get('/api/v1/getCoupons','Checkout@getCoupons');
Route::get('/api/v1/getOrderDetails','Checkout@getOrderDetails');
Route::get('/api/v1/confirmOrder','Checkout@confirmOrder');
Route::post('/api/v1/removeFromCart','Checkout@removeFromCart');

Route::get('/api/v1/getOrderHistory','Order@getOrderHistory');
Route::get('/api/v1/cancelOrder','Order@cancelOrder');
Route::post('/api/v1/removeProductFromOrder','Order@removeProductFromOrder');
Route::get('/api/v1/returnOrder','Order@returnOrder');

Route::get('/api/v1/getVivoHeader','Mobile@getVivoHeader');

Route::get('/api/v1/sitemap','Sitemap@getSitemap');

// Catch all undefined routes. Always gotta stay at the bottom since order of routes matters.
Route::any('{undefinedRoute}', function ($undefinedRoute) {
    //return view('index');
    return File::get(public_path() . '/p-404.php');
})->where('undefinedRoute', '(.*)');