var isServer = true;
var path = "/";

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

/*********For Local*************/
if (isServer) {
    path = "/";

} else {
    path = "vivo-web-2";
}
var w = window.innerWidth;

//***************** Product page **********
if (w <= 1024 && ( document.location.pathname == "/p-product.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/p/"+getUrlParameter('id');}
}

//**************** Product list page *********
if (w <= 1024 && ( document.location.pathname == "/p-list.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');}
}

//***************** Individual brand/store page **********
if (w <= 1024 && ( document.location.pathname == "/p-store.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/"+getUrlParameter('store');}
}

//***************** brand/store with type page **********
if (w <= 1024 && ( document.location.pathname == "/p-splist.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type');}
}

//***************** Search page **********
if (w <= 1024 && ( document.location.pathname == "/p-search.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/search/"+getUrlParameter('text');}
}

//***************** my account page **********
if (w <= 1024 && ( document.location.pathname == "/p-account.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/account";}
}

//***************** wishlist page **********
if (w <= 1024 && ( document.location.pathname == "/p-wishlist.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/wishlist";}
}

//***************** login page **********
if (w <= 1024 && ( document.location.pathname == "p-login.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/login/"+getUrlParameter('id');}
}

//***************** chekout page **********
if (w <= 1024 && ( document.location.pathname == "/p-checkout.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/checkout";}
}

//***************** home page or vivocarat.com/index_1.html **********
if (w <= 1024 && ( document.location.pathname == "/index_1.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index_1.html#/i/home";}
}

//***************** home page or vivocarat.com/index.html **********
if (w <= 1024 && ( document.location.pathname == "/default.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index_1.html#/i/home";}
}


//***************** orders page **********
if (w <= 1024 && ( document.location.pathname == "/p-orders.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/orders";}
} 

//***************** lookbook page **********
if (w <= 1024 && ( document.location.pathname == "/p-lookbook.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/lookbook";}
}

//***************** Jewellery Education page **********
if (w <= 1024 && ( document.location.pathname == "/p-jewelleryeducation.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/jewelleryeducation";}
} 

//***************** About page **********
if (w <= 1024 && ( document.location.pathname == "/p-about.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/about";}
} 

//***************** contact us page **********
if (w <= 1024 && ( document.location.pathname == "/p-contact.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/contact";}
} 

//***************** terms page **********
if (w <= 1024 && ( document.location.pathname == "/p-terms.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/terms";}
} 

//***************** privacy policy page **********
if (w <= 1024 && ( document.location.pathname == "/p-privacypolicy.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/privacypolicy";}
}

//***************** return policy page **********
if (w <= 1024 && ( document.location.pathname == "/p-returns.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/return-exchange";}
}

//***************** partner page **********
if (w <= 1024 && ( document.location.pathname == "/p-partner.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/partner";}
}

//***************** without index.hmtl **********
if (w <= 1024 && ( document.location.pathname == "")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/home";}
}

//***************** brands page **********
if (w <= 1024 && ( document.location.pathname == "/p-brands.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/brands";}
}


$(window).resize(function () {
    var w = window.innerWidth;
    
//***************** Product page **********
if (w <= 1024 && ( document.location.pathname == "/p-product.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/p/"+getUrlParameter('id');}
}

//**************** Product list page *********
if (w <= 1024 && ( document.location.pathname == "/p-list.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');}
}
    
//***************** Individual brand/store page **********
if (w <= 1024 && ( document.location.pathname == "/p-store.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/"+getUrlParameter('store');}
}    
    
//***************** brand/store with type page **********
if (w <= 1024 && ( document.location.pathname == "/p-splist.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type');}
}  
    
//***************** Search page **********
if (w <= 1024 && ( document.location.pathname == "/p-search.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/search/"+getUrlParameter('text');}
}
    
//***************** my account page **********
if (w <= 1024 && ( document.location.pathname == "/p-account.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/account";}
} 

//***************** wishlist page **********
if (w <= 1024 && ( document.location.pathname == "/p-wishlist.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/wishlist";}
}
    
//***************** login page **********
if (w <= 1024 && ( document.location.pathname == "/p-login.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/login/"+getUrlParameter('id');}
}
    
//***************** chekout page **********
if (w <= 1024 && ( document.location.pathname == "/p-checkout.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/checkout";}
}    

//***************** home page or vivocarat.com/index.html **********
if (w <= 1024 && ( document.location.pathname == "/index.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/home";}
}  
    
//***************** orders page **********
if (w <= 1024 && ( document.location.pathname == "/p-orders.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/orders";}
}   
    
//***************** lookbook page **********
if (w <= 1024 && ( document.location.pathname == "/p-lookbook.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/lookbook";}
}
    
//***************** Jewellery Education page **********
if (w <= 1024 && ( document.location.pathname == "/p-jewelleryeducation.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/jewelleryeducation";}
}    

//***************** About page **********
if (w <= 1024 && ( document.location.pathname == "/p-about.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/about";}
}  
    
//***************** contact us page **********
if (w <= 1024 && ( document.location.pathname == "/p-contact.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/contact";}
}     

//***************** terms page **********
if (w <= 1024 && ( document.location.pathname == "/p-terms.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/terms";}
} 

//***************** privacy policy page **********
if (w <= 1024 && ( document.location.pathname == "/p-privacypolicy.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/privacypolicy";}
}    

//***************** return policy page **********
if (w <= 1024 && ( document.location.pathname == "/p-returns.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/return-exchange";}
}
    
//***************** partner page **********
if (w <= 1024 && ( document.location.pathname == "/p-partner.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/partner";}
}
    
//***************** without index.hmtl **********
if (w <= 1024 && ( document.location.pathname == "")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/home";}
}  
    
//***************** brands page **********
if (w <= 1024 && ( document.location.pathname == "/p-brands.php")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/brands";}
}    

});