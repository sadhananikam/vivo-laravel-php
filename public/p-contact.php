<!DOCTYPE html>
<html lang="en" data-ng-app="vivoContactus">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-contact.php">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/contact">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/contact" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/contact";
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->    
</head>

<body ng-cloak>
    
<vivo-header></vivo-header>
    
<div data-ng-controller='contactusCtrl'>
 <div class="container padding-bottom-60px">

  <div class="row">
    <div class="col-lg-3 vivocarat-heading">CONTACT US</div>

    <div class="col-lg-9 categoryGrad margin-top-68px">

    </div>
  </div>

  <div class="row">
   <div class="col-md-6 padding-top-60px">
    <div class="row">
     <div class="col-md-6">
      <p class="heading bold-font">General</p>

      <p class="contact-information">
        +91 9167 645 314 hello@vivocarat.com
      </p>
     </div>

     <div class="col-md-6">
      <p class="heading bold-font">Seller Connect</p>

      <p class="contact-information">
        +91 9503 781 870 partners@vivocarat.com
      </p>
     </div>
    </div>

    <div class="row">
     <div class="col-md-12 padding-top-60px">
      <p class="heading bold-font">Address</p>

      <p class="contact-information">
       302,Swaroop Centre
      </p>
      <p class="contact-information">
       JB Nagar Circle, Chakala
      </p>
      <p class="contact-information">
        Andheri East
      </p>
      <p class="contact-information">
        Mumbai - 400 059
      </p>
     </div>
    </div>
   </div>

   <div class="col-md-6 padding-top-5px">
    <div class="row">
        <div class="col-md-12 have-a-query">
            HAVE A QUERY?
        </div>
    </div>

    <div class="row">
     <form name="contactForm" ng-submit="saveContactform()" autocomplete="off" novalidate>
     <div class="row contact-fields-background">
      <div class="col-md-12">

        <div class="row padding-top-17px">
         <div class="col-md-2 normal-text grey-colour no-padding-left-right bold-font">
                Name
         </div>

         <div class="col-md-1 normal-text grey-colour bold-font">
                :
         </div>

         <div class="col-md-9 no-padding-left-right">
          <input class="contact-fields" type="text" Placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" data-ng-model="contact.cname" name="cname" id="cname" data-ng-required="true" data-ng-pattern="/^[a-zA-Z\s]+$/">
         </div>

         <div class="row">
          <div class="col-md-9 col-md-offset-3 normal-text grey-colour" data-ng-show="contactForm.cname.$dirty && contactForm.cname.$error.required">Name is required</div>
          <div class="col-md-9 col-md-offset-3 normal-text grey-colour" data-ng-show="contactForm.cname.$dirty && contactForm.cname.$error.pattern">Name can contain only alphabets</div>
        </div>
       </div>

        <div class="row padding-top-17px">
         <div class="col-md-2 normal-text grey-colour no-padding-left-right bold-font">
                Email
         </div>

         <div class="col-md-1 normal-text grey-colour bold-font">
                :
         </div>

         <div class="col-md-9 no-padding-left-right">
          <input class="contact-fields" type="email" Placeholder="Enter your email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email'" data-ng-model="contact.cemail" name="cemail" id="cemail" data-ng-required="true">
         </div>

         <div class="row">
          <div class="col-md-9 col-md-offset-3 normal-text grey-colour" data-ng-show="contactForm.cemail.$dirty && contactForm.cemail.$error.required">Email is required</div>
          <div class="col-md-9 col-md-offset-3 normal-text grey-colour" data-ng-show="contactForm.cemail.$dirty && contactForm.cemail.$error.email">Invalid Email</div>
         </div>
        </div>

        <div class="row padding-top-17px">
         <div class="col-md-2 normal-text grey-colour no-padding-left-right bold-font">
                Phone
         </div>

         <div class="col-md-1 normal-text grey-colour bold-font">
                :
         </div>

         <div class="col-md-9 no-padding-left-right">
          <input class="contact-fields" type="text" Placeholder="Enter your phone number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your phone number'" data-ng-model="contact.cphone" name="cphone" id="cphone" data-ng-required="true" data-ng-pattern="/^[0-9]{10,10}$/">
         </div>

         <div class="row">
          <div class="col-md-9 col-md-offset-3 normal-text grey-colour" data-ng-show="contactForm.cphone.$dirty && contactForm.cphone.$error.required">Phone is required</div>
          <div class="col-md-9 col-md-offset-3 normal-text grey-colour" data-ng-show="contactForm.cphone.$dirty && contactForm.cphone.$error.pattern">Enter correct phone number</div>
         </div>
        </div>

        <div class="row padding-top-17px padding-bottom-20px">
         <div class="col-md-2 normal-text grey-colour no-padding-left-right bold-font">
                Message
         </div>

         <div class="col-md-1 normal-text grey-colour bold-font">
                :
         </div>

         <div class="col-md-9 no-padding-left-right">
          <input class="contact-fields" type="text" Placeholder="Enter your message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your message'" data-ng-model="contact.cmessage" name="cmessage" id="cmessage">
         </div>
        </div>
      </div>
     </div>

     <div>
        <button class="btn contact-button" type="submit" ng-disabled="!contactForm.$valid">Submit</button>
     </div>
     </form>
    </div>
   </div>
  </div>

  <div class="row padding-top-30px">
   <div class="col-md-12">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.0342944589356!2d72.8632563144684!3d19.106151387070582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c8478e27b5d5%3A0xf3d09043b2998e9e!2sSwaroop+Centre!5e0!3m2!1sen!2sin!4v1482234650022" width="1111" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
   </div>
  </div>

 </div>

</div>

<vivo-footer></vivo-footer>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoContactus.js"></script>
<!-- end angularjs modules -->

<script src="app/data.js"></script>
<script src="app/directives.js"></script>

<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->

<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>
