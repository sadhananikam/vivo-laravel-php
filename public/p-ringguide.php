<!DOCTYPE html>
<html lang="en" data-ng-app="vivoRingGuide">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-ringguide.php">

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body ng-cloak>

<style> 
    /*Table CSS*/
    
    .CSSTableGenerator {
        margin: 0px;
        padding: 0px;
        width: 100%;
        /*        border: 1px solid #fafafa;*/
        -moz-border-radius-bottomleft: 0px;
        -webkit-border-bottom-left-radius: 0px;
        border-bottom-left-radius: 0px;
        -moz-border-radius-bottomright: 0px;
        -webkit-border-bottom-right-radius: 0px;
        border-bottom-right-radius: 0px;
        -moz-border-radius-topright: 0px;
        -webkit-border-top-right-radius: 0px;
        border-top-right-radius: 0px;
        -moz-border-radius-topleft: 0px;
        -webkit-border-top-left-radius: 0px;
        border-top-left-radius: 0px;
    }
    
    .CSSTableGenerator table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }
    
    .CSSTableGenerator tr:last-child td:last-child {
        -moz-border-radius-bottomright: 0px;
        -webkit-border-bottom-right-radius: 0px;
        border-bottom-right-radius: 0px;
    }
    
    .CSSTableGenerator table tr:first-child td:first-child {
        -moz-border-radius-topleft: 0px;
        -webkit-border-top-left-radius: 0px;
        border-top-left-radius: 0px;
    }
    
    .CSSTableGenerator table tr:first-child td:last-child {
        -moz-border-radius-topright: 0px;
        -webkit-border-top-right-radius: 0px;
        border-top-right-radius: 0px;
    }
    
    .CSSTableGenerator tr:last-child td:first-child {
        -moz-border-radius-bottomleft: 0px;
        -webkit-border-bottom-left-radius: 0px;
        border-bottom-left-radius: 0px;
    }
    
    .CSSTableGenerator tr:hover td {}
    
    .CSSTableGenerator tr:nth-child(odd) {
        background-color: #fafafa;
    }
    
    .CSSTableGenerator tr:nth-child(even) {
        background-color: #ffffff;
    }
    
    .CSSTableGenerator td {
        vertical-align: middle;
        border: 1px solid #e3e3e3;
        border-width: 0px 1px 1px 0px;
        text-align: left;
        padding: 3px;
        font-size: 13px;
        font-family: 'leela';
        font-weight: normal;
        color: #FFF5E9;
    }
    
    .CSSTableGenerator tr:last-child td {
        border-width: 0px 1px 0px 0px;
    }
    
    .CSSTableGenerator tr td:last-child {
        border-width: 0px 0px 1px 0px;
    }
    
    .CSSTableGenerator tr:last-child td:last-child {
        border-width: 0px 0px 0px 0px;
    }
    
    .CSSTableGenerator tr:first-child td {
        background: -o-linear-gradient(bottom, #e62737 5%, #e62737 100%);
        background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #e62737), color-stop(1, #e62737));
        background: -moz-linear-gradient( center top, #e62737 5%, #e62737 100%);
        filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#e62737", endColorstr="#e62737");
        background: -o-linear-gradient(top, #e62737, e62737);
        background-color: #e62737;
        border: 0px solid #fafafa;
        text-align: center;
        border-width: 0px 0px 1px 1px;
        font-size: 16px;
        font-family: 'leela';
        font-weight: bold;
        color: #ffffff;
    }
    
    .CSSTableGenerator tr:first-child:hover td {
        background: -o-linear-gradient(bottom, #e62737 5%, #e62737 100%);
        background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #e62737), color-stop(1, #e62737));
        background: -moz-linear-gradient( center top, #e62737 5%, #e62737 100%);
        filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#e62737", endColorstr="#e62737");
        background: -o-linear-gradient(top, #e62737, e62737);
        background-color: #e62737;
    }
    
    .CSSTableGenerator tr:first-child td:first-child {
        border-width: 0px 0px 1px 0px;
    }
    
    .CSSTableGenerator tr:first-child td:last-child {
        border-width: 0px 0px 1px 1px;
    }
    /*END of table CSS*/
</style>
<vivo-header></vivo-header>

<div data-ng-controller='ringGuideCtrl'>
    
<div class="container padding-bottom-60px">
    
<div class="row padding-bottom-20px">
  <div class="col-lg-2 vivocarat-heading">
       RING SIZER
  </div>

  <div class="col-lg-10 categoryGrad no-padding-left-right margin-top-68px">

  </div>
</div>

<div class="row">
  <div class="col-lg-12 normal-text">
    This method illustrates the steps which will be helpful in determining your ring size.
    <br>Please make sure you compare your measurement with the chart below.
  </div>
</div>

<div class="row ring-guide-position">
    <div class="col-lg-6">
        <img src="images/ringguide/step1.png" alt="step 1">

        <div class="row" style="padding-top: 10px;">
            <div class="col-lg-12">
                <img src="images/ringguide/ring_sizer_1.jpg" alt="Take a piece of paper or thread">
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <img src="images/ringguide/step2.png" alt="step 2">
        <div class="row" style="padding-top: 10px;">
            <div class="col-lg-12">
                <img src="images/ringguide/ring_sizer_2.jpg" alt="wrap paper/thread around finger">
            </div>
        </div>
    </div>
</div>

<div class="row ring-guide-position">
    <div class="col-lg-6">
        <img src="images/ringguide/step3.png" alt="step 3">

        <div class="row padding-top-10px">
            <div class="col-lg-12">
                <img src="images/ringguide/ring_sizer_3.jpg" alt="mark spot where paper/thread meets">
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <img src="images/ringguide/step4.png" alt="step 4">
        <div class="row padding-top-10px">
            <div class="col-lg-12">
                <img src="images/ringguide/ring_sizer_4.jpg" alt="measure length with ruler">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 subheader text-center padding-bottom-20px padding-top-60px">
        Check your measurement with the chart below to determine your ring size.
    </div>
</div>

<div class="row CSSTableGenerator">
    <div class="col-lg-6 no-padding">
        <table style="border: 1px solid #E3E3E3;width: 60%;margin-left: 220px;">
            <tr>
                <td style="color: #FFF5E9 !important;">
                    Ring Size (Indian)
                </td>
                <td colspan="2" style="color: #FFF5E9 !important;">
                    Circumference
                </td>

            </tr>
            <tr>
                <td>

                </td>
                <td class="bold-font subheader">
                    Inches
                </td>
                <td class="bold-font subheader">
                    MM
                </td>
            </tr>
            <tr>
                <td>
                    1
                </td>
                <td>
                    1.61
                </td>
                <td>
                    40.8
                </td>
            </tr>
            <tr>
                <td>
                    2
                </td>
                <td>
                    1.66
                </td>
                <td>
                    42.1
                </td>
            </tr>
            <tr>
                <td>
                    3
                </td>
                <td>
                    1.69
                </td>
                <td>
                    43
                </td>
            </tr>
            <tr>
                <td>
                    4
                </td>
                <td>
                    1.73
                </td>
                <td>
                    44
                </td>
            </tr>
            <tr>
                <td>
                    5
                </td>
                <td>
                    1.77
                </td>
                <td>
                    44.9
                </td>
            </tr>
            <tr>
                <td>
                    6
                </td>
                <td>
                    1.81
                </td>
                <td>
                    45.9
                </td>
            </tr>
            <tr>
                <td>
                    7
                </td>
                <td>
                    1.86
                </td>
                <td>
                    47.1
                </td>
            </tr>
            <tr>
                <td>
                    8
                </td>
                <td>
                    1.89
                </td>
                <td>
                    48.1
                </td>
            </tr>
            <tr>
                <td>
                    9
                </td>
                <td>
                    1.93
                </td>
                <td>
                    49
                </td>
            </tr>
            <tr>
                <td>
                    10
                </td>
                <td>
                    1.97
                </td>
                <td>
                    50
                </td>
            </tr>
            <tr>
                <td>
                    11
                </td>
                <td>
                    2
                </td>
                <td>
                    50.9
                </td>
            </tr>
            <tr>
                <td>
                    12
                </td>
                <td>
                    2.04
                </td>
                <td>
                    51.8
                </td>
            </tr>
            <tr>
                <td>
                    13
                </td>
                <td>
                    2.08
                </td>
                <td>
                    52.8
                </td>
            </tr>
            <tr>
                <td>
                    14
                </td>
                <td>
                    2.13
                </td>
                <td>
                    54
                </td>
            </tr>
            <tr>
                <td>
                    15
                </td>
                <td>
                    2.16
                </td>
                <td>
                    55
                </td>
            </tr>
        </table>
    </div>

    <!--2nd table-->
    <div class="col-lg-6 no-padding">
        <table style="border: 1px solid #E3E3E3;width: 60%;margin-right: 280px;">
            <tr>
                <td style="color: #FFF5E9 !important;">
                    Ring Size (Indian)
                </td>
                <td colspan="2" style="color: #FFF5E9 !important;">
                    Circumference
                </td>

            </tr>
            <tr>
                <td>

                </td>
                <td class="bold-font subheader">
                    Inches
                </td>
                <td class="bold-font subheader">
                    MM
                </td>
            </tr>
            <tr>
                <td>
                    16
                </td>
                <td>
                    2.2
                </td>
                <td>
                    55.9
                </td>
            </tr>
            <tr>
                <td>
                    17
                </td>
                <td>
                    2.24
                </td>
                <td>
                    56.9
                </td>
            </tr>
            <tr>
                <td>
                    18
                </td>
                <td>
                    2.28
                </td>
                <td>
                    57.8
                </td>
            </tr>
            <tr>
                <td>
                    19
                </td>
                <td>
                    2.33
                </td>
                <td>
                    59.1
                </td>
            </tr>
            <tr>
                <td>
                    20
                </td>
                <td>
                    2.36
                </td>
                <td>
                    60
                </td>
            </tr>
            <tr>
                <td>
                    21
                </td>
                <td>
                    2.4
                </td>
                <td>
                    60.9
                </td>
            </tr>
            <tr>
                <td>
                    22
                </td>
                <td>
                    2.44
                </td>
                <td>
                    61.9
                </td>
            </tr>
            <tr>
                <td>
                    23
                </td>
                <td>
                    2.47
                </td>
                <td>
                    62.8
                </td>
            </tr>
            <tr>
                <td>
                    24
                </td>
                <td>
                    2.51
                </td>
                <td>
                    63.8
                </td>
            </tr>
            <tr>
                <td>
                    25
                </td>
                <td>
                    2.55
                </td>
                <td>
                    64.7
                </td>
            </tr>
            <tr>
                <td>
                    26
                </td>
                <td>
                    2.6
                </td>
                <td>
                    66
                </td>
            </tr>
            <tr>
                <td>
                    27
                </td>
                <td>
                    2.63
                </td>
                <td>
                    66.9
                </td>
            </tr>
            <tr>
                <td>
                    28
                </td>
                <td>
                    2.67
                </td>
                <td>
                    67.9
                </td>
            </tr>
            <tr>
                <td>
                    29
                </td>
                <td>
                    2.72
                </td>
                <td>
                    69.1
                </td>
            </tr>
            <tr>
                <td>
                    30
                </td>
                <td>
                    2.76
                </td>
                <td>
                    70.1
                </td>
            </tr>
        </table>
    </div>

</div>
    
</div>
<!--End of container-->

</div>
    
<vivo-footer></vivo-footer>
    
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoRingGuide.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>    