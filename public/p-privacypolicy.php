<!DOCTYPE html>
<html lang="en" data-ng-app="vivoPrivacy">

<head>

    <meta http-equiv="Content-Language" content="en"/>

    <meta name="keywords" content=""/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon"/>
    <meta property="og:url" content="http://www.vivocarat.com"/>
    <meta property="og:type" content="Online Jewellery"/>
    <meta property="og:title" content="Vivocarat"/>
    <meta property="og:description" content="India's finest online jewellery"/>
    <meta property="og:image" content="http://www.vivocarat.com"/>
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery"/>
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more."/>

    <!-- SEO-->
    <meta name="robots" content="index,follow"/>
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4"/>
    
    <link rel="canonical" href="https://www.vivocarat.com/p-privacypolicy.php">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/privacypolicy">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/privacypolicy" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/privacypolicy";
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body ng-cloak>

<vivo-header></vivo-header>

<div data-ng-controller='privacyPolicyCtrl'>

<div class="container padding-bottom-60px">


<div class="col-sm-12">
 <div class="row padding-bottom-20px">
  <div class="col-lg-3 vivocarat-heading">
       PRIVACY POLICY
  </div>

  <div class="col-lg-9 categoryGrad no-padding-left-right margin-top-68px">

  </div>
 </div>
        
 <p class="paragraph-text text-justify">
  Thank you for visiting VivoCarat.com. We value the trust you place in us. That's why we insist upon the highest standards for secure transactions and customer information privacy. This section pertains to the Privacy Policy of the website and native mobile applications.
 </p>
    
 <h4 class="privacy-heading">
     Collection of Personally Identifiable Information
 </h4>
    
 <p class="paragraph-text text-justify">
  By accepting this privacy policy, you authorize VivoCarat.com to collect, store and use any information that you provide on our Website or native mobile applications. The information collected by us include but are not restricted to,
 </p>
    
 <p class="paragraph-text text-justify">
   A) All information entered by you on our website and native mobile applications such as your name, address, contact number, email ID , chat history , and such other information sent by you via emails to our email ID.
 </p>
    
 <p class="paragraph-text text-justify">
  B) Information collected by means of cookies that are installed on your hard drive. Information such as the IP Address of your computer, the server from which you are accessing our website and native mobile applications, details of web browser and operating system used to access our website or native mobile applications, date, time and place of accessing of our website and native mobile applications etc.
 </p>
    
 <h4 class="privacy-heading">Cookies</h4>
    
 <p class="paragraph-text text-justify">
   A "cookie" is a small piece of information stored by a Web server on a Web browser so it can be later read back from that browser. Cookies are useful for enabling the browser to remember information specific to a given user. VivoCarat.com places both permanent and temporary cookies in your computer's hard drive. VivoCarat.com's cookies do not contain any of your personally identifiable information.
 </p>
 
 <h4 class="privacy-heading">Use of information collected</h4>
    
 <p class="paragraph-text text-justify">
  VivoCarat.com owns all the information collected via the Website or applications installed on the website or native mobile applications. As applicable, the information collected by VivoCarat.com shall be used to contact you about the Website or native mobile applications and related news and Services available on the Website or native mobile applications; to monitor and improve the Website or native mobile applications; calculate the number of visitors to the Website or native mobile applications and to know the geographical locations of the visitors; update you on all the special offers available on the Website or native mobile applications and provide you with a better shopping experience. This includes sending email intimating regarding the various offers on the website or native mobile applications. You may at any time choose to unsubscribe from such email. Some of your information may be shared with and Used by third parties who shall need to have access to Information, such as courier companies, credit card processing companies, vendors etc. to enable them and VivoCarat.com perform their duties and fulfill your order requirements. VivoCarat.com does not allow any unauthorized persons or organization to use any information that VivoCarat.com may collect from you through the Website and native mobile applications. However, VivoCarat.com and our native mobile applications are not responsible for any information collected or shared or used by any other third party website or mobile applications due to your browser settings.
 </p>
    
 <p class="paragraph-text text-justify">
  VivoCarat.com, reserves the right to share any of your personal information to comply with the orders of subpoenas, court orders or other legal process. Your Personal Information may be disclosed pursuant to such subpoenas, court order or legal process, which shall be without notice to you.
 </p>
    
 <p class="paragraph-text text-justify">
  VivoCarat.com may share collective information such as demographics and Website or mobile application usage statistics with our sponsors, advertisers or other third parties (such third parties do not include VivoCarat.com's marketing partners and network providers). When this type of information is shared, such parties do not have access to your Personal Information. When you contact VivoCarat.com through any means such as chat/ email, VivoCarat.com reserves the right to include your email ID for marketing communications. You can unsubscribe from such communications anytime you wish to do so.
 </p>
    
 <p class="paragraph-text text-justify">
   The Website and native mobile applications may contain links which may lead you to other Websites or other mobile applications. Please note that once you leave our Website or native mobile applications you will be subjected to the Privacy Policy of the other website, mobile applications and this Privacy Policy will no longer apply.
 </p>
    
 <p class="paragraph-text text-justify margin-top-30px margin-bottom-20px">
   BY USING THE WEBSITE OR NATIVE MOBILE APPLICATIONS, YOU SIGNIFY YOUR AGREEMENT TO THE TERMS OF THIS PRIVACY POLICY, VIVOCARAT.COM RESERVES THE RIGHT, IN OUR SOLE DISCRETION, TO CHANGE, MODIFY, ADD OR DELETE PORTIONS OF THE TERMS OF THIS PRIVACY POLICY AT ANY TIME.
 </p>
    
 <h4 class="privacy-heading">Queries</h4>
    
 <p class="paragraph-text text-justify">
  For any Queries regarding this section please contact at hello@vivocarat.com
 </p>
    
</div>
</div>

</div>
    
<vivo-footer></vivo-footer>
    
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoPrivacy.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>        