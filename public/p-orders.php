<!DOCTYPE html>
<html lang="en" data-ng-app="vivoOrders">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-orders.php">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/orders">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/orders" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/orders";
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body ng-cloak>

<style>
    
    /*Arrow nav tabs CSS*/
    .arrow {
        position: relative;
        color: white !important;
        margin-bottom: 5px;
    }
    .red-gradient {
        height: 20px;
        padding: 10px;
        width: 200px;
        color: #000000;
        background: #e62739;
        /* Old browsers */
    }
    .red {
        color: white !important;
        background: #e62739;
        padding: 8px 62px 8px 25px;
        font-weight: bold;
    }
    .red:hover {
        color: white !important;
        background: #e62739;
        padding: 8px 62px 8px 25px;
    }
    /*Arrow tip css*/
    #arrow4:after {
        content: '';
        height: 0;
        display: block;
        border-color: transparent transparent transparent #e62739;
        border-width: 17px;
        border-style: solid;
        position: absolute;
        top: -7px;
        left: 174px;
    }
    /*END of Arrow nav tabs CSS*/
</style>

<vivo-header></vivo-header>

<div data-ng-controller='ordersCtrl'>

<div class="container padding-bottom-60px">

 <div class="row">
  <div class="tabs-left">

   <ul class="col-lg-2 no-padding-left-right tabs-style">
    <li class="row text-style tab-head first-tab-size">
     <a class="inactive-tab-text-colour" href="p-wishlist.php">
          Wishlist
     </a>
    </li>

    <li class="row text-style tab-head arrow" id="arrow4">
     <a class="red" href="p-orders.php">
         Order history
     </a>
    </li>

    <li class="row text-style tab-head second-tab-size" style="padding-top:5px;">
     <a class="inactive-tab-text-colour" href="p-account.php">
         Account details
     </a>
    </li>
   </ul>

   <div data-ng-hide="!authenticated || isLoaded">
    <div class="col-lg-10" align="center">
     <div class="row  text-center home-panel-row">
      <div class="well">Please wait...</div>
     </div>
    </div>
   </div>

   <div data-ng-show="!isauthenticated || isLoaded" class="register_account col-lg-10" style="margin-top: -2px;">

    <div class="well not-loggedin-pad" data-ng-show="!authenticated">
     <h4 class="title normal-text">
          Not Logged In
     </h4>

     <p class="normal-text margin-bottom-20px">
      Kindly <a class="link-hover" href="p-login.php?id=0">login</a> to see your orders.
     </p>
    </div>

    <div class="wrap" data-ng-show="authenticated && watchlist.length<1">
     <h4 class="title">Order list is empty</h4>
      <p>You have no items in your shopping cart.
       <br>Click<a class="link-hover" href="#i/home"> here</a> to continue shopping
      </p>
    </div>

    <div data-ng-hide="!authenticated" class="col-lg-10 no-padding" style="margin-top: -17px;">
     <div class="row home-panel-row" style="margin-top:0px;">
      <div data-ng-repeat="o in orderHistoryList" style="padding-top: 24px;">

       <h4 class="panel-title">
        <div class="row padding-left-30px">
         <div class="col-xs-12 normal-text bold-font grey-colour no-padding uppercase">
              Order Id: {{o.id}} 
         </div>
     
         <div class="col-xs-12 normal-text bold-font grey-colour no-padding margin-bottom-20px">
             Placed on {{o.created_at}}
         </div>
     
         <div class="col-xs-12 normal-text bold-font grey-colour no-padding uppercase">
            Total amount Rs: {{o.total | INR}}
         </div>
             
       </div>
      </h4>

       <div class="col-xs-12 vivo-panel order-status-structure" data-ng-repeat="c in o.products">
           
        <div class="row">
         <div class="col-xs-2 no-padding-left-right">
          <img data-ng-src="images/products-v2/{{c.item.VC_SKU}}-1.jpg" class="width-100-percent" style="border: 1px solid #eee;" alt="{{c.item.title}}">
         </div>

         <div class="col-xs-10">
          <div class="row">
              
           <div class="col-xs-4 text-left">
            <span class="normal-text uppercase" style="color: #000 !important;letter-spacing:0px;">
             <a class="grey-color" data-ng-href="p-orders.php?id={{c.item.id}}">   
                {{c.item.title}}
             </a>
            </span>
           </div>

           <div class="col-xs-4 text-center">
            <span class="grey-color normal-text">
                  {{o.status}}
            </span>
           </div>

           <div class="col-xs-4 normal-text" style="font-size:15px;">
             <a class="btn btn-xs btn-style uppercase border-radius-none" style="margin-right:10px;" data-ng-click="returnOrder(o.id)" data-ng-if="o.status=='DELIVERED'">
                RETURN
             </a>

             <a class="btn btn-xs btn-style uppercase border-radius-none" data-ng-click="cancelOrder(o.id)" data-ng-if="o.status=='NOT_CONFIRMED'">
                Cancel
             </a>

             <a class="btn btn-xs btn-style uppercase border-radius-none" data-ng-click="buyNow(o)" data-ng-if="o.status=='NOT_CONFIRMED'">
                Buy Now
             </a>
            </div>
            
          </div>

          <div class="row">
           <div class="col-xs-12 text-left" style="margin-top: 20px;">

            <div class="row">
             <div class="col-xs-2 no-padding-left">
              <p class="normal-text grey-color">
                 Qty:{{c.quantity}}
              </p>
             </div>
            </div>
                                                        
            <div data-ng-show="c.item.ring_size != null && c.item.ring_size != '' && c.item.ring_size != 0" class="row">                                  
             <div class="col-xs-2 no-padding-left">
              <p class="normal-text grey-color">
                 Size:{{c.item.ring_size}}
              </p>
             </div>
            </div>
                                                        
            <div data-ng-show="c.item.bangle_size != null && c.item.bangle_size != '' && c.item.bangle_size != 0" class="row">                                 
             <div class="col-xs-2 no-padding-left">
              <p class="normal-text grey-color">
                 Size:{{c.item.bangle_size}}
              </p>
             </div>
            </div>
                                                        
            <div data-ng-show="c.item.bracelet_size != null && c.item.bracelet_size != '' && c.item.bracelet_size != 0" class="row">     
             <div class="col-xs-2 no-padding-left">
              <p class="normal-text grey-color">
                 Size:{{c.item.bracelet_size}}
              </p>
             </div>
            </div>

           </div>
  
          </div>
         </div>
        </div>
      
       </div>

       <div class="process padding-bottom-60px">
        <div class="process-row">
         <div class="col-lg-8 no-padding" style="background-color:#F8F8F8;border:solid 1px #E5E5E5;">

          <div class="row">
           <div class="process-step col-xs-2">
            <p class="feedback-style">ORDERED</p>

            <a data-ng-class="o.status=='NOT_CONFIRMED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
            <img src="images/orders/icons/grey-circle.png" data-ng-if="o.status!='CONFIRMED'" alt="not confirmed">
            <img src="images/orders/icons/green-circle.png" data-ng-if="o.status=='CONFIRMED'" alt="confirmed">
            </a>
           </div>

           <div class="process-step col-xs-2">
            <p class="feedback-style">CONFIRMED</p>

            <a data-ng-class="o.status=='CONFIRMED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
            <img src="images/orders/icons/grey-circle.png" data-ng-if="o.status!='CONFIRMED'" alt="not confirmed">
            <img src="images/orders/icons/green-circle.png" data-ng-if="o.status=='CONFIRMED'" alt="confirmed">
            </a>
           </div>

           <div class="process-step col-xs-2">
            <p class="feedback-style">PROCESSING</p>

            <a data-ng-class="o.status=='PROCESSING'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
            <img src="images/orders/icons/grey-circle.png" data-ng-if="o.status!='PROCESSING'" alt="process in queue">
            <img src="images/orders/icons/green-circle.png" data-ng-if="o.status=='PROCESSING'" alt="processing">
           </a>
          </div>

           <div class="process-step col-xs-2">
            <p class="feedback-style">SHIPPING</p>

            <a data-ng-class="o.status=='DISPATCHED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
            <img src="images/orders/icons/grey-circle.png" data-ng-if="o.status!='DISPATCHED'" alt="waiting for dispatch">
            <img src="images/orders/icons/green-circle.png" data-ng-if="o.status=='DISPATCHED'" alt="dispatched">
            </a>
           </div>

           <div class="process-step col-xs-2">
            <p class="feedback-style">DELIVERED</p>

            <a data-ng-class="o.status=='DELIVERED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
            <img src="images/orders/icons/grey-circle.png" data-ng-if="o.status!='DELIVERED'" alt="not delievered">
            <img src="images/orders/icons/green-circle.png" data-ng-if="o.status=='DELIVERED'" alt="delievred">
            </a>
           </div>

           <div class="process-step col-xs-2 no-padding-left-right">
            <p class="feedback-style">THANK YOU</p>

            <a data-ng-class="o.status=='RECEIVED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
            <img src="images/orders/icons/thank.png" alt="thank you">
            </a>
           </div>
          </div>
                    
         </div>
        </div>
       </div>

      </div>
     </div>
    </div>
   </div>
  </div>
 </div>

 <div class="row need-help-structure">
  <div class="col-lg-3" style="border-right:1px solid #afafaf;padding-bottom: 10px;">
   <p class="need-help-text">need help?</p>
  </div>

  <div class="col-lg-3 email-us-structure">
   <img class="padding-right-15px" src="images/orders/icons/email.png" alt="email us">
   <span class="grey-colour bold-font normal-text" style="font-size:15px;">
        hello@vivocarat.com
   </span>
  </div>

  <div class="col-lg-3 call-us-structure">
   <img class="padding-right-15px" src="images/orders/icons/phone.png" alt="call us">
   <span class="grey-colour normal-text bold-font" style="font-size:15px;">
        +91 9167645314
   </span>
  </div>

  <div class="col-lg-3 chat-with-us-structure">
   <img class="padding-right-15px" src="images/orders/icons/chat.png" alt="chat">
   <span class="normal-text grey-colour bold-font" style="font-size:15px;">
        Chat
   </span>
  </div>

 </div>

</div>

</div>
    
<vivo-footer></vivo-footer>

<div class="modal fade" id="returnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reason for Return</h4>
                </div>
                <div class="modal-body">
                    <textarea style="width:100%" data-ng-model="reason"></textarea>
                </div>
                <div class="modal-footer">
                    <a data-ng-click="forgotPassword()" class="btn btn-default btn-sx" data-dismiss="modal">Cancel</a>
                    <a data-ng-click="returnOrderConfirm(rid,reason)" data-ng-click="forgotPassword()" class="btn btn-vivo btn-sm"> Return Order</a>

                </div>
            </div>
        </div>
    </div>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoOrders.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>    