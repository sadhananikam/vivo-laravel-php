var isServer = false;
var path = "/";

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

/*********For Local*************/
if (isServer) {
    path = "/";

} else {
    //path = "vivo-laravel-v1";
    path = "/";
}
var w = window.innerWidth;
//console.log(document.location.pathname);

//***************** Product page **********
if (w <= 1024 && document.location.pathname == "/p-product.php") {    
    if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/p/"+getUrlParameter('id')+"/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/p/"+getUrlParameter('id');
            }
        }
        else
        {
            document.location = "m-index.html#/p/"+getUrlParameter('id');
        }
    }
}

//**************** Product list page *********
if (w <= 1024 && document.location.pathname == "/p-list.php") {
    if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype')+"/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');
            }
        }
        else
        {
            document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');
        }
    }
}

//***************** Individual brand/store page **********
if (w <= 1024 && document.location.pathname == "/p-store.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/"+getUrlParameter('store');}
}

//***************** brand/store with type page **********
if (w <= 1024 && document.location.pathname == "/p-splist.php") {   
    if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type')+"/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type');
            }
        }
        else
        {
            document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type');
        }
    }    
}

//***************** Search page **********
if (w <= 1024 && document.location.pathname == "/p-search.php") {    
     if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/i/search/"+getUrlParameter('text')+"/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/i/search/"+getUrlParameter('text');
            }
        }
        else
        {
            document.location = "m-index.html#/i/search/"+getUrlParameter('text');
        }
    }
}

//***************** my account page **********
if (w <= 1024 && document.location.pathname == "/p-account.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/account";}
}

//***************** wishlist page **********
if (w <= 1024 && document.location.pathname == "/p-wishlist.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/wishlist";}
}

//***************** login page **********
if (w <= 1024 && document.location.pathname == "/p-login.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/login/"+getUrlParameter('id');}
}

//***************** chekout page **********
if (w <= 1024 && document.location.pathname == "/p-checkout.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/checkout";}
}

//***************** home page or vivocarat.com/index.html **********
if (w <= 1024 && ( document.location.pathname == "/default.php")) {   
    if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/i/home/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/i/home";
            }
        }
        else
        {
            document.location = "m-index.html#/i/home";
        }
    }
}

//***************** orders page **********
if (w <= 1024 && document.location.pathname == "/p-orders.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/orders";}
} 

//***************** lookbook page **********
if (w <= 1024 && document.location.pathname == "/p-lookbook.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/lookbook";}
}

//***************** lookbookArticle page **********
if (w <= 1024 && document.location.pathname == "/p-lookbookArticle.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/lookbookArticle/"+getUrlParameter('id');}
}

//***************** Jewellery Education page **********
if (w <= 1024 && document.location.pathname == "/p-jewelleryeducation.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/jewelleryeducation";}
}

//***************** Affiliate page **********
if (w <= 1024 && document.location.pathname == "/p-affiliate.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/affiliate";}
}

//***************** Jewellery Guide page **********
if (w <= 1024 && document.location.pathname == "/p-jewelleryguide.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/jewelleryguide";}
}

//***************** About page **********
if (w <= 1024 && document.location.pathname == "/p-about.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/about";}
} 

//***************** contact us page **********
if (w <= 1024 && document.location.pathname == "/p-contact.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/contact";}
} 

//***************** terms page **********
if (w <= 1024 && document.location.pathname == "/p-terms.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/terms";}
} 

//***************** privacy policy page **********
if (w <= 1024 && document.location.pathname == "/p-privacypolicy.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/privacypolicy";}
}

//***************** return policy page **********
if (w <= 1024 && document.location.pathname == "/p-returns.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/return-exchange";}
}

//***************** partner page **********
if (w <= 1024 && document.location.pathname == "/p-partner.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/partner";}
}

//***************** without index.html **********
if (w <= 1024 && (document.location.pathname == path)) {    
    if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/i/home/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/i/home";
            }
        }
        else
        {
            document.location = "m-index.html#/i/home";
        }
    }
}

//***************** brands page **********
if (w <= 1024 && document.location.pathname == "/p-brands.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/brands";}
}

//***************** 404 page **********
if (w <= 1024 && (document.location.pathname == "/p-404.php" || (document.location.pathname != "/p-product.php" && document.location.pathname != "/p-list.php" && document.location.pathname != "/p-store.php" && document.location.pathname != "/p-splist.php" && document.location.pathname != "/p-search.php" && document.location.pathname != "/p-account.php" && document.location.pathname != "/p-wishlist.php" && document.location.pathname != "/p-login.php" && document.location.pathname != "/p-checkout.php" && document.location.pathname != "/default.php" && document.location.pathname != "/p-orders.php" && document.location.pathname != "/p-lookbook.php" && document.location.pathname != "/p-lookbookArticle.php" && document.location.pathname != "/p-jewelleryeducation.php"  && document.location.pathname != "/p-affiliate.php" && document.location.pathname != "/p-jewelleryguide.php" && document.location.pathname != "/p-about.php" && document.location.pathname != "/p-contact.php" && document.location.pathname != "/p-terms.php" && document.location.pathname != "/p-privacypolicy.php" && document.location.pathname != "/p-returns.php" && document.location.pathname != "/p-partner.php" && document.location.pathname != path && document.location.pathname != "/p-brands.php" && document.location.pathname != "/p-resetpassword.php" && document.location.pathname != "/p-paymentsuccess.php" && document.location.pathname != "/p-faq.php" && document.location.pathname != "/p-ringguide.php"))) {    
    if(document.location.href.indexOf("#") === -1){ document.location.href= "https://www.vivocarat.com/m-index.html#/i/404";}
}

//***************** Reset password page **********
if (w <= 1024 && document.location.pathname == "/p-resetpassword.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/resetpassword/"+getUrlParameter('token');}
}

//***************** payment success page **********
if (w <= 1024 && document.location.pathname == "/p-paymentsuccess.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/paymentsuccess/"+getUrlParameter('id');}
} 

//***************** faq page **********
if (w <= 1024 && document.location.pathname == "/p-faq.php") { 
    if (window.location.search.indexOf('id') > -1) {
        var id = getUrlParameter('id');
        if(id.length)
        {
            if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq/"+getUrlParameter('id');}
        }
        else
        {
            if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq";}
        }
    }
    else
    {
        if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq";}
    }    
    
}

//***************** ring guide page **********
if (w <= 1024 && document.location.pathname == "/p-ringguide.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/ringguide";}
}


$(window).resize(function () {
    var w = window.innerWidth;
    
//***************** Product page **********
if (w <= 1024 && document.location.pathname == "/p-product.php") {    
     if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/p/"+getUrlParameter('id')+"/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/p/"+getUrlParameter('id');
            }
        }
        else
        {
            document.location = "m-index.html#/p/"+getUrlParameter('id');
        }
    }
}

//**************** Product list page *********
if (w <= 1024 && document.location.pathname == "/p-list.php") {    
    if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype')+"/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');
            }
        }
        else
        {
            document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');
        }
    }
}

//***************** Individual brand/store page **********
if (w <= 1024 && document.location.pathname == "/p-store.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/"+getUrlParameter('store');}
}

//***************** brand/store with type page **********
if (w <= 1024 && document.location.pathname == "/p-splist.php") {    
    if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type')+"/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type');
            }
        }
        else
        {
            document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type');
        }
    } 
}

//***************** Search page **********
if (w <= 1024 && document.location.pathname == "/p-search.php") {   
     if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/i/search/"+getUrlParameter('text')+"/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/i/search/"+getUrlParameter('text');
            }
        }
        else
        {
            document.location = "m-index.html#/i/search/"+getUrlParameter('text');
        }
    }
}

//***************** my account page **********
if (w <= 1024 && document.location.pathname == "/p-account.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/account";}
}

//***************** wishlist page **********
if (w <= 1024 && document.location.pathname == "/p-wishlist.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/wishlist";}
}

//***************** login page **********
if (w <= 1024 && document.location.pathname == "/p-login.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/login/"+getUrlParameter('id');}
}

//***************** chekout page **********
if (w <= 1024 && document.location.pathname == "/p-checkout.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/checkout";}
}

//***************** home page or vivocarat.com/index.html **********
if (w <= 1024 && document.location.pathname == "/default.php") {    
    if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/i/home/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/i/home";
            }
        }
        else
        {
            document.location = "m-index.html#/i/home";
        }
    }
}

//***************** orders page **********
if (w <= 1024 && document.location.pathname == "/p-orders.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/orders";}
} 

//***************** lookbook page **********
if (w <= 1024 && document.location.pathname == "/p-lookbook.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/lookbook";}
}
    
//***************** lookbookArticle page **********
if (w <= 1024 && document.location.pathname == "/p-lookbookArticle.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/lookbookArticle/"+getUrlParameter('id');}
}    

//***************** Jewellery Education page **********
if (w <= 1024 && document.location.pathname == "/p-jewelleryeducation.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/jewelleryeducation";}
} 
    
//***************** Affiliate page **********
if (w <= 1024 && document.location.pathname == "/p-affiliate.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/affiliate";}
}
    
//***************** Jewellery Guide page **********
if (w <= 1024 && document.location.pathname == "/p-jewelleryguide.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/jewelleryguide";}
}    

//***************** About page **********
if (w <= 1024 && document.location.pathname == "/p-about.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/about";}
} 

//***************** contact us page **********
if (w <= 1024 && document.location.pathname == "/p-contact.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/contact";}
} 

//***************** terms page **********
if (w <= 1024 && document.location.pathname == "/p-terms.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/terms";}
} 

//***************** privacy policy page **********
if (w <= 1024 && document.location.pathname == "/p-privacypolicy.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/privacypolicy";}
}

//***************** return policy page **********
if (w <= 1024 && document.location.pathname == "/p-returns.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/return-exchange";}
}

//***************** partner page **********
if (w <= 1024 && document.location.pathname == "/p-partner.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/partner";}
}

//***************** without index.hmtl **********
if (w <= 1024 && (document.location.pathname == path)) {    
    if(document.location.href.indexOf("#") === -1)
    {
        if (window.location.search.indexOf('utm_campaign') > -1) 
        {
            var utm_campaign = getUrlParameter('utm_campaign');
            if(utm_campaign.length)
            {
                var utm_source = getUrlParameter('utm_source');
                var utm_medium = getUrlParameter('utm_medium');

                document.location = "m-index.html#/i/home/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
            }
            else
            {
                document.location = "m-index.html#/i/home";
            }
        }
        else
        {
            document.location = "m-index.html#/i/home";
        }
    }
}

//***************** brands page **********
if (w <= 1024 && document.location.pathname == "/p-brands.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/brands";}
}
    
//***************** 404 page **********
if (w <= 1024 && (document.location.pathname == "/p-404.php" || (document.location.pathname != "/p-product.php" && document.location.pathname != "/p-list.php" && document.location.pathname != "/p-store.php" && document.location.pathname != "/p-splist.php" && document.location.pathname != "/p-search.php" && document.location.pathname != "/p-account.php" && document.location.pathname != "/p-wishlist.php" && document.location.pathname != "/p-login.php" && document.location.pathname != "/p-checkout.php" && document.location.pathname != "/default.php" && document.location.pathname != "/p-orders.php" && document.location.pathname != "/p-lookbook.php" && document.location.pathname != "/p-lookbookArticle.php" && document.location.pathname != "/p-jewelleryeducation.php"  && document.location.pathname != "/p-affiliate.php" && document.location.pathname != "/p-jewelleryguide.php" && document.location.pathname != "/p-about.php" && document.location.pathname != "/p-contact.php" && document.location.pathname != "/p-terms.php" && document.location.pathname != "/p-privacypolicy.php" && document.location.pathname != "/p-returns.php" && document.location.pathname != "/p-partner.php" && document.location.pathname != path && document.location.pathname != "/p-brands.php" && document.location.pathname != "/p-resetpassword.php" && document.location.pathname != "/p-paymentsuccess.php" && document.location.pathname != "/p-faq.php" && document.location.pathname != "/p-ringguide.php"))) {    
    if(document.location.href.indexOf("#") === -1){ document.location.href= "https://www.vivocarat.com/m-index.html#/i/404";}
}
    
    
//***************** Reset password page **********
if (w <= 1024 && document.location.pathname == "/p-resetpassword.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/resetpassword/"+getUrlParameter('token');}
} 
    
//***************** payment success page **********
if (w <= 1024 && document.location.pathname == "/p-paymentsuccess.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/paymentsuccess/"+getUrlParameter('id');}
}  
//***************** faq page **********
if (w <= 1024 && document.location.pathname == "/p-faq.php") { 
    if (window.location.search.indexOf('id') > -1) {
        var id = getUrlParameter('id');
        if(id.length)
        {
            if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq/"+getUrlParameter('id');}
        }
        else
        {
            if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq";}
        }
    }
    else
    {
        if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq";}
    }    
    
}
    
//***************** ring guide page **********
if (w <= 1024 && document.location.pathname == "/p-ringguide.php") {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/ringguide";}
}

});