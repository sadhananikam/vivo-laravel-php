<!DOCTYPE html>
<html lang="en" data-ng-app="vivoAboutus">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-about.php">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/about">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/about" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/about";
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->    
</head>

<body>

<vivo-header></vivo-header>

<div data-ng-controller='aboutCtrl' ng-cloak>
    
<div class="container padding-bottom-60px">

<div class="row">
 <div class="col-lg-4 vivocarat-heading">ABOUT VIVOCARAT</div>

 <div class="col-lg-8 categoryGrad margin-top-68px">

 </div>
</div>
    
<div class="row padding-top-20px">
 <div class="col-md-6">
  <p class="snippet-description">VivoCarat is a promising e-commerce platform that features authentic and remarkable jewellery products. We take pride in showcasing our vivid, spectacular and assorted diamond, gold and gemstone jewellery collection. Each piece is distinctive and flawless in itself since it has been passed under the eye of strict professionals resulting in unparalleled craftsmanship. 
  </p>  
 </div>
    
 <div class="col-md-6">
  <p class="snippet-description">Since its inception, Vivocarat has positively collaborated with pioneering jewellery brands in and around your city to avoid the tedious procedure of browsing through jewellery pieces from store to store. We ensure to bring out top notch products from your trusted neighbourhood jewellery brands that excel in quality and design. 
  </p>  
 </div>
    
</div>
    
<div class="row">
 <div class="col-lg-2 vivocarat-heading">OUR STORY</div>

 <div class="col-lg-10 categoryGrad margin-top-68px">

 </div>
</div>
    
<div class="row padding-top-20px">
 <div class="col-md-6">
  <p class="snippet-description">Turning back the pages of our book, the story behind conceiving Vivocarat leads us to an interesting story. The mastermind this portal Ritesh Oza found out that his friend was struggling real hard to purchase an engagement ring. Sooner, they adapted the fact that the options for the unique design was real less plus the absence of a catalogue for prior “make to order” rings added to the inconvenience. 
  </p>  
 </div>
    
 <div class="col-md-6">
  <p class="snippet-description">So, learning from this experience, Vivocarat has in store selected products not based on categories, but based on collections. As Ritesh Oza says “There is no point in displaying a fancy ring to someone who’s looking for a daily-wear ring” and thus we believe in catering to the needs of each customer. 
  </p>  
 </div>
    
</div>
    
<div class="row">
 <div class="col-lg-3 vivocarat-heading">VIVO OFFERINGS</div>

 <div class="col-lg-9 categoryGrad margin-top-68px">

 </div>
</div>
    
<div class="row padding-top-20px">
 <div class="col-md-12 no-padding-right">
  <div class="row discovery-platform-background">
   <div class="col-md-6 discovery-platform-paragraph-position">
    <p class="offerings-heading">Discovery Platform</p>
       
    <p class="snippet-description">Presenting, Vivocarat “Lookbook” - your personal fashion stylist and jewellery encyclopaedia. It is an assemblage of jewellery tips, fashion guide posts, brands introductions, latest arrivals, latest updates, celebrity inspirations and much more to be added to the gallery. Stay tuned to receive up to date infotainment regarding fashion and jewellery that will help you to experiment with your style and explore your creative side.  
    </p>  
   </div>   
  </div>
     
  <img src="images/about us/discovery platform.png" class="discovery-platform-image-position" alt="discovery platform"> 
     
 </div>
 
</div>
    
<div class="row">
 <div class="col-md-12 no-padding-right">
  <div class="row trusted-brands-background">
      
   <div class="col-md-6"></div>
      
   <div class="col-md-6 trusted-brands-paragraph-position">
    <p class="offerings-heading">Trusted Brands</p>
       
    <p class="snippet-description">Vivocarat takes pride in announcing that we have associated with prestigious jewellery brands all over Mumbai, Surat, Jaipur, Delhi and planning to expand more. From traditional vintage gold jewellery to contemporary diamond jewellery we have a vivid spread of versatile products in terms of design, style, category and occasion. We assure 100 percent authentic and genuine products with certification while delivering it to your doorstep.   
    </p>  
   </div>   
  </div>
     <img src="images/about us/trusted brands.png" class="trusted-brands-image-position" alt="trusted brands">
 </div>
 
</div>
    
<div class="row">
 <div class="col-md-12 no-padding-right">
  <div class="row curated-products-background">
      
   <div class="col-md-6 discovery-platform-paragraph-position">
    <p class="offerings-heading">Curated Products </p>
       
    <p class="snippet-description">Unlike other upcoming jewellery portals, Vivocarat has a restricted collection of diamond and gold jewellery. We do not buy the idea of displaying around 50,000 design with taking design and quality into consideration. Instead, we aim to deal with limited jewellery collection that is crafted to perfection using superior quality metals and impeccable diamonds and coruscating gemstones. Because, at the end of the day, its quality that matters not quantity.  
    </p>  
   </div>   
  </div>
     
  <img src="images/about us/curated products.png" class="curated-products-image-position" alt="curated products"> 
     
 </div>
 
</div>
    
<div class="row">
 <div class="col-md-12 no-padding-right">
  <div class="row customization-background">
   <div class="col-md-6">
    <img src="images/about us/customization top border.png" class="customization-top-border-position" alt="customization top border">
   </div>
      
   <div class="col-md-6 trusted-brands-paragraph-position">
    <p class="offerings-heading">Customization</p>
       
    <p class="snippet-description">At Vivocarat, we aspire to provide you with the opportunity to design your jewellery product according to your taste and preference. We give you the liberty to customise your valuables, awaken the artist in you and come up with precious jewellery so that you can cherish it in the years to come. From opting your precious metal to choosing the correct diamond size or settling for a subtle gemstone colour, we will do it all for you and make your beloved possession just like the way you had dreamt of. And, the best part is, our customisation offer is not limited to rings but also other products including necklaces, earrings, pendant, and much more.    
    </p>  
   </div>  
  </div>
     
     <img src="images/about us/customization.gif" class="customization-image-position" alt="customization gif">
     <img src="images/about us/customization bottom border.png" class="customization-bottom-border-position" alt="customization bottom border">
 </div>
 
</div>
    
<div class="row">
 <div class="col-lg-3 vivocarat-heading">VIVO ON THE GO</div>

 <div class="col-lg-9 categoryGrad margin-top-68px">

 </div>
</div>
    
<div class="row">
 <div class="col-md-12 padding-top-60px">
  <img src="images/about us/vivo on the go.png">  
 </div>   
</div>
    
<div class="row">
 <div class="col-lg-3 vivocarat-heading">VIVO ADVANTAGE</div>

 <div class="col-lg-9 categoryGrad margin-top-68px">

 </div>
</div>
    
<div class="row padding-top-20px">
 <div class="col-md-12 no-padding-right">
  <div class="row">
   <div class="col-md-6 certified-background-image">
    <p class="vivo-advantage-heading">Certified &amp; Hallmarked</p>
     
    <p class="vivo-advantage-paragraph">We ensure our products are B.I.S hallmarked which considerably is the highest form of gold certification and for diamond certification we provide certificates from superior international institutes which include G.I.A (Gemological Institute of America), I.G.I (International Gemological Institute) I.G.L (International Gemological Laboratory) and S.G.L. (Solitaire Gemological Laboratories).
    </p>
   </div> 
      
   <div class="col-md-6 free-shipping-background-image">
    <p class="vivo-advantage-heading">Free Shipping</p>
     
    <p class="vivo-advantage-paragraph">At VivoCarat we commit the best of service, for our precious customers and thus we provide free shipping on every order and ensure timely and safe delivery of the product to your doorstep.
    </p>
   </div>
      
  </div>  
 </div>
 
</div>
    
<div class="row padding-top-20px">
 <div class="col-md-12 no-padding-right">
  <div class="row">
   <div class="col-md-6 payment-gateway-background-image">
    <p class="vivo-advantage-heading">Payment Gateways</p>
     
    <p class="vivo-advantage-paragraph">For a hassle free and smooth shopping experience at Vivocarat, we are offering various secure payment gateways for your utmost convenience. We gladly accept Credit Card, Debit Card, Net Banking, Paytm Wallet and Cash on Delivery (COD). We hereby ensure that all your personal banking information is protected while the payment options are reliable and trustworthy. 
    </p>
   </div> 
      
   <div class="col-md-6 return-background-image">
    <p class="vivo-advantage-heading">Easy Return Policy</p>
     
    <p class="vivo-advantage-paragraph">If by any chance you are not satisfied with a product, you can easily return it to us, and the amount will be refunded to your account through the same payment mode in a span of 5-7 working days.
    </p>
   </div>
      
  </div>  
 </div>
 
</div>




</div>
    
</div>
<vivo-footer></vivo-footer>
    
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoAboutus.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>    

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>        