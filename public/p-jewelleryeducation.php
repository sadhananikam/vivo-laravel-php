<!DOCTYPE html>
<html lang="en" data-ng-app="vivoJewelleryEducation">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-jewelleryeducation.php">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/jewelleryeducation">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/jewelleryeducation" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/jewelleryeducation";
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body style="overflow-x: hidden;" ng-cloak>
    
<vivo-header></vivo-header>  
    
<div data-ng-controller='jewelleryEducationCtrl'>
    
<div class="container padding-bottom-60px padding-top-5px">
 <div class="row" style="background-color: #f1f1f1;padding-bottom: 100px;">
  <div class="row" style="padding: 30px;">
   <img src="images/jewellery-education/4c-diamonds-1.jpg" alt="4c of diamonds">
  </div>

  <div class="row margin-left-right-30px">
   <div class="col-lg-6">
    <div class="row" style="padding-left: 20px;">
     <div class="col-lg-12 box-shadow-outside yellowGradient four-C-box-size">
      <h4 class="four-C-text">CARAT</h4>
     </div>
    </div>
   </div>

   <div class="col-lg-6">
    <div class="row" style="padding-right: 20px;margin-left: 77px;">
     <div class="col-lg-12 box-shadow-outside yellowGradient four-C-box-size">
      <h4 class="four-C-text">COLOR</h4>
     </div>
    </div>
   </div>
 </div>

 <div class="row">
  <div class="col-lg-6 box-shadow-outside left-paragraph-structure no-padding-left" style="padding: 0px 34px 25px 0px;">
                
   <img class="box-shadow-outside" src="images/jewellery-education/carat.jpg" style="margin: 70px 55px 40px 97px;border: solid 1px #d2d2d2;" alt="carat">
                
   <p class="paragraph-text text-justify" style="padding: 0px 0px 10px 34px;margin-bottom: 104px;">
    Carat represents the weight of the diamond and is abbreviated as CT. Not to be mistaken by the quality of a diamond, the Carat unit merely denotes its size by weight. It is also one of the easiest measurement to determine amongst the 4C’s. One carat is one fifth of a gram. Large diamonds are rare to find and because of this the price increases with the increase in its carat. So two one-carat diamonds are less expensive than one two-carat diamond.
   </p>

  </div>

  <div class="col-lg-5 box-shadow-outside right-paragraph-structure">
   
   <img class="box-shadow-outside" src="images/jewellery-education/colour.jpg" style="margin: 70px 55px 40px 54px;border: solid 1px #d2d2d2;" alt="colour">
                
   <p class="paragraph-text text-justify" style="padding: 0px 34px 10px 34px;">
    Diamonds are measured by the lack of their color and its denoted by their respective grades. The grades have been devised by The Gemological Institute of America and they follow certain guidelines. The grading starts from letter D and stretches upto letter Z. The highest grades are D, E, F and are absolutely colorless and rarest. G through J comes next and are nearly colorless. K, L, M grades are noticeable color and are faint yellow. N through Z are further shades of yellow; from very light yellow to light yellow.
   </p>
   <p class="paragraph-text text-justify" style="padding-left: 34px;margin-bottom: 45px;">
    <span class="vivocarat-theme-colour">Fact</span> - Absolutely colorless diamonds are the best in the category.
   </p>

  </div>
 </div>

 <div class="row margin-left-right-30px" style="padding-top: 40px;">
  <div class="col-lg-6" style="padding-right: 4px;">
   <div class="row" style="padding-left: 20px;">
    <div class="col-lg-12 box-shadow-outside yellowGradient four-C-box-size">
     <h4 class="four-C-text">CLARITY</h4>
    </div>
   </div>
  </div>

  <div class="col-lg-6">
   <div class="row" style="padding-right: 20px;margin-left: 77px;">
    <div class="col-lg-12 box-shadow-outside yellowGradient four-C-box-size">
     <h4 class="four-C-text">CUT</h4>
    </div>
   </div>
  </div>
</div>

 <div class="row">
  <div class="col-lg-6 box-shadow-outside left-paragraph-structure" style="padding: 0px 34px 26px 0px;">
   <img class="box-shadow-outside" src="images/jewellery-education/clarity.jpg" style="margin: 70px 55px 40px 98px;border: solid 1px #d2d2d2;">
                
   <p class="paragraph-text text-justify" style="padding: 0px 0px 10px 34px;">
    Diamonds are bound to have some imperfections. Clarity is the measure of tiny little imperfections. The internal imperfections are called inclusions while the external ones are called blemishes. In short, the clearer the diamond, the better it is. Clarity grades FL and IF denotes Flawless and Internally Flawless which are very rare. VVS1 and VVS2 is Very Very Slightly Included where imperfections are very difficult to see under 10x magnification. Imperfections are vaguely detectable under 10x magnification in VS1 and VS2, Very Slightly Included. SI1 and SI2 are Slightly Included where imperfections are easily detectable under 10x magnification.
  </p>
                
  <p class="paragraph-text text-justify" style="padding-left: 34px;padding-top: 16px;margin-bottom: 16px;">
   <span class="vivocarat-theme-colour">Fact</span> - Diamonds are more often than not judged on their clarity.
  </p>
 </div>

 <div class="col-lg-5 box-shadow-outside right-paragraph-structure">
  <img class="box-shadow-outside" src="images/jewellery-education/cut.jpg" style="margin: 70px 55px 40px 54px;border: solid 1px #d2d2d2;">
                
  <p class="paragraph-text text-justify" style="padding: 0px 34px 10px 34px;">  
   Diamond’s cut grade indicates its light performance. It’s one of the most characteristic feature of a diamond. Also because all the other C’s are influenced by nature while cut is determined by it’s makers. Cut grades are Ideal/Excellent, Very Good, Good, Fair and Poor; in the decreasing order of their light performance. Alternatively, the more a diamond reflects the light, the higher is its quality. The important thing to note here is that the cut of a diamond is different from the its shape. Shape is the external appearance of the diamond, while the cut is more indicative of its reflective qualities.
  </p>
                
  <p class="paragraph-text text-justify" style="margin-bottom: 40px;padding-left: 34px;">
   <span class="vivocarat-theme-colour">Fact</span> - Diamond cuts are identifiable by their sparkle.
  </p>
 </div>
</div>

    
    </div>
</div>

</div>

<vivo-footer></vivo-footer>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoJewelleryEducation.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>
   
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>    