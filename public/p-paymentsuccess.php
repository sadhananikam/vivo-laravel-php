<?php
$desk = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if(isset($_GET['id']) && $_GET['id'] != '')
{
    $id = $_GET['id'];
    $mob = 'https://' . $_SERVER['HTTP_HOST'] . "/m-index.html#/i/paymentsuccess/".$id;
}
?>
<!DOCTYPE html>
<html lang="en" data-ng-app="vivoPaymentSuccess">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />

    <link rel="canonical" href="<?php echo $desk; ?>">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo $mob; ?>">
    <link rel="alternate" media="handheld" href="<?php echo $mob; ?>" />
    
    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/paymentsuccess/"+getUrlParameter('id');
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body ng-cloak>

<vivo-header></vivo-header>

<div data-ng-controller='paymentSuccessCtrl'>

<div class="container padding-bottom-60px">

<div class="row">
    <div class="col-lg-12 payment-success-text">
        YOUR ORDER HAS BEEN PLACED. THANK YOU FOR YOUR ORDER.
    </div>
</div>

<div class="row margin-top-30px">
    <div class="col-lg-12 success-paragraph">Vivocarat team will shortly send you an email to {{email}} with all the above details.</div>
    <div class="col-lg-12 success-paragraph">You will also receive a confirmation call on {{phone}} from the team to verify your order.</div>
    <div class="col-lg-12 success-paragraph">In case you are unreachable, please call us on +91 9167645314 to confirm your order.</div>
</div>

<div class="row" style="border-bottom:1px solid #eee;">
 <div class="col-lg-6" style="padding-left:0px !important;border-right:1px solid #eee;">

 <div class="row">
    <div class="col-lg-12 product-summary-heading">ORDER SUMMARY</div>
 </div>

 <div class="row" style="padding-top:15px;">
  <div class="col-lg-4">
    <p class="bold-font grey-colour">Order No.</p>
  </div>
  <div class="col-lg-1">
    <p class="credsfont">:</p>
  </div>
  <div class="col-lg-7">
    <p class="credsfont">{{order.id}}</p>
  </div>
 </div>
     
 <div class="row">
  <div class="col-lg-4">
    <p class="bold-font grey-colour">Ordered On</p>
  </div>
  <div class="col-lg-1">
    <p class="credsfont">:</p>
  </div>
  <div class="col-lg-7">
    <p class="credsfont">{{order.created_at}}</p>
  </div>
 </div>

 <div class="row">
  <div class="col-lg-4">
    <p class="bold-font grey-colour">Payment Mode</p>
  </div>
  <div class="col-lg-1">
    <p class="credsfont">:</p>
  </div>
  <div class="col-lg-7">
    <p class="credsfont">{{order.isCod=='0'?'Online':'Cash on delivery'}}</p>
  </div>
 </div>

</div>

 <div class="col-lg-6">
  <div class="row">
    <div class="col-lg-12 product-summary-heading">
        PRODUCT SUMMARY
    </div>
  </div>

  <div data-ng-repeat="p in products" style="border-bottom: 1px solid #eee;">

    <div class="row" style="padding-top:15px;">
        <div class="col-lg-4">
            <p class="bold-font grey-colour">Product Id</p>
        </div>
        <div class="col-lg-1">
            <p class="credsfont">:</p>
        </div>
        <div class="col-lg-7">
            <p class="credsfont">{{p.item.id}}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <p class="bold-font grey-colour">Product Name</p>
        </div>
        <div class="col-lg-1">
            <p class="credfont">:</p>
        </div>
        <div class="col-lg-7">
            <p class="credsfont">{{p.item.title}}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <p class="bold-font grey-colour">Product Quantity</p>
        </div>
        <div class="col-lg-1">
            <p class="credsfont">:</p>
        </div>
        <div class="col-lg-7">
            <p class="credsfont">{{p.quantity}}</p>
        </div>
    </div>
 
  </div>
     
  <div class="row" style="padding-top: 15px;">
   <div class="col-lg-4">
    <p class="bold-font grey-colour">Grand Total</p>
   </div>
   <div class="col-lg-1">
    <p class="credsfont">:</p>
   </div>
   <div class="col-lg-7">
    <p class="credsfont">Rs.{{order.total | INR}}</p>
   </div>
  </div>

 </div>
</div>

<div class="row detas" style="border-bottom:1px solid #eee;">
    <div class="col-lg-12" style="margin-bottom:30px !important;padding-top: 30px;">
        <h3 class="customer-details-heading">Customer Details</h3> 
    </div>

    <div class="row normal-text">
        <div class="row padding-bottom-10px">
            <div class="col-lg-3 bold-font grey-colour">Name</div>
            <div class="col-lg-1 credsfont">:</div>
            <div class="col-lg-8 credsfont">{{order.name}}</div>
        </div>

        <div class="row padding-bottom-10px">
            <div class="col-lg-3 bold-font grey-colour">Email</div>
            <div class="col-lg-1 credsfont">:</div>
            <div class="col-lg-8 credsfont">{{order.email}}</div>
        </div>

        <div class="row padding-bottom-10px">
            <div class="col-lg-3 bold-font grey-colour">Phone</div>
            <div class="col-lg-1 credsfont">:</div>
            <div class="col-lg-8 credsfont">{{order.phone}}</div>
        </div>

        <div class="row padding-bottom-10px">
            <div class="col-lg-3 bold-font grey-colour">Billing Address</div>
            <div class="col-lg-1 credsfont">:</div>
            <div class="col-lg-8 credsfont">
                {{order.first_line}},{{order.city}},
                {{order.state}} - {{order.pin}},
                {{order.country}}
            </div>
        </div>

        <div class="row padding-bottom-10px">
            <div class="col-lg-3 bold-font grey-colour">Shipping Address</div>
            <div class="col-lg-1 credsfont">:</div>
            <div class="col-lg-8 credsfont">
                {{ship.first_line}},{{ship.city}}, 
                {{ship.state}} - {{ship.pin}},
                {{ship.country}}
            </div>
        </div>

    </div>
</div>

<div class="row pull-right margin-top-30px">
    <div class="col-lg-12">
        <a href="default.php" class="btn big-button"> 
            Home 
        </a>
    </div>
</div>

<div class="row pull-right margin-top-30px">
    <div class="col-lg-12">
        <a href="p-orders.php" class="btn big-button"> 
            My orders 
        </a>

    </div>
</div>

</div>

</div>
    
<vivo-footer></vivo-footer>
    
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoPaymentSuccess.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>        