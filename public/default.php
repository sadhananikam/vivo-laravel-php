<!DOCTYPE html>
<html lang="en" data-ng-app="vivoHome">

<head>
    <meta http-equiv="Content-Language" content="en" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <link rel="canonical" href="https://www.vivocarat.com">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/home">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/home" />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <meta name="google-site-verification" content="jcP_vd70IOn4T8kX7Tix5aBBzNiuck3vVZT_y8gX5Vo" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    
    <script>
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            if (window.location.search.indexOf('utm_campaign') > -1) 
            {
                var utm_campaign = getUrlParameter('utm_campaign');
                if(utm_campaign.length)
                {
                    var utm_source = getUrlParameter('utm_source');
                    var utm_medium = getUrlParameter('utm_medium');

                    document.location = "m-index.html#/i/home/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
                }
                else
                {
                    document.location = "m-index.html#/i/home";
                }
            }
            else
            {
                document.location = "m-index.html#/i/home";
            }
        }
    </script>
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->
</head>

<body ng-cloak>

<vivo-header></vivo-header>

<div data-ng-controller='homeCtrl'>
    
 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="position:relative;z-index:1">
  <!-- Indicators -->
  <ol class="carousel-indicators" style="margin-bottom: 0;">
   <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
   <li data-target="#carousel-example-generic" data-slide-to="1"></li>
   <li data-target="#carousel-example-generic" data-slide-to="2"></li>
   <li data-target="#carousel-example-generic" data-slide-to="3"></li>
   <li data-target="#carousel-example-generic" data-slide-to="4"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
   <div data-ng-repeat="b in banners" class="item" data-ng-class="$index==0?'active':''">
    <a data-ng-if="b.is_link==1" data-ng-href="{{b.href}}" target="{{b.target}}">
     <img class="ban-img img-responsive" data-ng-src="{{b.img_url}}" alt="{{b.alt}}">
    </a>

    <img data-ng-if="b.is_link==0" class="ban-img img-responsive" data-ng-src="{{b.img_url}}" style="height:calc(100vh - 173px) !important;" alt="{{b.alt}}">
   </div>
  </div>
     
  <!-- Controls -->
  <div class="left carousel-control banner-arrow-structure" data-target="#carousel-example-generic" role="button" data-slide="prev">
   <div class="left-scroll center-block"></div>
  </div>

  <div class="right carousel-control banner-arrow-structure" data-target="#carousel-example-generic" role="button" data-slide="next">
   <div class="right-scroll center-block"></div>
  </div>
 </div>

 <div class="container">

 <div class="container infyscroll scroller-parent-container">
 <div id="scroller" class="img-responsive" style="max-width:none;">

  <img class="first pointer" id="incocu" src="images/header/brands logos dropdown/Incocu Jewellers hover.png" alt="Incocu Jewellers logo">
                    
  <img class="pointer" id="shankaram" src="images/header/brands logos dropdown/Shankaram Jewellers hover.png" alt="Shankaram Jewellers logo">
                    
  <img class="pointer" id="lagu" src="images/header/brands logos dropdown/Lagu Bandhu hover.png" alt="Lagu Bandhu logo">
                    
  <img class="pointer" id="arkina" src="images/header/brands logos dropdown/Arkina-Diamonds hover.png" alt="Arkina-Diamonds logo">
                    
  <img class="pointer" id="ornomart" src="images/header/brands logos dropdown/OrnoMart hover.png" alt="OrnoMart logo">
                    
  <img class="pointer" id="kundan" src="images/header/brands logos dropdown/Kundan Jewellers hover.png" alt="Kundan Jewellers logo">
                    
  <img class="pointer" id="mayura" src="images/header/brands logos dropdown/Mayura Jewellers hover.png" alt="Mayura Jewellers logo">
                    
  <img class="pointer" id="megha" src="images/header/brands logos dropdown/Megha Jewellers hover.png" alt="Megha Jewellers logo">
     
  <img class="pointer" id="regaalia" src="images/header/brands logos dropdown/Regaalia Jewels hover.png" alt="Regaalia Jewels logo">
     
  <img class="pointer" id="glitter" src="images/header/brands logos dropdown/Glitter Jewels hover.png" alt="Glitter Jewels logo">
     
<!--  <img class="pointer" id="mani" src="images/header/brands logos dropdown/Mani Jewels hover.png" alt="Mani Jewels logo">-->
     
  <img class="pointer" id="pp" src="images/header/brands logos dropdown/PP-Gold hover.png" alt="Parshwa Padmavati Gold(PPG) logo">
     
  <img class="pointer" id="karatcraft" src="images/header/brands logos dropdown/KaratCraft hover.png" alt="KaratCraft logo">
     
  <img class="pointer" id="zkd" src="images/header/brands logos dropdown/ZKD-Jewels hover.png" alt="Zaveri Kapoorchand Dalichand & Sons (ZKD)-Jewels logo">
     
  <img class="pointer" id="iskiuski" src="images/header/brands logos dropdown/IskiUski hover.png" alt="IskiUski logo">
     
  <img class="pointer" id="myrah" src="images/header/brands logos dropdown/Myrah-Silver-Works hover.png" alt="Myrah-Silver-Works logo">
     
  <img class="pointer" id="charu" src="images/header/brands logos dropdown/Charu-Jewels hover.png" alt="Charu-Jewels logo">
     
  <img class="pointer" id="tsara" src="images/header/brands logos dropdown/tsara hover.png" alt="Tsara-Jewellery logo">
     
  <img class="pointer" id="sarvada" src="images/header/brands logos dropdown/sarvada hover.png" alt="Sarvada-Jewels logo">      

 </div>
</div>
          
 <div class="row padding-bottom-20px">
  <div class="col-lg-3 vivocarat-heading">
      CATEGORIES
  </div>

  <div class="col-lg-9 categoryGrad no-padding-left-right margin-top-68px">

  </div>
 </div>

 <div class="row">  
  <div class="col-md-6 no-padding-right">
    <a href="p-list.php?type=diamond&subtype=earrings">
        <img src="images/home/categories/earrings.png" alt="Diamond Earrings" title="Diamond Earrings">
    </a>
  </div>

  <div class="col-md-6 no-padding-left">
     
   <div class="row">
    <div class="col-md-6 no-padding-left-right">
     <a href="p-list.php?type=diamond&subtype=rings">
      <img src="images/home/categories/rings.png" alt="Diamond Rings" title="Diamond Rings">
     </a>
    </div>
      
    <div class="col-md-6 no-padding-left-right">
     <a href="p-list.php?type=diamond&subtype=pendants">
      <img src="images/home/categories/pendants.png" alt="Diamond Pendants" title="Diamond Pendants">
     </a>
    </div>
   </div>

 </div>
    
 </div>

 <div class="row">
    
  <div class="col-md-6 no-padding-right">
     
   <div class="row">
    <div class="col-md-6 no-padding-left-right">
     <a href="p-list.php?type=diamond&subtype=rings">
      <img src="images/home/categories/bangles.png" alt="Diamond Rings" title="Diamond Rings">
     </a>
    </div>
      
    <div class="col-md-6 no-padding-left-right">
     <a href="p-list.php?type=diamond&subtype=nosepins">
      <img src="images/home/categories/nosepins.png" alt="Diamond Nosepins" title="Diamond Nosepins">
     </a>
    </div>
   </div>

  </div>

  <div class="col-md-6 no-padding-left">
    <a href="p-search.php?text=summer-collection">
        <img src="images/home/categories/summer collection.png" alt="Summer Collection" title="Summer Collection">
    </a>
  </div>


 </div>
               
 <div class="row padding-bottom-20px">
 <div class="col-md-4 vivocarat-heading">
      FEATURED PRODUCTS
 </div>

 <div class="col-md-7 categoryGrad no-padding-left-right margin-top-68px">

 </div>
                        
 <div class="col-md-1 controls pull-right featured-products-arrow-structure">

  <a data-ng-init="look1='images/home/icons/home icons.png'" data-ng-mouseout="look1='images/home/icons/home icons.png'" data-ng-mouseover="look1='images/home/icons/home icons.png'" class="left carousel-control featured-products-arrow-background" data-target="#carousel-example-feat" data-slide="prev">
   <div class="left-arrow" data-ng-src="images/home/icons/home icons.png" src="images/home/icons/home icons.png"></div>
  </a>

  <a data-ng-init="look2='images/home/icons/home icons.png'" data-ng-mouseout="look2='images/home/icons/home icons.png'" data-ng-mouseover="look2='images/home/icons/home icons.png'" class="right carousel-control featured-products-arrow-background" data-target="#carousel-example-feat" data-slide="next">
   <div class="right-arrow featured-products-right-arrow-position" data-ng-src="images/home/icons/home icons.png" src="images/home/icons/home icons.png"></div>
  </a>
 </div>
                        
</div>

 <vivo-featured-product-carousel></vivo-featured-product-carousel>    
    
 <div class="row padding-bottom-20px">
  <div class="col-lg-3 vivocarat-heading">
      OUR CATALOGUE
  </div>

  <div class="col-lg-9 categoryGrad no-padding-left-right margin-top-68px">

  </div>
 </div>

 <div class="row">
  <div class="col-lg-6 no-padding-right">
   <img src="images/home/catalogue/engagement.jpg" alt="VivoCarat catalogue">
  </div>

  <div class="col-lg-6 no-padding-left">

   <div class="row">
   <div class="col-lg-6 no-padding-left-right">
    <a href="p-list.php?type=Gold&subtype=All">
     <img src="images/home/catalogue/gold.jpg" alt="Gold catalogue">
    </a>
   </div>

   <div class="col-lg-6 no-padding-left-right">
    <a href="p-list.php?type=Diamond&subtype=All">
     <img src="images/home/catalogue/diamond.jpg" alt="Diamond catalogue">
    </a>
   </div>
   </div>

  </div>

 </div>


<div class="row padding-bottom-20px">
 <div class="col-lg-3 vivocarat-heading">
          LOOKBOOK
 </div>

 <div class="col-lg-9 categoryGrad no-padding-left-right margin-top-68px">

 </div>
</div>

</div>
                 
 
 <!--End of container div-->

 <div class="row">
  <div class="col-md-12 lookbook-structure">
             
   <div class="container">
    <div class="row">          
     <div class="col-md-4">
      <p class="normal-text margin-none padding-bottom-30px">
       Stay tuned to stay updated! Treat us as your personal stylist and let us take you to the world of fad and fancy.
      </p>
      
      <a data-ng-href="p-lookbookArticle.php?id={{title[0].id}}">
       <div class="row" style="border:1px solid #d5d5d5;">
        <div class="col-md-12 no-padding-left-right">
         <p class="title-background">
          {{title[0].title}}
         </p>
        </div>
       </div>

       <div class="row" style="border:1px solid #d5d5d5;border-top: transparent;">
        <div class="col-md-12 no-padding-left-right">
         <img data-ng-src="images/lookbook/blogs/{{title[0].banner_img}}.jpg" alt="{{title[0].banner_img}}">
        </div>
       </div>
      </a>     

      <div class="row lookbook-footer">
       <div class="col-md-8">
          {{title[0].category}}
       </div>

       <div data-ng-bind="formatDate(date) |  date:'dd MMMM'" class="col-md-4 text-right">
          {{title[0].created_at}}
       </div>
      </div>
     </div>   
            
     <div class="col-md-4">
      <a data-ng-href="p-lookbookArticle.php?id={{title[1].id}}">            
       <div class="row" style="border:1px solid #d5d5d5;">
        <div class="col-md-12 no-padding-left-right">  
         <p class="title-background">
          {{title[1].title}}
         </p>
        </div>
       </div>

       <div class="row" style="border:1px solid #d5d5d5;border-top: transparent;">
        <div class="col-md-12 no-padding-left-right">
         <img data-ng-src="images/lookbook/blogs/{{title[1].banner_img}}.jpg" alt="{{title[1].banner_img}}">
        </div>
       </div>
      </a>

      <div class="row lookbook-footer">
       <div class="col-md-8">
          {{title[1].category}}
       </div>

       <div data-ng-bind="formatDate(date) |  date:'dd MMMM'" class="col-md-4 text-right">
         {{title[1].created_at}}
       </div>
      </div>
                
      <div class="row">
       <div class="col-md-12 padding-top-30px text-center">
        <a class="btn browse-more-button" href="p-lookbook.php"> 
         Browse more styles 
        </a>
       </div>
      </div>
     </div>
               
     <div class="col-md-4">
      <p class="normal-text margin-none padding-bottom-30px">
       Our lookbook features trendy posts to enlighten you with the latest who’s who of fashion, jewellery, celebrity style and much more.
      </p>
    
      <a data-ng-href="p-lookbookArticle.php?id={{title[2].id}}">      
       <div class="row" style="border:1px solid #d5d5d5;">
        <div class="col-md-12 no-padding-left-right">
         <p class="title-background">
          {{title[2].title}}
        </p>
       </div>
      </div>

      <div class="row" style="border:1px solid #d5d5d5;border-top: transparent;">
       <div class="col-md-12 no-padding-left-right">
        <img data-ng-src="images/lookbook/blogs/{{title[2].banner_img}}.jpg" alt="{{title[2].banner_img}}">
       </div>
      </div>
     </a> 
               
     <div class="row lookbook-footer">
      <div class="col-md-8">
         {{title[2].category}}
      </div>

      <div data-ng-bind="formatDate(date) |  date:'dd MMMM'" class="col-md-4 text-right">
         {{title[2].created_at}}
      </div>
     </div>
    </div>                 
   </div>
  </div>
   <!--END of container div-->
             
  </div>
 </div>

 <div class="container">
            
 <div class="row padding-bottom-20px">
  <div class="col-md-3 vivocarat-heading">
      TESTIMONIAL
  </div>

  <div class="col-md-9 categoryGrad no-padding-left-right margin-top-68px">

  </div>
 </div>
                        
 <div class="row testimonial-side-space">           
  <div class="col-md-4">
              
   <div class="row">
    <div class="col-md-12 text-center testimonial-structure-1">
     <img class="img-circle testimonial-image-structure" src="images/home/testimonial/benson.png" alt="attestant Benson">
             
     <p class="testimonial-text">
      Beautiful collection of jewellery. 
      The delivery packaging is the best amongst all ecomm sites.
      One happy customer.
     </p> 
                
    </div>
   </div>
              
  </div>
             
  <div class="col-md-4">
              
   <div class="row">
    <div class="col-md-12 text-center testimonial-structure-2">
     <img class="img-circle testimonial-image-structure" src="images/home/testimonial/ajinkya.png" alt="attestant Ajinkya">
             
     <p class="testimonial-text">
      Had to get an anniversary gift for my wife.
      VivoCarat delivered the necklace in 5 days. 
      Earlier than what was promised. Strongly recommend it.
     </p> 
                
    </div>
   </div>
              
  </div>
             
  <div class="col-md-4">
              
   <div class="row">
    <div class="col-md-12 text-center testimonial-structure-3">
     <img class="img-circle testimonial-image-structure" src="images/home/testimonial/sarika.png" alt="attestant Sarika">
             
     <p class="testimonial-text">
      VivoCarat sells the best jewellery available online. 
      Smooth ordering and express delivery. Loved it.
     </p>  
                
    </div>
   </div>
              
   </div>
             
 </div>
 
 <div class="row padding-bottom-20px">
  <div class="col-md-4 vivocarat-heading">
      OUR COMMITMENT
  </div>

  <div class="col-md-8 categoryGrad no-padding-left-right margin-top-68px">

  </div>
 </div>

 <div class="row normal-text our-commitment-structure">
  <div class="col-md-3 text-center no-padding-left-right" data-ng-init="abouthover1='images/home/our commitment/our commitment.png'" data-ng-mouseout="abouthover1='images/home/our commitment/our commitment.png'" data-ng-mouseover="abouthover1='images/home/our commitment/our commitment.png'">
   <a href="p-about.php">
    <div class="trusted-jewellers" data-ng-src="{{abouthover1}}" title="Trusted Jewellers"></div>
   </a>
   <p class="our-commitment-text">Trusted Jewellers</p>
  </div>
                
  <div class="col-md-3 text-center no-padding-left-right" data-ng-init="abouthover2='images/home/our commitment/our commitment.png'" data-ng-mouseout="abouthover2='images/home/our commitment/our commitment.png'" data-ng-mouseover="abouthover2='images/home/our commitment/our commitment.png'">
   <a href="p-about.php">
    <div class="commitment-certified" data-ng-src="{{abouthover2}}" title="Certified & Hallmarked"></div>
   </a>
   <p class="our-commitment-text">Certified &amp; Hallmarked</p>
  </div>
                
  <div class="col-md-3 text-center no-padding-left-right" data-ng-init="abouthover3='images/home/our commitment/our commitment.png'" data-ng-mouseout="abouthover3='images/home/our commitment/our commitment.png'" data-ng-mouseover="abouthover3='images/home/our commitment/our commitment.png'">
   <a href="p-about.php">
    <div class="commitment-free-shipping" data-ng-src="{{abouthover3}}" title="Free Shipping"></div>
   </a>
   <p class="our-commitment-text">Free Shipping</p>
  </div>
                
  <div class="col-md-3 text-center no-padding-left-right" data-ng-init="abouthover4='images/home/our commitment/our commitment.png'" data-ng-mouseout="abouthover4='images/home/our commitment/our commitment.png'" data-ng-mouseover="abouthover4='images/home/our commitment/our commitment.png'">
   <a href="p-about.php">
    <div class="commitment-easy-return" data-ng-src="{{abouthover4}}" title="Easy Return Policy"></div>
   </a>
   <p class="our-commitment-text">Easy Return Policy</p>
  </div>               
 </div>

 <div class="row padding-bottom-20px">
  <div class="col-md-4 vivocarat-heading">
      VIVOCARAT IN NEWS
  </div>

  <div class="col-md-8 categoryGrad no-padding-left-right margin-top-68px">

  </div>
 </div>
            
 </div>
 <!--End of container-->
        
 <div class="row media-background-colour">
  <div class="container">
    
   <ul class="media-ul">
    <li class="media-li">         
     <a href="http://bwdisrupt.businessworld.in/article/VivoCarat-com-an-Online-Jewellery-Marketplace-Secures-50K-in-Seed-Funding/20-12-2016-110077/" target="_blank" rel="noopener noreferrer">
      <img src="images/home/media/business-standard-logo.png" alt="business-standard-logo">
     </a>
    </li>
             
    <li class="media-li">  
     <a href="http://www.dealstreetasia.com/stories/india-dealbook-vivocarat-tiyo-events-high-raise-funding-60935/" target="_blank" rel="noopener noreferrer">
      <img src="images/home/media/deal-street-asia-logo.png" alt="deal-street-asia-logo">
     </a>
    </li>
             
    <li class="media-li">  
    <a href="http://techcircle.vccircle.com/2016/12/19/online-jewellery-marketplace-vivocarat-raises-seed-funding/" target="_blank" rel="noopener noreferrer">
     <img src="images/home/media/vcccircle logo.png" alt="vcccircle logo">
    </a>
    </li>
             
    <li class="media-li">
     <a href="http://techstory.in/jewellery-vivocarat-funding-1912/" target="_blank" rel="noopener noreferrer">
      <img src="images/home/media/techstory logo.png" alt="techstory logo">
     </a>
    </li>
             
    <li class="media-li">
     <a href="http://economictimes.indiatimes.com/small-biz/money/vivocarat-com-raises-50000-in-seed-funding/articleshow/56061909.cms" target="_blank" rel="noopener noreferrer">
      <img src="images/home/media/ettech-logo.png" alt="ettech-logo">
     </a>
    </li>
             
    <li class="media-li">  
     <a href="http://www.moneycontrol.com/news/sme/online-jewellery-store-vivocaratcom-raises-3650kseed-funding_8135561.html" target="_blank" rel="noopener noreferrer">
      <img src="images/home/media/money-control-logo.png" alt="money-control-logo">
     </a>
    </li>
             
    <li class="media-li">
     <a href="https://www.techinasia.com/4-rising-startups-in-india-dec-19-2016" target="_blank" rel="noopener noreferrer">
      <img src="images/home/media/techinasia-logo.png" alt="techinasia-logo">
     </a>
    </li>
   </ul>
             
  </div>
  <!--End of container div-->
 </div>
        
 <div class="container">
    
 <div class="row padding-bottom-20px">
  <div class="col-md-3 vivocarat-heading">
      FOLLOW US ON
  </div>

  <div class="col-md-9 categoryGrad no-padding-left-right margin-top-68px">

  </div>
 </div>
            
 <div class="row padding-bottom-30px" style="padding-left:40px;">
             
  <div class="col-md-2 text-center">
   <a href="https://twitter.com/vivocarat" title="Twitter" target="_blank" rel="noopener noreferrer">
    <div class='twitter'></div>
   </a>   
  </div>
             
  <div class="col-md-2 text-center">
   <a href="https://www.facebook.com/VivoCarat/" title="Facebook" target="_blank" rel="noopener noreferrer">
    <div class='facebook'></div> 
   </a>   
  </div>
             
  <div class="col-md-2 text-center">
   <a href="https://www.pinterest.com/vivocarat/" title="Pinterest" target="_blank" rel="noopener noreferrer">
    <div class='pinterest'></div>
   </a>   
  </div>
             
  <div class="col-md-2 text-center">
   <a href="https://www.instagram.com/vivocarat/" title="Instagram" target="_blank" rel="noopener noreferrer">
    <div class='insta'></div> 
   </a>   
  </div>
             
  <div class="col-md-2 text-center">
   <a href="https://plus.google.com/+vivocarat" title="Google +" target="_blank" rel="noopener noreferrer">
    <div class='googleplus'></div>  
   </a>   
  </div>
             
  <div class="col-md-2 text-center">
   <a href="https://www.linkedin.com/company/vivocarat" title="Linkedin" target="_blank" rel="noopener noreferrer">
    <div class='linkedin'></div>
   </a>   
  </div>
 
 </div>
    
 </div>
 <!--End of container-->

</div>
    
<vivo-footer></vivo-footer>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<!--Script for making the image scroller clickable for navigating to Jeweller's page-->    
<script>
 $(document).ready(function(){
    $("#scroller").simplyScroll();
        
  $('#incocu').click(function () {
    window.location.href = "p-store.php?store=Incocu Jewellers"
   });
        
  $('#glitter').click(function () {
    window.location.href = "p-store.php?store=Glitter Jewels"
  });

  $('#kundan').click(function () {
    window.location.href = "p-store.php?store=Kundan Jewellers"
  });

  $('#lagu').click(function () {
    window.location.href = "p-store.php?store=Lagu Bandhu"
  });

//  $('#mani').click(function () {
//    window.location.href = "p-store.php?store=Mani Jewels"
//  });

  $('#mayura').click(function () {
    window.location.href = "p-store.php?store=Mayura Jewellers"
  });

  $('#megha').click(function () {
    window.location.href = "p-store.php?store=Megha Jewellers"
  });

  $('#shankaram').click(function () {
    window.location.href = "p-store.php?store=Shankaram Jewellers"
  });
        
  $('#arkina').click(function () {
    window.location.href = "p-store.php?store=Arkina-Diamonds"
  });
        
  $('#ornomart').click(function () {
    window.location.href = "p-store.php?store=OrnoMart"
  });
        
  $('#regaalia').click(function () {
    window.location.href = "p-store.php?store=Regaalia Jewels"
  });
        
  $('#pp').click(function () {
    window.location.href = "p-store.php?store=PP-Gold"
  });
        
  $('#karatcraft').click(function () {
    window.location.href = "p-store.php?store=KaratCraft"
  });
        
  $('#zkd').click(function () {
    window.location.href = "p-store.php?store=ZKD-Jewels"
  });
        
  $('#iskiuski').click(function () {
    window.location.href = "p-store.php?store=IskiUski"
  });
        
  $('#myrah').click(function () {
    window.location.href = "p-store.php?store=Myrah-Silver-Works"
  });
        
  $('#charu').click(function () {
    window.location.href = "p-store.php?store=Charu-Jewels"
  });

    });
	
</script> 

<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoHome.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script> 

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');
</script>

 </body>
</html>