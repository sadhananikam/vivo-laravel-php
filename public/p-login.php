<?php
$desk = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if(isset($_GET['id']) && $_GET['id'] != '')
{
    $id = $_GET['id'];
    $mob = 'https://' . $_SERVER['HTTP_HOST'] . "/m-index.html#/login/".$id;
}
?>
<!DOCTYPE html>
<html lang="en" data-ng-app="vivoLogin">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="<?php echo $desk; ?>">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo $mob; ?>">
    <link rel="alternate" media="handheld" href="<?php echo $mob; ?>" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/login/"+getUrlParameter('id');
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body ng-cloak>

    <div id="fb-root"></div>
    
    <style>
    .register_account form input[type="numeric"]:hover,
    .register_account form select:hover {
        box-shadow: 0 0 4px #aaa;
        -webkit-box-shadow: 0 0 4px #aaa;
        -moz-box-shadow: 0 0 4px #aaa;
        -o-box-shadow: 0 0 4px #aaa;
    }
    
    .register_account form input[type="text"]:invalid {
        color: red !important;
    }
    
    .register_account form input[type="text"]:valid {
        color: green !important;
    }
    
    .register_account form input[type="email"]:invalid {
        color: red !important;
    }
    
    .register_account form input[type="email"]:valid {
        color: green !important;
    }
    
    .register_account form input[type="numeric"]:invalid {
        color: red !important;
    }
    
    .register_account form input[type="numeric"]:valid {
        color: green !important;
    }
    /*CSS for materializing input fields*/
    
    * {
        box-sizing: border-box;
    }
    /* form starting stylings ------------------------------- */
    
    .group {
        position: relative;
    }
    
    input {
        font-family: 'leela';
        font-size: 18px;
        padding: 10px 10px 10px 5px;
        display: block;
        width: 300px;
        border: none;
        border-bottom: 1px solid #888888;
        /*For Mozilla Gecko browser rendering compatibility,make box shadow nonw*/
        box-shadow: none;
    }
    
    input:focus {
        outline: none;
    }
    /* LABEL ======================================= */
    
    label {
        color: #797979 !important;
        font-size: 15px;
        font-weight: normal;
        font-family: 'leela';
        position: absolute;
        pointer-events: none;
        left: 5px;
        top: -10px;
        transition: 0.2s ease all;
        -moz-transition: 0.2s ease all;
        -webkit-transition: 0.2s ease all;
    }
    /* active state */
    
    input:focus ~ label,
    input:valid ~ label {
        top: -20px;
        font-size: 14px;
        color: #E62739;
    }
    /* BOTTOM BARS ================================= */
    
    .bar {
        position: relative;
        display: block;
        width: 100%;
    }
    
    .bar:before,
    .bar:after {
        content: '';
        height: 2px;
        width: 0;
        bottom: 1px;
        position: absolute;
        background: #E62739;
        transition: 0.2s ease all;
        -moz-transition: 0.2s ease all;
        -webkit-transition: 0.2s ease all;
    }
    
    .bar:before {
        left: 50%;
    }
    
    .bar:after {
        right: 50%;
    }
    /* active state */
    
    input:focus ~ .bar:before,
    input:focus ~ .bar:after {
        width: 50%;
    }
    /* HIGHLIGHTER ================================== */
    
    .highlight {
        position: absolute;
        height: 60%;
        width: 100px;
        top: 25%;
        left: 0;
        pointer-events: none;
        opacity: 0.5;
    }
    /* active state */
    
    input:focus ~ .highlight {
        -webkit-animation: inputHighlighter 0.3s ease;
        -moz-animation: inputHighlighter 0.3s ease;
        animation: inputHighlighter 0.3s ease;
    }
    /* ANIMATIONS ================ */
    
    @-webkit-keyframes inputHighlighter {
        from {
            background: #E62739;
        }
        to {
            width: 0;
            background: transparent;
        }
    }
    
    @-moz-keyframes inputHighlighter {
        from {
            background: #E62739;
        }
        to {
            width: 0;
            background: transparent;
        }
    }
    
    @keyframes inputHighlighter {
        from {
            background: #E62739;
        }
        to {
            width: 0;
            background: transparent;
        }
    }
    /*END of Material CSS design*/
</style>


<vivo-header></vivo-header>

<div data-ng-controller='loginCtrl'>

<div class="container">
<div id="fb-root"></div>

<div class="row padding-top-20px">
 <div class="col-sm-6" style="border-right:1px solid #eee;">
  <div class="vivo-login-panel">
    <h4 class="text-center gothbody" style="font-size:15px;">New Customer</h4>
    <div class="register_account">
     <div class="wrap">
      <form name="form">
        <div class="row text-center">
         <div class="col-lg-12 placeholdlight padding-2px">

          <div class="group bottom-margin-15px-padding-10px">
            <input type="text" data-ng-model="customer_new.name" required>
            <span class="highlight"></span>
            <span class="bar"></span>
            <label>Name
             <sup class="required-star">*</sup>
            </label>
          </div>

          <div class="group bottom-margin-15px-padding-10px">
            <input type="text" data-ng-model="customer_new.phone" maxlength="13" pattern="(\+?\d[- .]*){7,13}" required>
            <span class="highlight"></span>
            <span class="bar"></span>
            <label>Phone Number
             <sup class="required-star">*</sup>
            </label>
          </div>

          <div class="group bottom-margin-15px-padding-10px">
            <input type="email" data-ng-model="customer_new.email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
            <span class="highlight"></span>
            <span class="bar"></span>
            <label>Email
             <sup class="required-star">*</sup>
            </label>
          </div>

          <div class="group margin-bottom-15px">
            <input type="password" data-ng-class="isPasswordError?'error-text':'correct-text'" name="password" data-ng-model="customer_new.password" required>{{message}}
            <span class="highlight"></span>
            <span class="bar"></span>
            <label>Password
             <sup class="required-star">*</sup>
            </label>
          </div>

          <div class="row normal-text grey-colour" align="left" style="font-size:15px;">
            By clicking 'Create Account' you agree to the <a class="underline grey-colour" href="p-terms.php" target="_blank">Terms &amp; Conditions</a>.
          </div>
         </div>
        </div>
      </form>
    <div class="padding-left-right-48px">
     <input class="btn create-account-button" type="submit" name="Submit" data-ng-click="signup()" value="Create an Account">
    </div>
  </div>
 </div>
</div>
 </div>
 <div class="col-sm-6" style="padding-bottom: 30px;">
  <div class="vivo-login-panel">
   <h4 class="text-center gothbody" style="font-size:15px;">Registered Customers</h4>
    <div class="register_account">
     <div class="wrap">
      <form>
       <div class="row text-center">
        <div class="col-lg-12 placeholdlight padding-2px">

         <div class="group bottom-margin-15px-padding-10px">
            <input id="modlgn_username" data-ng-model="customer.email" type="text" name="email" size="18" autocomplete="off" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
            <span class="highlight"></span>
            <span class="bar"></span>
            <label>Email
             <sup class="required-star">*</sup>
            </label>
         </div>

         <div class="group margin-bottom-15px">
            <input type="password" id="modlgn_passwd" name="password" data-ng-model="customer.password" required>
            <span class="highlight"></span>
            <span class="bar"></span>
            <label>Password
             <sup class="required-star">*</sup>
            </label>
         </div>
            
        <div class="underline" align="left">
            <a class="normal-text grey-colour" data-toggle="modal" data-target="#myModal" style="font-size:15px;">Forgot Your Password ? </a>
        </div>
       </div>
      </div>
      </form>
         
      <div class="padding-left-right-48px">
       <input type="submit" name="Submit" class="btn create-account-button" data-ng-click="loginUser(customer)" value="Login">
      </div>

      <div class="row">
       <p class="text-center or">OR</p>
      </div>

      <div class="row">
         <div ng-click="authenticate('facebook')" class="col-lg-6 no-padding-left">
          <div class="btn facebook-login-button row">
           <div class="col-lg-8 login-with-text">
              Login with
              </div> 

              <div class="col-lg-4 no-padding-left-right">
              <img src="images/login/icons/facebook login.png" alt="Facebook login">
              </div>

          </div>
        </div>

        <div ng-click="authenticate('google')" class="col-lg-6" style="padding-right: 0px;">
         <div class="btn google-plus-login-button row">
             <div class="col-lg-8 login-with-text">
              Login with
             </div>

             <div class="col-lg-4 no-padding-left-right">
              <img src="images/login/icons/google login.png" alt="Google login">
             </div>

          </div>
         </div>
    </div>

    <fb:login-button scope="public_profile,email" show-faces="true" max-rows="1" size="large"></fb:login-button>
  </div>
 </div>
</div>
</div>

 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="font-family: 'leela';">Enter your email</h4>
    </div>
    <div class="modal-body">
        <input type="email" data-ng-model="femail" required>
    </div>
    <div class="modal-footer">
        <a data-ng-click="forgotPassword()" class="btn btn-vivo btn-sm" style="font-family: 'leela';"> Forgot Password</a>
        <a class="btn btn-default btn-sx" data-dismiss="modal" style="font-family: 'leela';">Cancel</a>
    </div>
   </div>
  </div>
 </div>
    
 </div>
</div>
    
</div>
<vivo-footer></vivo-footer>
    
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoLogin.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>    

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>    