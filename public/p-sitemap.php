<!DOCTYPE html>
<html lang="en" data-ng-app="vivoSitemap">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-sitemap.php">

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body ng-cloak>


<vivo-header></vivo-header>

<div data-ng-controller='sitemapCtrl'>

<div class="container padding-bottom-60px">
   
<div class="row padding-bottom-20px">
  <div class="col-lg-2 vivocarat-heading">
       SITEMAP
  </div>

  <div class="col-lg-10 categoryGrad no-padding-left-right margin-top-68px">

  </div>
 </div>
    

<div class="row">
    <div class="col-lg-4">
        <h5 class="sitemap-heading" style="width: 155px;">GOLD JEWELLERY</h5>
        <ul class="normal-text">
            <li class="bottom-list-space"><a href="p-list.php?type=gold&subtype=rings">Gold Rings</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=gold&subtype=earrings">Gold Earrings</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=gold&subtype=pendants">Gold Pendants</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=gold&subtype=nosepins">Gold Nose Pins</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=gold&subtype=bangles">Gold Bangles</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=gold&subtype=bracelets">Gold Bracelets</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=gold&subtype=necklaces">Gold Necklaces</a></li>
            <li><a href="p-list.php?type=Gold&subtype=All">All Gold Jewelleries</a></li>

        </ul>
    </div>
    <div class="col-lg-4">
        <h5 class="sitemap-heading" style="width: 188px;">
            DIAMOND JEWELLERY
        </h5>
        <ul class="normal-text">
            <li class="bottom-list-space"><a href="p-list.php?type=diamond&subtype=rings">Diamond Rings</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=diamond&subtype=earrings">Diamond Earrings</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=diamond&subtype=pendants">Diamond Pendants</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=diamond&subtype=nosepins">Diamond Nose Pins</a></li>
            <li><a href="p-list.php?type=Diamond&subtype=All">All Diamond Jewelleries</a></li>
        </ul>
    </div>
    <div class="col-lg-4">
        <h5 class="sitemap-heading" style="width: 163px;">SILVER JEWELLERY</h5>
        <ul class="normal-text">
            <li class="bottom-list-space"><a href="p-list.php?type=silver&subtype=rings">Silver Rings</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=silver&subtype=earrings">Silver Earrings</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=silver&subtype=pendants">Silver Pendants</a></li>
            <li class="bottom-list-space"><a href="p-list.php?type=silver&subtype=accessories">Silver Accessories</a></li>
            <li><a href="p-list.php?type=Silver&subtype=All">All Silver Jewelleries</a></li>

        </ul>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <h5 class="sitemap-heading" style="width: 71px;">BRANDS</h5>
        <ul class="normal-text">
            <li class="bottom-list-space"><a href="p-brands.php">Brands</a></li>
            <li class="bottom-list-space"><a href="p-store.php?store=Kundan Jewellers">Kundan Jewellers</a></li>
            <li class="bottom-list-space"><a href="p-store.php?store=Lagu Bandhu">Lagu Bandhu</a></li>
            <li class="bottom-list-space"><a href="p-store.php?store=Megha Jewellers">Megha Jewellers</a></li>
            <li class="bottom-list-space"><a href="p-store.php?store=Mayura Jewellers">Mayura Jewellers</a></li>
            <li class="bottom-list-space"><a href="p-store.php?store=Mani Jewellers">Mani Jewellers</a></li>
            <li class="bottom-list-space"><a href="p-store.php?store=Shankaram Jewellers">Shankaram Jewellers</a></li>
            <li><a href="p-store.php?store=Incocu Jewellers">Incocu Jewellers</a></li>
            <li><a href="p-store.php?store=Glitter Jewels">Glitter Jewels</a></li>
        </ul>
    </div>

    <div class="col-lg-4">
        <h5 class="sitemap-heading" style="width: 143px;">MISCELLANEOUS</h5>
        <ul class="normal-text">
            <li class="bottom-list-space"><a href="p-about.php">About Us</a></li>
            <li class="bottom-list-space"><a href="p-faq.php">FAQ</a></li>
            <li class="bottom-list-space"><a href="p-terms.php">Terms Of Use</a></li>
            <li class="bottom-list-space"><a href="p-privacypolicy.php">Privacy Policy</a></li>
            <li class="bottom-list-space"><a href="p-returns.php">Return Policy</a></li>
            <li class="bottom-list-space"><a href="p-lookbook.php">Look Book</a></li>
            <li class="bottom-list-space"><a href="p-jewelleryeducation.php">Jewellery Education</a></li>
            <li class="bottom-list-space"><a href="p-moissanite.php">Moissanite</a></li>
            <li><a href="p-jewelleryguide.php">Jewellery Guide</a></li>
        </ul>
    </div>

</div>

</div>

</div>
<vivo-footer></vivo-footer>
    
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoSitemap.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>        