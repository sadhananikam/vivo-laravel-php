<!DOCTYPE html>
<html lang="en" data-ng-app="vivoTerms">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-terms.php">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/terms">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/terms" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/terms";
        }
    </script>
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body ng-cloak>

<vivo-header></vivo-header>

<div data-ng-controller='termsCtrl'>
<div class="container padding-bottom-60px">

<div class="col-md-12">
 <div class="row padding-bottom-20px">
  <div class="col-md-4 vivocarat-heading">
       TERMS &amp; CONDITIONS
  </div>

  <div class="col-md-8 categoryGrad no-padding-left-right margin-top-68px">

  </div>
 </div>
        

        
<p class="paragraph-text text-justify">
   This document is an electronic record in terms of Information Technology Act, 2000 (“IT Act, 2000”), the applicable rules thereunder and the provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures.
</p>
        
<p class="paragraph-text text-justify">
   This document is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries Guidelines) Rules, 2011 that require publishing the rules and regulations, privacy policy and Terms of Use for access to or usage of www.vivocarat.com website.
</p>
        
<p class="paragraph-text text-justify">
   The domain name www.vivocarat.com (hereinafter referred to as 'Website') is owned by VivoCarat Retail Private Limited (hereinafter referred to as 'VivoCarat'), a company incorporated under the Companies Act, 2013, with its registered office at 44, Awas, Plot -2, Sec -3, Kandivali, Mumbai – 400067 India.
</p>
        
<p class="paragraph-text text-justify">
   These terms and conditions apply to VivoCarat website located at www.VivoCarat.com, and all associated products and apps/websites linked to VivoCarat. Please read these terms and conditions (the 'Terms and Conditions') carefully. BY USING THE SITE, YOU AGREE TO BE BOUND BY THESE TERMS AND CONDITIONS. These Terms and Conditions govern your use of, and any purchase from, the VivoCarat Site, and constitute an agreement between you and VivoCarat. VivoCarat reserves the right to change or modify any of these Terms and Conditions or any policy or guideline of the Site at any time, and in its sole discretion. Any change or modification will be effective immediately upon posting of the revisions on the Site. Your continued use of the Site following the posting of its changes or modifications will constitute your acceptance of such changes or modifications. Therefore, you should frequently review these Terms and Conditions and any other applicable policies from time-to-time to understand the terms and conditions that apply to your use of the Site. If you do not agree to the amended terms, you must stop using the Site.
</p>
        
<h4 class="privacy-heading">
    Product Availability and Pricing
</h4>
        
<p class="paragraph-text text-justify">
    If you're interested in any product that is currently on back order, contact us and we can tell you when the item will be back in stock. Sometimes with the volume of orders we receive, an item may go out of stock before we are able to post a notification on the Site. If this happens, we will contact you directly to discuss possible options. Data, including prices, may be inaccurately displayed on our Site due to system or typographical errors. While we make every attempt to avoid these errors, they may occur. We reserve the right to correct any and all errors when they do occur, and we do not honor inaccurate or erroneous prices. If a product's listed price is lower than its actual price, we will, at our discretion, either contact you for instructions before shipping the product or cancel the order and notify you of such cancellation. If the order has been shipped, you agree to either return the product or pay the difference between the actual and charged prices. Our prices are also subject to change without notice. We apologize for any inconvenience that this may cause. If you have any questions, please do not hesitate to contact our customer services department at hello@vivocarat.com. We do not negotiate prices on our products and all our prices are final.
</p>
        
<h4 class="privacy-heading">
    Information on our Site
</h4>
        
<p class="paragraph-text text-justify">
    At VivoCarat, we make every attempt to ensure that our online catalogue is as accurate and complete as possible. In order to give you the opportunity to view our products in great detail, some products may appear larger or smaller than their actual size in our photographs; and since every computer monitor is set differently, color and size may vary slightly. Our objective is to provide you with as much information and detail about your prospective purchase as possible so that you can see the beauty and shape of a particular item. In compliance with industry standards VivoCarat states that carat total weight in all purchases may vary from stated weight. On the Site, we provide the measurement of our products based on our manufacturing specifications. Slight tolerances may be accounted for based on finishing during the manufacturing. For diamond jewelry set with multiple diamonds, we provide the minimum total carat weight for the piece. 
</p>
        
<h4 class="privacy-heading">
    Privacy Policy
</h4>
        
<p class="paragraph-text text-justify">
   Please refer to our Privacy Policy for information on how VivoCarat collects, uses, and discloses personally identifiable information from its customers.
</p>
        
<h4 class="privacy-heading">
     Site Content
</h4>
        
<p class="paragraph-text text-justify">
     The Site and all content and other materials, including, without limitation, the VivoCarat logo, and all designs, text, graphics, pictures, selection, coordination, information, data, software, sound files, other files and the selection and arrangement thereof (collectively, the 'Site Materials') are the proprietary property of VivoCarat or its licensors or users and are protected by trade dress, copyright, patent and trademark laws, and various other intellectual property rights and unfair competition laws.
</p>
        
<h4 class="privacy-heading">
     Trademarks
</h4>
        
<p class="paragraph-text text-justify">
   VivoCarat, the VivoCarat logos, and any other product or service name or slogan contained in our Site are trademarks of VivoCarat and its suppliers or licensors, and may not be copied, imitated or used, in whole or in part, without the prior written permission of VivoCarat or the applicable trademark holder. You may not use any metatags or any other 'hidden text' utilizing 'VivoCarat' or any other name, trademark or product or service name of VivoCarat without our prior written permission. All other trademarks, registered trademarks, product names and VivoCarat names or logos mentioned in our Site are the property of their respective owners.
</p>
        
<h4 class="privacy-heading">
    Use of the Site
</h4>
        
<p class="paragraph-text text-justify">
    You are granted a personal, limited, non-sublicensable license to access and use our Site and electronically copy, (except where prohibited without a license) and print to hard copy portions of our Site Materials for your informational, non-commercial and personal use only. Such license is subject to these Terms and Conditions and does not include: (a) any resale or commercial use of our Site or the Site Materials therein; (b) the collection and use of any product listings, pictures or descriptions for commercial purposes; (c) the distribution, public performance or public display of any Site Materials, (d) modifying or otherwise making any derivative uses of our Site and the Site Materials, or any portion thereof; (e) use of any automated means to access, monitor or interact with any portion of our Site, including through data mining, robots, spiders, scraping, or similar data gathering or extraction methods; (f) downloading (other than the page caching) of any portion of our Site, the Site Materials or any information contained therein, except as expressly permitted on our Site; (g) cause to appear any pop-up, pop-under, exit windows, expanding buttons, banners, advertisement, or anything else which minimizes, covers, or frames or inhibits the full display of our Site; (h) use our web sites in any way which interferes with the normal operation of our sites; or (i) any use of our Site or the Site Materials other than for its intended purpose. Any use of our Site or the Site Materials other than as specifically authorized herein, without the prior written permission of VivoCarat, is strictly prohibited and will terminate the license granted herein. Such unauthorized use may also violate applicable laws, including without limitation copyright and trademark laws and applicable communications regulations and statutes. Unless explicitly stated herein, nothing in these Terms and Conditions shall be construed as conferring any license to intellectual property rights, whether by estoppels, implication, or otherwise. This license is revocable at any time.
</p>
        
<h4 class="privacy-heading">
    Infringer Policy
</h4>
        
<p class="paragraph-text text-justify">
    In accordance with the Digital Millennium Copyright Act (DMCA) and other applicable law, VivoCarat has adopted a policy of terminating and barring, in appropriate circumstances and at VivoCarat's sole discretion, site users or account holders who are deemed to be repeat infringers. VivoCarat may also at its sole discretion limit access to this site and/or terminate the accounts of any users who infringe any intellectual property rights of others, whether or not there is any repeat infringement.
</p>
        
<h4 class="privacy-heading">
    Third Party Content
</h4>
        
<p class="paragraph-text text-justify">
    VivoCarat may provide links to Web pages and content of third parties ('Third Party Content') as a service to those interested in this information. VivoCarat does not monitor or have any control over any Third Party Content or third party Sites. VivoCarat does not endorse any Third Party Content and can make no guarantee as to its accuracy or completeness. VivoCarat does not represent or warrant the accuracy of any information contained therein, and undertakes no responsibility to update or review any Third Party Content. Users use these links and Third Party Content contained therein at their own risk.
</p>
        
<h4 class="privacy-heading">
    Submissions
</h4>
        
<p class="paragraph-text text-justify">
    You acknowledge and agree that any materials, including but not limited to questions, comments, suggestions, ideas, plans, notes, drawings, original or creative materials or other information, regarding this site, VivoCarat, or our products or services that are provided by you to VivoCarat are non-confidential and shall become the sole property of VivoCarat. VivoCarat will own exclusive rights, including all intellectual property rights, and will be entitled to the unrestricted use and dissemination of these materials for any purpose, commercial or otherwise, without acknowledgment or compensation to you. You grant VivoCarat and its affiliates and sublicensees the right to use the name that you submit in connection with such content, if they choose. You represent and warrant that (a) you own and control all of the rights to the content that you submit, or that you otherwise have the right to submit such content to this site; (b) the content is accurate and not misleading; and (c) use and posting of the content you supply will not violate any rights of or cause injury to any person or entity.
</p>
        
<h4 class="privacy-heading">
    Indemnification
</h4>
        
<p class="paragraph-text text-justify">
    You agree to defend, indemnify and hold harmless VivoCarat, its independent contractors, service providers and consultants, and their respective directors, employees and agents, from and against any claims, damages, costs, liabilities, and expenses (including, but not limited to, reasonable attorneys' fees) arising out of or related to any Content you post, store or otherwise transmit on or through our Site or your use of or inability to use our Site, including without limitation any actual or threatened suit, demand or claim made against VivoCarat and/or its independent contractors, service providers, employees, directors or consultants, arising out of or relating to the Content, your conduct, your violation of these Terms and Conditions or your violation of the rights of any third party.
</p>
        
<h4 class="privacy-heading">
    Disclaimer of Warranty
</h4>
        
<p class="paragraph-text text-justify">
    EXCEPT AS EXPRESSLY PROVIDED TO THE CONTRARY IN A WRITING BY VIVOCARAT, THIS SITE, THE CONTENT CONTAINED THEREIN AND THE PRODUCTS AND SERVICES PROVIDED ON OR IN CONNECTION THEREWITH (THE 'PRODUCTS AND SERVICES') ARE PROVIDED ON AN 'AS IS' BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. VIVOCARAT DISCLAIMS ALL OTHER WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT AS TO THE INFORMATION, CONTENT, AND MATERIALS IN OUR SITE. VIVOCARAT DOES NOT REPRESENT OR WARRANT THAT MATERIALS IN OUR SITE OR THE SERVICES ARE ACCURATE, COMPLETE, RELIABLE, CURRENT OR ERROR-FREE. VIVOCARAT DOES NOT REPRESENT OR WARRANT THAT OUR SITE OR ITS SERVERS ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.
</p>
        
<h4 class="privacy-heading">
    Limitation of Liability
</h4>
        
<p class="paragraph-text text-justify">
    IN NO EVENT SHALL VIVOCARAT, ITS DIRECTORS, MEMBERS, EMPLOYEES OR AGENTS BE LIABLE FOR ANY DIRECT, SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES, OR ANY OTHER DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO LOSS OF USE, LOSS OF PROFITS, OR LOSS OF DATA, WHETHER IN AN ACTION IN CONTRACT, TORT (INCLUDING BUT NOT LIMITED TO NEGLIGENCE) OR OTHERWISE, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF OUR SITE, THE PRODUCTS AND SERVICES, OR THE CONTENT CONTAINED IN OR ACCESSED THROUGH OUR SITE, INCLUDING WITHOUT LIMITATION ANYDAMAGES CASED BY OR RESULTING FROM RELIANCE BY USER ON ANY INFORMATION OBTAINED FROM VIVOCARAT, OR THAT RESULT FROM MISTAKES, OMISSIONS, INTERRUPTIONS, DELETION OF FILES OR EMAIL, ERRORS, DEFECTS, VIRUSES, DELAYS IN OPERATION OR TRANSMISSION OR ANY FAILURE OFPERFORMANCE, WHETHER OR NOT RESULTING FROM ACTS OF GOD, COMMUNICATIONS FAILURE, THEFT, DESTRUCTION OR UNAUTHORIZED ACCESS TO VIVOCARAT'S RECORDS, PROGRAMS OR SERVICES.
</p>
        
<h4 class="privacy-heading">
    Applicable Law and Venue
</h4>
        
<p class="paragraph-text text-justify">
    These Terms and Conditions and your use of this site will be governed by and construed in accordance with the laws &amp; exclusive jurisdiction of the courts of Mumbai, India, applicable to agreements made and to be entirely performed within the exclusive jurisdiction of the courts of Mumbai, India, without resort to its conflict of law provisions. You agree that any action at law or in equity arising out of or relating to these Terms and Conditions shall be filed only in exclusive jurisdiction of the courts of Mumbai, India and you hereby irrevocably and unconditionally consent and submit to the exclusive jurisdiction of such courts over any suit, action or proceeding arising out of your use of this site, any purchase from this site, or these Terms and Conditions.
</p>

<h4 class="privacy-heading">
    Modification and Notice
</h4>
        
<p class="paragraph-text text-justify">
    You agree that VivoCarat may modify these Terms and Conditions and any other policies on our Site at any time and that posting the modified Terms and Conditions or policies on our Site will constitute sufficient notice of such modification.
</p>
                
<h4 class="privacy-heading">
    Termination
</h4>
        
<p class="paragraph-text text-justify">
    Notwithstanding any of these Terms and Conditions, VivoCarat reserves the right, without notice and in its sole discretion, to terminate your license to use this site, and to block or prevent future your access to and use of the Site.
</p>
                
<h4 class="privacy-heading">
    Severability
</h4>
        
<p class="paragraph-text text-justify">
    If any provision of these Terms and Conditions shall be deemed unlawful, void or for any reason unenforceable, then that provision shall be deemed severable from these Terms and Conditions and shall not affect the validity and enforceability of any remaining provisions.
</p>
                
<h4 class="privacy-heading">
    Comments or Questions
</h4>
        
<p class="paragraph-text text-justify">
   If the User has any questions, comments or concerns arising from the Website, the privacy policy or any other relevant terms and conditions, policies and notices or desires to provide input on the Information, the Website, material provided by another User or the use of the Website by another User, the User may contact VivoCarat at hello@vivocarat.com
</p> 
                
</div>
</div>

</div>
    
<vivo-footer></vivo-footer>
    
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script> 

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoTerms.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!-- Start include Controller for angular -->
    
<script src="device-router.js"></script> 

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>        