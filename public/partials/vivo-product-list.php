<div class="well" data-ng-hide="isLoaded"> Loading</div>

<div class="top-box1" data-ng-mouseover="closeFilter()" data-ng-show="isLoaded">

<div data-ng-show="resultList.length<1" class="row well"> 
    No products found
</div>

<div class="container" style="padding-left: 26px;padding-right: 26px;">

<div class="row">
 <div class="col-sm-3 vivo-box-padding" data-ng-show="isGrid" data-ng-repeat="p in resultList track by $index | orderBy:predicate:reverse">

  <div class="row">
   <div class="col-sm-3 vivo-box-padding vivo-grid" style="margin-top: 3px;">
    <div class="inner_content1 clearfix" style="background: #F8F8F8;padding: 10px 10px 0px 10px;">
     <div class="tag">
      <img data-ng-if="p.ready_to_ship==1" src="images/list/icons/ready-to-ship.png" alt="ready to ship">
     </div>
    <div class="tag">
      <img data-ng-if="p.bestseller==1" src="images/list/icons/best-seller-icon.png" alt="ready to ship">
     </div>
      <a data-ng-href="p-product.php?id={{p.id}}&title={{p.slug}}">
       <img src="images/list/icons/image filler.jpg" actual-image="images/products-v2/{{p.VC_SKU}}-t.jpg" alt="{{p.title}}">
      </a>

      <div class="row margin-bottom-10px">
       <div class="col-sm-8 no-padding-left-right">

        <div class="row" data-ng-hide="p.discount==0">
         <div class="col-sm-6 grid-price-before no-padding-left">
             RS. {{p.price_before_discount | INR}}
         </div>

         <div class="col-sm-6 grid-off"> 
             {{p.discount}}% OFF
         </div>
        </div>

        <div class="row" data-ng-if="!(p.discount==0)">
         <div class="col-sm-12 grid-price-after no-padding-left-right">
             RS. {{p.price_after_discount | INR}}
         </div>
        </div>

        <div class="row" data-ng-if="p.discount==0">
         <div class="col-sm-12 grid-price-after no-padding-left-right padding-top-20px">
             RS. {{p.price_after_discount | INR}}
         </div>
        </div>

       </div>

       <div class="col-sm-4 no-padding-right" data-ng-click="addToCompare(p)">
        <div class="col-sm-6 no-padding pull-right margin-top-10px">
         <img class="pointer" src="images/list/icons/compare.png" title="Compare" alt="compare products">
        </div>
       </div>
      </div>    
    </div>   
   </div>
  </div>
     
 </div>
</div>
    
</div>
    
</div>
