<div class="hidden-background-colour">
    <div class="container hidden-header-structure">
        <div class="row">

            <div data-ng-if="!authenticated" class="col-md-8">
                <img src="images/header/icons/email.jpg" alt="email us">&nbsp;&nbsp;
                <span class="white-colour">
                    hello@vivocarat.com &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                </span>

                <img src="images/header/icons/call.jpg" alt="call us"> &nbsp;
                <span class="white-colour">
                    +91&nbsp;9167645314 &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                </span>

                <i class="fa fa-map-marker p_loc_text" style="color:#a9a9a9; font-size:15px;" aria-hidden="true"></i>
                <span class="white-colour p_loc_text">
                    &nbsp;&nbsp; Partner Locator &nbsp;&nbsp;
                    <span class="p_loc_caret">
                        <i class="fa fa-caret-down"></i>
                    </span>
                </span>
                <i class="fa fa-question-circle p_loc_text" style="color:#a9a9a9; font-size:15px;" aria-hidden="true"></i>
            </div>

            <div data-ng-if="authenticated" class="col-md-7">
                <img src="images/header/icons/email.jpg" alt="email us">&nbsp;&nbsp;
                <span class="white-colour">
        hello@vivocarat.com &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
    </span>

                <img src="images/header/icons/call.jpg" alt="call us"> &nbsp;
                <span class="white-colour">
                    +91&nbsp;9167645314
                </span>

            </div>

            <div data-ng-if="!authenticated" class="col-md-4 text-right unauthenticated-structure">
                <a class="userProfile padding-right-10px" href="p-account.php">
       My Account
    </a>

                <h class="white-colour">|</h>

                <a class="userProfile padding-left-10px" href="p-login.php?id=0" data-ng-hide="authenticated">
        Login/Register
    </a>
            </div>

            <div data-ng-if="authenticated" class="col-md-5 authenticated-structure">
                <div class="row">
                    <div class="col-md-12 text-right white-colour">
                        <a class="userProfile padding-right-10px" href="p-account.php">
        My Account 
      </a>

                        <h class="white-colour padding-right-10px padding-left-10px">|</h>

                        <a class="userProfile padding-right-10px padding-left-10px" href="p-orders.php">
        Orders
      </a>

                        <h class="white-colour">|</h>

                        <a class="userProfile hi-username">Hi, {{name}}</a>

                        <a class="userProfile padding-left-10px" data-ng-click="logoutJquery()">
          Sign Out
      </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="container" style="height: 88px;">
    <div class="row" style="margin-top: 8px;">
        <div class="col-md-2">
            <a href="default.php" title="VivoCarat Retail Pvt. Ltd.">
                <img src="images/header/logos/VivoCarat logo.png" class="padding-top-20px padding-bottom-20px" alt="VivoCarat Retail Pvt. Ltd.">
            </a>
        </div>

        <div class="col-md-8 text-center usp-header">
            Trusted Jewellers &nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;Certified &amp; Hallmarked &nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; Free Shipping &nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; Easy Return Policy
        </div>

        <div class="col-md-2 text-right">
            <a href="p-checkout.php">

                <img class="cart-image-position" src="images/header/icons/cart bag.png" alt="checkout cart">

                <div class="cart-length" data-ng-if="cart.length>0">
                    ({{cart.length}})
                </div>

                <div class="cart-length-nil" data-ng-if="cart.length<1 || !cart || !cart.length">
                    (0)
                </div>

            </a>
        </div>

    </div>
</div>

<vivo-header-menu></vivo-header-menu>

<div class="row">

    <div class="row compare-background" data-ng-if="compare.length>0">

        <div data-ng-repeat="p in compare" class="col-md-2 no-padding-left" style="padding-right:10px;">

            <div class="row">
                <div class="col-md-12 compare-product-background">

                    <span class="remove" title="remove" data-ng-click="removeFromCompare($index)"></span>
                    <a href="p-product.php?id={{p.id}}&title={{p.slug}}">
                        <img class="img-responsive" src="images/products-v2/{{p.VC_SKU}}-1.jpg" alt="{{p.title}}">

                        <div class="row white-background padding-bottom-10px">
                            <div class="col-md-12">

                                <div class="row" data-ng-hide="p.discount==0">
                                    <div class="col-md-6 grid-price-before no-padding-left">
                                        RS. {{p.price_before_discount | INR}}
                                    </div>

                                    <div class="col-md-6 grid-off">
                                        {{p.discount}}% OFF
                                    </div>
                                </div>

                                <div class="row" data-ng-if="!(p.discount==0)">
                                    <div class="col-md-12 grid-price-after no-padding-left-right">
                                        RS. {{p.price_after_discount | INR}}
                                    </div>
                                </div>

                                <div class="row" data-ng-if="p.discount==0">
                                    <div class="col-md-12 grid-price-after no-padding-left-right padding-top-20px">
                                        RS. {{p.price_after_discount | INR}}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a>

                </div>
            </div>

        </div>


        <div class="col-md-2 compare-background" data-ng-if="compare.length>0 && compare.length<4">
            <a href="p-list.php?type=All&subtype={{compare[0].category}}">
                <img src="images/product/add product.jpg" alt="add product to compare">
            </a>
        </div>

        <div class="col-md-2 pull-right">
            <div class="row">
                <div class="col-md-12 text-center" style="padding-top: 65px;">
                    <div class="text-center padding-top-20px">
                        <a class="btn btn-vivo" data-ng-click="checkCompare()" role="button" style="border-radius: 0;" align="right">COMPARE</a>
                    </div>

                    <div class="text-center padding-top-20px">
                        <a class="btn btn-vivo" data-ng-click="removeAllCompare()" role="button" style="border-radius: 0;" align="right">CLEAR ALL</a>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>


<!-- Partner Locator Dialogue -->
<div class="p_loc_dialogue">
    <div class="gradient_black_white_corner">
        <div style="background: white;">
            <div style="padding: 10px;">
                <span class="map_marker_icon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </span>
                <span class="pull-right close_p_loc">
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                </span>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="text-center">LOCATE A PARTNER STORE NEAR YOU</h3>
                    <ul class="list_based_on_fa" style="padding-left:25px;">
                        <li>
                            <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Lifetime free repair/polish for any jewellery bought on VivoCarat.
                        </li>
                        <li>
                            <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Try at Shop - try the jewellery at a partner shop before buying.
                        </li>
                        <li>
                            <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Find your proper ring/bangle/bracelet size before ordering online.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <a href="p-sellerlocate.php" class="btn browse-more-button" style="margin: 15px auto; width: 180px; display: block;">Get Started for Free</a>
            </div>
        </div>
    </div>
</div>
<div class="overlay_transparent"></div>