<style>
/*Done to adjust dropdown of jewellers within the filter box area*/
.k-animation-container{
    height: 117px !important;
}

.k-list-container{
    height: 117px !important;
}

.k-list-scroller{
    height: 117px !important;
}

.k-popup.k-list-container {
    padding-top: 0px !important;
}
</style>

<div class="row vivo-main-header padding-bottom-10px">
 <div class="col-md-12 no-padding-left-right">
  <div class="row subheader">
 <div class="col-md-6" style="padding-top: 10px;">
  <h class="head vivocarat-theme-colour subheader bold-font uppercase padding-top-20px">
     {{resultfor === "1" ? "All" : resultfor}} ({{subtype}})
  </h>
 </div>

  <div class="col-md-6 filter-header-text padding-top-10px" style="padding-bottom:8px;">
   <h class="pull-right normal-text">
     Total {{count}} items
   </h>
  </div>
 </div>

<div class="row">
 <div class="col-md-12">
  <div class="row normal-text bold-font">
   <div class="col-md-6 filter-header-text no-padding-left" style="width: 498px;">
        Home / {{resultfor === "1" ? "All" : resultfor}} - {{subtype}}
   </div>
                                            
   <div class="col-md-1 text-center filter-header-text pointer no-padding-right no-padding-left" style="font-size: 15px;border: solid 1px #EF8D96;" data-ng-mouseover="openFilter()">
        Filter by<img src="images/list/icons/Arrow.jpg" style="padding-left: 4px;" alt="filter arrow">
   </div>

   <div class="col-md-2 text-right pull-right grey-colour no-padding-right">
    <div class="filter-header-text">
         Sort By
     <select name="singleSelect" id="singleSelect" data-ng-model="selectedFilter">
      <option value="">Popularity</option>
      <option value="Price L to H">Price L to H </option>
      <option value="Price H to L">Price H to L</option>
     </select>
    </div>
   </div>

  </div>
 </div>
</div>

                                    
<div class="row">
 <div class="col-md-12" style="display:none" id="filterArea">
  <div class="row normal-text" style="margin-top:20px;">
   <div class="col-md-3 filter-header-text no-padding-left normal-text" style="padding-right: 80px;"> Price(in Rs.)
    <br/>
    <div range-slider min="0" max="200000" model-min="demo1.min" model-max="demo1.max">
       
    </div>
       
    <div class="row">
     <div class="col-md-5 no-padding-left">
      <input class="width-100-percent padding-left-5px" placeholder="Min" type="number" data-ng-model="demo1.min">
     </div>
                                                    
     <div class="col-md-2 no-padding-left-right" style="margin-top: 4px;">TO</div>
                                                    
     <div class="col-md-5 no-padding-left-right">
      <input class="width-100-percent padding-left-5px" type="number" placeholder="Max" data-ng-model="demo1.max">
     </div>
    </div>
   </div>
      
   <div class="col-md-2 normal-text no-padding-left">
        <p>Metal</p>            
               
    <div class="row">
     <div class="col-md-12 no-pad">
      <label class="filter-text">
       <input class="checkbox-position" type="checkbox" data-ng-model="isGold"> 
          Yellow Gold
      </label>
     </div>

     <div class="col-md-12 no-padding-left-right">
      <label class="filter-text">
       <input class="checkbox-position" type="checkbox" data-ng-model="isWhiteGold"> 
          White gold
      </label>                                              
     </div>

     <div class="col-md-12 no-padding-left-right">
      <label class="filter-text">                                                  <input class="checkbox-position" type="checkbox" data-ng-model="isRoseGold"> 
          Rose Gold
      </label> 
     </div>

     <div class="col-md-12 no-pad">
      <label class="filter-text">                                                  <input class="checkbox-position" type="checkbox" data-ng-model="isSilver">   
          Silver
      </label>
     </div>
    </div>

   </div>
   
   <div class="col-md-2 normal-text"> 
        <p>Metal purity</p>
                                              
    <div class="row">
     <div class="col-md-12 no-pad">
      <label class="filter-text">                                                  <input class="checkbox-position" type="checkbox" data-ng-model="is22"> 
          22 KT
      </label>                                              
     </div>

     <div class="col-md-12 no-pad">
      <label class="filter-text">                                                  <input class="checkbox-position" type="checkbox" data-ng-model="is18"> 
          18 KT
      </label>
    </div>

    <div class="col-md-12 no-pad">
     <label class="filter-text">                                                    <input class="checkbox-position" type="checkbox" data-ng-model="is14"> 
         14 KT
     </label>
    </div>

    <div class="col-md-12 no-pad">
     <label class="filter-text">
      <input class="checkbox-position" type="checkbox" data-ng-model="isOther"> 
        Other
     </label>   
    </div>
   </div>
  </div>
                                            
  <div class="col-md-2 normal-text"> 
       <p>Occasion</p>
                                                
   <div class="row">
    <div class="col-md-12 no-pad">
     <label class="filter-text">                                                    <input class="checkbox-position" type="checkbox" data-ng-model="isCasual"> 
         Casual
     </label>    
    </div>

    <div class="col-md-12 no-pad">
     <label class="filter-text">                                                    <input class="checkbox-position" type="checkbox" data-ng-model="isFashion"> 
         Fashion
     </label>                                               
    </div>

    <div class="col-md-12 no-pad">
     <label class="filter-text">
      <input class="checkbox-position" type="checkbox" data-ng-model="isBridal"> 
         Bridal
     </label> 
    </div>
   </div>
  </div>
                                            
  <div class="col-md-3 normal-text no-padding-right">

   <div class="demo-section k-content">
    <select kendo-multi-select k-options="selectOptions" k-ng-model="jewellers"></select>
   </div>

  </div>

 </div>

 <div class="row">
  <div class="col-sm-12 text-right no-padding-right">
   <a data-ng-click="resetFilter()" class="btn btn-list">
      Clear
   </a>

   <a data-ng-click="filter()" class="btn btn-list">
      Search
   </a>

   <a data-ng-click="closeFilter()">
      <img src="images/list/icons/close.png" alt="close filter">
   </a>
  </div>
 </div>
 </div>
</div> 
     
 </div>
</div>



                                    
                                
