<div id="carousel-similar" class="carousel slide" data-ride="carousel" style="position:relative;z-index:1;padding-top: 50px;">
    
<!-- Wrapper for slides -->
<div class="height carousel-inner">
 <div class="item active row">
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[0].id}}&title={{same_list[0].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{same_list[0].VC_SKU}}-t.jpg" alt="{{same_list[0].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[0].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
         <div class="col-sm-12 text-center" data-ng-if="same_list[0].category != 'Rings' && same_list[0].category != 'Bangles' && same_list[0].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[0])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[0].category == 'Rings' || same_list[0].category == 'Bangles' || same_list[0].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[0].id}}&title={{same_list[0].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div> 
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[1].id}}&title={{same_list[1].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{same_list[1].VC_SKU}}-t.jpg" alt="{{same_list[1].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[1].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[1].category != 'Rings' && same_list[1].category != 'Bangles' && same_list[1].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[1])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[1].category == 'Rings' || same_list[1].category == 'Bangles' || same_list[1].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[1].id}}&title={{same_list[1].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[2].id}}&title={{same_list[2].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{same_list[2].VC_SKU}}-t.jpg" alt="{{same_list[2].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[2].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[2].category != 'Rings' && same_list[2].category != 'Bangles' && same_list[2].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[2])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[2].category == 'Rings' || same_list[2].category == 'Bangles' || same_list[2].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[2].id}}&title={{same_list[2].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[3].id}}&title={{same_list[3].slug}}">
      
      <img data-ng-src="images/products-v2/{{same_list[3].VC_SKU}}-t.jpg" alt="{{same_list[3].title}}">
     
     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[3].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[3].category != 'Rings' && same_list[3].category != 'Bangles' && same_list[3].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[3])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[3].category == 'Rings' || same_list[3].category == 'Bangles' || same_list[3].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[3].id}}&title={{same_list[3].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
  </div>
     
 <div class="item row">
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[4].id}}&title={{same_list[4].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{same_list[4].VC_SKU}}-t.jpg" alt="{{same_list[4].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[4].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[4].category != 'Rings' && same_list[4].category != 'Bangles' && same_list[4].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[4])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[4].category == 'Rings' || same_list[4].category == 'Bangles' || same_list[4].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[4].id}}&title={{same_list[4].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[5].id}}&title={{same_list[5].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{same_list[5].VC_SKU}}-t.jpg" alt="{{same_list[5].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[5].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[5].category != 'Rings' && same_list[5].category != 'Bangles' && same_list[5].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[5])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[5].category == 'Rings' || same_list[5].category == 'Bangles' || same_list[5].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[5].id}}&title={{same_list[5].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[6].id}}&title={{same_list[6].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{same_list[6].VC_SKU}}-t.jpg" alt="{{same_list[6].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[6].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[6].category != 'Rings' && same_list[6].category != 'Bangles' && same_list[6].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[6])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[6].category == 'Rings' || same_list[6].category == 'Bangles' || same_list[6].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[6].id}}&title={{same_list[6].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[7].id}}&title={{same_list[7].slug}}">
      
      <img data-ng-src="images/products-v2/{{same_list[7].VC_SKU}}-t.jpg" alt="{{same_list[7].title}}">
     
     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[7].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[7].category != 'Rings' && same_list[7].category != 'Bangles' && same_list[7].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[7])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[7].category == 'Rings' || same_list[7].category == 'Bangles' || same_list[7].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[7].id}}&title={{same_list[7].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
  </div>
    
 <div class="item row">
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[8].id}}&title={{same_list[8].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{same_list[8].VC_SKU}}-t.jpg" alt="{{same_list[8].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[8].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[8].category != 'Rings' && same_list[8].category != 'Bangles' && same_list[8].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[8])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[8].category == 'Rings' || same_list[8].category == 'Bangles' || same_list[8].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[8].id}}&title={{same_list[8].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[9].id}}&title={{same_list[9].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{same_list[9].VC_SKU}}-t.jpg" alt="{{same_list[9].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[9].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[9].category != 'Rings' && same_list[9].category != 'Bangles' && same_list[9].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[9])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[9].category == 'Rings' || same_list[9].category == 'Bangles' || same_list[9].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[9].id}}&title={{same_list[9].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[10].id}}&title={{same_list[10].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{same_list[10].VC_SKU}}-t.jpg" alt="{{same_list[10].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[10].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[10].category != 'Rings' && same_list[10].category != 'Bangles' && same_list[10].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[10])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[10].category == 'Rings' || same_list[10].category == 'Bangles' || same_list[10].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[10].id}}&title={{same_list[10].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{same_list[11].id}}&title={{same_list[11].slug}}">
      
      <img data-ng-src="images/products-v2/{{same_list[11].VC_SKU}}-t.jpg" alt="{{same_list[11].title}}">
     
     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{same_list[11].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
        <div class="col-sm-12 text-center" data-ng-if="same_list[11].category != 'Rings' && same_list[11].category != 'Bangles' && same_list[11].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(same_list[11])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="same_list[11].category == 'Rings' || same_list[11].category == 'Bangles' || same_list[11].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{same_list[11].id}}&title={{same_list[11].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
  </div>
    
</div>   
</div> 