<div id="carousel-jeweller" class="carousel slide" data-ride="carousel" style="position:relative;z-index:1;padding-top: 50px;">

<!-- Wrapper for slides -->
<div class="height carousel-inner">
 <div class="item active row">
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[0].id}}&title={{list[0].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{list[0].VC_SKU}}-t.jpg" alt="{{list[0].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[0].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[0].category != 'Rings' && list[0].category != 'Bangles' && list[0].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[0])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[0].category == 'Rings' || list[0].category == 'Bangles' || list[0].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[0].id}}&title={{list[0].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[1].id}}&title={{list[1].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{list[1].VC_SKU}}-t.jpg" alt="{{list[1].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[1].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[1].category != 'Rings' && list[1].category != 'Bangles' && list[1].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[1])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[1].category == 'Rings' || list[1].category == 'Bangles' || list[1].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[1].id}}&title={{list[1].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[2].id}}&title={{list[2].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{list[2].VC_SKU}}-t.jpg" alt="{{list[2].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[2].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[2].category != 'Rings' && list[2].category != 'Bangles' && list[2].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[2])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[2].category == 'Rings' || list[2].category == 'Bangles' || list[2].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[2].id}}&title={{list[2].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[3].id}}&title={{list[3].slug}}">
      
      <img data-ng-src="images/products-v2/{{list[3].VC_SKU}}-t.jpg" alt="{{list[3].title}}">
     
     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[3].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[3].category != 'Rings' && list[3].category != 'Bangles' && list[3].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[3])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[3].category == 'Rings' || list[3].category == 'Bangles' || list[3].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[3].id}}&title={{list[3].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
  </div>
     
 <div class="item row">
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[4].id}}&title={{list[4].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{list[4].VC_SKU}}-t.jpg" alt="{{list[4].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[4].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[4].category != 'Rings' && list[4].category != 'Bangles' && list[4].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[4])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[4].category == 'Rings' || list[4].category == 'Bangles' || list[4].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[4].id}}&title={{list[4].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[5].id}}&title={{list[5].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{list[5].VC_SKU}}-t.jpg" alt="{{list[5].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[5].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[5].category != 'Rings' && list[5].category != 'Bangles' && list[5].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[5])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[5].category == 'Rings' || list[5].category == 'Bangles' || list[5].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[5].id}}&title={{list[5].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[6].id}}&title={{list[6].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{list[6].VC_SKU}}-t.jpg" alt="{{list[6].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[6].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[6].category != 'Rings' && list[6].category != 'Bangles' && list[6].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[6])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[6].category == 'Rings' || list[6].category == 'Bangles' || list[6].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[6].id}}&title={{list[6].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[7].id}}&title={{list[7].slug}}">
      
      <img data-ng-src="images/products-v2/{{list[7].VC_SKU}}-t.jpg" alt="{{list[7].title}}">
     
     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[7].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[7].category != 'Rings' && list[7].category != 'Bangles' && list[7].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[7])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[7].category == 'Rings' || list[7].category == 'Bangles' || list[7].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[7].id}}&title={{list[7].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
  </div>
    
 <div class="item row">
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[8].id}}&title={{list[8].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{list[8].VC_SKU}}-t.jpg" alt="{{list[8].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[8].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[8].category != 'Rings' && list[8].category != 'Bangles' && list[8].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[8])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[8].category == 'Rings' || list[8].category == 'Bangles' || list[8].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[8].id}}&title={{list[8].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[9].id}}&title={{list[9].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{list[9].VC_SKU}}-t.jpg" alt="{{list[9].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[9].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[9].category != 'Rings' && list[9].category != 'Bangles' && list[9].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[9])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[9].category == 'Rings' || list[9].category == 'Bangles' || list[9].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[9].id}}&title={{list[9].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[10].id}}&title={{list[10].slug}}">
      
     <div class="product_image">
      <img data-ng-src="images/products-v2/{{list[10].VC_SKU}}-t.jpg" alt="{{list[10].title}}">
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[10].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[10].category != 'Rings' && list[10].category != 'Bangles' && list[10].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[10])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[10].category == 'Rings' || list[10].category == 'Bangles' || list[10].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[10].id}}&title={{list[10].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a data-ng-href="p-product.php?id={{list[11].id}}&title={{list[11].slug}}">
      
      <img data-ng-src="images/products-v2/{{list[11].VC_SKU}}-t.jpg" alt="{{list[11].title}}">
     
     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[11].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center" data-ng-if="list[11].category != 'Rings' && list[11].category != 'Bangles' && list[11].category != 'Bracelets'">
           <a class="btn btn-book" data-ng-click="buyNow(list[11])"> 
                    Buy Now
           </a>
        </div>
        <div class="col-sm-12 text-center" data-ng-if="list[11].category == 'Rings' || list[11].category == 'Bangles' || list[11].category == 'Bracelets'">
            <a class="btn btn-book" data-ng-href="p-product.php?id={{list[11].id}}&title={{list[11].slug}}"> 
                    Buy Now
           </a>
        </div>
     </div>

     
    
    </a>
   </div>
     
  </div>
    
</div>   
</div> 