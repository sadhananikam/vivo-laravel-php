<nav id="nav-1" class="menushadow">
 <div class="row yellowGradient menushadow fixed-header-structure">

 <div class="container" data-ng-init="brandMenuImg='images/header/menu/gold menu header.jpg'">
 <div class="row" id="main-menu" style="margin-bottom:0px;">

  <div class="col-md-2">
   <a href="default.php" title="VivoCarat Retail Pvt. Ltd.">
    <img src="images/header/logos/VivoCarat logo.png" data-ng-show="logo" style="padding-top: 4px;" alt="VivoCarat Retail Pvt. Ltd.">
   </a>
  </div>

  <div class="col-md-8 text-center">

   <ul class="megamenu">

    <li>
     <a href="#" class="bold-font">
            COLLECTIONS
     </a>
     <div class="megapanel collection-menu" id="arrow">
      <div class="row text-left">
       <div data-ng-init="brandMenuImg='images/header/menu/gold menu header.jpg'" data-ng-mouseover="brandMenuImg='images/header/menu/gold menu header.jpg'" data-ng-mouseout="brandMenuImg='images/header/menu/gold menu header.jpg'" class="col-xs-2">
        <div class="h_nav" style="padding-left: 8px;">
         <h4 class="base-metal-heading">
          <a href="p-list.php?type=Gold&subtype=All">
                Gold
          </a>
         </h4>

         <ul>
          <li><a href="p-list.php?type=gold&subtype=rings">Rings</a></li>
          <li><a href="p-list.php?type=gold&subtype=earrings">Earrings</a></li>
          <li><a href="p-list.php?type=gold&subtype=pendants"> Pendants</a></li>
          <li><a href="p-list.php?type=gold&subtype=nosepins">Nose Pins</a></li>
          <li><a href="p-list.php?type=gold&subtype=bangles">Bangles</a></li>
          <li><a href="p-list.php?type=gold&subtype=bracelets">Bracelets</a></li>
          <li><a href="p-list.php?type=gold&subtype=necklaces">Necklaces</a></li>
          <li><a href="p-list.php?type=gold&subtype=goldcoins">Coins</a></li>
         </ul>
        </div>
       </div>
           
       <div data-ng-init="brandMenuImg='images/header/menu/diamond menu header.jpg'" data-ng-mouseover="brandMenuImg='images/header/menu/diamond menu header.jpg'" data-ng-mouseout="brandMenuImg='images/header/menu/gold menu header.jpg'" class="col-xs-2">
        <div class="h_nav padding-left-10px">
         <h4 class="base-metal-heading">
          <a href="p-list.php?type=Diamond&subtype=All">
             Diamonds
          </a>
         </h4>

         <ul>
          <li><a href="p-list.php?type=diamond&subtype=rings">Rings</a></li>
          <li><a href="p-list.php?type=diamond&subtype=earrings">Earrings </a></li>
          <li><a href="p-list.php?type=diamond&subtype=pendants"> Pendants</a></li>
          <li><a href="p-list.php?type=diamond&subtype=nosepins">Nose Pins</a></li>
          <li><a href="p-list.php?type=diamond&subtype=Tanmaniya">Tanmaniya</a></li>
          <li><a href="p-list.php?type=diamond&subtype=bracelets">Bracelets</a></li>
          </ul>
         </div>
        </div>
           
       <div data-ng-init="brandMenuImg='images/header/menu/silver menu header.jpg'" data-ng-mouseover="brandMenuImg='images/header/menu/silver menu header.jpg'" data-ng-mouseout="brandMenuImg='images/header/menu/gold menu header.jpg'" class="col-xs-2">
        <div class="h_nav">
         <h4 class="base-metal-heading">
          <a href="p-list.php?type=Silver&subtype=All">
              Silver
          </a>
         </h4>
         
         <ul>
          <li><a href="p-list.php?type=silver&subtype=rings">Rings</a></li>
          <li><a href="p-list.php?type=silver&subtype=earrings">Earrings</a></li>
          <li><a href="p-list.php?type=silver&subtype=pendants"> Pendants</a></li>
          <li><a href="p-list.php?type=silver&subtype=accessories">Accessories</a></li>
          <li><a href="p-list.php?type=silver&subtype=bangles">Bangles</a></li>
          </ul>
         </div>
        </div>


       <div class="col-xs-5 col-xs-offset-1" style="padding: 15px 15px 15px 0px;">
        <div class="h_nav">
         <ul>
          <li class="no-padding-left-right">
           <img data-ng-src="{{brandMenuImg}}" alt="">
          </li>
         </ul>
        </div>
       </div>
    </li>

    <li>
     <a class="lookbook-menu" href="p-lookbook.php">
         LOOK BOOK
     </a>
    </li>

    <li>
     <a class="text-center bold-font" href="p-brands.php">
         BRANDS
     </a>

     <div class="megapanel brands-menu" id="caret1">
      <div class="row" style="margin-top: 2px;">
       <div class="col-xs-12 no-padding-left-right">
        <div class="h_nav pull-left">

         <ul class="text-left-img-ul">

          <li class="text-left-img">
           <div data-ng-init="brandBanner1='images/header/brands logos dropdown/incocu.png';" data-ng-mouseover="brandBanner1='images/header/brands logos dropdown/Incocu Jewellers hover.png';" data-ng-mouseout="brandBanner1='images/header/brands logos dropdown/incocu.png';">
            <a href="p-store.php?store=Incocu Jewellers">
             <img data-ng-src="{{brandBanner1}}" alt="Incocu Jewellers">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner2='images/header/brands logos dropdown/charu.png';" data-ng-mouseover="brandBanner2='images/header/brands logos dropdown/Charu-Jewels hover.png';" data-ng-mouseout="brandBanner2='images/header/brands logos dropdown/charu.png';">
            <a href="p-store.php?store=Charu-Jewels">
             <img data-ng-src="{{brandBanner2}}" alt="Charu-Jewels">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner3='images/header/brands logos dropdown/shankaram.png';" data-ng-mouseover="brandBanner3='images/header/brands logos dropdown/Shankaram Jewellers hover.png';" data-ng-mouseout="brandBanner3='images/header/brands logos dropdown/shankaram.png';">
            <a href="p-store.php?store=Shankaram Jewellers">
             <img data-ng-src="{{brandBanner3}}" alt="Shankaram Jewellers">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner4='images/header/brands logos dropdown/lagu.png';" data-ng-mouseover="brandBanner4='images/header/brands logos dropdown/Lagu Bandhu hover.png';" data-ng-mouseout="brandBanner4='images/header/brands logos dropdown/lagu.png';">
            <a href="p-store.php?store=Lagu Bandhu">
             <img data-ng-src="{{brandBanner4}}" alt="Lagu Bandhu">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner5='images/header/brands logos dropdown/arkina.png';" data-ng-mouseover="brandBanner5='images/header/brands logos dropdown/Arkina-Diamonds hover.png';" data-ng-mouseout="brandBanner5='images/header/brands logos dropdown/arkina.png';">
            <a href="p-store.php?store=Arkina-Diamonds">
             <img data-ng-src="{{brandBanner5}}" alt="Arkina-Diamonds">
            </a>
           </div>
          </li>

         </ul>

        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-xs-12 no-padding-left-right">
        <div class="h_nav pull-left">
                    
         <ul class="text-left-img-ul">

          <li class="text-left-img">
           <div data-ng-init="brandBanner6='images/header/brands logos dropdown/ornomart.png';" data-ng-mouseover="brandBanner6='images/header/brands logos dropdown/OrnoMart hover.png';" data-ng-mouseout="brandBanner6='images/header/brands logos dropdown/ornomart.png';">
            <a href="p-store.php?store=OrnoMart">
             <img data-ng-src="{{brandBanner6}}" alt="OrnoMart">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner7='images/header/brands logos dropdown/kundan.png';" data-ng-mouseover="brandBanner7='images/header/brands logos dropdown/Kundan Jewellers hover.png';" data-ng-mouseout="brandBanner7='images/header/brands logos dropdown/kundan.png';">
            <a href="p-store.php?store=Kundan Jewellers">
             <img data-ng-src="{{brandBanner7}}" alt="Kundan Jewellers">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner8='images/header/brands logos dropdown/mayura.png';" data-ng-mouseover="brandBanner8='images/header/brands logos dropdown/Mayura Jewellers hover.png';" data-ng-mouseout="brandBanner8='images/header/brands logos dropdown/mayura.png';">
            <a href="p-store.php?store=Mayura Jewellers">
             <img data-ng-src="{{brandBanner8}}" alt="Mayura Jewellers">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner9='images/header/brands logos dropdown/megha.png';" data-ng-mouseover="brandBanner9='images/header/brands logos dropdown/Megha Jewellers hover.png';" data-ng-mouseout="brandBanner9='images/header/brands logos dropdown/megha.png';">
            <a href="p-store.php?store=Megha Jewellers">
             <img data-ng-src="{{brandBanner9}}" alt="Megha Jewellers">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner10='images/header/brands logos dropdown/regaalia.png';" data-ng-mouseover="brandBanner10='images/header/brands logos dropdown/Regaalia Jewels hover.png';" data-ng-mouseout="brandBanner10='images/header/brands logos dropdown/regaalia.png';">
            <a href="p-store.php?store=Regaalia Jewels">
             <img data-ng-src="{{brandBanner10}}" alt="Regaalia Jewels">
            </a>
           </div>
          </li>

         </ul>
                      
        </div>
       </div>  
      </div>

      <div class="row">
       <div class="col-xs-12 no-padding-left-right">
        <div class="h_nav pull-left">

         <ul class="text-left-img-ul">

          <li class="text-left-img">
           <div data-ng-init="brandBanner11='images/header/brands logos dropdown/glitter.png';" data-ng-mouseover="brandBanner11='images/header/brands logos dropdown/Glitter Jewels hover.png';" data-ng-mouseout="brandBanner11='images/header/brands logos dropdown/glitter.png';">
            <a href="p-store.php?store=Glitter Jewels">
             <img data-ng-src="{{brandBanner11}}" alt="Glitter Jewels">
            </a>
           </div>
          </li>

<!--
          <li class="text-left-img">
           <div data-ng-init="brandBanner12='images/header/brands logos dropdown/mani.png';" data-ng-mouseover="brandBanner12='images/header/brands logos dropdown/Mani Jewelshover.png';" data-ng-mouseout="brandBanner12='images/header/brands logos dropdown/mani.png';">
            <a href="p-store.php?store=Mani Jewels">
             <img data-ng-src="{{brandBanner12}}" alt="Mani Jewels">
            </a>
           </div>
          </li>
-->

          <li class="text-left-img">
           <div data-ng-init="brandBanner13='images/header/brands logos dropdown/pp.png';" data-ng-mouseover="brandBanner13='images/header/brands logos dropdown/PP-Gold hover.png';" data-ng-mouseout="brandBanner13='images/header/brands logos dropdown/pp.png';">
            <a href="p-store.php?store=PP-Gold">
             <img data-ng-src="{{brandBanner13}}" alt="PP-Gold">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner14='images/header/brands logos dropdown/karatcraft.png';" data-ng-mouseover="brandBanner14='images/header/brands logos dropdown/KaratCraft hover.png';" data-ng-mouseout="brandBanner14='images/header/brands logos dropdown/karatcraft.png';">
            <a href="p-store.php?store=KaratCraft">
             <img data-ng-src="{{brandBanner14}}" alt="KaratCraft">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner15='images/header/brands logos dropdown/ZAVERI KAPOORCHAND DALICHAND & SONS.png';" data-ng-mouseover="brandBanner15='images/header/brands logos dropdown/ZKD-Jewels hover.png';" data-ng-mouseout="brandBanner15='images/header/brands logos dropdown/ZAVERI KAPOORCHAND DALICHAND & SONS.png';">
            <a href="p-store.php?store=ZKD-Jewels">
             <img data-ng-src="{{brandBanner15}}" alt="ZAVERI KAPOORCHAND DALICHAND & SONS Jewels">
            </a>
           </div>
          </li>

          <li class="text-left-img">
           <div data-ng-init="brandBanner16='images/header/brands logos dropdown/iski uski.png';" data-ng-mouseover="brandBanner16='images/header/brands logos dropdown/IskiUski hover.png';" data-ng-mouseout="brandBanner16='images/header/brands logos dropdown/iski uski.png';">
            <a href="p-store.php?store=IskiUski">
             <img data-ng-src="{{brandBanner16}}" alt="IskiUski">
            </a>
           </div>
          </li>

         </ul>

        </div>
       </div>  
      </div>

      <div class="row">
       <div class="col-xs-12 no-padding-left-right">
        <div class="h_nav pull-left">

         <ul class="text-left-img-ul">

          <li class="text-left-img">
           <div data-ng-init="brandBanner17='images/header/brands logos dropdown/myrah.png';" data-ng-mouseover="brandBanner17='images/header/brands logos dropdown/Myrah-Silver-Works hover.png';" data-ng-mouseout="brandBanner17='images/header/brands logos dropdown/myrah.png';">
            <a href="p-store.php?store=Myrah-Silver-Works">
             <img data-ng-src="{{brandBanner17}}" alt="Myrah-Silver-Works">
            </a>
           </div>
          </li>
             
           <li class="text-left-img">
             <div data-ng-init="brandBanner18='images/header/brands logos dropdown/tsara.png';" data-ng-mouseover="brandBanner18='images/header/brands logos dropdown/tsara hover.png';" data-ng-mouseout="brandBanner18='images/header/brands logos dropdown/tsara.png';">
              <a href="p-store.php?store=Tsara-Jewellery">
               <img data-ng-src="{{brandBanner18}}">
              </a>
             </div>
            </li> 
               
            <li class="text-left-img">
             <div data-ng-init="brandBanner19='images/header/brands logos dropdown/sarvada.png';" data-ng-mouseover="brandBanner19='images/header/brands logos dropdown/sarvada hover.png';" data-ng-mouseout="brandBanner19='images/header/brands logos dropdown/sarvada.png';">
              <a href="p-store.php?store=Sarvada-Jewels">
               <img data-ng-src="{{brandBanner19}}">
              </a>
             </div>
            </li>       
         
         </ul>

        </div>
       </div>  
      </div>
     </div>
    </li>
      
   </ul>
     
  </div>
                       
  <div class="col-md-2 text-right" data-ng-show="logo">
   <a href="p-checkout.php">

    <img src="images/header/icons/stick cart bag.png" style="padding-top: 8px;" alt="checkout cart">

     <div data-ng-if="cart.length>0" class="fixed-cart-length-structure">
      <p style="padding-right: 7px;">({{cart.length}})</p>
     </div>

     <div data-ng-if="cart.length<1 || !cart || !cart.length" class="fixed-cart-length-nil">
      <p style="padding-right: 7px;">(0)</p>
     </div>

   </a>
  </div>

 </div>

 </div>

</div>

</nav>