<!DOCTYPE html>
<html lang="en" data-ng-app="vivoSellerlocate">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />

    <link rel="canonical" href="https://www.vivocarat.com/p-sellerlocate.php">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/sellerlocate">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/sellerlocate" />

    <link href="css/style.css" rel="stylesheet" media="all">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">

    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if (isMobile.any()) {
            document.location = "m-index.html#/i/about";
        }
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '293278664418362', {
            em: 'insert_email_variable,'
        });
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->
</head>

<body>

    <vivo-header></vivo-header>

    <div data-ng-controller='sellerlocateCtrl' ng-cloak>

        <div class="container" style="padding: 30px 0;">
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr">
                            <span>Locate a Vivocarat Partner store near you</span>
                        </h2>
                    </div>
                </div>
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-xs-12">
                        <p>
                            VivoCarat Partner Store will help you discover, buy, repair and know more about the jewellery that you buy. 
                            <br>Few services offered by our Partner Store are,
                        </p>
                        <ul class="list_based_on_fa">
                            <li>
                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Lifetime free repair/polish for any jewellery bought on VivoCarat.
                            </li>
                            <li>
                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Try at Shop - try the jewellery at a partner shop before buying.
                            </li>
                            <li>
                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Find your proper ring/bangle/bracelet size before ordering online.
                            </li>
                        </ul>
                    </div>
                </div>
            </section>

            <section style="margin: 30px 0 60px;">
                <div class="row" style="padding: 0 15px;">
                    <div class="col-md-6 col-sm-12 no-padding-left-right">
                        <div class="partner_locate">
                            <div class="row">
                                <div class="col-xs-12" style="padding-left: 30px !important; padding-right: 30px !important;">
                                    <form>
                                        <div class="partner_locate_search_form">
                                            <span>
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </span>
                                            <input type="search" placeholder="Enter your city" data-ng-model="searchtext">
                                            <span class="submit">
                                            <button data-ng-click="getSellerDetailBySearch(searchtext)">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        <div class="row partner_loc" style="margin-top: 15px;" data-ng-repeat="s in sellers track by $index">
                            <div class="col-md-12 no-padding-left-right">
                                <h3>{{s.name}}</h3>
                                <address>
                                    {{s.address}} {{s.pincode}}
                                </address>
                                <address>
                                    Phone: {{s.phone}}
                                </address>
                            </div>
<!--
                            <div class="col-md-6 no-padding-left-right">
                                <form>
                                    <div class="user_input_form">
                                        <div class="form_label" id="getsms-{{s.id}}" onclick="opensmsbox(this)">
                                            <span>Get this on your phone</span>
                                            <input type="submit" class="btn_submit" value="Send">
                                        </div>
                                        <div class="get_user_data">
                                            <span>Send to:</span>
                                            <form>
                                                <input type="text" placeholder="+91  Enter Your Number">
                                            </form>
                                        </div>
                                    </div>
                                </form>
                            </div>
-->
                        </div>
                        
                        <!--<div class="row partner_loc">
                            <div class="col-md-6 no-padding-left-right">
                                <h3>Mayura Jewellers</h3>
                                <address>
                                    302,Swaroop Centre<br>
                                    JB Nagar Circle, Chakala<br>
                                    Andheri East<br>
                                    Mumbai - 400 059
                                </address>
                            </div>
                            <div class="col-md-6 no-padding-left-right">
                                <form>
                                    <div class="user_input_form">
                                        <div class="form_label">
                                            <span>Get this on your phone</span>
                                            <input type="submit" class="btn_submit" value="Send">
                                        </div>
                                        <div class="get_user_data">
                                            <span>Send to:</span>
                                            <form>
                                                <input type="text" placeholder="+91  Enter Your Number">
                                            </form>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                                                
                        <div class="row partner_loc">
                            <div class="col-md-6 no-padding-left-right">
                                <h3>Mayura Jewellers</h3>
                                <address>
                                    302,Swaroop Centre<br>
                                    JB Nagar Circle, Chakala<br>
                                    Andheri East<br>
                                    Mumbai - 400 059
                                </address>
                            </div>
                            <div class="col-md-6 no-padding-left-right">
                                <form>
                                    <div class="user_input_form">
                                        <div class="form_label">
                                            <span>Get this on your phone</span>
                                            <input type="submit" class="btn_submit" value="Send">
                                        </div>
                                        <div class="get_user_data">
                                            <span>Send to:</span>
                                            <form>
                                                <input type="text" placeholder="+91  Enter Your Number">
                                            </form>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>-->
                            <div id="sellernotfound">
                            <div class="row">
                                <div class="col-xs-12" style="padding: 25px;">
                                    <p>
                                            Sorry! We are not available in this city at present. But we will be there soon.<br> Please provide your details below and we will inform you when we get a Partner Store in your city.
                                        </p>
                                </div>
                            </div>
                            <div class="default_form">
                                <form name="sellerenquiryForm" ng-submit="saveSellerLocationEnquiry()" autocomplete="off" novalidate>
                                <div class="row">
                                    <div class="col-md-3">
                                        Name
                                    </div>
                                    <div class="col-md-1">
                                        :
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" placeholder="Enter Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Name'" data-ng-model="sellerenquiry.pname" name="pname" id="pname" data-ng-required="true" data-ng-pattern="/^[a-zA-Z\s]+$/">
                                    </div>
                                    <div class="row">
                                         <div class="col-md-8 col-md-offset-3 normal-text" data-ng-show="sellerenquiryForm.pname.$dirty && sellerenquiryForm.pname.$error.required">Name is required</div>
                                         <div class="col-md-8 col-md-offset-3 normal-text" data-ng-show="sellerenquiryForm.pname.$dirty && sellerenquiryForm.pname.$error.pattern">Name can contain only alphabets</div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        City/Location
                                    </div>
                                    <div class="col-md-1">
                                        :
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" placeholder="Enter City" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter City'" data-ng-model="sellerenquiry.plocation" name="plocation" id="plocation" data-ng-required="true" data-ng-pattern="/^[a-zA-Z\s]+$/">
                                    </div>
                                    <div class="row">
                                         <div class="col-md-8 col-md-offset-3 normal-text" data-ng-show="sellerenquiryForm.plocation.$dirty && sellerenquiryForm.plocation.$error.required">City/Location is required</div>
                                         <div class="col-md-8 col-md-offset-3 normal-text" data-ng-show="sellerenquiryForm.plocation.$dirty && sellerenquiryForm.plocation.$error.pattern">City/Location can contain only alphabets</div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        Email
                                    </div>
                                    <div class="col-md-1">
                                        :
                                    </div>
                                    <div class="col-md-8">
                                        <input type="email" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email'" data-ng-model="sellerenquiry.pemail" name="pemail" id="pemail" data-ng-required="true">
                                    </div>
                                    
                                    <div class="row">
                                         <div class="col-md-9 col-md-offset-3 normal-text" data-ng-show="sellerenquiryForm.pemail.$dirty && sellerenquiryForm.pemail.$error.required">Email is required</div>
                                         <div class="col-md-9 col-md-offset-3 normal-text" data-ng-show="sellerenquiryForm.pemail.$dirty && sellerenquiryForm.pemail.$error.email">Invalid Email</div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        Phone
                                    </div>
                                    <div class="col-md-1">
                                        :
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" placeholder="+91 Enter Phone" onfocus="this.placeholder = ''" onblur="this.placeholder = '+91 Enter Phone'" data-ng-model="sellerenquiry.pphone" name="pphone" id="pphone" data-ng-required="true" data-ng-pattern="/^[0-9]{10,10}$/">
                                    </div>
                                    <div class="row">
                                         <div class="col-md-9 col-md-offset-3 normal-text" data-ng-show="sellerenquiryForm.pphone.$dirty && sellerenquiryForm.pphone.$error.required">Phone is required</div>
                                         <div class="col-md-9 col-md-offset-3 normal-text grey-colour" data-ng-show="sellerenquiryForm.pphone.$dirty && sellerenquiryForm.pphone.$error.pattern">Enter correct phone number</div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-12" style="padding-top: 15px;">
                                        <button type="submit" class="btn browse-more-button pull-right" ng-disabled="!sellerenquiryForm.$valid">Submit</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 no-padding-left-right">
                        <!--<iframe style="width: 100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.0342944589356!2d72.8632563144684!3d19.106151387070582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c8478e27b5d5%3A0xf3d09043b2998e9e!2sSwaroop+Centre!5e0!3m2!1sen!2sin!4v1482234650022" width="1111" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                        <div id="map" style="width: 1111px; height: 370px;"></div>
                    </div>
                </div>
            </section>
        </div>

    </div>
    <vivo-footer></vivo-footer>

    <script src="js/jquery.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/css3-mediaqueries.js"></script>
    <script src="js/megamenu.js"></script>
    <script src="js/slides.min.jquery.js"></script>
    <script src="js/jquery.jscrollpane.min.js"></script>
    <script src="js/jquery.easydropdown.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/angular-ui-router.min.js"></script>
    <script src="js/angular-animate.min.js"></script>
    <script src="js/angular-sanitize.js"></script>
    <script src="js/satellizer.min.js"></script>
    <script src="js/angular.rangeSlider.js"></script>
    <script src="js/select.js"></script>
    <script src="js/toaster.js"></script>
    <script src="js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="js/taggedInfiniteScroll.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/angular-google-plus.min.js"></script>
    <script src="js/jquery.etalage.min.js"></script>
    <script src="js/jquery.simplyscroll.js"></script>

    <!--  start angularjs modules  -->
    <script src="app/modules/vivoCommon.js"></script>
    <script src="app/modules/vivoSellerlocate.js"></script>
    <!-- end angularjs modules -->

    <script src="app/data.js"></script>
    <script src="app/directives.js"></script>

    <!-- Start include Controller for angular -->
    <script src="app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->

    <script src="device-router.js"></script>

    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67690535-1', 'auto');
        ga('send', 'pageview');
    </script>

<!--    <script>
        function opensmsbox(e)
        {
            var str = "#"+$(e).attr('id');
//            alert($(e).attr('id'));
            $(str).next('.get_user_data').fadeToggle();
            $(str).children('.btn_submit').fadeToggle();
        }
        
        $(document).ready(function() {
            $('.form_label').click(function() {
                $(this).next('.get_user_data').fadeToggle();
                $(this).children('.btn_submit').fadeToggle();

            });
        });
    </script>-->
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATiG7u40IAJS0xYnaZJg6d_vSwc26iIac&extension=.js"></script> 

</body>

</html>