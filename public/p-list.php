<?php
$desk = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if(isset($_GET['type']) && $_GET['type'] != '' && isset($_GET['subtype']) && $_GET['subtype'] != '')
{
    $type = $_GET['type'];
    $subtype = $_GET['subtype'];
    $mob = 'https://' . $_SERVER['HTTP_HOST'] . "/m-index.html#/c/".$type."/".$subtype;
    
    $SITE_ROOT = "https://www.vivocarat.com/";
    $cData = getCData($SITE_ROOT,$type,$subtype);
    $cData = json_decode($cData,true);
    
    $title = $cData['page_title'];
    $meta_keywords = $cData['meta_keywords'];
    $meta_description = $cData['meta_description'];
    $meta_robots = $cData['meta_robots'];
    $og_type = $cData['og_type'];
    $og_title = $cData['og_title'];
    $og_description = $cData['og_description'];
}

function getCData($siteRoot,$type,$subtype) {

    $rawData = file_get_contents($siteRoot.'api/v1/getcategorydescription?category='.urlencode($subtype).'&parent_category='.urlencode($type));
    return $rawData;
}
?>
<!DOCTYPE html>
<html lang="en" data-ng-app="vivoCategory" data-ng-controller='categoryCtrl'>

<head>
    <title><?php echo $title; ?></title>
    <meta name="keywords" content="<?php echo $meta_keywords; ?>" >
    <meta name="description" content="<?php echo $meta_description; ?>" />
    <meta name="robots" content="<?php echo $meta_robots; ?>" >
    <meta property="og:url" content="<?php echo $desk; ?>" />  
    <meta property="og:description" content="<?php echo $og_description; ?>" />
    <meta property="og:title"         content="<?php echo $og_title; ?>" />
    <meta property="og:type"          content="<?php echo $og_type; ?>" />
    
    <meta http-equiv="Content-Language" content="en" />
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />

    <!-- SEO-->
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="<?php echo $desk; ?>">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo $mob; ?>">
    <link rel="alternate" media="handheld" href="<?php echo $mob; ?>" />
    
    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            if (window.location.search.indexOf('utm_campaign') > -1) 
            {
                var utm_campaign = getUrlParameter('utm_campaign');
                if(utm_campaign.length)
                {
                    var utm_source = getUrlParameter('utm_source');
                    var utm_medium = getUrlParameter('utm_medium');

                    document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype')+"/"+utm_campaign+"/"+utm_source+"/"+utm_medium;
                }
                else
                {
                    document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');
                }
            }
            else
            {
                document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');
            }
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>


<body ng-cloak>

<div id="fb-root"></div>

<style>
.no-padding {
    padding: 0px;
}

.no-pad {
    padding: 0px;
}


.ngrs-range-slider .ngrs-join {
    position: absolute;
    z-index: 1;
    top: 50%;
    left: 0;
    right: 100%;
    height: 8px;
    margin: -4px 0 0 0;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
    background-color: #E22B46;
    background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
    background-size: 100%;
    background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #E22B46), color-stop(100%, #E22B46));
    background-image: -moz-linear-gradient(#5bc0de, #2f96b4);
    background-image: -webkit-linear-gradient(#5bc0de, #2f96b4);
    background-image: linear-gradient(#E22B46, #E22B46);
}

.vivo-grid {
    padding: 3px;
    width: 100%;
    height: 334px;
}

.vivo-box-padding {
    padding: 2px;
}

/*changed to remove white space after description*/
.home-panel-row {
    margin-top: 0px;
    padding: 0px;
}
</style>

<vivo-header></vivo-header>

<div>

<div class="container no-padding-left-right padding-bottom-60px">

<div class="mens">
<div class="main margin-none">

<div class="row">
 <div class="col-sm-12 cont no-padding-left-right">
  <div class="container" style="padding-top: 5px;padding-left: 30px;padding-right: 30px;">
   <vivo-product-filter></vivo-product-filter>
  </div>

  <vivo-product-list></vivo-product-list>

  <div class="row" id='loadmore' tagged-infinite-scroll='loadMore()'></div>
 </div>
</div>
    
<div class="clear"></div>
    
</div>
</div>
    
</div>

<!-- start Category description  -->
<div class="middle-footer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p ng-bind-html="catdesc.description | unsafe"></p>
      </div>
    </div>
  </div>
</div>
<!-- end Category description  -->
</div>
    
<vivo-footer></vivo-footer>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoCategory.js"></script>
<!-- end angularjs modules -->

<script src="app/data.js"></script>
<script src="app/directives.js"></script>

<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->

<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>
