var checkoutapp = angular.module("vivoCheckout", ['vivoCommon']);

checkoutapp.controller('checkoutCtrl',['$scope','$rootScope','$location','$http','Data','$window','$timeout','$auth','API_URL', function ($scope, $rootScope, $location, $http, Data, $window, $timeout, $auth ,API_URL) {
    $scope.code = null;
    $scope.getCombo = function (id) {

        $http({
            method: 'GET',
            url : API_URL + 'getProductDetail',
            params : {id:id}
        }).then(function successCallback(response){
            $scope.combo = response.data[0];
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.stepNo = 1;
    $scope.customer_new = {
        name: null,
        email: null,
        phone: null,
        password: null
    }
    
    $scope.customer = {
        email: null,
        password: null
    }
    
    $scope.getallcoupons = function(){
            $http({
                method: 'GET',
                url : API_URL + 'getCoupons'
            }).then(function successCallback(response){
                $scope.allcoupons = response.data;
            },function errorCallback(response){
                console.log(response.data);
            });
    }
    $scope.getallcoupons();
    
    $('#modlgn_passwd').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $scope.loginUser($scope.customer);
        }
    });
    
    $scope.authenticate = function (provider) {
        $auth.authenticate(provider).then(function (data) {

        if (provider == 'facebook') {
            $http.get('https://graph.facebook.com/v2.5/me?fields=id,email,first_name,last_name,link,name&access_token=' + data.access_token).success(function (data) 
            {
                
                waitingDialog.show();
                $http({
                    method: 'POST',
                    url : API_URL + 'oauth_login',
                    params: {
                        customer: data,
                        items: JSON.stringify($rootScope.cart)
                    }
                }).then(function successCallback(response){

                    var results = response.data;
                    $rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                    $rootScope.authenticated = true;
                    $rootScope.uid = results.uid;
                    $rootScope.name = results.name;
                    $rootScope.email = results.email;
                    
                    if (results.status == "success") {
                        if(results.items != null)
                        {
                            var pr = JSON.parse(results.items);

                            var k = 1;
                            for (var i = 0; i < pr.length; i++) {

                                var p_id = pr[i].product_id;
                                var ring_size = pr[i].ring_size;
                                var bangle_size = pr[i].bangle_size;
                                var bracelet_size = pr[i].bracelet_size;
                                var quantity = pr[i].quantity;           

                                $http({
                                    method: 'GET',
                                    url : API_URL + 'getProductDetail',
                                    params : {id:p_id}
                                }).then(function successCallback(response){
                                    var p = response.data[0];
                                    p.ring_size = pr[k-1].ring_size;
                                    p.bangle_size = pr[k-1].bangle_size;
                                    p.bracelet_size = pr[k-1].bracelet_size;
                                    p.quantity = pr[k-1].quantity;

                                    $rootScope.showmodal = false;
                                    $rootScope.addToCart(p);
                                    $rootScope.showmodal = true;

                                    if(k == pr.length){
                                        Data.get('session').then(function (results) {
                                            $scope.placeOrder();
                                        });
                                    }
                                    k = k+1;
                                    waitingDialog.hide();

                                },function errorCallback(response){
                                    console.log(response.data);
                                    waitingDialog.hide();
                                });

                            }
                        }
                        else
                        {
                             Data.get('session').then(function (results) {
                                $scope.placeOrder();
                            });
                            waitingDialog.hide();
                        }

                    }	
                    else
                    {
                        waitingDialog.hide();
                        $rootScope.response = results.message;
                        $('#loggedIn').modal('show');
                        $timeout(function () {
                            $('#loggedIn').modal('hide');

                        }, 2000);
                    }
                },function errorCallback(response){
                    console.log(response.data);
                    waitingDialog.hide();
                });

            }).error(function (data) {


            });
        }


        if (provider == 'google') {
            waitingDialog.show();
            $http({
                method: 'POST',
                url : API_URL + 'oauth_login_googleplus',
                params: {
                    customer: data.config.data,
                    items: JSON.stringify($rootScope.cart)
                }
            }).then(function successCallback(response){
                var results = response.data;
                $rootScope.response = results.message;
                $('#loggedIn').modal('show');
                $timeout(function () {
                    $('#loggedIn').modal('hide');

                }, 2000);
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                $rootScope.name = results.name;
                $rootScope.email = results.email;
                if (results.status == "success") {
                    if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);

                        var k = 1;
                        for (var i = 0; i < pr.length; i++) {
                            var p_id = pr[i].product_id;
                            var ring_size = pr[i].ring_size;
                            var bangle_size = pr[i].bangle_size;
                            var bracelet_size = pr[i].bracelet_size;
                            var quantity = pr[i].quantity;             

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;
                                $rootScope.showmodal = false;
                                $rootScope.addToCart(p);
                                $rootScope.showmodal = true;

                                if(k == pr.length){
                                    Data.get('session').then(function (results) {
                                        $scope.placeOrder();
                                    });
                                }
                                k = k+1;
                                waitingDialog.hide();

                            },function errorCallback(response){
                                console.log(response.data);
                                waitingDialog.hide();
                            });

                        }
                    }
                    else
                    {
                         Data.get('session').then(function (results) {
                            $scope.placeOrder();
                        });
                        waitingDialog.hide();
                    }

                }	
                else
                {
                    $rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                    waitingDialog.hide();
                }

            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });

        }

    })
    .catch(function (error) {
        if (error.error) {
            // Popup error - invalid redirect_uri, pressed cancel button, etc.
            console.log(error.error);
        } else if (error.data) {
            // HTTP response error from server
            console.log(error.data.message, error.status);
        } else {
            console.log(error);
        }
    });
    };


    $scope.isPasswordError = false;
    $scope.$watch('customer_new.password', function () {
        if ($scope.customer_new.password !== undefined && $scope.customer_new.password !== null && $scope.customer_new.password.length < 8) {
            $scope.message = "Password must be atleast 8 characters";
            $scope.isPasswordError = true;
        } else {
            $scope.isPasswordError = false;
            $scope.message = "";
        }
    }, true);
    
    $scope.loginUser = function (c) {
        
        if ($scope.customer.email == null || $scope.customer.email == "" || $scope.customer.password == null || $scope.customer.password == "") {
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } 
        else 
        {
            waitingDialog.show();
            $http({
                method: 'POST',
                url : API_URL + 'login',
                params : {
                    customer: JSON.stringify(c),
                    items: JSON.stringify($rootScope.cart)
                }
            }).then(function successCallback(response){
                waitingDialog.hide();
                var results = response.data;
                if (results.status == "success") {
                    $rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                    $rootScope.authenticated = true;
                    $rootScope.uid = results.uid;
                    $rootScope.name = results.name;
                    $rootScope.email = results.email;
                  
                    if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);
                        
                        var k = 1;
                        for (var i = 0; i < pr.length; i++) {

                            var p_id = pr[i].product_id;

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;
                                $rootScope.addToCart(p);
                                
                                if(k == pr.length){
                                    Data.get('session').then(function (results) {
                                    $scope.placeOrder();
                                    });
                                }
                                k = k+1;
                            
                            },function errorCallback(response){
                                console.log(response.data);
                            });

                        }
                    }
                    else
                    {
                        Data.get('session').then(function (results) {
                            $scope.placeOrder();
                        });
                    }
                        
                } else {
					$rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                }
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });
            
        }
    }

    $scope.address = {
        email: $rootScope.email,

        fline: null,

        uid: $rootScope.uid,

        city: null,
        state: null,
        pin: null,
        country: null,

    };
    $scope.ship_address = {
        email: $rootScope.email,

        fline: null,

        uid: $rootScope.uid,

        city: null,
        state: null,
        pin: null,
        country: null,

    };
    $scope.signup = function () {
        if ($scope.customer_new.name == null || $scope.customer_new.name == null || $scope.customer_new.phone == null || $scope.customer_new.phone == null || $scope.customer_new.email == null || $scope.customer_new.email == "" || $scope.customer_new.password == null || $scope.customer_new.password == "") {
            //Please verify the details entered.
            $("#verify").modal('show');

            $timeout(function () {
                $("#verify").modal('hide');
            }, 2000);

        } else {
            waitingDialog.show();
            $http({
                method: 'POST',
                url : API_URL + 'signUp',
                params : {
                    customer: JSON.stringify($scope.customer_new),
                    items: JSON.stringify($rootScope.cart)
                }
            }).then(function successCallback(response){
                waitingDialog.hide();
                var results = response.data;
                
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                if (results.status == "error") {
                    //alert(results.message);
                    $rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                } else {

                  if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);
                        
                        var k = 1;
                        for (var i = 0; i < pr.length; i++) 
                        {

                            var p_id = pr[i].product_id;
                            var ring_size = pr[i].ring_size;
                            var bangle_size = pr[i].bangle_size;
                            var bracelet_size = pr[i].bracelet_size;
                            var quantity = pr[i].quantity;               

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;
                                $rootScope.addToCart(p);
                                
                                if(k == pr.length){     
                                    Data.get('session').then(function (results) {
                                        $rootScope.name = results.name;
                                        $rootScope.email = results.email;
                                        $rootScope.phone = results.phone;
                                        $scope.address.name = $rootScope.name;
                                        $scope.address.phone = $rootScope.phone;
                                        $scope.placeOrder();
                                    });
                                }
                                k = k+1;
                            
                            },function errorCallback(response){
                                console.log(response.data);
                            });

                        }
                    }
                    else
                    {
                        Data.get('session').then(function (results) {
                            $rootScope.name = results.name;
                            $rootScope.email = results.email;
                            $rootScope.phone = results.phone;
                            $scope.address.name = $rootScope.name;
                            $scope.address.phone = $rootScope.phone;
                            $scope.placeOrder();
                        });
                    }
                
                }
                
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });
        }

    }
    
    $scope.forgotPassword = function () {
        
        if ($scope.femail == null || $scope.femail == "") {
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } 
        else 
        {
            waitingDialog.show();
            var url = API_URL + 'forgotpassword';
            $http({
                method: 'POST',
                url: url, 
                params: {
                        email: $scope.femail   
                    }
            })
            .then(function successCallback(response){
                waitingDialog.hide();
                if (response.data.trim() == "ND") {
                    //Oops we do not have this email id registered with us.
                    $("#noemail").modal('show');

                    $timeout(function () {
                        $("#noemail").modal('hide');
                    }, 2000);


                } else {
                    //An email has been sent to you to reset the password.
                    $("#pwdreset").modal('show');

                    $timeout(function () {
                        $("#pwdreset").modal('hide');
                    }, 2000);
                }            
            },function errorCallback(response){
                console.log(response.data);
                //An error has occured. Please check the log for details.
                waitingDialog.hide();
           });
        }
    }

    $scope.$watch('address.pin', function () {
        if ($scope.address.pin && $scope.address.pin.length > 5) {
			var city = '';
            var state = '';
            var country = '';
            
            waitingDialog.show();
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+$scope.address.pin).success(function(response){
                
                if(response.status == 'OK')
                {      if(response.results[0].address_components.length)
                    {
                        var address_components = response.results[0].address_components;

                        $.each(address_components, function(index, component){
                          var types = component.types;
                            
                          $.each(types, function(index, type){
                            if(type == 'locality') {
                                city = component.long_name;
                                
                                return true;
                            }
                            if(type == 'administrative_area_level_1') {
                                state = component.long_name;
                                
                                return true;
                            }
                            if(type == 'country') {
                                country = component.long_name;
                                
                                return true;
                            }
                          });
                            
                        });
                        
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.address.city = city;
                                $scope.address.state = state;
                                $scope.address.country = country;
                                waitingDialog.hide();
                            });
                        }, 2000);
                    }
                    else
                    {
                        waitingDialog.hide();
                    }
                }
                else
                {
                    $("#WrongPinCode").modal('show');

                    $timeout(function () {
                        $("#WrongPinCode").modal('hide');
                    }, 2000);
                    waitingDialog.hide();
                }
                
            });
			
        }

    }, true);

    $scope.$watch('ship_address.pin', function () {
        if ($scope.ship_address.pin && $scope.ship_address.pin.length > 5) {
			var city = '';
            var state = '';
            var country = '';
            
            waitingDialog.show();
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+$scope.ship_address.pin).success(function(response){
                               
                if(response.status == 'OK')
                {      if(response.results[0].address_components.length)
                    {
                        var address_components = response.results[0].address_components;

                        $.each(address_components, function(index, component){
                          var types = component.types;
                          $.each(types, function(index, type){
                            if(type == 'locality') {
                                city = component.long_name;
                            }
                            if(type == 'administrative_area_level_1') {
                                state = component.long_name;
                            }
                            if(type == 'country') {
                                country = component.long_name;
                            }
                          });
                        });
                        
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ship_address.city = city;
                                $scope.ship_address.state = state;
                                $scope.ship_address.country = country;
                                waitingDialog.hide();
                            });
                        }, 2000);
                    }
                    else
                    {
                        waitingDialog.hide();
                    }
                }
                else
                {
                    $("#WrongPinCode").modal('show');

                    $timeout(function () {
                        $("#WrongPinCode").modal('hide');
                    }, 2000);
                    waitingDialog.hide();
                }               
            });
			
        }

    }, true);
    $scope.isNewAddress = true;
    $scope.$watch('isNewAddress', function (newValue, oldValue) {

        $scope.address.name = $rootScope.name;
        $scope.address.phone = $rootScope.phone;

        if ($scope.isNewAddress) {
            $scope.address = $scope.ship_address;
        } else {
            $scope.address = {
                email: $rootScope.email,

                fline: null,

                uid: $rootScope.uid,

                city: null,
                state: null,
                pin: null,
                country: null,
                name: $rootScope.name,
                phone: $rootScope.phone

            };
        }
    }, true);

    $scope.addressList = [];
    $scope.getAddresses = function () {
        
        $http({
            method: 'GET',
            url : API_URL + 'getAddress',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            if (response.data.length > 0) {

                $scope.ship_address = response.data[0];
                $scope.address = $scope.ship_address;
            }
        },function errorCallback(response){
            console.log(response.data);
        });
    }

    $scope.addAddress = function () {
        waitingDialog.show();
        
        if ($scope.isNewAddress && ($scope.ship_address != null && ($scope.ship_address.name != null && $scope.ship_address.name != "" &&
                $scope.ship_address.phone != null && $scope.ship_address.phone != "" &&
                $scope.ship_address.first_line != null && $scope.ship_address.first_line != "" &&
                $scope.ship_address.pin != null && $scope.ship_address.pin != "" &&

                $scope.ship_address.city != null && $scope.ship_address.city != "" &&
                $scope.ship_address.state != null && $scope.ship_address.state != ""))) {
            $scope.address.email = $rootScope.email;
            $scope.ship_address.email = $rootScope.email;
            $scope.address.uid = $rootScope.uid;
            $scope.ship_address.uid = $rootScope.uid;
            $scope.address.orderId = $scope.orderId;

            $http({
                method: 'POST',
                url : API_URL + 'addNewAddress',
                params : {address: JSON.stringify($scope.address),ship_address: JSON.stringify($scope.ship_address)}
            }).then(function successCallback(response){
                waitingDialog.hide();
                
                $scope.getOrderDetails();
                $("#d").addClass('active');
                $("#c").removeClass('active');
                $("#a").removeClass('active');
                $("#b").removeClass('active');
                $("#d-tab").addClass('active');
                $("#c-tab").removeClass('active');
                $("#b-tab").removeClass('active');
                $("#a-tab").removeClass('active');

                $("#d-md").addClass('active');
                $("#cmd").removeClass('active');
                $("#a-md").removeClass('active');
                $("#b-md").removeClass('active');
                $("#d-md-tab").addClass('active');
                $("#c-md-tab").removeClass('active');
                $("#b-md-tab").removeClass('active');
                $("#a-md-tab").removeClass('active');
                $(".progress-bar").animate({
                    width: '84%'
                }, 800);
                $scope.stepNo = 4;
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });
            
        } else if (!$scope.isNewAddress && ($scope.ship_address != null && ($scope.ship_address.name != null && $scope.ship_address.name != "" &&
                $scope.ship_address.phone != null && $scope.ship_address.phone != "" &&
                $scope.ship_address.first_line != null && $scope.ship_address.first_line != "" &&
                $scope.ship_address.pin != null && $scope.ship_address.pin != "" &&

                $scope.ship_address.city != null && $scope.ship_address.city != "" &&
                $scope.ship_address.state != null && $scope.ship_address.state != "")) && ($scope.address != null && ($scope.address.name != null && $scope.address.name != "" &&
                $scope.address.phone != null && $scope.address.phone != "" &&
                $scope.address.first_line != null && $scope.address.first_line != "" &&
                $scope.address.pin != null && $scope.address.pin != "" &&

                $scope.address.city != null && $scope.address.city != "" &&
                $scope.address.state != null && $scope.address.state != ""))) {


            $scope.address.email = $rootScope.email;
            $scope.ship_address.email = $rootScope.email;
            $scope.address.uid = $rootScope.uid;
            $scope.ship_address.uid = $rootScope.uid;
            $scope.address.orderId = $scope.orderId;

            $http({
                method: 'POST',
                url : API_URL + 'addNewAddress',
                params : {address: JSON.stringify($scope.address),ship_address: JSON.stringify($scope.ship_address)}
            }).then(function successCallback(response){
                waitingDialog.hide();
                
                $scope.getOrderDetails();
                $("#d").addClass('active');
                $("#c").removeClass('active');
                $("#a").removeClass('active');
                $("#b").removeClass('active');
                $("#d-tab").addClass('active');
                $("#c-tab").removeClass('active');
                $("#b-tab").removeClass('active');
                $("#a-tab").removeClass('active');

                $("#d-md").addClass('active');
                $("#cmd").removeClass('active');
                $("#a-md").removeClass('active');
                $("#b-md").removeClass('active');
                $("#d-md-tab").addClass('active');
                $("#c-md-tab").removeClass('active');
                $("#b-md-tab").removeClass('active');
                $("#a-md-tab").removeClass('active');
                
                $scope.stepNo = 4;
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });
            
        } else {
            waitingDialog.hide();
            //Kindly fill all fields of Shipping details to enable us ship your jewellery.
            $("#fillShipDetail").modal('show');

            $timeout(function () {
                $("#fillShipDetail").modal('hide');
            }, 2000);
        }

    }


    $scope.confirmOrder = function (total) {
        if ($rootScope.authenticated) {
            $scope.ship_address.name = $rootScope.name;
            $scope.ship_address.phone = $rootScope.phone;

            $scope.placeOrder();

        } else {
            $scope.stepNo = 2;
            $("#b").addClass('active');
            $("#c").removeClass('active');
            $("#a").removeClass('active');
            $("#d").removeClass('active');
            $("#b-tab").addClass('active');
            $("#c-tab").removeClass('active');
            $("#d-tab").removeClass('active');
            $("#a-tab").removeClass('active');
            $("#b-md").addClass('active');
            $("#c-md").removeClass('active');
            $("#a-md").removeClass('active');
            $("#d-md").removeClass('active');
            $("#b-md-tab").addClass('active');
            $("#c-md-tab").removeClass('active');
            $("#d-md-tab").removeClass('active');
            $("#a-md-tab").removeClass('active');
            $(".progress-bar").animate({
                width: '32%'
            }, 800);

        }
    }
    
    $scope.isCouponApplied = false;
    $scope.applyCode = function () {
        
        waitingDialog.show();
        
        $scope.goldcoincount = 0;
        $scope.othercount = 0;
        $scope.cartlen = $rootScope.cart.length;
        $scope.cflag = false;
        if($rootScope.cart != null)
        {
            for (var i = 0; i < $rootScope.cart.length; i++) 
            {
                if($rootScope.cart[i].item.category === "Goldcoins")
                {
                    $scope.goldcoincount = $scope.goldcoincount + 1;
                } 
                else
                {
                    $scope.othercount = $scope.othercount + 1;
                }
            }

            if($scope.goldcoincount > 0 && $scope.othercount > 0)
            {
                $scope.cflag = true;
            }
            else if($scope.goldcoincount > 0 && $scope.othercount == 0)
            {
                $scope.cflag = false;
            }
            else if($scope.goldcoincount == 0 && $scope.othercount > 0)
            {
                $scope.cflag = true;
            }
            else if($scope.goldcoincount > 0)
            {
                $scope.cflag = false;
            }
            else if($scope.othercount > 0)
            {
                $scope.cflag = true;
            }
            
            if($scope.cflag == false)
            {
                $scope.code = null;
                $scope.ccode = null;
                $("#GoldcoinsPromoErr").modal('show');

                $timeout(function () {
                    $("#GoldcoinsPromoErr").modal('hide');
                }, 3000);
                waitingDialog.hide();
            }
            else
            {
        
                if (!$scope.isCouponApplied) 
                {
            
                    $http({
                        method: 'GET',
                        url : API_URL + 'getCoupons'
                    }).then(function successCallback(response){
                        waitingDialog.hide();
                        $scope.coupons = response.data;
                        angular.forEach($scope.coupons, function (item, index) {
                            if (!$scope.isCouponApplied) {
                                if ($scope.code == item.coupon_code) {

                                    if ($scope.total > item.min_amount) {
                                        $scope.codeInt = parseInt(item.coupon_value);
                                        $scope.total = $scope.total - $scope.codeInt;
                                        $scope.isCouponApplied = true;
                                        $scope.disp_code = item.coupon_code;

                                    } else {

                                        $("#VC500").modal('show');

                                        $timeout(function () {
                                            $("#VC500").modal('hide');
                                        }, 5000);

                                    }
                                }
                            }
                        })

                        if (!$scope.isCouponApplied) {
                            $scope.code = null;
                            $("#WrongPromoCode").modal('show');

                            $timeout(function () {
                                $("#WrongPromoCode").modal('hide');
                            }, 5000);
                        }
                    },function errorCallback(response){
                        console.log(response.data);
                        waitingDialog.hide();
                    });
                }
            }
        }
        waitingDialog.hide();
    }

    $('.promocode').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $scope.applyCode();
            $scope.$apply();
        }
    });

    $scope.$watch('total', function () {
        if ($scope.isCouponApplied) {
            
            $http({
                method: 'GET',
                url : API_URL + 'getCoupons'
            }).then(function successCallback(response){
                $scope.coupons = response.data;
                angular.forEach($scope.coupons, function (item, index) {

                    if ($scope.code == item.coupon_code) {
                        if (!$scope.isCouponApplied) {
                            if ($scope.total < parseInt(item.min_amount)) {
                                $scope.codeInt = parseInt(item.coupon_value);
                                // $scope.total = $scope.total + $scope.codeInt;
                                $scope.isCouponApplied = false;
                                $scope.code = null;
                            }
                        }
                    }
                })
            },function errorCallback(response){
                console.log(response.data);
            }); 
        }

    }, true);


    $scope.removeCode = function () {
        
        $http({
            method: 'GET',
            url : API_URL + 'getCoupons'
        }).then(function successCallback(response){
            $scope.coupons = response.data;
            angular.forEach($scope.coupons, function (item, index) {

                if ($scope.code == item.coupon_code) {

                    if ($scope.total > parseInt(item.min_amount)) {
                        $scope.codeInt = parseInt(item.coupon_value);
                        $scope.total = $scope.total + $scope.codeInt;
                        $scope.isCouponApplied = false;
                        $scope.code = null;
                    }
                }
            })
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.isGift = "0";
    $scope.order = {
        promo: null,
        total: null,
        uid: $rootScope.uid,
        items: $rootScope.cart,
        comment: null,
        isGift: $scope.isGift
    }
    
    $scope.comment = "";
    
    $scope.placeOrder = function () {
        
        $scope.order = {
            promo: $scope.code,
            total: $scope.total,
            uid: $rootScope.uid,
            items: $rootScope.cart,
            comment: $scope.comment,
            isGift: $scope.isGift == "1" ? "1" : "0"
        }
        
        $http({
            method: 'POST',
            url : API_URL + 'addOrder',
            data: {
                order: JSON.stringify($scope.order)
            }
        }).then(function successCallback(response){
            $scope.orderId = response.data[0].id;
            $scope.getAddresses();
            $("#c").addClass('active');
            $("#d").removeClass('active');
            $("#a").removeClass('active');
            $("#b").removeClass('active');
            $("#c-tab").addClass('active');
            $("#d-tab").removeClass('active');
            $("#b-tab").removeClass('active');
            $("#a-tab").removeClass('active');

            $("#c-md").addClass('active');
            $("#d-md").removeClass('active');
            $("#a-md").removeClass('active');
            $("#b-md").removeClass('active');
            $("#c-md-tab").addClass('active');
            $("#d-md-tab").removeClass('active');
            $("#b-md-tab").removeClass('active');
            $("#a-md-tab").removeClass('active');
            $(".progress-bar").animate({
                width: '58%'
            }, 800);

            $(".progress-bar").animate({
                left: '35%'
            }, 800);
            $scope.stepNo = 3;
        },function errorCallback(response){
            console.log(response.data);
        });

    }


    $scope.removeFromCart = function (index) {

        var cart = localStorage.getItem('vivo-cart');

        var items = JSON.parse(cart);
        
        var p = items[index];

        items.splice(index, 1);
        localStorage.setItem('vivo-cart', JSON.stringify(items));

        $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
        
        if($rootScope.uid != null && $rootScope.uid != ''){
            $http({
                method: 'POST',
                url : API_URL + 'removeFromCart',
                params : {
                    uid:$rootScope.uid,
                    item:JSON.stringify(p.item)  
                }
            }).then(function successCallback(response){
            },function errorCallback(response){
                console.log(response.data);
            });
        }
        $scope.isCouponApplied = false;
        $scope.code = null;

    }
    
    $scope.$watch('cart', function (newValue, oldValue) {
        $scope.total = 0;
        for (var i = 0; i < $rootScope.cart.length; i++) {
            $scope.total = $scope.total + ($rootScope.cart[i].item.price_after_discount * $rootScope.cart[i].quantity);
        }
    }, true);


    $scope.getOrderDetails = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getOrderDetails',
            params : {id:$scope.orderId}
        }).then(function successCallback(response){
            $scope.order = response.data[0];
            $scope.ship = response.data[1];
            $scope.products = JSON.parse(response.data[0].items);
        },function errorCallback(response){
            console.log(response.data);
            //Oops! We are facing some error fetching your order for you. Just give us some time and we will be up and running.
            $("#orderfetcherr").modal('show');

            $timeout(function () {
                $("#orderfetcherr").modal('hide');
            }, 2000);
        });
    }

    $scope.isCod = "0";

    $scope.finalConfirmation = function (amt, $event) {
        waitingDialog.show();

        //Oops! We are facing some error placing the order for you. Just give us some time and we will be up and running.
        if ($scope.isCod == "1") {
        
            $http({
                method: 'GET',
                url : API_URL + 'confirmOrder',
                params : {
                    id:$scope.orderId,
                    isCod:1,
					isGift: $scope.isGift == "1" ? "1" : "0",
                    txid:'NA',
                    email:$rootScope.email,
                    name:$rootScope.name
                }
            }).then(function successCallback(response){            
                $("#ordersuccess").modal('show');

                $timeout(function () {
                    $("#ordersuccess").modal('hide');
                }, 2000);

                localStorage.setItem('vivo-cart', "");
                $rootScope.cart = null;
                window.location.href = "p-paymentsuccess.php?id=" + $scope.orderId;
                waitingDialog.hide();

            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
                //Oops! We are facing some error placing the order for you. Just give us some time and we will be up and running.
                $("#orderfetcherr").modal('show');

                $timeout(function () {
                    $("#orderfetcherr").modal('hide');
                }, 2000);
            });
            
        } else {
            var options = {

                "key": "rzp_live_ZWUbbMT3Ltg2Hs",
                // "key": "rzp_test_c9GtkbIBKHKLGS",
                "amount": (amt * 100).toString(),
                "name": "Vivocarat Retail Pvt Ltd",
                "description": "Online Jewellery Purchase payment",
                "image": "images/checkout/icons/vivo-logo.png",
                "handler": function (response) {
                  
                $http({
                    method: 'GET',
                    url : API_URL + 'confirmOrder',
                    params : {
                        id:$scope.orderId,
                        isCod:0,
						isGift: $scope.isGift == "1" ? "1" : "0",
                  txid:response.razorpay_payment_id,
                        email:$rootScope.email,
                        name:$rootScope.name
                    }
                }).then(function successCallback(response){                
                    //Your order has been placed successfully.

                    $("#ordersuccess").modal('show');

                    $timeout(function () {
                        $("#ordersuccess").modal('hide');
                    }, 2000);

                    localStorage.setItem('vivo-cart', "");
                    $rootScope.cart = null;
                    window.location.href = "p-paymentsuccess.php?id=" + $scope.orderId;
					waitingDialog.hide();
                },function errorCallback(response){
                    console.log(response.data);
                    waitingDialog.hide();
                    //Oops! We are facing some error placing the order for you. Just give us some time and we will be up and running.
                    $("#orderfetcherr").modal('show');

                    $timeout(function () {
                        $("#orderfetcherr").modal('hide');
                    }, 2000);
                });    
                    
                },
                "prefill": {

                    "email": $rootScope.email != null ? $rootScope.email : "",
                    "contact": $rootScope.phone != null ? $rootScope.phone : ""
                },
                "notes": {
                    "user_id": $rootScope.uid != null ? $rootScope.uid : "Not Logged in"
                },
                "theme": {
                    "color": "#E62739"
                }
            };
            waitingDialog.hide();
            var rzp1 = new Razorpay(options);
            rzp1.open();
            waitingDialog.hide();
            $event.preventDefault();

        }
    }
    
}]);