var editacapp = angular.module("vivoEditAccount", ['vivoCommon']);

editacapp.controller('editAccountCtrl',['$scope','$rootScope','$location','$http','$timeout','API_URL', function ($scope, $rootScope, $location, $http, $timeout, API_URL) {
           
    $scope.updateAccountDetails = function () {
        $scope.address = {
            email: $scope.email,
            uid: $rootScope.uid,
            addId: $scope.ad[0].address_id,
            name: $scope.name,
            phone: $scope.phone,
            dob: $scope.dob,
            first_line: $scope.first_line,

            city: $scope.city,
            state: $scope.state,
            pin: $scope.pin,
            country: $scope.country

        }
        
        $http({
            method: 'POST',
            url : API_URL + 'updateAccountDetails',
            params : {address: JSON.stringify($scope.address)}
        }).then(function successCallback(response){
            window.location.href = "p-account.php";

        },function errorCallback(response){
            console.log(response.data);
        });    
    }
    
    $scope.getAccountDetails = function () {
        
        $http({
            method: 'GET',
            url : API_URL + 'getAccountDetails',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            $scope.ad = response.data;
            $scope.name = $scope.ad[0].name;
            $scope.dob = $scope.ad[0].dob;
            $scope.email = $scope.ad[0].email;
            $scope.phone = $scope.ad[0].phone;
            $scope.first_line = $scope.ad[0].first_line;

            $scope.city = $scope.ad[0].city;
            $scope.state = $scope.ad[0].state;
            $scope.pin = $scope.ad[0].pin;
            $scope.country = $scope.ad[0].country;
            $scope.isLoaded = true;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    $rootScope.$on('loadAccountDetailsEvent', function () {
        $scope.getAccountDetails();
    });
    
}]);