var compareapp = angular.module("vivoCompare", ['vivoCommon']);

compareapp.controller('compareCtrl',['$scope','$rootScope','$http','API_URL', function ($scope, $rootScope, $http, API_URL) {
    $scope.p = $rootScope.compare;
    if ($rootScope.compare !== undefined && $rootScope.compare !== null && $rootScope.compare.length > 1) {
        $rootScope.compare = null;
    }
    localStorage.removeItem('vivo-compare');     
    
}]);