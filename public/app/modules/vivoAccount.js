var accountapp = angular.module("vivoAccount", ['vivoCommon']);

accountapp.controller('accountCtrl',['$scope','$rootScope','$http','$timeout','API_URL', function ($scope, $rootScope, $http, $timeout, API_URL) {

    $scope.getAccountDetails = function () {
        
        $http({
            method: 'GET',
            url : API_URL + 'getAccountDetails',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            $scope.ad = response.data;
            $scope.isLoaded = true;
        },function errorCallback(response){
            console.log(response.data);
            //Oops! We are facing some error fetching your account details for you. Just give us some time and we will be up and running
            $("#acfetcherr").modal('show');

            $timeout(function () {
                $("#acfetcherr").modal('hide');
            }, 2000);
        });
    }
    
    $rootScope.$on('loadAccountDetailsEvent', function () {
        $scope.getAccountDetails();
    });
    
}]);