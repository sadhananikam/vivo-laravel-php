var contactapp = angular.module("vivoContactus", ['vivoCommon']);

contactapp.controller('contactusCtrl',['$scope','$rootScope','$timeout','$location','$http','Data','$window','API_URL', function ($scope, $rootScope, $timeout, $location, $http, Data, $window,API_URL) {
    
    $scope.resetContact = function () {
            $scope.contact = {};
            angular.copy({}, $scope.contactForm);
    }
    $scope.resetContact();
     
    $scope.saveContactform = function(){
        waitingDialog.show();
        var url = API_URL + 'contactus';
        
        $http({
            method: 'POST',
            url: url, 
            data: $.param($scope.contact),
            headers: { 'Content-type':'application/x-www-form-urlencoded' }           
        })
        .then(function successCallback(response){

            if (response.data.success == false) {
                // if not successful, bind errors to error variables
                var msg = '';
                if(response.data.errors.name)
                {
                    msg = msg + response.data.errors.name + "\n";
                }
                
                if(response.data.errors.email)
                {
                    msg = msg + response.data.errors.email + "\n";
                }
                
                if(response.data.errors.phone)
                {
                    msg = msg + response.data.errors.phone + "\n";
                }
                
              if(response.data.errors.errorMessage)
                {
                    msg = msg + response.data.errors.errorMessage;
                }
                
                $rootScope.responseCUMsg = msg;
                $('#contactusMsg').modal('show');
                $timeout(function () {
                    $('#contactusMsg').modal('hide');

                }, 2000);
                
                waitingDialog.hide();
            } 
            else {
                
                $rootScope.responseCUMsg = response.data.successMessage;
                $('#contactusMsg').modal('show');
                $timeout(function () {
                    $('#contactusMsg').modal('hide');

                }, 2000);
                
                waitingDialog.hide();
                $scope.resetContact();
            }
        },function errorCallback(response){
            console.log(response.data);
            
            $rootScope.responseCUMsg = 'An error has occured. Please check the log for details';
            $('#contactusMsg').modal('show');
            $timeout(function () {
                $('#contactusMsg').modal('hide');

            }, 2000);
            waitingDialog.hide();
        });
    };    
    
}]);