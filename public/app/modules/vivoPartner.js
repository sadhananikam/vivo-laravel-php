var partnerapp = angular.module("vivoPartner", ['vivoCommon']);

partnerapp.controller('partnerCtrl',['$scope','$rootScope','$timeout','$location','$http','Data','$window','API_URL', function ($scope, $rootScope, $timeout, $location, $http, Data, $window,API_URL) {
    
    $scope.resetPartner = function () {
            $scope.partner = {};
            angular.copy({}, $scope.partnerForm);
    }
    $scope.resetPartner();
     
    $scope.savePartnerform = function(){
        waitingDialog.show();
        var url = API_URL + 'partner';

        $http({
            method: 'POST',
            url: url, 
            data: $.param($scope.partner),
            headers: { 'Content-type':'application/x-www-form-urlencoded' }           
        })
        .then(function successCallback(response){

            if (response.data.success == false) {
                // if not successful, bind errors to error variables
                var msg = '';
                if(response.data.errors.name)
                {
                    msg = msg + response.data.errors.name + "\n";
                }
                
                if(response.data.errors.email)
                {
                    msg = msg + response.data.errors.email + "\n";
                }
                
                if(response.data.errors.phone)
                {
                    msg = msg + response.data.errors.phone + "\n";
                }
                
              if(response.data.errors.errorMessage)
                {
                    msg = msg + response.data.errors.errorMessage;
                }
                
                $rootScope.responsePartnerMsg = msg;
                $('#partnerMsg').modal('show');
                $timeout(function () {
                    $('#partnerMsg').modal('hide');

                }, 2000);
                
                waitingDialog.hide();
            } 
            else {
                
                $rootScope.responsePartnerMsg = response.data.successMessage;
                $('#partnerMsg').modal('show');
                $timeout(function () {
                    $('#partnerMsg').modal('hide');

                }, 2000);
                waitingDialog.hide();
                $scope.resetPartner();
            }
        },function errorCallback(response){
            console.log(response.data);
            $rootScope.responsePartnerMsg = 'An error has occured. Please check the log for details';
            $('#partnerMsg').modal('show');
            $timeout(function () {
                $('#partnerMsg').modal('hide');

            }, 2000);
            waitingDialog.hide();
        });
    };    
    
}]);