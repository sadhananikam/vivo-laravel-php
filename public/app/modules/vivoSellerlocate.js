var sellerlocateapp = angular.module("vivoSellerlocate", ['vivoCommon']);

sellerlocateapp.controller('sellerlocateCtrl',['$scope','$rootScope','$location','$http','Data','$window','$timeout','API_URL', function ($scope, $rootScope, $location, $http, Data, $window, $timeout,API_URL) {
    $("#sellernotfound").hide(); 
    $scope.resetEnquiry = function () {
            $scope.sellerenquiry = {};
            angular.copy({}, $scope.sellerenquiryForm);
    }
    $scope.resetEnquiry();
    
    var alldata = [];
    
    function initMap() {
        $http({
            method: 'GET',
            url : API_URL + 'getsellerlocation'
        }).then(function successCallback(response){
            document.getElementById("map").innerHTML = "";  
            $("#sellernotfound").hide(); 
            alldata = [];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 6,
                center: new google.maps.LatLng(20.593684, 78.962880),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow({});
            var bounds = new google.maps.LatLngBounds();
            var geocoder = new google.maps.Geocoder();

            var marker, i;
            
//            console.log(response.data);
            $scope.allseller = response.data;

            if(response.data)
            {
                for (var i = 0; i < $scope.allseller.length; i++)
                {
                    alldata.push({address:$scope.allseller[i].address + ' ' + $scope.allseller[i].pincode,lat:$scope.allseller[i].latitude,long:$scope.allseller[i].longitude,name:$scope.allseller[i].name});
                }
            }

            if(alldata.length > 0)
            {
               for( i = 0; i < alldata.length; i++ ) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(alldata[i].lat, alldata[i].long),
                        map: map,
                        icon: "images/icons/map.png"
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            infowindow.setContent('<div><strong>'+alldata[i].name+'</strong><br>'+alldata[i].address);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    bounds.extend(new google.maps.LatLng(alldata[i].lat, alldata[i].long));
                    map.fitBounds(bounds);
               }
            }
            else
            {
                    geocoder.geocode({
                      'address': 'India'
                   }, 
                   function(results, status) {
                      if(status == google.maps.GeocoderStatus.OK) {
                         map.setCenter(results[0].geometry.location);
                         map.fitBounds(results[0].geometry.viewport);
                      }
                   });
            }
        },function errorCallback(response){
            console.log(response.data);
        });    
    }
    initMap();
   
    
    $scope.getallseller = function(){
        $http({
            method: 'GET',
            url : API_URL + 'getsellerlocation'
        }).then(function successCallback(response){
            console.log(response.data);
            $scope.sellers = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });
    }   
    $scope.getallseller();
    
    $scope.getSellerDetailBySearch = function (searchtext) {
        
        waitingDialog.show();
        if (searchtext == null || searchtext == "" ) 
        {
            initMap();
            $scope.getallseller();
        } 
        else 
        { 
            $http({
                method: 'POST',
                url : API_URL + 'getSellerDetailBySearch',
                params : {
                        searchtext:searchtext
                    }
            }).then(function successCallback(response){
                console.log(response.data);
//                $scope.searchtext = '';
                
                /* set seller*/
                $scope.sellers = response.data;
                
                /* map code start*/
                document.getElementById("map").innerHTML = "";  
                $("#sellernotfound").hide(); 
                alldata = [];
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 6,
                    center: new google.maps.LatLng(20.593684, 78.962880),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var infowindow = new google.maps.InfoWindow({});
                var bounds = new google.maps.LatLngBounds();
                var geocoder = new google.maps.Geocoder();

                var marker, i;
                
                $scope.allseller = response.data;

                if(response.data)
                {
                    for (var i = 0; i < $scope.allseller.length; i++)
                    {
                        alldata.push({address:$scope.allseller[i].address + ' ' + $scope.allseller[i].pincode,lat:$scope.allseller[i].latitude,long:$scope.allseller[i].longitude,name:$scope.allseller[i].name});
                    }
                }

                if(alldata.length > 0)
                {
                   for( i = 0; i < alldata.length; i++ ) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(alldata[i].lat, alldata[i].long),
                            map: map,
                            icon: "images/icons/map.png"
                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent('<div><strong>'+alldata[i].name+'</strong><br>'+alldata[i].address);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        bounds.extend(new google.maps.LatLng(alldata[i].lat, alldata[i].long));
                        map.fitBounds(bounds);
                   }
                }
                else
                {
                    $("#sellernotfound").show();
                        geocoder.geocode({
                          'address': 'India'
                       }, 
                       function(results, status) {
                          if(status == google.maps.GeocoderStatus.OK) {
                             map.setCenter(results[0].geometry.location);
                             map.fitBounds(results[0].geometry.viewport);
                          }
                       });
                } 
                
            },function errorCallback(response){
                console.log(response.data);
                $('#customizeError').modal('show');
                $timeout(function () {
                    $('#customizeError').modal('hide');
                }, 2000);
            });   
        }
        waitingDialog.hide();
    }
    
    
    $scope.saveSellerLocationEnquiry = function(){
        waitingDialog.show();
        var url = API_URL + 'saveSellerLocationEnquiry';

        $http({
            method: 'POST',
            url: url, 
            data: $.param($scope.sellerenquiry),
            headers: { 'Content-type':'application/x-www-form-urlencoded' }           
        })
        .then(function successCallback(response){

            if (response.data.success == false) {
                // if not successful, bind errors to error variables
                var msg = '';
                if(response.data.errors.name)
                {
                    msg = msg + response.data.errors.name + "\n";
                }
                
                if(response.data.errors.email)
                {
                    msg = msg + response.data.errors.email + "\n";
                }
                
                if(response.data.errors.phone)
                {
                    msg = msg + response.data.errors.phone + "\n";
                }
                
                if(response.data.errors.location)
                {
                    msg = msg + response.data.errors.phone + "\n";
                }
                
              if(response.data.errors.errorMessage)
                {
                    msg = msg + response.data.errors.errorMessage;
                }
                
                $rootScope.responsePartnerMsg = msg;
                $('#partnerMsg').modal('show');
                $timeout(function () {
                    $('#partnerMsg').modal('hide');

                }, 2000);
                
                waitingDialog.hide();
            } 
            else {
                
               $rootScope.responsePartnerMsg = response.data.successMessage;
                $('#partnerMsg').modal('show');
                $timeout(function () {
                    $('#partnerMsg').modal('hide');

                }, 2000);
                $("#sellernotfound").hide();
                $scope.searchtext = '';
                initMap();
                $scope.getallseller();
                waitingDialog.hide();
                $scope.resetEnquiry();
            }
        },function errorCallback(response){
            console.log(response.data);
            $rootScope.responsePartnerMsg = 'An error has occured. Please check the log for details';
            $('#partnerMsg').modal('show');
            $timeout(function () {
                $('#partnerMsg').modal('hide');

            }, 2000);
            waitingDialog.hide();
        });
    };    
}]);