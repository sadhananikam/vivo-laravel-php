var ordersapp = angular.module("vivoOrders", ['vivoCommon']);

ordersapp.controller('ordersCtrl',['$scope','$rootScope','$http','$timeout','API_URL', function ($scope, $rootScope, $http, $timeout, API_URL) {
    $scope.isLoaded = false;
    $scope.buyNow = function (o) {
        var products = o.products;
        var check = false;
        var k = 1;
        for (var i = 0; i < products.length; i++) 
        {
            var id= products[i].item.id;
            var p = null;
            
        $http({
            method: 'GET',
            url : API_URL + 'getProductDetail',
            params : {id:id}
        }).then(function successCallback(response){
            p = response.data[0];
            
            if(p.bracelet_size == '')
            {
                p.bracelet_size = null;
            }
            if(p.ring_size == '')
            {
                p.ring_size = null;
            }
            if(p.bangle_size == '')
            {
                p.bangle_size = null;
            }
            
            if (p.category == 'Bracelets') {
            if (p.bracelet_size == null) {

                //Please select a braclet size
                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {
                check=true;
                $rootScope.addToCart(p);
                
                if(k == products.length){
                    window.location.href = "p-checkout.php";
                }
            }

        } else if (p.category == 'Rings') {
            if (p.ring_size == null) {

                //Please select a ring size
                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {
                check=true;
                $rootScope.addToCart(p);
                
                if(k == products.length){
                    window.location.href = "p-checkout.php";
                }
            }

        } else if (p.category == 'Bangles') {
            if (p.bangle_size == null) {

                //Please select a bangle size
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {
                check=true;
                $rootScope.addToCart(p);
                
                if(k == products.length){
                    window.location.href = "p-checkout.php";
                }
            }

        } else {
            check=true;
            $rootScope.addToCart(p);
            
            if(k == products.length){
                window.location.href = "p-checkout.php";
            }
        }
            k = k+1;
            
        },function errorCallback(response){
            console.log(response.data);
        }); 
        }
    }

    $scope.removeProductFromOrder = function (oId, index, pIndex) {

        var products = JSON.parse($scope.orderHistoryList[pIndex].items);
        products.splice(index, 1);
        $scope.order = {
            oid: oId,
            items: products,
        }
        
        $http({
            method: 'POST',
            url : API_URL + 'removeProductFromOrder',
            params : {order: JSON.stringify($scope.order)}
        }).then(function successCallback(response){
            //Updated order successfully
            $("#orderupdate").modal('show');

            $timeout(function () {
                $("#orderupdate").modal('hide');
            }, 2000);

            $scope.getOrderHistory();
        },function errorCallback(response){
            console.log(response.data);
        });
    }

    $scope.returnOrder = function (id) {
        $scope.rid = id;
        $("#returnModal").modal('show');
    }
    
    $scope.returnOrderConfirm = function (id, reason) {
        
        $http({
            method: 'GET',
            url : API_URL + 'returnOrder',
            params : {oid:id,reason:reason}
        }).then(function successCallback(response){
            //Return has been placed successfully
            $("#returnsuccess").modal('show');

            $timeout(function () {
                $("#returnsuccess").modal('hide');
            }, 2000);

            $("#returnModal").modal('hide');
            $scope.getOrderHistory();

        },function errorCallback(response){
            console.log(response.data);
            //Oops! We are facing some error placing a return for you. Just give us some time and we will be up and running
            $("#returnfail").modal('show');

            $timeout(function () {
                $("#returnfail").modal('hide');
            }, 2000);
        });
        
    }
    
    $scope.cancelOrder = function (id) {
        
        $http({
            method: 'GET',
            url : API_URL + 'cancelOrder',
            params : {oid:id}
        }).then(function successCallback(response){
            //Order has been cancelled successfully
            $("#ordercancel").modal('show');

            $timeout(function () {
                $("#ordercancel").modal('hide');
            }, 2000);

            $scope.getOrderHistory();
        },function errorCallback(response){
            console.log(response.data);
            //Oops! We are facing some error fetching your order for you. Just give us some time and we will be up and running
            $("#orderfetcherr").modal('show');

            $timeout(function () {
                $("#orderfetcherr").modal('hide');
            }, 2000);
        });        
    }
    
    $scope.getOrderHistory = function () {
        
        $http({
            method: 'GET',
            url : API_URL + 'getOrderHistory',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            if (response.data == "") {
                $scope.orderHistoryList = [];
            } else {
                $scope.orderHistoryList = response.data;
                for (var i = 0; i < response.data.length; i++) {
                    $scope.orderHistoryList[i].products = JSON.parse($scope.orderHistoryList[i].items);
                }
            }
            $scope.isLoaded = true;

        },function errorCallback(response){
            console.log(response.data);
            //Oops! We are facing some error fetching your order history for you. Just give us some time and we will be up and running
            $("#orderfetchhisterr").modal('show');

            $timeout(function () {
                $("#orderfetchhisterr").modal('hide');
            }, 2000);

        });
    }
    
    $rootScope.$on('loadOrderHistoryEvent', function () {
        $scope.getOrderHistory();
    });
    
}]);