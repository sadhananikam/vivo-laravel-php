var paymentSuccessapp = angular.module("vivoPaymentSuccess", ['vivoCommon']);

paymentSuccessapp.controller('paymentSuccessCtrl',['$scope','$http','$timeout','API_URL', function ($scope, $http, $timeout, API_URL) {

    $scope.id = getUrlParameter('id');


    $scope.getOrderDetails = function () {
        
        $http({
            method: 'GET',
            url : API_URL + 'getOrderDetails',
            params : {id:$scope.id}
        }).then(function successCallback(response){
            $scope.order = response.data[0];
            $scope.ship = response.data[1];
            $scope.products = JSON.parse(response.data[0].items);
        },function errorCallback(response){
            console.log(response.data);
            //Oops! We are facing some error fetching your order for you. Just give us some time and we will be up and running
            $("#facingerror").modal('show');

            $timeout(function () {
                $("#facingerror").modal('hide');
            }, 2000);
        });
    }
    $scope.getOrderDetails();
    
}]);