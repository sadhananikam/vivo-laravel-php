Date.prototype.AddDays = function (noOfDays) {
    this.setTime(this.getTime() + (noOfDays * (1000 * 60 * 60 * 24)));
    return this;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    // Creating modal dialog's DOM
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m" style="width: 300px;">' +
        '<div class="modal-content"style="background-color: rgba(255, 0, 0, 0);box-shadow: none;border: none;">' +

        '<div class="modal-body text-center">' +
        '<svg width="60px" height="60px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ring"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><circle cx="50" cy="50" r="47.5" stroke-dasharray="193.99334635916975 104.45795573186061" stroke="#e62739 " fill="none" stroke-width="5"><animateTransform attributeName="transform" type="rotate" values="0 50 50;180 50 50;360 50 50;" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite" begin="0s"></animateTransform></circle></svg>' +
        '</div>' +
        '</div></div></div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Please wait';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };

})(jQuery);

function logoutJquery() {
    
    $.get("api/v1/logout", function (data) {

        location.reload();
        FB.init({
            appId: '235303226837165',
            status: true,

            cookie: true,

            xfbml: true
        });

        FB.getLoginStatus(function (response) {
            if (response && response.status === 'connected') {
                FB.logout(function (response) {
                    location.reload(true);
                });
            }
        });

    });
}


var app = angular.module("vivoCommon", ['ui.router', 'ngSanitize', 'ui.select', 'ngAnimate', 'toaster', 'ui-rangeSlider', 'kendo.directives', 'googleplus', 'satellizer', 'tagged.directives.infiniteScroll']).constant('API_URL','api/v1/');

app.config(['$stateProvider','$urlRouterProvider','$locationProvider','$authProvider','GooglePlusProvider',function($stateProvider,$urlRouterProvider,$locationProvider,$authProvider,GooglePlusProvider){
        
    
        // Optional: For client-side use (Implicit Grant), set responseType to 'token'
        $authProvider.facebook({
            clientId: '235303226837165',
            responseType: 'token'
        });

        $authProvider.google({
            clientId: '491733683292-ic3mvgnhbq4ek4k5gcmgfvhnout2ejvb.apps.googleusercontent.com'
        });

        $authProvider.github({
            clientId: 'GitHub Client ID'
        });

        $authProvider.linkedin({
            clientId: 'LinkedIn Client ID'
        });

        $authProvider.instagram({
            clientId: 'Instagram Client ID'
        });

        $authProvider.yahoo({
            clientId: 'Yahoo Client ID / Consumer Key'
        });

        $authProvider.live({
            clientId: 'Microsoft Client ID'
        });

        $authProvider.twitch({
            clientId: 'Twitch Client ID'
        });

        $authProvider.bitbucket({
            clientId: 'Bitbucket Client ID'
        });

        // No additional setup required for Twitter

        $authProvider.oauth2({
            name: 'foursquare',
            url: '/auth/foursquare',
            clientId: 'Foursquare Client ID',
            redirectUri: window.location.origin,
            authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate',
        });


        // Facebook
        $authProvider.facebook({
            name: 'facebook',
            url: '/api/v1/oauth_login',
            authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
            redirectUri: window.location.origin,
            requiredUrlParams: ['display', 'scope'],
            scope: ['email'],
            scopeDelimiter: ',',
            display: 'popup',
            type: '2.0',
            popupOptions: {
                width: 580,
                height: 400
            }
        });

        // Google
        $authProvider.google({
            url: '/api/v1/oauth_login_google',
            authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
            redirectUri: window.location.origin,
            requiredUrlParams: ['scope'],
            optionalUrlParams: ['display'],
            scope: ['profile', 'email'],
            scopePrefix: 'openid',
            scopeDelimiter: ' ',
            display: 'popup',
            type: '2.0',
            popupOptions: {
                width: 452,
                height: 633
            }
        });

        // GitHub
        $authProvider.github({
            url: '/auth/github',
            authorizationEndpoint: 'https://github.com/login/oauth/authorize',
            redirectUri: window.location.origin,
            optionalUrlParams: ['scope'],
            scope: ['user:email'],
            scopeDelimiter: ' ',
            type: '2.0',
            popupOptions: {
                width: 1020,
                height: 618
            }
        });

        // Instagram
        $authProvider.instagram({
            name: 'instagram',
            url: '/auth/instagram',
            authorizationEndpoint: 'https://api.instagram.com/oauth/authorize',
            redirectUri: window.location.origin,
            requiredUrlParams: ['scope'],
            scope: ['basic'],
            scopeDelimiter: '+',
            type: '2.0'
        });

        // LinkedIn
        $authProvider.linkedin({
            url: '/auth/linkedin',
            authorizationEndpoint: 'https://www.linkedin.com/uas/oauth2/authorization',
            redirectUri: window.location.origin,
            requiredUrlParams: ['state'],
            scope: ['r_emailaddress'],
            scopeDelimiter: ' ',
            state: 'STATE',
            type: '2.0',
            popupOptions: {
                width: 527,
                height: 582
            }
        });

        // Twitter
        $authProvider.twitter({
            url: '/auth/twitter',
            authorizationEndpoint: 'https://api.twitter.com/oauth/authenticate',
            redirectUri: window.location.origin,
            type: '1.0',
            popupOptions: {
                width: 495,
                height: 645
            }
        });

        // Twitch
        $authProvider.twitch({
            url: '/auth/twitch',
            authorizationEndpoint: 'https://api.twitch.tv/kraken/oauth2/authorize',
            redirectUri: window.location.origin,
            requiredUrlParams: ['scope'],
            scope: ['user_read'],
            scopeDelimiter: ' ',
            display: 'popup',
            type: '2.0',
            popupOptions: {
                width: 500,
                height: 560
            }
        });

        // Windows Live
        $authProvider.live({
            url: '/auth/live',
            authorizationEndpoint: 'https://login.live.com/oauth20_authorize.srf',
            redirectUri: window.location.origin,
            requiredUrlParams: ['display', 'scope'],
            scope: ['wl.emails'],
            scopeDelimiter: ' ',
            display: 'popup',
            type: '2.0',
            popupOptions: {
                width: 500,
                height: 560
            }
        });

        // Yahoo
        $authProvider.yahoo({
            url: '/auth/yahoo',
            authorizationEndpoint: 'https://api.login.yahoo.com/oauth2/request_auth',
            redirectUri: window.location.origin,
            scope: [],
            scopeDelimiter: ',',
            type: '2.0',
            popupOptions: {
                width: 559,
                height: 519
            }
        });

        // Bitbucket
        $authProvider.bitbucket({
            url: '/auth/bitbucket',
            authorizationEndpoint: 'https://bitbucket.org/site/oauth2/authorize',
            redirectUri: window.location.origin + '/',
            optionalUrlParams: ['scope'],
            scope: ['email'],
            scopeDelimiter: ' ',
            type: '2.0',
            popupOptions: {
                width: 1020,
                height: 618
            }
        });

        // Generic OAuth 2.0
        $authProvider.oauth2({
            name: null,
            url: null,
            clientId: null,
            redirectUri: null,
            authorizationEndpoint: null,
            defaultUrlParams: ['response_type', 'client_id', 'redirect_uri'],
            requiredUrlParams: null,
            optionalUrlParams: null,
            scope: null,
            scopePrefix: null,
            scopeDelimiter: null,
            state: null,
            type: null,
            popupOptions: null,
            responseType: 'code',
            responseParams: {
                code: 'code',
                clientId: 'clientId',
                redirectUri: 'redirectUri'
            }
        });

        // Generic OAuth 1.0
        $authProvider.oauth1({
            name: null,
            url: null,
            authorizationEndpoint: null,
            redirectUri: null,
            type: null,
            popupOptions: null
        });
    
}]);

app.run(function ($rootScope, $location, Data, $stateParams, $http, $window, $timeout, sAuth,API_URL) {
	
	$rootScope.member_new = {
        name: null,
        email: null,
        phone: null,
        password: null
    }
    
    $rootScope.savememberform = function(){
        if ($rootScope.member_new.name == null || $rootScope.member_new.name == "" || $rootScope.member_new.phone == null || $rootScope.member_new.phone == "" || $rootScope.member_new.email == null || $rootScope.member_new.email == "" || $rootScope.member_new.password == null || $rootScope.member_new.password == "") {

            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } else {

            $rootScope.signUp($rootScope.member_new);
        }
    };
        
        Data.get('session').then(function (results) {
            
            if (results.uid) {
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                $rootScope.name = results.name;

                $rootScope.email = results.email;
                $rootScope.phone = results.phone;


                $timeout(function () {
                    if (window.location.pathname == '/p-orders.php') {
                        $rootScope.$broadcast('loadOrderHistoryEvent');

                    }
                    if (window.location.pathname == "/p-account.php" || window.location.pathname == "/p-editaccount.php") {
                        $rootScope.$broadcast('loadAccountDetailsEvent');

                    }
                    if (window.location.pathname == "/p-wishlist.php") {
                        $rootScope.$broadcast('loadWatchlistEvent');
                    }
                    if (window.location.pathname == "/p-confirmOrder.php") {
                        $rootScope.$broadcast('loadConfirmOrderEvent');
                    }
                    if (window.location.pathname == "/p-address.php") {
                        $rootScope.$broadcast('loadAddressEvent');
                    }
                }, 1500);


                if (window.location.pathname == '/p-signup.php' || window.location.pathname == '/p-login.php') {
                    window.location.href = "default.php";
                }
            } else {
                if (window.location.pathname == '/p-wishlist.php' || window.location.pathname == '/p-account.php' || window.location.pathname == '/p-orders.php') {
                    window.location.href = "p-login.php?id=0";
                }
            }
        });

        
//        $rootScope.mouseOverCartIcon = function (i, ii) {
//            $("#" + i + "-" + ii + "-cart").attr("src", 'images/icons/cart-filled.png');
//        }
//
//        $rootScope.mouseOutCartIcon = function (i, ii) {
//            $("#" + i + "-" + ii + "-cart").attr("src", 'images/icons/cart-color.png');
//        }
//        
//        $rootScope.mouseOverShareIcon = function (i, ii) {
//            $("#" + i + "-" + ii + "-share").attr("src", 'images/icons/share-filled.png');
//        }
//        
//        $rootScope.mouseOutShareIcon = function (i, ii) {
//            $("#" + i + "-" + ii + "-share").attr("src", 'images/icons/share-color.png');
//        }
//
//        $rootScope.mouseOverWishIcon = function (i, ii) {
//            $("#" + i + "-" + ii + "-wish").attr("src", 'images/icons/wishlist-filled.png');
//        }
//        
//        $rootScope.mouseOutWishIcon = function (i, ii) {
//            $("#" + i + "-" + ii + "-wish").attr("src", 'images/icons/wishlist.png');
//        }
//
//        $rootScope.cartIcon = 'images/icons/cart.png';
//        $rootScope.goToLogin = function () {
//            window.location.href = "templates/p-login.php?id=c";
//            $('#registerModal').modal('hide');
//        }
        
        $rootScope.couponSignUp = function (coupon) {
            Data.post('couponSignUp', {
                coupon: coupon
            }).then(function (results) {
                Data.toast(results);

            });
        };

        $rootScope.date = new Date();
        $rootScope.isGrid = true;
        $rootScope.setLayout = function (isGrid) {
            $rootScope.isGrid = isGrid;
        }
        $rootScope.addToWatchlist = function (p) {
            if (!$rootScope.authenticated) {

                //Please Login.
                $("#login").modal('show');

                $timeout(function () {
                    $("#login").modal('hide');
                }, 2000);


            } else {
                
                $http({
                    method: 'GET',
                    url : API_URL + 'addToWatchlist',
                    params : {
                            uid:$rootScope.uid,
                            pid:p.id
                        }
                }).then(function successCallback(response){                
                    //Product added to  wishlist.

                    $("#addwish").modal('show');

                    $timeout(function () {
                        $("#addwish").modal('hide');
                    }, 2000);

                },function errorCallback(response){
                    console.log(response.data);
                    //Oops! We are facing some error adding your product to watchlist. Just give us some time and we will be up and running.
                    $("#addwisherr").modal('show');

                    $timeout(function () {
                        $("#addwisherr").modal('hide');
                    }, 2000);
                });                
            }

        }
        
        $rootScope.checkCompare = function () 
        {   
            var compare = localStorage.getItem('vivo-compare');
            
            if (compare != null && compare != "") {
                var items = JSON.parse(compare);
                if (items.length <= 1) {
                    alert('Please select at least two products to compare.');
                }
                else{
                    window.open(
                      'p-compare.php'
                    );
                }
            }        
        }
        
        $rootScope.removeFromCompare = function (index) {

            var cart = localStorage.getItem('vivo-compare');

            var items = JSON.parse(cart);

            items.splice(index, 1);
            localStorage.setItem('vivo-compare', JSON.stringify(items));

            $rootScope.compare = JSON.parse(localStorage.getItem('vivo-compare'));

        }
        $rootScope.removeAllCompare = function () {

            localStorage.removeItem('vivo-compare');

            $rootScope.compare = null;

        }
        $rootScope.compare = JSON.parse(localStorage.getItem('vivo-compare'));
        $rootScope.addToCompare = function (p) {
            var isAdded = false;
            var compare = localStorage.getItem('vivo-compare');
            var isExist = false;
            if (compare != null && compare != "") {
                var items = JSON.parse(compare);
                if (items.length < 4) {

                    if (items[0] && items[0].category != p.category) {
                        isExist = true;
                        $("#diffCompare").modal('show');

                        $timeout(function () {
                            $("#diffCompare").modal('hide');
                        }, 2000);


                    } else {
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].id == p.id) {

                                isExist = true;
                                $("#alreadyAddedToCompare").modal('show');

                                $timeout(function () {
                                    $("#alreadyAddedToCompare").modal('hide');
                                }, 2000);

                            }

                        }
                        if (!isExist) {
                            items.push(p);
                            localStorage.setItem('vivo-compare', JSON.stringify(items));
                            $("#addToCompareSuccess").modal('show');

                            $timeout(function () {
                                $("#addToCompareSuccess").modal('hide');
                            }, 2000);
                        }

                    }

                } else {
                    $("#addToCompareFail").modal('show');

                    $timeout(function () {
                        $("#addToCompareFail").modal('hide');
                    }, 2000);

                }


            } else {
                localStorage.setItem('vivo-compare', JSON.stringify([p]));
            }
            $rootScope.compare = JSON.parse(localStorage.getItem('vivo-compare'));
            //Product has been added to cart successfully.
            $("#addToCompareSuccess").modal('show');

            $timeout(function () {
                $("#addToCompareSuccess").modal('hide');
            }, 2000);

        }

        $rootScope.addToCart = function (p) {
            if (p.category == 'Bracelets') {
                if (p.bracelet_size == null) {

                    //Please select a braclet size
                    $("#BraceletSize").modal('show');

                    $timeout(function () {
                        $("#BraceletSize").modal('hide');
                    }, 2000);
                } else {

                    $rootScope.addCartConfirmed(p);
                }

            } else if (p.category == 'Rings') {
                if (p.ring_size == null) {

                    //Please select a ring size
                    $("#RingSize").modal('show');

                    $timeout(function () {
                        $("#RingSize").modal('hide');
                    }, 2000);
                } else {

                    $rootScope.addCartConfirmed(p);
                }

            } else if (p.category == 'Bangles') {
                if (p.bangle_size == null) {

                    //Please select a bangle size
                    $("#BangleSize").modal('show');

                    $timeout(function () {
                        $("#BangleSize").modal('hide');
                    }, 2000);
                } else {

                    $rootScope.addCartConfirmed(p);
                }

            } else {

                $rootScope.addCartConfirmed(p);
            }


        }

        $rootScope.addCartConfirmed = function (p) {
            $rootScope.feed = null;
            var isAdded = false;
            var cart = localStorage.getItem('vivo-cart');
            if (cart != null && cart != "") {
                var items = JSON.parse(cart);
                for (var i = 0; i < items.length; i++) {
                    if ((items[i].item.VC_SKU == p.VC_SKU) && (items[i].item.VC_SKU_2 == p.VC_SKU_2)) {

                        //The product has already been added to the cart.If you would want to increase the quantity of please do so during checkout
                        $("#addToCartAlready").modal('show');

                        isAdded = true;
                    }
                }
                if (!isAdded) {
                    if(p.quantity !== undefined && p.quantity !== null){
                        items.push({
                            item: p,
                            quantity: p.quantity
                        });
                    }else{
                        items.push({
                            item: p,
                            quantity: 1
                        });
                    }
                    
                    $("#addToCartSuccess").modal('show');

                    $timeout(function () {
                        $("#addToCartSuccess").modal('hide');
                    }, 2000);
                }
                localStorage.setItem('vivo-cart', JSON.stringify(items));
            } else {
                if(p.quantity !== undefined && p.quantity !== null){
                    localStorage.setItem('vivo-cart', JSON.stringify([{
                        item: p,
                        quantity: p.quantity
                    }]));
                }else{
                    localStorage.setItem('vivo-cart', JSON.stringify([{
                        item: p,
                        quantity: 1
                    }]));
                }
                
                $("#addToCartSuccess").modal('show');

                $timeout(function () {
                    $("#addToCartSuccess").modal('hide');
                }, 2000);
            }
            $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
            //Product has been added to cart successfully.
            
            if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                },function errorCallback(response){
                    console.log(response.data);
                });
            }

        }
        
        $rootScope.subQuantity = function (item, index) {
            if ($rootScope.cart[index].quantity == 1) {
                $rootScope.cart.splice(index, 1);
            } else {
                $rootScope.cart[index].quantity--;
            }
            localStorage.setItem('vivo-cart', JSON.stringify($rootScope.cart));
            
            if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
        }
        
        $rootScope.addQuantity = function (item, index) {

            $rootScope.cart[index].quantity++;
            localStorage.setItem('vivo-cart', JSON.stringify($rootScope.cart));
            
            if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
        }
        
        if (localStorage.getItem('vivo-cart') != "" && localStorage.getItem('vivo-cart') != null && localStorage.getItem('vivo-cart') != undefined) {
            $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
        } else {
            $rootScope.cart = null;
        }
        $rootScope.search = function () {
            $location.path('c/q/' + $rootScope.searchText);
        }
        $rootScope.login = {};
        $rootScope.signup = {};
    
        $rootScope.doLogin = function (customer) {       
            waitingDialog.show();     
            $http({
                method: 'POST',
                url : API_URL + 'login',
                params : {
                    customer: JSON.stringify(customer),
                    items: JSON.stringify($rootScope.cart)
                }
            }).then(function successCallback(response){
                waitingDialog.hide();
                var results = response.data;
                $rootScope.response = results.message;
                $('#loggedIn').modal('show');
                $timeout(function () {
                    $('#loggedIn').modal('hide');

                }, 2000);
                $rootScope.from = getUrlParameter('from');
                if (results.status == "success") {
                    if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);
                        
                        var k = 1;
                        for (var i = 0; i < pr.length; i++) {
                            var p_id = pr[i].product_id;
                            var ring_size = pr[i].ring_size;
                            var bangle_size = pr[i].bangle_size;
                            var bracelet_size = pr[i].bracelet_size;
                            var quantity = pr[i].quantity;               

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;

                                $rootScope.addToCart(p);
                                
                                if(k == pr.length){
                                    $rootScope.quantity = null;
                                    if ($rootScope.from == 'c') {
                                        window.location.href = "p-checkout.php";

                                    } else {
                                        window.location.href = "p-home.php";

                                    }
                                    location.reload(true);
                                }
                                k = k+1;
                            
                            },function errorCallback(response){
                                console.log(response.data);
                            });

                        }
                    }
                    else
                    {
                         if ($rootScope.from == 'c') {
                            window.location.href = "p-checkout.php";

                        } else {
                            window.location.href = "p-home.php";

                        }
                        location.reload(true);
                    }
                    
                }
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });
            
        };
    
        $rootScope.doOAuthLogin = function (customer) {
            Data.post('oauth_login', {
                customer: customer
            }).then(function (results) {
                // alert(results.message);
                $("#LoggedIn").modal('show');

                $timeout(function () {
                    $("#LoggedIn").modal('hide');
                }, 2000);
                $rootScope.from = getUrlParameter('from');
                if (results.status == "success") {
                    if ($rootScope.from == 'c') {
                        window.location.href = "p-checkout.php";

                    } else {
                        window.location.href = "p-home.php";

                    }
                    location.reload(true);
                }
            });

        };
        $rootScope.signup = {
            email: '',
            password: '',
            name: '',
            phone: '',
            address: ''
        };
    
        $rootScope.signUp = function (customer) {
            waitingDialog.show();
            
            $http({
                method: 'POST',
                url : API_URL + 'signUp',
                params : {
                    customer: JSON.stringify(customer),
                    items: JSON.stringify($rootScope.cart)
                }
            }).then(function successCallback(response){
                var results = response.data;
                if (results.status == "error") {
                    // alert(results.message);
                    $rootScope.response = results.message;
                    $rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                } else {
                   
                    if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);
                        
                        var k = 1;
                        for (var i = 0; i < pr.length; i++) 
                        {

                            var p_id = pr[i].product_id;
                            var ring_size = pr[i].ring_size;
                            var bangle_size = pr[i].bangle_size;
                            var bracelet_size = pr[i].bracelet_size;
                            var quantity = pr[i].quantity;               

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;

                                $rootScope.addToCart(p);
                                
                                if(k == pr.length){     
                                    location.reload(true);
                                }
                                k = k+1;
                            
                            },function errorCallback(response){
                                console.log(response.data);
                            });

                        }
                    }
                    else
                    {
                        location.reload(true);
                    }       
                }
                waitingDialog.hide();
                
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });
            
        };
        $rootScope.couponSignUp = function (coupon) {
            Data.post('couponSignUp', {
                coupon: coupon
            }).then(function (results) {
                alert(results.message);

            });
        };
        $rootScope.logout = function () {
            Data.get('logout').then(function (results) {
                Data.toast(results);
                $window.location.reload();
            });
        }

        $rootScope.logoutJquery = function() {
            
            var cart = localStorage.getItem('vivo-cart');

            if(cart != '')
            {
                var items = JSON.parse(cart);
                items=[];
            }
            else{
                var items=[];
            }
            
            localStorage.setItem('vivo-cart', JSON.stringify(items));

            $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));

            $.get("api/v1/logout", function (data) {

                location.reload();
                FB.init({
                    appId: '235303226837165',
                    status: true,

                    cookie: true,

                    xfbml: true
                });

                FB.getLoginStatus(function (response) {
                    if (response && response.status === 'connected') {
                        FB.logout(function (response) {
                            location.reload(true);
                        });
                    }
                });

            });
        }

        $rootScope
            .$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    $timeout(function () {
                        waitingDialog.hide();
                    }, 2000);
                    //code for scroll to top of document
                    setTimeout(function () {
                        $(document.body).scrollTop(0);
                    }, 15);

                });
        $rootScope.$on("$stateChangeStart", function (event, next, current) {

            waitingDialog.show();
            
            if (!$window.ga)
                  return;
            
            $window.ga('send', 'pageview', {
                page: $location.url()
            });


            $rootScope.authenticated = false;
        });
    });

app.directive('vivoHeader', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {

                                                  
        },
        link: function ($scope, $rootScope, Data, $location) {



            if (localStorage.getItem('vivo-cart') != "" && localStorage.getItem('vivo-cart') != null && localStorage.getItem('vivo-cart') != undefined) {
                $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
            } else {
                $rootScope.cart = null;
            }

            if (localStorage.getItem('vivo-compare') != "" && localStorage.getItem('vivo-compare') != null && localStorage.getItem('vivo-compare') != undefined) {
                $rootScope.compare = JSON.parse(localStorage.getItem('vivo-compare'));
            } else {
                $rootScope.compare = null;
            }
        },
        templateUrl: 'partials/vivo-header.php'
    };
});

app.directive('vivoHeaderMenu', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
            
            //Sticky header 
            $scope.logo = false;

            $(window).bind('scroll', function () {
                //Need to change the height here as well as in ".fixed class" of vivo-header file to make it work //properly
                var navHeight = 50;
                if ($(window).scrollTop() > navHeight) {
                    $('nav').addClass('fixed');
                    $scope.logo = true;
                    $scope.$apply();

                } else {
                    $('nav').removeClass('fixed');
                    $scope.logo = false;
                    $scope.$apply();
                }
            });

            //END of Sticky header
        },
        link: function ($scope, $rootScope, Data, $location) {
            $(".megamenu").megamenu();
            
            if (localStorage.getItem('vivo-cart') != "" && localStorage.getItem('vivo-cart') != null && localStorage.getItem('vivo-cart') != undefined) {
                $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
            } else {
                $rootScope.cart = null;
            }
        },
        templateUrl: 'partials/vivo-header-menu.php'
    };
});


app.directive('vivoFooter', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'partials/vivo-footer.php'
    };
});

app.directive('vivoFooterFiles', function ($compile) {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'partials/vivo-footer-files.php'
    };
});

app.directive('vivoAddToCart', function ($compile) {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'partials/vivo-add-to-cart.php'
    };
});

app.directive('vivoBuyNow', function ($compile) {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'partials/vivo-buy-now.php'
    };
});

app.directive('vivoProductList', function ($compile) {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'partials/vivo-product-list.php'
    };
});

app.directive('vivoProductFilter', function ($compile) {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'partials/vivo-product-filter.php'
    };
});

app.directive('vivoProductCarousel', function ($compile) {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'partials/vivo-product-carousel.php'
    };
});

app.directive('vivoFeaturedProductCarousel', function ($compile) {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'partials/vivo-featured-product-carousel.php'
    };
});

app.directive('vivoProductSameJewellerCarousel', function ($compile) {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'partials/vivo-product-same-jeweller-carousel.php'
    };
});


app.directive("fbLogin", function ($rootScope) {
    return function (scope, iElement, iAttrs) {
        if (FB) {
            location.reload(true);
        }
    };
});

//Below code is for showing grey images in list,splist page while product images gets loaded
app.directive('actualImage', ['$timeout', function ($timeout) {
    return {
        link: function ($scope, element, attrs) {
            function waitForImage(url) {
                var tempImg = new Image();
                tempImg.onload = function () {
                    $timeout(function () {
                        element.attr('src', url);
                    });
                }
                tempImg.src = url;
            }

            attrs.$observe('actualImage', function (newValue) {
                if (newValue) {
                    waitForImage(newValue);
                }
            });
        }
    }
}]);

app.directive('jewellerStarRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '@',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }
});

app.directive('productStarRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '@',
            max: '=',
            onRatingSelected: '&',
            reviewscore: '='
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                    scope.reviewscore=scope.ratingValue;
                }
            });
        }
    }
});

app.directive('starRating',
    function () {
        return {
            restrict: 'A',
            template: '<ul class="rating"> <li ng-repeat="star in stars" ng-class="star" ng-click= "toggle($index)"><i class = "fa fa-star-o"></i></li></ul>',
            scope: {
                ratingValue: '=',
                max: '=',
                onRatingSelected: '&'
            },
            link: function (scope, elem, attrs) {
                var updateStars = function () {
                    scope.stars = [];
                    for (var i = 0; i < scope.max; i++) {
                        scope.stars.push({
                            filled: i < scope.ratingValue
                        });
                    }
                };

                scope.toggle = function (index) {
                    scope.ratingValue = index + 1;
                    scope.onRatingSelected({
                        rating: index + 1
                    });
                };

                scope.$watch('ratingValue',
                    function (oldVal, newVal) {
                        if (newVal) {
                            updateStars();
                        }
                    }
                );
            }
        };
    }
);

app.factory('sAuth', function ($q, $rootScope) {
    return {
        logout: function () {

            var _self = this;

            FB.logout(function (response) {

                $rootScope.$apply(function () {

                    $rootScope.user = _self.user = {};
                    $rootScope.logout();

                });

            });

        },
        getUserInfo: function () {

            var _self = this;

            FB.api('/me?fields=first_name,last_name,gender,email', function (res) {

                $rootScope.$apply(function () {

                    $rootScope.doOAuthLogin({
                        name: res.first_name + res.last_name,
                        email: res.email,
                        gender: res.gender
                    });
                });

            });

        },
        watchLoginChange: function () {

            var _self = this;

            FB.Event.subscribe('auth.login', function (res) {

                if (res.status === 'connected') {
                    
                    _self.getUserInfo();

                } else {



                }

            });
        }
    }
});

//Below code is for INR comma system
app.filter('INR', function () {
    return function (input) {
        if (!isNaN(input)) {
            var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            if (result.length > 1) {
                output += "." + result[1];
            }

            return currencySymbol + output;
        }
    }
});