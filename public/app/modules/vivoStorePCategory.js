var storepcapp = angular.module("vivoStorePCategory", ['vivoCommon']);

storepcapp.controller('storePCategoryCtrl',['$scope','$rootScope','$location','$http','Data','$window','API_URL','$compile', function ($scope, $rootScope, $location, $http, Data, $window,API_URL,$compile) {

    $scope.store = getUrlParameter('store');
    $scope.subtype = getUrlParameter('type');
    
    $scope.allN = 0;
    $scope.filterN = 0;
    $scope.sortN = 0;
    
    $scope.getStoreProductsCount = function ()
    {
        $http({
            method: 'GET',
            url : API_URL + 'getStoreProductsCount',
            params : {
                    c: $scope.store,
                    type: $scope.subtype
                }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.allN = Math.floor($scope.count/8) + 1;
            $scope.allarr = Array.apply(null, {length: $scope.allN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.filterStorePriceCount = function ()
    {
        $http({
            method: 'GET',
            url : API_URL + 'filterStorePriceCount',
            params : {
                c : $scope.store,
                isGold : $scope.isGold, 
                isWhiteGold : $scope.isWhiteGold, isPlatinum : $scope.isPlatinum,
                isRoseGold : $scope.isRoseGold,
                isSilver : $scope.isSilver,
                is22 : $scope.is22, 
                is18 : $scope.is18, 
                is14 : $scope.is14, 
                isOther : $scope.isOther, 
                isCasual : $scope.isCasual, isBridal : $scope.isBridal, isFashion : $scope.isFashion, jewellers : JSON.stringify($scope.jewellers),
                type : $scope.subtype,
                s : $scope.demo1.min,
                e : $scope.demo1.max
            }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.filterN = Math.floor($scope.count/8) + 1;
            $scope.filterarr = Array.apply(null, {length: $scope.filterN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.sortStoreListCount = function(){
        $http({
            method: 'GET',
            url : API_URL + 'sortStoreListCount',
            params: {
                p: $scope.sortType,
                page: $scope.lastpage,
                c: $scope.store,
                isGold: $scope.isGold,
                isWhiteGold: $scope.isWhiteGold,
                isPlatinum: $scope.isPlatinum,
                isRoseGold: $scope.isRoseGold,
                isSilver: $scope.isSilver,
                is22: $scope.is22,
                is18: $scope.is18,
                is14: $scope.is14,
                isOther: $scope.isOther,
                isCasual: $scope.isCasual,
                isBridal: $scope.isBridal,
                isFashion: $scope.isFashion,
                jewellers: JSON.stringify($scope.jewellers),
                type: $scope.subtype,
                s: $scope.demo1.min,
                e: $scope.demo1.max
            }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.sortN = Math.floor($scope.count/8) + 1;
            $scope.sortarr = Array.apply(null, {length: $scope.sortN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });  
    }
    
    $scope.share = function (p) {
        $('.share-icon').socialShare({
            social: 'facebook,google,twitter',
            shareUrl: 'www.vivocarat.com/%23/p/' + p.id
        });
    }
    $scope.demo1 = {
        min: 20,
        max: 200000
    };
    
    $scope.getAllJewellers = function(){
        $http({
            method: 'GET',
            url : API_URL + 'getAllJewellers'
        }).then(function successCallback(response){
            var result = response.data;
            var jsonObj = [];
            for(var k =0; k<result.length; k++){
                var j_id = result[k].id;
                var j_name = result[k].name;
                var j_dispname = j_name;
                
                if(j_name === 'Lagu Bandhu')
                {
                    j_dispname = 'Lagu Bandhu Jewellers';
                }
                if(j_name === 'PP-Gold')
                {
                    j_dispname = 'PP Gold';
                }
                if(j_name === 'Arkina-Diamonds')
                {
                    j_dispname = 'Arkina Diamonds';
                }
                if(j_name === 'ZKD-Jewels')
                {
                    j_dispname = 'ZKD Jewels';
                }
                if(j_name === 'Myrah-Silver-Works')
                {
                    j_dispname = 'Myrah Silver Works';
                }
                if(j_name === 'Charu-Jewels')
                {
                    j_dispname = 'Charu Jewels';
                }
                               
                var item = {}
                item ["jname"] = j_dispname;
                item ["jvalue"] = j_name;
                jsonObj.push(item);
            }
                      
            $scope.selectOptions = {
                placeholder: "Select Jewellers..",
                dataTextField: "jname",
                dataValueField: "jvalue",
                valuePrimitive: true,
                autoBind: false,
                dataSource: {
                    type: "jsonp",
                    serverFiltering: true,
                    data:jsonObj 
                }
            };
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    $scope.getAllJewellers();

    $scope.resetFilter = function () {

        // $scope.isFilter = false;
        $scope.selectedIds = [];
        $scope.isFirst = 1;
        $scope.jewellers = [];
        $scope.isGold = false;
        $scope.isWhiteGold = false;
        $scope.isPlatinum = false;
        $scope.isRoseGold = false;
        $scope.isSilver = false;
        $scope.is22 = false;
        $scope.is18 = false;
        $scope.is14 = false;
        $scope.isOther = false;
        $scope.isCasual = false;
        $scope.isBridal = false;
        $scope.isFashion = false;
        $scope.demo1 = {
            min: 20,
            max: 200000
        };
        $scope.lastpage = 0;
        $scope.resultList = [];
        $scope.getStoreProductsCount();
    }
    $scope.resetFilter();
    $scope.openFilter = function () {
        $("#filterArea").slideDown("slow", function () {

        }).delay(3000);

    }
    $scope.closeFilter = function () {
        $("#filterArea").slideUp("slow", function () {

        });

    }
    $scope.$watch('selectedFilter', function () {
        if ($scope.selectedFilter != null || $scope.selectedFilter == "") {
            if ($scope.selectedFilter == "Price H to L") {
                $scope.sortType = "price_after_discount desc";
            } else if ($scope.selectedFilter == "Price L to H") {
                $scope.sortType = "price_after_discount asc";
            } else if ($scope.selectedFilter == "Name") {
                $scope.sortType = "title desc";
            }else if ($scope.selectedFilter == "") {
                $scope.sortType = "id desc";
            }
            
            $scope.isFirst = 3;
            $scope.lastpage = 0;
            $scope.resultList = [];
            $scope.sortStoreListCount();
            //$scope.disabled = false;
            //$scope.loadMore();
        }   
    }, true);
    
    $scope.banImg = $scope.category + "_" + $scope.subtype;
    $scope.list = [];
    $scope.isLoaded = false;


    var el = angular.element(document.querySelector('#loadMore'));
    el.attr('tagged-infinite-scroll', 'loadMore()');
    $compile(el)($scope);
    $scope.lastpage = 0;
    $scope.resultList = [];
    $scope.isFirst = 1;

    $scope.loadMore = function () {

        $scope.fetching = true; // Block fetching until the AJAX call returns

        if ($scope.isFirst == 1 && $scope.disabled == false) {
            
            if($.inArray( $scope.lastpage, $scope.allarr )>-1 && typeof $scope.allarr !== 'undefined' && $scope.allarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.allarr );
                $scope.allarr.splice(pos, 1);
            
                $http({
                    method: 'GET',
                    url : API_URL + 'getStoreProducts',
                    params: {
                        c: $scope.store,
                        type: $scope.subtype,
                        page: $scope.lastpage
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {
                        $scope.disabled = true; // Disable further calls if there are no more items
                        $scope.isLoaded = true;
                    };
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
          
        } else if ($scope.isFirst == 2 && $scope.disabled == false) {
            
            if($.inArray( $scope.lastpage, $scope.filterarr )>-1 && typeof $scope.filterarr !== 'undefined' && $scope.filterarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.filterarr );
                $scope.filterarr.splice(pos, 1);
            
                $http({
                    method: 'GET',
                    url : API_URL + 'filterStorePrice',
                    params: {
                        page: $scope.lastpage,
                        c: $scope.store,
                        isGold: $scope.isGold,
                        isWhiteGold: $scope.isWhiteGold,
                        isPlatinum: $scope.isPlatinum,
                        isRoseGold: $scope.isRoseGold,
                        isSilver: $scope.isSilver,
                        is22: $scope.is22,
                        is18: $scope.is18,
                        is14: $scope.is14,
                        isOther: $scope.isOther,
                        isCasual: $scope.isCasual,
                        isBridal: $scope.isBridal,
                        isFashion: $scope.isFashion,
                        jewellers: JSON.stringify($scope.jewellers),
                        type: $scope.subtype,
                        s: $scope.demo1.min,
                        e: $scope.demo1.max
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;

                    } else {

                        $scope.disabled = true; // Disable further calls if there are no more items

                    }             
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
                 
        } else if ($scope.isFirst == 3 && $scope.disabled == false) {
              
            if($.inArray( $scope.lastpage, $scope.sortarr )>-1 && typeof $scope.sortarr !== 'undefined' && $scope.sortarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.sortarr );
                $scope.sortarr.splice(pos, 1);
            
                $http({
                    method: 'GET',
                    url : API_URL + 'sortStoreList',
                    params: {
                        p: $scope.sortType,
                        page: $scope.lastpage,
                        c: $scope.store,
                        isGold: $scope.isGold,
                        isWhiteGold: $scope.isWhiteGold,
                        isPlatinum: $scope.isPlatinum,
                        isRoseGold: $scope.isRoseGold,
                        isSilver: $scope.isSilver,
                        is22: $scope.is22,
                        is18: $scope.is18,
                        is14: $scope.is14,
                        isOther: $scope.isOther,
                        isCasual: $scope.isCasual,
                        isBridal: $scope.isBridal,
                        isFashion: $scope.isFashion,
                        jewellers: JSON.stringify($scope.jewellers),
                        type: $scope.subtype,
                        s: $scope.demo1.min,
                        e: $scope.demo1.max
                    }
                }).then(function successCallback(response){
 
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {
                        $scope.disabled = true; // Disable further calls if there are no more items
                    }

                },function errorCallback(response){
                    console.log(response.data);
                });  
            }
            
        }
    };

    $scope.filter = function () {
        $scope.isFirst = 2;
        $scope.lastpage = 0;
        $scope.resultList = [];
        $scope.filterStorePriceCount();
        //$scope.disabled = false;
        //$scope.loadMore();
    }
    
}]);