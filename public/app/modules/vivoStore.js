var storeapp = angular.module("vivoStore", ['vivoCommon']);

storeapp.controller('storeCtrl',['$scope','$rootScope','$location','$http','Data','$window','API_URL', function ($scope, $rootScope, $location, $http, Data, $window,API_URL) {

    $scope.store = getUrlParameter('store');
    
    $http({
        method: 'GET',
        url : API_URL + 'getStoreCategories',
        params : {store:$scope.store}
    }).then(function successCallback(response){
        $scope.categoriesList = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $http({
        method: 'GET',
        url : API_URL + 'getJewellerInformationByName',
        params : {store:$scope.store}
    }).then(function successCallback(response){
        $scope.jeweller = response.data[0];
    },function errorCallback(response){
        console.log(response.data);
    });
    
}]);