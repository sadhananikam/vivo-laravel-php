var homeapp = angular.module("vivoHome", ['vivoCommon']);

homeapp.controller('homeCtrl',['$scope','$rootScope','$location','$http','Data','$window','API_URL', function ($scope, $rootScope, $location, $http, Data, $window,API_URL) {
       
    $http({
        method: 'GET',
        url : API_URL + 'getbanners',
        params : {p:'home',m:0}
    }).then(function successCallback(response){
        $scope.banners = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $http({
        method: 'GET',
        url : API_URL + 'getlookbook'
    }).then(function successCallback(response){
        $scope.title = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $http({
        method: 'GET',
        url : API_URL + 'getfeaturedproducts'
    }).then(function successCallback(response){
        $scope.list = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });
    
    
    $scope.buyNow = function (p) {

        if (p.category == 'Bracelets') {
            if (p.bracelet_size == null) {

                //Please select a braclet size
                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    window.location.href = "p-checkout.php";
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else{
                window.location.href = "p-checkout.php";
            } 
            }

        } else if (p.category == 'Rings') {
            if (p.ring_size == null) {

                //Please select a ring size
                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    window.location.href = "p-checkout.php";
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else{
                window.location.href = "p-checkout.php";
            } 
            }

        } else if (p.category == 'Bangles') {
            if (p.bangle_size == null) {

                //Please select a bangle size
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    window.location.href = "p-checkout.php";
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else{
                window.location.href = "p-checkout.php";
            } 
        }

        } else {
            $rootScope.addToCart(p);
            if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    window.location.href = "p-checkout.php";
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else{
                window.location.href = "p-checkout.php";
            } 

        }

    }
    
    

}]);