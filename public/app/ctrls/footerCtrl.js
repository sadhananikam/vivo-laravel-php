app.controller("footerCtrl",['$scope','$rootScope','$timeout','$http','API_URL',function($scope,$rootScope,$timeout,$http,API_URL){
    $scope.emailPlaceholder = 'Enter your email';
    
    $scope.sendNewsletter = function(){
        waitingDialog.show();
        var url = API_URL + 'newsletter';
        $http({
            method: 'POST',
            url: url, 
            data: $.param($scope.newsletter),
            headers: { 'Content-type':'application/x-www-form-urlencoded' }           
        })
        .then(function successCallback(response){
            
            if (response.data.success == false) {
                waitingDialog.hide();
                
                var msg = '';
                if(response.data.errors.newsletteremail)
                {
                    msg = msg + response.data.errors.newsletteremail + "\n";
                }
                
              if(response.data.errors.errorMessage)
                {
                    msg = msg + response.data.errors.errorMessage;
                }
                
                $rootScope.responseNLMsg = msg;
                $('#newsletterMsg').modal('show');
                $timeout(function () {
                    $('#newsletterMsg').modal('hide');

                }, 2000);
            } 
            else {
                waitingDialog.hide();
                
                $rootScope.responseNLMsg = response.data.successMessage;
                $('#newsletterMsg').modal('show');
                $timeout(function () {
                    $('#newsletterMsg').modal('hide');

                }, 2000);
  
            }
            
            $scope.newsletter = {};
            angular.copy({}, $scope.newsletterForm);
            
        },function errorCallback(response){
            console.log(response.data);
            waitingDialog.hide();
            
            $rootScope.responseNLMsg = 'An error has occured. Please check the log for details';
            $('#newsletterMsg').modal('show');
            $timeout(function () {
                $('#newsletterMsg').modal('hide');

            }, 2000);

        });
    };
}]);