<?php
$desk = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if(isset($_GET['id']) && $_GET['id'] != '')
{
    $id = $_GET['id'];
    $mob = 'https://' . $_SERVER['HTTP_HOST'] . "/m-index.html#/i/faq/".$id;
}
else
{
    $mob = 'https://' . $_SERVER['HTTP_HOST'] . "/m-index.html#/i/faq";
}
?>
<!DOCTYPE html>
<html lang="en" data-ng-app="vivoFaq">

<head>
    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="<?php echo $desk; ?>">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo $mob; ?>">
    <link rel="alternate" media="handheld" href="<?php echo $mob; ?>" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            if (window.location.search.indexOf('id') > -1) 
            {
                var id = getUrlParameter('id');
                if(id.length)
                {
                    document.location = "m-index.html#/i/faq/"+getUrlParameter('id');
                }
                else
                {
                    document.location = "m-index.html#/i/faq";
                }
            }
            else
            {
                document.location = "m-index.html#/i/faq";
            }
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
  
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body ng-cloak>

<style>
/*----- Accordion -----*/
.accordion, .accordion * {
	-webkit-box-sizing:border-box; 
	-moz-box-sizing:border-box; 
	box-sizing:border-box;
}

.accordion {
	overflow:hidden;
	/*box-shadow:0px 1px 3px rgba(0,0,0,0.25);
	border-radius:3px;
	background:#f7f7f7;*/
    padding-left: 15px;
    padding-right: 15px;
}

/*----- Section Titles -----*/
.accordion-section-title {
	width:100%;
	padding:15px;
	display:inline-block;
	/*border-bottom:1px solid #1a1a1a;*/
	background:#F4F4F4;
	transition:all linear 0.15s;
	/* Type */
	font-size:15px;
	/*text-shadow:0px 1px 0px #1a1a1a;*/
	color: #797979 !important;
    margin-bottom: 20px;
    font-family: 'leela';
    font-weight: bold;
    text-transform: uppercase;
}

.accordion-section-title.active, .accordion-section-title:hover {
	background:#EAEAEA;
	/* Type */
	text-decoration:none;
}

.accordion-section:last-child .accordion-section-title {
	border-bottom:none;
}

/*----- Section Content -----*/
.accordion-section-content {
	padding:15px;
	display:none;
    margin-bottom: 20px;
    border: 1px solid #eee;
}
        
/*----- Section ques -----*/
.accordion-section-ques {
	width:100%;
	padding:15px;
	display:inline-block;
	/* border-bottom: 1px solid #1a1a1a; */
    background: #F1F1F1;
    transition: all linear 0.15s;
    font-size: 14px;
    /* text-shadow: 0px 1px 0px #1a1a1a; */
    color: #797979 !important;
    font-family: 'leela';
    font-weight: bold;
}

.accordion-section-ques.active, .accordion-section-ques:hover {
	background:#E5E4E4;
	/* Type */
	text-decoration:none;
    border-bottom: 1px solid #797979;
}

.accordion-section:last-child .accordion-section-ques {
	border-bottom:none;
}

/*----- Section ans -----*/
.accordion-section-ans {
	padding:15px;
	display:none;
    background-color: #F5F5F5;
    margin-bottom: 10px;
} 

.answer-text{
    background-color: #F5F5F5;
    color: #797979 !important;
    font-size: 14px;
    font-family: 'leela';
} 

.bullet{
    list-style-type:disc;
    padding-left: 34px;
} 

.plusminus { 
    float:right;
    font-weight: bold;
    font-size: 30px;
    line-height: 10px;
    color: #797979 !important;
    pointer-events: none;
}   
</style>

<vivo-header></vivo-header>

<div data-ng-controller='faqCtrl'>
<div class="container padding-bottom-60px">

<div class="row padding-bottom-30px">
 <div class="col-md-1 vivocarat-heading temp_faq">FAQs</div>

 <div class="col-md-11 categoryGrad margin-top-68px">

 </div>
</div>
    
<div class="accordion">

 <div class="accordion-section">
  <a id="Diamonds" class="accordion-section-title" href="#accordion-1">
     Diamonds and Jewellery
     <span class="plusminus">+</span>
  </a>

  <div id="accordion-1" class="accordion-section-content">

   <a id="Diamonds-ques1" class="accordion-section-ques" href="#Diamonds-ans1">
      How do I know if a diamond is genuine?
      <span class="plusminus">+</span>
   </a>

   <div id="Diamonds-ans1" class="accordion-section-ans">
    <p class="answer-text">
       You will not. You need to be an expert and use specialised equipment to make sure a diamond is real
       and genuine. Do not trust ideas like scratching a beer bottle (it may damage the diamond too),
       thermal conductivity, looking through, comparing weight, etc. If the diamond is accompanied by a
       certificate, you will know it is real and if it is treated artificially or not.
    </p>
   </div>
                    
   <a id="Diamonds-ques2" class="accordion-section-ques" href="#Diamonds-ans2">
      Which diamond shape gives me the greatest value for money?
      <span class="plusminus">+</span>
   </a>
                    
   <div id="Diamonds-ans2" class="accordion-section-ans">
	<p class="answer-text">
       Round brilliant diamond has the most brilliance, and the most popular. Fancy diamonds look more
       elegant in larger sizes. Also, they tend to look larger than they are by virtue of their shape. The
       choice of shape is also governed by the shape of the hand and Diamond Colour and Diamond Clarity.
       Round brilliant diamond hides defects and yellow tints the best.
    </p>
   </div>
                    
   <a id="Diamonds-ques3" class="accordion-section-ques" href="#Diamonds-ans3">
      What are the "four C's" relating to diamond quality?
      <span class="plusminus">+</span>
   </a>

   <div id="Diamonds-ans3" class="accordion-section-ans">
	<p class="answer-text">
       The four C's are Carat (weight), Colour, Clarity and Cut of the stone.
       Check <a class="grey-colour bold-font" href="p-jewelleryeducation.php">this page</a> for more details.
    </p>
   </div>

   <a id="Diamonds-ques4" class="accordion-section-ques" href="#Diamonds-ans4">
      What is the most important C in the choice of a diamond? Colour or Clarity?
      <span class="plusminus">+</span>
   </a>

   <div id="Diamonds-ans4" class="accordion-section-ans">
	<p class="answer-text">
       The most desirable colour of the diamond is white. The Colour scale ranges from D to Z, where D is
       the whitest of the white and hence most expensive. However, colours between E to H are regarded
       as very white, and you cannot make out the difference in colour once a diamond is set. However, the
       diamond colour becomes more obvious as in larger size diamonds, or in shapes other than the round
       brilliant. For instance, the Asscher and the emerald cuts are more see-through with large facets and
       require a higher colour and clarity than other shapes. Colour is something that can be discerned with
       the naked eye with practice (such as looking at the engagement ring every day. Hence, it is advisable
       to buy as white a diamond as you can afford in your carat range, in an eye clean or better clarity. If a
       diamond has slight blue fluorescence, it could render your H or J colour whiter. Also, if the stone is
       very well cut, it makes both the clarity and colour look better. Clarity is not a factor you can judge
       only by looking at the diamond under magnification. Step cut diamond shapes like emerald and
       Asscher cuts require a higher quality of colour and clarity as they have large see-through facets
       which make it easy to see any imperfections.
    </p>
   </div>

  </div><!--end .accordion-section-content-->
 </div><!--end .accordion-section-->

 <div class="accordion-section">
  <a id="certified" class="accordion-section-title" href="#accordion-7">
     JEWELLERY HALLMARK & CERTIFICATION
     <span class="plusminus">+</span>
  </a>

  <div id="accordion-7" class="accordion-section-content">
	
   <a id="certification-q1" class="accordion-section-ques" href="#certification-ans1">
      What is BIS hallmark?
      <span class="plusminus">+</span>
   </a>

   <div id="certification-ans1" class="accordion-section-ans">
	<p class="answer-text">
       The BIS hallmark is a hallmarking system for gold as well as silver jewellery sold in India certifying the purity of the metal. The standards are defined by the Bureau of Indian Standards, the national standards organization of India.
    </p>
   </div>
      
   <a id="certification-q2" class="accordion-section-ques" href="#certification-ans2">
      How does the BIS hallmark look like and where can I find it on the jewellery?
      <span class="plusminus">+</span>
   </a>

   <div id="certification-ans2" class="accordion-section-ans">
	<p class="answer-text">
       The BIS hallmark can be found on the inner side or back of a jewellery.
It consists of the following five components:
        <br>
        - The BIS mark logo
        - The Gold Fineness number (ex. 916 is for a 22 carat purity of gold)
        - Assaying and Hallmarking Centre's mark
        - Jeweller's identification mark
        - The year of marking denoted by a code letter and decided by BIS (e.g. code letter `A' was approved by BIS for year 2000, `B' being used for the year 2001 and `C' for 2002 and 'J' for 2008).
    </p>
  </div><!--end .accordion-section-content-->
      
  <a id="certification-q3" class="accordion-section-ques" href="#certification-ans3">
      Why should I trust a BIS hallmark?
      <span class="plusminus">+</span>
   </a>

   <div id="certification-ans3" class="accordion-section-ans">
	<p class="answer-text">
       Government of India has identified BIS as a sole agency and The BIS Hallmarking Scheme has been aligned with International criteria on hallmarking. As per this scheme, licence is granted to the jewellers by BIS under Hallmarking Scheme. The BIS certified jewellers can get their jewellery hallmarked from any of the BIS recognized Assaying and Hallmarking Centre.
    </p>
   </div><!--end .accordion-section-content-->
       
    <a id="certification-q4" class="accordion-section-ques" href="#certification-ans4">
      How are diamonds certified?
      <span class="plusminus">+</span>
   </a>

   <div id="certification-ans4" class="accordion-section-ans">
	<p class="answer-text">
       A diamond certificate, also known as a diamond report, is issued by an accredited independent gemological laboratory like GIA, IGI, SGL or DGLA. In addition to the diamond's carat weight and measurements, a certificate includes grades for the diamond's cut, color and clarity. All diamonds bought on <a href="https://www.vivocarat.com/p-list.php?type=Diamond&subtype=All" class="read-more-link" style="font-size: 14px; padding-left: 0;">VivoCarat</a> are accompanied by a diamond certificate so you can shop without any worries.
    </p>
   </div><!--end .accordion-section-content-->
 </div><!--end .accordion-section-->
    
 <div class="accordion-section">
  <a id="Payment" class="accordion-section-title" href="#accordion-2">
     Payment Options
     <span class="plusminus">+</span>
  </a>

  <div id="accordion-2" class="accordion-section-content">
					
   <a id="Payment-ques1" class="accordion-section-ques" href="#Payment-ans1">
      Is Cash on Delivery (COD) available? How does it work?
      <span class="plusminus">+</span>
   </a>
                    
   <div id="Payment-ans1" class="accordion-section-ans">
	<p class="answer-text">
       Yes. COD is available on select pin codes up to Rs.49,000. When placing an order select the COD
       option during checkout. You pay the complete amount in cash to the courier once the product is
       delivered to you. Please remember payment through cheque, draft or any other means will not be
       accepted by the courier company.
    </p>
   </div>
                    
   <a id="Payment-ques2" class="accordion-section-ques" href="#Payment-ans2">
      Can I pay Online? What are the options available?
      <span class="plusminus">+</span>
   </a>
   
   <div id="Payment-ans2" class="accordion-section-ans">
	<p class="answer-text">
       Yes. You can pay using any of the following options,
       <ul class="bullet answer-text">
        <li>Credit Card: All Visa,Master and American Express Credit Cards are accepted</li>
        <li>Debit Card: All Visa, Maestro and RuPay Debit Cards are accepted</li> 
        <li>Netbanking</li>
        <li>Wallets (Paytm, Mobikwik, freecharge, etc)</li>    
       </ul>
    </p>
   </div>
                    
   <a id="Payment-ques3" class="accordion-section-ques" href="#Payment-ans3">
      Is there any additional charge for the CoD shipment?
      <span class="plusminus">+</span>
   </a>
   
   <div id="Payment-ans3" class="accordion-section-ans">
	<p class="answer-text">
       No, our CoD service is done at no extra cost.
    </p>
   </div>

   <a id="Payment-ques4" class="accordion-section-ques" href="#Payment-ans4">
      What is the option if no one is at home when the courier person comes?
      <span class="plusminus">+</span>
   </a>
   
   <div id="Payment-ans4" class="accordion-section-ans">
	<p class="answer-text">
       Most courier companies will call you before coming over to deliver the item. Some important points
       to ensure that your CoD transaction goes smoothly please ensure that the mobile number you
       provide for the Cash on Delivery payment is always available and answered. It is important that your
       address and landmark are very clear. This will help us locate you easily and complete the process
       faster. Please be available at the designated address with the complete cash as payment for the item
       purchased.
    </p>
   </div>

  </div><!--end .accordion-section-content-->
 </div><!--end .accordion-section-->

 <div class="accordion-section">
  <a id="Shipping" class="accordion-section-title" href="#accordion-3">
     Shipping Policy
     <span class="plusminus">+</span>
  </a>

  <div id="accordion-3" class="accordion-section-content">
	
   <a id="Shipping-ques1" class="accordion-section-ques" href="#Shipping-ans1">
      Is product shipping free?
      <span class="plusminus">+</span>
   </a>

   <div id="Shipping-ans1" class="accordion-section-ans">
	<p class="answer-text">
       Yes. We believe in creating an online buying experience which is similar or better than buying
       jewellery offline. Hence all products delivered by VivoCarat are free and insured only within India.
       We urge all customers to inspect the package for any damage or tamper before receiving or signing
       for receipt.
    </p>
   </div>
                    
   <a id="Shipping-ques2" class="accordion-section-ques" href="#Shipping-ans2">
      It's gold and diamond. Is the product delivery safe?
      <span class="plusminus">+</span>
   </a>
                    
   <div id="Shipping-ans2" class="accordion-section-ans">
	<p class="answer-text">
       Yes. All products shipped are safe and insured till the time they reach the customer's hands.
    </p>
   </div>
                    
   <a id="Shipping-ques3" class="accordion-section-ques" href="#Shipping-ans3">
      Which courier company delivers my jewellery?
      <span class="plusminus">+</span>
   </a>
   
   <div id="Shipping-ans3" class="accordion-section-ans">
	<p class="answer-text">
       For our Indian customers, after your item has been packaged, it will be shipped and delivered free
       via one of the following carriers.
       <ul class="bullet padding-top-10px answer-text">
        <li class="answer-text">Blue Dart</li>
        <li class="answer-text">Speed Post</li>
        <li class="answer-text">BVC Logistics</li>
       </ul>
    </p>
   </div>

   <a id="Shipping-ques4" class="accordion-section-ques" href="#Shipping-ans4">
      How long will it take for me to receive the product?
      <span class="plusminus">+</span>
   </a>
                    
   <div id="Shipping-ans4" class="accordion-section-ans">
	<p class="answer-text">
       We strive to get all products shipped and delivered as per the promised times but in case of any
       delay, we will keep you informed via E-mail and SMS. In case a product is not delivered even after 30
       days from the promised date of delivery, we will cancel the order and refund the amount to you.
    </p>
   </div>

   <a id="Shipping-ques5" class="accordion-section-ques" href="#Shipping-ans5">
      I have received a damaged product. What should I do?
      <span class="plusminus">+</span>
   </a>
                    
   <div id="Shipping-ans5" class="accordion-section-ans">
	<p class="answer-text">
       If you find any damage in the product please report the same to us at <span class="vivocarat-theme-colour">hello@vivocarat.com</span> within
       24 hours of the delivery. We shall arrange for the appropriate corrective measure/s to be taken and
       the same shall be initiated immediately.
    </p>
   </div>

  </div><!--end .accordion-section-content-->
 </div><!--end .accordion-section-->

 <div class="accordion-section">
  <a id="Return" class="accordion-section-title" href="#accordion-4">
     Return/Refund/Exchanges
     <span class="plusminus">+</span>
  </a>

  <div id="accordion-4" class="accordion-section-content">
	
   <a id="Return-ques1" class="accordion-section-ques" href="#Return-ans1">
      I don’t like the product delivered. Can I return it?
      <span class="plusminus">+</span>
   </a>

   <div id="Return-ans1" class="accordion-section-ans">
	<p class="answer-text">
       Yes. We ensure that all products that are sold through our website are of good quality and we
       provide all information related to it, still, if you are unsatisfied with the product, you can return the
       product within 10 days of receiving the product.
    </p>

    <p class="answer-text">
       In case a product is returned within the stipulated time we will refund the full order value will be
       refunded to the customer up to a maximum of the amount paid by the customer
    </p>
   </div>
                    
   <a id="Return-ques2" class="accordion-section-ques" href="#Return-ans2">
      I have worn the jewellery once. Can I still return it?
      <span class="plusminus">+</span>
   </a>
                    
   <div id="Return-ans2" class="accordion-section-ans">
	<p class="answer-text">
       Any item that shows the sign of wear or has been altered, resized(by a Jeweller other than the
       (company), or damaged cannot be accepted for the 10 days money back. But you can still use the
       <a class="link-hover" id="Lifetime-ques1">Lifetime Buyback/Exchange facility</a>
    </p>
   </div>
                    
   <a id="Return-ques3" class="accordion-section-ques" href="#Return-ans3">
      The product has been customised according to my request. Can I still return it?
      <span class="plusminus">+</span>
   </a>
   
   <div id="Return-ans3" class="accordion-section-ans">
	<p class="answer-text">
       Customised (including personalised/engraved products) Jewellery is not eligible for a Moneyback,10
       Day Exchange or BuyBack. Only Life Time Exchange Policy applies. But if there are any manufacturing
       defects you can get in touch with us at <span class="vivocarat-theme-colour">hello@vivocarat.com</span> and the exchange request will be
       processed.
    </p>
   </div>

   <a id="Return-ques4" class="accordion-section-ques" href="#Return-ans4">
      How do I ship the product to VivoCarat?
      <span class="plusminus">+</span>
   </a>
                    
   <div id="Return-ans4" class="accordion-section-ans">
	<p class="answer-text">
       After the return/exchange request has been approved, VivoCarat will schedule a pickup request with
       one of its courier partners. This will be communicated to you via Email/SMS. Please handover the
       product along with the original packing and delivery box. The pick-up person will provide a receipt
       after you have handed over the product to him. 
    </p>
   </div>

   <a id="Return-ques5" class="accordion-section-ques" href="#Return-ans5">
      How will the money be refunded?
      <span class="plusminus">+</span>
   </a>
                    
   <div id="Return-ans5" class="accordion-section-ans">
	<p class="answer-text">
       All Refunds will be processed to the original mode of payment, in case the original mode of payment
       is inaccessible by the customer, the refund will be done via NEFT to a bank account as provided by
       the customer.
    </p>
   </div>

  </div><!--end .accordion-section-content-->
 </div><!--end .accordion-section-->

 <div class="accordion-section">
  <a id="Lifetime" class="accordion-section-title" href="#accordion-5">
     Lifetime BuyBack/Exchange Policy
     <span class="plusminus">+</span>
  </a>

  <div id="accordion-5" class="accordion-section-content">
	
   <a id="Lifetime-ques1" class="accordion-section-ques" href="#Lifetime-ans1">
      What is the lifetime buyback/exchange policy?
      <span class="plusminus">+</span>
   </a>

   <div id="Lifetime-ans1" class="accordion-section-ans">
	<p class="answer-text">
       VivoCarat offer a Lifetime Exchange &amp; Buy-Back Policy on all purchases* made from
       Vivocarat, within India. The product along with the original product certificate, can be
       returned or exchanged basis its current market value, with deductions towards making
       charges. Loss of the original product certificate would result in a deduction of Rs. 500.
    </p>

    <p class="answer-text">
       This means your product's current value of metal, diamonds and coloured stones, will be
       determined based on product specifications and the current price listed on our website.
    </p>

    <p class="answer-text">
       If you received a discount or free gift while making your purchase, we will deduct the
       original discount amount or free gift value, as applicable. You can choose to return the free
       gift as well.
    </p>
   </div>

  </div><!--end .accordion-section-content-->
 </div><!--end .accordion-section-->

 <div class="accordion-section">
  <a id="Packaging" class="accordion-section-title" href="#accordion-6">
     Packaging
     <span class="plusminus">+</span>
  </a>

  <div id="accordion-6" class="accordion-section-content">
	
   <a id="Packaging-ques1" class="accordion-section-ques" href="#Packaging-ans1">
      How is the product packed?
      <span class="plusminus">+</span>
   </a>

   <div id="Packaging-ans1" class="accordion-section-ans">
	<p class="answer-text">
       Every jewellery package from us arrives at your doorstep in a durable, tamper-proof packing.
       Whether it is a jewellery or gold coin, your item is delivered to you in an exclusive VivoCarat bag
       along with the relevant certificates and invoice.
    </p>
   </div>
                    
   <a id="Packaging-ques2" class="accordion-section-ques" href="#Packaging-ans2">
      I want to gift the jewellery to my wife. Can you giftwrap it?
      <span class="plusminus">+</span>
   </a>
                    
   <div id="Packaging-ans2" class="accordion-section-ans">
	<p class="answer-text">
       Yes, not just giftwrap you can even send a personalised message with the box. 
    </p>

    <p class="answer-text">
       In case your purchase is intended as a gift, we offer free gift packing, along with a gift message of
       your choice. Your gift will be delivered in distinctive VivoCarat gift packing. So whether you order a
       gift to give to your loved ones personally, or want us to deliver it, the recipient is sure to be
       impressed. 
    </p>

    <p class="answer-text">
       To send a free giftwrap select the ‘I want to gift wrap’ button at checkout. 
    </p>
   </div>
   
  </div><!--end .accordion-section-content-->
 </div><!--end .accordion-section-->

</div><!--end .accordion-->
	
    
</div><!-- container -->
</div>
<vivo-footer></vivo-footer>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoFaq.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>

<script>
jQuery(document).ready(function() {

    if (window.location.search.indexOf('id') > -1) {
        var id = getUrlParameter('id');
        if(id.length)
        {
            var str = "#"+id;
            
            var currentAttrValue = jQuery(str).attr('href');

            close_accordion_section();

            // Add active class to section title
            jQuery(str).addClass('active');
            // Open up the hidden content panel
            jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
            jQuery(str).children(".plusminus").text('-');
        }
    }      
    
	function close_accordion_section() {
		jQuery('.accordion .accordion-section-title').removeClass('active');
		jQuery('.accordion .accordion-section-content').slideUp(300).removeClass('open');
        jQuery('.accordion .accordion-section-title').children(".plusminus").text('+');
	}

	jQuery('.accordion-section-title').click(function(e) {
		// Grab current anchor value
		var currentAttrValue = jQuery(this).attr('href');

		if(jQuery(e.target).is('.active')) {
			close_accordion_section();
            jQuery(this).children(".plusminus").text('+');
		}else {
			close_accordion_section();

			// Add active class to section title
			jQuery(this).addClass('active');
			// Open up the hidden content panel
			jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
            jQuery(this).children(".plusminus").text('-');
		}

		e.preventDefault();
	});
    
    function close_accordion_ans() {
		jQuery('.accordion .accordion-section-ques').removeClass('active');
		jQuery('.accordion .accordion-section-ans').slideUp(300).removeClass('open');
        jQuery('.accordion .accordion-section-ques').children(".plusminus").text('+');
	}
    
    jQuery('.accordion-section-ques').click(function(e) {
		// Grab current anchor value
		var currentAttrValue = jQuery(this).attr('href');

		if(jQuery(e.target).is('.active')) {
			close_accordion_ans();
            jQuery(this).children(".plusminus").text('+');
		}else {
			close_accordion_ans();

			// Add active class to section title
			jQuery(this).addClass('active');
			// Open up the hidden content panel
			jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
            jQuery(this).children(".plusminus").text('-');
		}

		e.preventDefault();
	});

    function openmaincat(id)
    {
        var str = "#"+id;
        var currentAttrValue = jQuery(str).attr('href');
        
        close_accordion_section();

        // Add active class to section title
        jQuery(str).addClass('active');
        // Open up the hidden content panel
        jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
        jQuery(str).children(".plusminus").text('-');
            
        setTimeout(function(){
            jQuery("html,body").animate({
                scrollTop: jQuery(str).position().top-80
            }, 2000);
        }, 500);
    } 

    jQuery('#Lifetime-ques1').click(function(e){
        openmaincat('Lifetime');
    });
});    
</script> 

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>