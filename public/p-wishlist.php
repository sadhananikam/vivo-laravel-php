<!DOCTYPE html>
<html lang="en" data-ng-app="vivoWishlist">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-wishlist.php">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/wishlist">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/wishlist" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/wishlist";
        }
    </script>
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>

<body ng-cloak>

<style>   
/*Arrow nav tabs CSS*/
.arrow {
    position: relative;
    color: white !important;
    margin-bottom: 5px;
}
.red-gradient {
    height: 20px;
    padding: 10px;
    width: 200px;
    color: #000000;
    background: #e62739;
    /* Old browsers */
}
.red {
    color: white !important;
    background: #e62739;
    padding: 8px 97px 8px 25px;
    font-weight: bold;
}
.red:hover {
    color: white !important;
    background: #e62739;
    padding: 8px 97px 8px 25px;
}

/*Arrow tip css*/
#arrow4:after {
    content: '';
    height: 0;
    display: block;
    border-color: transparent transparent transparent #e62739;
    border-width: 17px;
    border-style: solid;
    position: absolute;
    top: -1px;
    left: 173px;
}

/*END of Arrow nav tabs CSS*/
</style>

<vivo-header></vivo-header>

<div data-ng-controller='wishlistCtrl'>

<div class="container padding-bottom-60px">

<div class="row">
 <div class="tabs-left">
  <ul class="col-lg-2 no-padding-left-right tabs-style">
   <li class="row text-style tab-head arrow" style="padding-top: 6px;" id="arrow4">
    <a class="red" href="p-wishlist.php">Wishlist</a>
   </li>

   <li class="row text-style tab-head first-tab-size">
    <a class="inactive-tab-text-colour" href="p-orders.php">Order history</a>
   </li>

   <li class="row text-style tab-head second-tab-size">
    <a class="inactive-tab-text-colour" href="p-account.php">Account details</a>
   </li>        
  </ul>

  <div data-ng-hide="!authenticated || isLoaded">
   <div class="col-lg-10 text-center home-panel-row">
    <div class="well">Please wait...</div>
   </div>
  </div>
            
  <div data-ng-show="!isauthenticated || isLoaded" class="register_account col-lg-10 padding-top-17px">

   <div class="well not-loggedin-pad" data-ng-show="!authenticated">
    <h4 class="normal-text">
        Not Logged In
    </h4>
    <p class="normal-text margin-bottom-20px">
         Kindly <a class="link-hover" href="p-login.php?id=0">login</a> to see your wishlist.
    </p>
   </div>

   <div class="margin-bottom-20px" data-ng-show="authenticated && watchlist.length<1" style="padding: 36px;">
    <p class="normal-text margin-bottom-20px">Your wishlist is empty.</p>
   </div>

   <div data-ng-hide="!authenticated" class="col-lg-10 no-padding-right">
    <div class="row margin-top-0px" data-ng-show="watchlist!=null && watchlist.length>0">
     <div class="col-lg-12 no-padding-left-right padding-top-10px padding-bottom-20px" data-ng-repeat="c in watchlist">

      <div class="row">
       <div class="col-lg-3">
        <img data-ng-src="images/products-v2/{{c.VC_SKU}}-1.jpg" style="border: 1px solid #eee;" alt="{{c.title}}">
       </div>

       <div class="col-lg-5">
        <div class="row">
         <h3 class="normal-text bold-font grey-colour">{{c.title}}</h3>
        </div>

        <div class="row">
         <img style="width:50px;" data-ng-src=" images/header/brands logos dropdown/{{c.supplier_name}} hover.png" alt="{{c.supplier_name}}">
        </div>

        <div class="row">
         <div class="col-xs-4 no-pad bold-font normal-text vivocarat-theme-colour no-padding-left">
            RS.&nbsp;{{c.price_after_discount | INR}}
         </div>

         <div class="col-xs-4 no-pad no-padding-left normla-text grey-colour" style="text-decoration:line-through;">
            RS.&nbsp;{{c.price_before_discount | INR}}
         </div>

         <div class="col-xs-4 no-pad no-padding-left normal-text grey-colour">
            {{c.discount}}% OFF
         </div>
        </div>
       </div>

       <div class="col-lg-4 no-padding-right padding-top-20px">
        <div class="row">
         <div class="col-lg-4 col-lg-offset-7 no-padding-left-right text-left rightrem">
          <img src="images/icons/checkout/remove.png" style="padding-right: 5px;" alt="remove">
           <a class="remove-link" data-ng-click="removeFromWatchlist(c.id)">
             Remove
           </a>
         </div>
        </div>

        <div class="row pull-right">
         <div class="col-lg-6 no-padding-left-right" style="padding-top: 77px;">
          <a class="padding-top-5px btn small-button btn-vivo" data-ng-click="addToCart(c)"> 
            ADD TO CART 
          </a>
         </div>
        </div>
       </div>
      </div>

      <div class="row padding-left-15px">
       <div class="col-lg-12 bottom-shadow"> </div>
      </div>

     </div>

    </div>
   </div>

  </div>
 </div>
</div>
    
</div>

</div>
    
<vivo-footer></vivo-footer>
    
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoWishlist.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!-- Start include Controller for angular -->
    
<script src="device-router.js"></script>  

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>