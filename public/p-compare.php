<!DOCTYPE html>
<html lang="en" data-ng-app="vivoCompare">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-compare.php">
    
    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1026045347487468&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->    
</head>

<body ng-cloak>

<style>
    
    /*Table CSS*/
    
    .CompareTable {
        margin: 0px;
        padding: 0px;
        width: 100%;
        border: 1px solid #000000;
        -moz-border-radius-bottomleft: 0px;
        -webkit-border-bottom-left-radius: 0px;
        border-bottom-left-radius: 0px;
        -moz-border-radius-bottomright: 0px;
        -webkit-border-bottom-right-radius: 0px;
        border-bottom-right-radius: 0px;
        -moz-border-radius-topright: 0px;
        -webkit-border-top-right-radius: 0px;
        border-top-right-radius: 0px;
        -moz-border-radius-topleft: 0px;
        -webkit-border-top-left-radius: 0px;
        border-top-left-radius: 0px;
    }
    
    .CompareTable table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }
    
    .CompareTable tr:last-child td:last-child {
        -moz-border-radius-bottomright: 0px;
        -webkit-border-bottom-right-radius: 0px;
        border-bottom-right-radius: 0px;
    }
    
    .CompareTable table tr:first-child td:first-child {
        -moz-border-radius-topleft: 0px;
        -webkit-border-top-left-radius: 0px;
        border-top-left-radius: 0px;
    }
    
    .CompareTable table tr:first-child td:last-child {
        -moz-border-radius-topright: 0px;
        -webkit-border-top-right-radius: 0px;
        border-top-right-radius: 0px;
    }
    
    .CompareTable tr:last-child td:first-child {
        -moz-border-radius-bottomleft: 0px;
        -webkit-border-bottom-left-radius: 0px;
        border-bottom-left-radius: 0px;
    }
    
    .CompareTable tr:hover td {}
    
    .CompareTable tr:nth-child(odd) {
        background-color: #fafafa;
    }
    
    .CompareTable tr:nth-child(even) {
        background-color: #ffffff;
    }
    
    .CompareTable td {
        vertical-align: middle;
        border: 1px solid #000000;
        border-width: 0px 1px 1px 0px;
        text-align: left;
        padding: 7px;
        font-size: 13px;
        font-family: 'leela';
        font-weight: normal;
        color: #000000;
    }
    
    .CompareTable tr:last-child td {
        border-width: 0px 1px 0px 0px;
    }
    
    .CompareTable tr td:last-child {
        border-width: 0px 0px 1px 0px;
    }
    
    .CompareTable tr:last-child td:last-child {
        border-width: 0px 0px 0px 0px;
    }
    
    .CompareTable tr:first-child td {
        background: -o-linear-gradient(bottom, #ffffff 5%, #ffffff 100%);
        background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ffffff), color-stop(1, #ffffff));
        background: -moz-linear-gradient( center top, #ffffff 5%, #ffffff 100%);
        filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#ffffff", endColorstr="#ffffff");
        background: -o-linear-gradient(top, #ffffff, ffffff);
        background-color: #ffffff;
        border: 0px solid #000000;
        text-align: center;
        border-width: 0px 0px 1px 1px;
        font-size: 13px;
        font-family: 'leela';
        font-weight: normal;
        color: #ffffff;
    }
    
    .CompareTable tr:first-child:hover td {
        background: -o-linear-gradient(bottom, #ffffff 5%, #ffffff 100%);
        background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ffffff), color-stop(1, #ffffff));
        background: -moz-linear-gradient( center top, #ffffff 5%, #ffffff 100%);
        filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#ffffff", endColorstr="#ffffff");
        background: -o-linear-gradient(top, #ffffff, ffffff);
        background-color: #ffffff;
    }
    
    .CompareTable tr:first-child td:first-child {
        border-width: 0px 0px 1px 0px;
    }
    
    .CompareTable tr:first-child td:last-child {
        border-width: 0px 0px 1px 1px;
    }
    /*END of table CSS*/
</style>
<vivo-header></vivo-header>

<div data-ng-controller='compareCtrl'>
<div class="container padding-bottom-60px">

<div class="row">
 <div class="col-md-4 vivocarat-heading">COMPARISON</div>

 <div class="col-md-8 categoryGrad margin-top-68px">

 </div>
</div>
    
<div data-ng-if="p.length<2" class="well normal-text">
    Please select at least two products to compare. 
</div>
    
<div data-ng-if="p.length>1" class="CompareTable">
 <table>
  <tr>
    <td style="width:15%;">

    </td>
    <td data-ng-if="p.length>0" style="width:15%;">
        <div class="row">
            <div class="col-lg-12 no-padding">
                <div class="row">
                    <a data-ng-href="p-product.php?id={{p[0].id}}">
                        <img class="img-responsive" data-ng-src="images/products-v2/{{p[0].VC_SKU}}-1.jpg" alt="{{p[0].title}}">
                    </a>
                    <div class="row">
                        <a href="#">
                            <div class="col-lg-12 no-padding">
                                <div class="row">
                                    <div class="col-lg-12 grid-price-after">RS.{{p[0].price_after_discount}}
                                    </div>
                                </div>

                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td data-ng-if="p.length>1" style="width:15%;">
        <div class="row">
            <div class="col-lg-12 no-padding">
                <div class="row">
                    <a data-ng-href="p-product.php?id={{p[1].id}}">
                        <img class="img-responsive" data-ng-src="images/products-v2/{{p[1].VC_SKU}}-1.jpg" alt="{{p[1].title}}">
                    </a>
                    <div class="row">
                        <a href="#">
                            <div class="col-lg-12 no-padding">
                                <div class="row">
                                    <div class="col-lg-12 grid-price-after">RS.{{p[1].price_after_discount}}
                                    </div>
                                </div>

                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td style="width:15%;" data-ng-if="p.length>2">
     <div class="row">
        <div class="col-lg-12 no-padding">
        <div class="row">
        <a data-ng-href="p-product.php?id={{p[2].id}}">
            <img class="img-responsive" data-ng-src="images/products-v2/{{p[2].VC_SKU}}-1.jpg" alt="{{p[2].title}}">
        </a>
        <div class="row">
        <a href="#">
            <div class="col-lg-12 no-padding">
                <div class="row">
                    <div class="col-lg-12 grid-price-after">RS.{{p[2].price_after_discount}}
                    </div>
                </div>

            </div>
        </a>
        </div>
        </div>
        </div>
     </div>
    </td>
    <td data-ng-if="p.length>3" style="width:15%;">
        <div class="row">
            <div class="col-lg-12 no-padding">
                <div class="row">
                    <a data-ng-href="p-product.php?id={{p[3].id}}">
                        <img class="img-responsive" data-ng-src="images/products-v2/{{p[3].VC_SKU}}-1.jpg" alt="{{p[3].title}}">
                    </a>
                    <div class="row">
                        <a href="#">
                            <div class="col-lg-12 no-padding">
                                <div class="row">
                                    <div class="col-lg-12 grid-price-after">RS.{{p[3].price_after_discount}}
                                    </div>
                                </div>

                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </td>
  </tr>
     
  <tr>
    <td>
        SKU
    </td>
    <td data-ng-if="p.length>0">
        {{p[0].VC_SKU}}
    </td>
    <td data-ng-if="p.length>1">
        {{p[1].VC_SKU}}
    </td>
    <td data-ng-if="p.length>2">
        {{p[2].VC_SKU}}
    </td>
    <td data-ng-if="p.length>3">
        {{p[3].VC_SKU}}
    </td>
  </tr>
     
  <tr>
    <td>
        Product Type
    </td>
    <td data-ng-if="p.length>0">
        {{p[0].category}}
    </td>
    <td data-ng-if="p.length>1">
        {{p[1].category}}
    </td>
    <td data-ng-if="p.length>2">
        {{p[2].category}}
    </td>
    <td data-ng-if="p.length>3">
        {{p[3].category}}
    </td>
  </tr>
     
  <tr>
    <td>
        Brand
    </td>
    <td data-ng-if="p.length>0">
        {{p[0].supplier_name}}
    </td>
    <td data-ng-if="p.length>1">
        {{p[1].supplier_name}}
    </td>
    <td data-ng-if="p.length>2">
        {{p[2].supplier_name}}
    </td>
    <td data-ng-if="p.length>3">
        {{p[3].supplier_name}}
    </td>
  </tr>
     
  <tr>
    <td>
        Item Package Quantity
    </td>
    <td data-ng-if="p.length>0">
        {{p[0].unit_quantity}}
    </td>
    <td data-ng-if="p.length>1">
        {{p[1].unit_quantity}}
    </td>
    <td data-ng-if="p.length>2">
        {{p[2].unit_quantity}}
    </td>
    <td data-ng-if="p.length>3">
        {{p[3].unit_quantity}}
    </td>
  </tr>
     
  <tr>
    <td>
        Occassion
    </td>
    <td data-ng-if="p.length>0">
        {{p[0].occasion}}
    </td>
    <td data-ng-if="p.length>1">
        {{p[1].occasion}}
    </td>
    <td data-ng-if="p.length>2">
        {{p[2].occasion}}
    </td>
    <td data-ng-if="p.length>3">
        {{p[3].occasion}}
    </td>
  </tr>
     
  <tr>
    <td>
        Metal
    </td>
    <td data-ng-if="p.length>0">
        {{p[0].metal}}
    </td>
    <td data-ng-if="p.length>1">
        {{p[1].metal}}
    </td>
    <td data-ng-if="p.length>2">
        {{p[2].metal}}
    </td>
    <td data-ng-if="p.length>3">
        {{p[3].metal}}
    </td>
  </tr>
     
  <tr>
    <td>
        Purity
    </td>
    <td data-ng-if="p.length>0">
        {{p[0].purity}}
    </td>
    <td data-ng-if="p.length>1">
        {{p[1].purity}}
    </td>
    <td data-ng-if="p.length>2">
        {{p[2].purity}}
    </td>
    <td data-ng-if="p.length>3">
        {{p[3].purity}}
    </td>
  </tr>
     
  <tr>
    <td>
        Approx. Metal Weight
    </td>
    <td data-ng-if="p.length>0">
        {{p[0].weight}}
    </td>
    <td data-ng-if="p.length>1">
        {{p[1].weight}}
    </td>
    <td data-ng-if="p.length>2">
        {{p[2].weight}}
    </td>
    <td data-ng-if="p.length>3">
        {{p[3].weight}}
    </td>
  </tr>
     
  <tr>
    <td>
        Available Sizes
    </td>
    <td data-ng-if="p.length>0">
        {{p[0].ring_size}}
    </td>
    <td data-ng-if="p.length>1">
        {{p[1].ring_size}}
    </td>
    <td data-ng-if="p.length>2">
        {{p[2].ring_size}}
    </td>
    <td data-ng-if="p.length>3">
        {{p[3].ring_size}}
    </td>
  </tr>
     
  <tr>
    <td>
        Wishlist
    </td>
    <td data-ng-if="p.length>0">
    <div class="row" style="padding-left:85px;">
        <div data-ng-init="wIcon='images/compare/icons/wishlist.png'" class="col-sm-4 no-padding-left-right">

            <a data-ng-mouseover="wIcon='images/compare/icons/wishlist-filled.png'" data-ng-mouseout="wIcon='images/compare/icons/wishlist.png'" data-ng-click="addToWatchlist(p)">
                <img data-ng-src="{{wIcon}}" alt="wishlist">
            </a>

        </div>
    </div>
    </td>
    <td data-ng-if="p.length>1">
    <div class="row" style="padding-left:85px;">
        <div data-ng-init="wIcon='images/compare/icons/wishlist.png'" class="col-sm-4 no-padding-left-right">

            <a data-ng-mouseover="wIcon='images/compare/icons/wishlist-filled.png'" data-ng-mouseout="wIcon='images/compare/icons/wishlist.png'" data-ng-click="addToWatchlist(p)">
                <img data-ng-src="{{wIcon}}" alt="wishlist">
            </a>

        </div>
    </div>
    </td>
    <td data-ng-if="p.length>2">
    <div class="row" style="padding-left:85px;">
        <div data-ng-init="wIcon='images/compare/icons/wishlist.png'" class="col-sm-4 no-padding-left-right">

            <a data-ng-mouseover="wIcon='images/compare/icons/wishlist-filled.png'" data-ng-mouseout="wIcon='images/compare/icons/wishlist.png'" data-ng-click="addToWatchlist(p)">
                <img data-ng-src="{{wIcon}}" alt="wishlist">
            </a>

        </div>
    </div>
    </td>
    <td data-ng-if="p.length>3">
    <div class="row" style="padding-left:85px;">
        <div data-ng-init="wIcon='images/compare/icons/wishlist.png'" class="col-sm-4 no-padding-left-right">

            <a data-ng-mouseover="wIcon='images/compare/icons/wishlist-filled.png'" data-ng-mouseout="wIcon='images/compare/icons/wishlist.png'" data-ng-click="addToWatchlist(p)">
                <img data-ng-src="{{wIcon}}" alt="wishlist">
            </a>

        </div>
    </div>
    </td>
  </tr>
 </table>
</div>

</div>
<!--End of container-->

</div>
    
<vivo-footer></vivo-footer>
    
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoCompare.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!-- Start include Controller for angular -->
    
<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>        