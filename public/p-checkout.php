<!DOCTYPE html>
<html lang="en" data-ng-app="vivoCheckout">

<head>
    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="https://www.vivocarat.com/p-checkout.php">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m-index.html#/i/checkout">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m-index.html#/i/checkout" />

    <link href="css/style.css" rel="stylesheet" media="all">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/checkout";
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1026045347487468&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->    
</head>

<body ng-cloak>

    <style>
    /*CSS for materializing input fields*/

        * {
            box-sizing: border-box;
        }
        /* form starting stylings ------------------------------- */

        .group {
            position: relative;
        }

        input {
            font-size: 13px;
            padding: 10px 10px 10px 5px;
            display: block;
            width: 300px;
            border: none;
            border-bottom: 1px solid #888888;
            /*For Mozilla Gecko browser rendering compatibility,make box shadow nonw*/
            box-shadow: none;
        }

        input:focus {
            outline: none;
        }
        /* LABEL ======================================= */

        label {
            color: #797979 !important;
            font-size: 15px;
            font-weight: normal;
            font-family: 'leela';
            position: absolute;
            pointer-events: none;
            left: 18px;
            top: -10px;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }
        /* active state */

        input:focus ~ label,
        input:valid ~ label {
            top: -20px;
            font-size: 14px;
            color: #E62739;
        }
        /* BOTTOM BARS ================================= */

        .bar {
            position: relative;
            display: block;
            width: 100%;
        }

        .bar:before,
        .bar:after {
            content: '';
            height: 2px;
            width: 0;
            bottom: 1px;
            position: absolute;
            background: #E62739;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }

        .bar:before {
            left: 50%;
        }

        .bar:after {
            right: 50%;
        }
        /* active state */

        input:focus ~ .bar:before,
        input:focus ~ .bar:after {
            width: 50%;
        }
        /* HIGHLIGHTER ================================== */

        .highlight {
            position: absolute;
            height: 60%;
            width: 100px;
            top: 25%;
            left: 0;
            pointer-events: none;
            opacity: 0.5;
        }
        /* active state */

        input:focus ~ .highlight {
            -webkit-animation: inputHighlighter 0.3s ease;
            -moz-animation: inputHighlighter 0.3s ease;
            animation: inputHighlighter 0.3s ease;
        }
        /* ANIMATIONS ================ */

        @-webkit-keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }

        @-moz-keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }

        @keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }
        /*END of Material CSS design*/
        /*CSS for textarea Material CSS design*/
        /*------------------------------input type text and textarea and other starts-----*/

        .form-radio,
        .form-group {
            position: relative;
        }

        .form-inline > .form-group,
        .form-inline > .btn {
            display: inline-block;
            margin-bottom: 0;
        }

        .form-group input {
            height: 1.9rem;
        }

        .form-group textarea {
            resize: none;
        }

        .form-group .control-label {
            position: absolute;
            -webkit-transition: all 0.28s ease;
            transition: all 0.28s ease;
        }

        .form-group .bar {
            position: relative;
            border-bottom: 0.0625rem solid #999;
            display: block;
        }

        .form-group .bar::before {
            content: '';
            height: 0.125rem;
            width: 0;
            left: 50%;
            bottom: -0.0625rem;
            position: absolute;
            background: #E62739;
            -webkit-transition: left 0.28s ease, width 0.28s ease;
            transition: left 0.28s ease, width 0.28s ease;
            z-index: 2;
        }

        .form-group input,
        .form-group textarea {
            display: block;
            background: none;
            margin-top: 28px;
            font-size: 18px;
            border-width: 0;
            border-color: transparent;
            line-height: 1.9;
            width: 100%;
            color: transparent;
            -webkit-transition: all 0.28s ease;
            transition: all 0.28s ease;
            box-shadow: none;
        }

        .form-group textarea:focus,
        .form-group textarea:valid,
        .form-group textarea.has-value {
            color: #333;
        }

        .form-group textarea:focus ~ .control-label,
        .form-group textarea:valid ~ .control-label,
        .form-group textarea.form-file ~ .control-label,
        .form-group textarea.has-value ~ .control-label {
            font-size: 0.8rem;
            color: gray;
            top: -1rem;
            left: 0;
        }

        .form-group textarea:focus {
            outline: none;
        }

        .form-group textarea:focus ~ .control-label {
            color: #E62739;
        }

        .form-group textarea:focus ~ .bar::before {
            width: 100%;
            left: 0;
        }
        /*------------------------------input type text and textarea and other ends-----*/
        /*END of textarea Material design CSS*/
        /*Breadcrumb CSS*/

        ul,
        li {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        #crumbs {
            height: 77px;
            border: 1px solid #e5e5e5;
            font-family: leela;
            font-size: 15px;
            font-weight: bold;
        }

        #crumbs li {
            float: left;
            line-height: 77px;
            padding-left: 85px;
        }

        #crumbs li a {
            background: url(images/checkout/icons/crumbs.png) no-repeat right center;
            display: block;
            padding-right: 100px;
            color: #797979 !important;
        }
        /*END of Breadcrumb CSS*/
        /*Cart progress bar CSS*/

        .progress {
            height: 3px !important;
            border-radius: 10px !important;
            box-shadow: none;
            -webkit-box-shadow: none;
            background-color: transparent;
        }

        .progress-bar {
            box-shadow: none;
            border-radius: 10px;
            -webkit-box-shadow: none;
            background: #e62739;
            /* Old browsers */
        }

/*
        #crumbs>li>a.active {
            color: #e62739 !important;
        }

*/
        .big-ht {
            height: 550px;
        }

        .small-ht {
            height: 260px;
        }
        /*
    .progress-bar:after {
        content: '';
        display: block;
        position: absolute;
        height: 10px;
        width: 10px;
        border-radius: 50%;
        left: 14%;
        background: #e62739;
        margin-top: -3px;
    }
*/
        /*END of cart progress bar CSS*/
        /*Radio buttons CSS*/
        /*END of CSS*/
		
		input[type="text"]::-webkit-input-placeholder {
        color: #797979 !important;
        }

        input[type="text"]:-moz-placeholder { /* Firefox 18- */
        color: #797979 !important;  
        }

        input[type="text"]::-moz-placeholder {  /* Firefox 19+ */
        color: #797979 !important;  
        }

        input[type="text"]:-ms-input-placeholder {  
        color: #797979 !important;  
        }
    </style>
    <vivo-header></vivo-header>

<div data-ng-controller='checkoutCtrl'>

<div class="container padding-bottom-60px">

 <div class="row well text-center no-products-in-cart" data-ng-hide="cart.length>0">
     No products in cart...
  <a class="shop-more" href="default.php">Shop more</a>
 </div>

<div data-ng-show="cart.length>0">
 <div class="tabs-left">
  <div class="row">
   <div class="col-lg-12">
    <ul id="crumbs" class="cartGrad" role="tablist">

     <li id="a-tab" role="presentation">
       <a role="tab" data-toggle="tab" class="active">Cart</a>
     </li>

     <li id="b-tab" role="presentation">
      <a role="tab" data-toggle="tab" class="grey">Account details</a>
     </li>

     <li id="c-tab" role="presentation">
      <a role="tab" data-toggle="tab" class="">Shipping details</a>
     </li>

     <li id="d-tab" role="presentation">
      <a role="tab" data-toggle="tab" style="background: none;">Product summary</a>
     </li>

   </ul>
   </div>
  </div>

  <div style="padding-left:15px;">
   <div class="progress">
    <div class="progress-bar" role="progressbar" style="width: 9%;">

     </div>
   </div>
  </div>

  <div class="row">

  <div class="col-lg-12 tab-content no-padding">
   <div role="tabpanel" class="row tab-pane active padding-top-0px padding-left-30px" id="a">
   <div class="panel-body no-padding-left-right">

   <div style="border-bottom: 1px solid #e8e8e8;box-shadow: none;margin-bottom: 20px;" class="col-lg-12 vivo-panel-checkout-product no-padding" data-ng-class="c.item.is_combo=='1'?'big-ht':'small-ht'" data-ng-repeat="c in cart track by $index">

   <div class="row">
    <div class="col-lg-3 no-padding-left-right">
     <img data-ng-src="images/products-v2/{{c.item.VC_SKU}}-1.jpg" style="border: 1px solid #e8e8e8;width:232px;height:210px;" alt="{{c.item.title}}">
    </div>

    <div class="col-lg-5 pad-left-right" style="padding-top: 20px;">

     <div class="row">
      <div class="col-lg-12 text-left padding-top-0px padding-bottom-20px">
       <span style="font-size: 20px;font-weight: bold;font-family:'leela';">
        <a class="grey-colour" data-ng-href="p-product.php?id={{c.item.id}}">{{c.item.title}}</a>
       </span>
      </div>
     </div>

     <div class="row">
      <div class="col-lg-12">
       <img data-ng-src="images/header/brands logos dropdown/{{c.item.supplier_name}} hover.png" style="width:50px;" title={{c.item.supplier_name}} alt="{{c.item.supplier_name}}">
      </div>
     </div>

     <div class="row" style="padding-top: 15px;">
      <div class="col-lg-12">

       <a class="btn btn-xs btn-vivo subtract-quantity" data-ng-click="subQuantity(c,$index)">-</a>
          {{c.quantity}}
       <a data-ng-click="addQuantity(c,$index)" class="btn btn-xs btn-vivo add-quantity">+</a>

      </div>
     </div>


    <div data-ng-show="c.item.ring_size != null && c.item.ring_size != '' && c.item.ring_size != 0" class="row" style="padding-top:15px;">
      <div class="col-lg-12 grey-colour">
       Size : {{c.item.ring_size}}
      </div>
     </div>


    <div data-ng-show="c.item.bangle_size != null && c.item.bangle_size != '' && c.item.bangle_size != 0" class="row" style="padding-top:15px;">
      <div class="col-lg-12 grey-colour">
       Size : {{c.item.bangle_size}}
      </div>
     </div>


    <div data-ng-show="c.item.bracelet_size != null && c.item.bracelet_size != '' && c.item.bracelet_size != 0" class="row" style="padding-top:15px;">
      <div class="col-lg-12 grey-colour">
       Size : {{c.item.bracelet_size}}
      </div>
     </div>

     <div class="row" style="padding-top:15px;">
      <div class="col-lg-12">
       <a data-ng-click="removeFromCart($index)">
       <img data-ng-src="images/checkout/icons/remove.png" style="padding-right: 10px;" alt="remove">
       <span class="remove-link">Remove</span>
       </a>
      </div>
     </div>

   </div>

   <div class="col-lg-4 pad-left-right" style="padding-top: 20px;">

    <div class="row">
     <div class="col-lg-7 rightfont pad-left-right" style="padding-right: 24px !important;">
         Price
     </div>

     <div class="col-lg-5 rightfont pad-left-right" align="right">
         RS.&nbsp;{{c.item.price_before_discount | INR}}
     </div>
    </div>

     <div class="row">
      <div class="col-lg-7 rightfont pad-left-right">
          Discount
      </div>

      <div class="col-lg-5 rightfont pad-left-right" align="right">
          {{c.item.discount}} %
      </div>
     </div>

     <div class="row">
      <div class="col-lg-7 rightfont pad-left-right">
          Quantity
      </div>

      <div class="col-lg-5 rightfont pad-left-right" align="right">
          {{c.quantity}}
      </div>
     </div>

     <div class="row">
      <div class="col-lg-7 rightfont pad-left-right">
          Subtotal
      </div>

      <div class="col-lg-5 rightfont pad-left-right" align="right">
          RS.&nbsp;{{c.item.price_after_discount*c.quantity | INR}}
      </div>
     </div>

    </div>

  </div>

   <div data-ng-if="c.item.is_combo=='1'" class="padding-top-20px">
    <span class="free-gift-text">
     Free Gift
    </span>
   </div>

   <div class="row padding-top-20px" data-ng-if="c.item.is_combo=='1'" data-ng-init="getCombo(c.item.combo_id)">

    <div class="col-lg-3 no-padding-left">
     <img data-ng-src="images/products-v2/{{combo.VC_SKU}}-1.jpg" style="border: 1px solid #e8e8e8;width:232px;height:210px;" alt="{{combo.title}}">
    </div>

    <div class="col-lg-5">
     <span class="bold-font normal-text" style="font-size: 20px;">
      <a class="grey-colour">
        {{combo.title}}
      </a>
     </span>
    </div>

    <div class="col-lg-4">
     <div class="row">
      <div class="col-lg-7 rightfont pad-left-right" style="padding-right: 24px !important;">
       Price
      </div>

      <div class="col-lg-5 rightfont pad-left-right" align="right">
       RS.&nbsp;{{combo.price_before_discount | INR}}
      </div>
     </div>

     <div class="row">
      <div class="col-lg-7 rightfont pad-left-right">
       Quantity
      </div>

      <div class="col-lg-5 rightfont pad-left-right" align="right">
       1
      </div>
     </div>

     <div class="row">
      <div class="col-lg-7 rightfont pad-left-right">
       Subtotal
      </div>

      <div class="col-lg-5 rightfont pad-left-right" align="right">
       FREE
      </div>
     </div>
    </div>
   </div>

   </div>
  </div>

  <div class="row">
  <div class="col-lg-7"></div>

  <div class="col-lg-5 no-padding-left-right">

  <div class="row">
   <div class="col-lg-9 normal-text" style="font-size:15px;padding-left: 68px;">
    <input type="text" Placeholder="Apply promo code" class="promocode" size="100" data-ng-model="code" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Apply promo code'">
       <!-- <select class="promocode" data-ng-model="code">
            <option value="" selected disabled>Apply promo code</option>

            <option data-ng-repeat="cp in allcoupons">{{cp.coupon_code}}</option>
        </select> -->
   </div>

   <div class="col-lg-3 no-padding-left-right">
    <a data-ng-click="applyCode()" style="z-index: 0;font-size:15px;" class="btn btn-sm btn-block btn-vivo normal-text">Apply</a>
   </div>
  </div>

  <div class="row" data-ng-if="isCouponApplied" style="padding-top:12px;">
   <div class="col-lg-12 text-right">
    <span class="grey-colour" style="font-size:15px;">
    <img class="padding-right-10px" src="images/checkout/icons/tick.png">
         {{disp_code}} promo code applied successfully &nbsp;&nbsp;&nbsp;
    <a data-ng-click="removeCode()">Remove</a>
    </span>
   </div>
  </div>

  <div class="row price_box_checkout_page normal-text" style="margin-left: 0px;margin-top: 23px;">
   <div class="col-lg-6 padding-top-5px" style="text-align:center;padding-bottom: 5px;">
   <h5 class="total-price-text">
       TOTAL
   </h5>
   </div>

   <div class="col-lg-6 text-center padding-top-5px padding-bottom-5px">
   <h5 class="total-price-text">
       RS. {{total | INR}}
   </h5>
   </div>

  </div>

  <a data-ng-click="confirmOrder(total)" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" aria-expanded="true" class="proceedbutton btn btn-block btn-vivo-checkout">
      PROCEED TO ACCOUNT DETAILS
  </a>

  <h6 style="text-align:right;margin-top: 15px;">
   <a href="default.php" class="continue-shopping-text">
       Continue shopping
   </a>
  </h6>

  </div>
 </div>

</div>

   <div role="tabpanel" class="tab-pane row no-padding" id="b">
    <div class="panel-body no-padding">

    <div class="col-sm-6 padding-top-0px" style="border-right:1px solid #eee;margin-top:0px !important;">
    <div class="vivo-login-panel padding-top-0px">

    <h4 class="text-center gothbody" style="font-size:15px;">
     New Customer
    </h4>

    <div class="register_account">
     <div class="wrap">
      <form name="form">
       <div class="row text-center">
        <div class="col-lg-12 placeholdlight padding-2px">

         <div class="group bottom-margin-15px-padding-10px">
          <input class="normal-text" type="text" data-ng-model="customer_new.name" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Name
          <sup class="required-star">*</sup>
          </label>
         </div>

         <div class="group bottom-margin-15px-padding-10px">
          <input class="normal-text" type="text" data-ng-model="customer_new.phone" maxlength="13" pattern="(\+?\d[- .]*){7,13}" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Phone Number
           <sup class="required-star">*</sup>
          </label>
         </div>

         <div class="group bottom-margin-15px-padding-10px">
          <input class="normal-text" type="email" data-ng-model="customer_new.email" name="email" data-error="Email address is invalid" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Email
           <sup class="required-star">*</sup>
          </label>
         </div>

         <div class="group" style="margin-bottom: 15px;">
          <input class="normal-text" type="password" data-ng-class="isPasswordError?'error-text':'correct-text'" name="password" data-ng-model="customer_new.password" required>
             {{message}}
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Password
           <sup class="required-star">*</sup>
          </label>
         </div>

         <div class="row normal-text grey-colour" align="left" style="font-size:15px;">
          By clicking 'Create Account' you agree to the <a class="grey-colour underline" href="p-terms.php">Terms &amp; Conditions</a>
         </div>
            
        </div>
       </div>
      </form>

      <div style="padding-left: 48px;padding-right: 48px;">
       <input class="btn btn-vivo-checkout border-radius-none bold-font uppercase white-colour" type="submit" style="margin-top:20px;font-size: 18px;" name="Submit" data-ng-click="signup()" value="Create Account">
      </div>

     </div>
    </div>
        
    </div>
    </div>

    <div class="col-sm-6">
     <div class="vivo-login-panel padding-top-0px">
      <h4 class="text-center gothbody" style="margin-bottom:30px !important;margin-top:0px !important;font-size:15px;">
          Registered Customers
      </h4>
      <div class="register_account">
      <div class="wrap">
      <form>
       <div class="row text-center">
        <div class="col-lg-12 placeholdlight padding-2px">

         <div class="group bottom-margin-15px-padding-10px">
          <input class="normal-text" id="modlgn_username" data-ng-model="customer.email" type="text" name="email" size="18" autocomplete="off" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Email
           <sup class="required-star">*</sup>
          </label>
         </div>

        <div class="group" style="margin-bottom: 15px;">
         <input class="normal-text" type="password" id="modlgn_passwd" name="password" data-ng-model="customer.password" size="18" autocomplete="off" required>
         <span class="highlight"></span>
         <span class="bar"></span>
         <label>Password
          <sup class="required-star">*</sup>
         </label>
        </div>

        <div class="underline" align="left" style="padding-top: 10px;font-size:15px;">
        <a class="grey-colour normal-text" data-toggle="modal" data-target="#myModal">Forgot Your Password?</a>
        </div>

       </div>
      </div>
     </form>

     <div style="padding-left: 48px;padding-right: 48px;">
     <input type="submit" name="Submit" class="btn btn-vivo-checkout border-radius-none uppercase bold-font" data-ng-click="loginUser(customer)" style="margin-top:20px;font-size: 18px;color: white !important;" class="button" value="Login">
     </div>

     <div class="row">
      <p class="text-center normal-text bold-font grey-colour margin-top-30px" style="margin-bottom:30px;">OR</p>
     </div>

     <div class="row">
      <div data-ng-click="authenticate('facebook')" class="col-lg-6 no-padding-left">
      <div class="row btn facebook-button">

      <div class="col-lg-8 normal-text bold-font white-colour" style="font-size: 18px;">
           Login with
      </div>

      <div class="col-lg-4">
       <img src="images/login/icons/facebook login.png" alt="Facebook login">
      </div>

      </div>
      </div>

     <div data-ng-click="authenticate('google')" class="col-lg-6">
     <div class="row btn google-plus-button">

     <div class="col-lg-8 white-colour normal-text bold-font" style="font-size: 18px;">
        Login with
     </div>

     <div class="col-lg-4">
      <img src="images/login/icons/google login.png" alt="Google login">
     </div>

     </div>
     </div>
    </div>

    <fb:login-button scope="public_profile,email" show-faces="true" max-rows="1" size="large"></fb:login-button>

         </div>
        </div>
     </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
       <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel" style="font-family: 'leela';">Enter your email</h4>
        </div>
        <div class="modal-body">
            <input type="email" data-ng-model="femail" required>
        </div>
        <div class="modal-footer">
            <a data-ng-click="forgotPassword()" class="btn btn-vivo btn-sm" style="font-family: 'leela';"> Forgot Password</a>
            <a class="btn btn-default btn-sx" data-dismiss="modal" style="font-family: 'leela';">Cancel</a>
       </div>
      </div>
     </div>
    </div>

    </div>
   </div>

   <div role="tabpanel" data-ng-init="isNewAddress=true;" class="tab-pane row" id="c" style="padding-top:0px;">
    <div class="panel-body no-padding">
    <div class="row">
     <div class="col-sm-6 no-padding">
     <div class="vivo-login-panel no-padding-left-right padding-top-0px" style="margin-top:0px !important;">

     <h4 class="text-center bold-font normal-text grey-colour" style="margin-top:0px !important;margin-bottom: 30px;font-size:15px;">
         Shipping Address
     </h4>

     <div class="register_account">
     <div class="wrap">

     <div class="row vivo-row">

     <div class="col-lg-12 placeholdlight group margin-bottom-20px">
      <input type="text" class="normal-text" data-ng-model="ship_address.name" required>
      <span class="highlight"></span>
      <span class="bar"></span>
      <label>Name
       <sup class="required-star">*</sup>
      </label>
     </div>

     </div>

     <div class="row vivo-row margin-bottom-20px">

      <div class="col-lg-12 placeholdlight form-group margin-bottom-20px">
       <textarea type="text" class="normal-text" data-ng-model="ship_address.first_line" required></textarea>
       <label for="textarea">Address
        <sup class="required-star">*</sup>
       </label>
       <i class="bar"></i>
      </div>
     </div>

     <div class="row vivo-row margin-bottom-20px">

      <div class="col-md-6 placeholdlight group margin-bottom-20px">
       <input type="text" class="normal-text" data-ng-model="ship_address.phone" style="width: 150px;" required>
       <span class="highlight"></span>
       <span class="bar"></span>
       <label>Phone No
        <sup class="required-star">*</sup>
       </label>
      </div>

      <div class="col-md-6 placeholdlight group margin-bottom-20px">
       <input type="text" class="normal-text" data-ng-model="ship_address.pin" style="width: 150px;" required>
       <span class="highlight"></span>
       <span class="bar"></span>
       <label>Pincode
        <sup class="required-star">*</sup>
       </label>
      </div>

     </div>

     <div class="row vivo-row">

      <div class="col-md-6 placeholdlight group margin-bottom-20px">
       <input type="text" class="normal-text" data-ng-model="ship_address.city" style="width:150px;" required>
       <span class="highlight"></span>
       <span class="bar"></span>
       <label>City
        <sup class="required-star">*</sup>
       </label>
      </div>

      <div class="col-md-6 placeholdlight group margin-bottom-20px">
       <input type="text" class="normal-text" data-ng-model="ship_address.state" style="width:150px;" required>
       <span class="highlight"></span>
       <span class="bar"></span>
       <label>State
        <sup class="required-star">*</sup>
       </label>
      </div>

     </div>

     <div class="row vivo-row">

      <div class="col-lg-2">
       <input type="checkbox" name="address" data-ng-model="isNewAddress" style="width: 50%;">
      </div>

      <div class="col-lg-10 no-padding">
       <h class="normal-text text-left grey-colour" style="font-size:15px;">
           My billing address is the same as shipping address
       </h>
      </div>

     </div>

     </div>
     </div>
     </div>
    </div>

    <div class="col-sm-6 no-padding margin-top-0px">
     <div data-ng-show="!isNewAddress" class="vivo-login-panel padding-top-0px no-padding-left" style="border-left: 1px solid #E5E5E5;">

     <h4 class="text-center margin-top-0px normal-text bold-font uppercase grey-colour no-padding-left" style="margin-bottom: 30px;font-size:15px;">
         Billing Address
     </h4>

     <div class="register_account">
     <div class="wrap">

     <div class="row vivo-row margin-bottom-20px">

      <div class="col-lg-12 placeholdlight group" style="margin-bottom:0px">
       <input type="text" class="normal-text" data-ng-model="address.name" required>
       <span class="highlight"></span>
       <span class="bar"></span>
       <label>Name
        <sup class="required-star">*</sup>
       </label>
      </div>

     </div>

     <div class="row vivo-row margin-bottom-20px">

      <div class="col-lg-12 placeholdlight form-group margin-bottom-20px">
       <textarea type="text" class="normal-text" data-ng-model="address.first_line" required></textarea>
       <label for="textarea" class="normal-text">Address
        <sup class="required-star">*</sup>
       </label>
       <i class="bar"></i>
      </div>

     </div>

     <div class="row vivo-row margin-bottom-20px">

      <div class="col-md-6 placeholdlight group margin-bottom-20px">
       <input type="text" class="normal-text" data-ng-model="address.phone" style="width:150px;" required>
       <span class="highlight"></span>
       <span class="bar"></span>
       <label>Phone No
        <sup class="required-star">*</sup>
       </label>
      </div>

      <div class="col-md-6 placeholdlight group margin-bottom-20px">
       <input type="text" class="vivo-input" data-ng-model="address.pin" style="width:150px;" required>
       <span class="highlight"></span>
       <span class="bar"></span>
       <label>Pincode
        <sup class="required-star">*</sup>
       </label>
      </div>

     </div>

     <div class="row vivo-row">

      <div class="col-md-6 placeholdlight group margin-bottom-20px">
       <input type="text" class="vivo-input" data-ng-model="address.city" style="width:150px;" required>
       <span class="highlight"></span>
       <span class="bar"></span>
       <label>City
        <sup class="required-star">*</sup>
       </label>
      </div>

      <div class="col-md-6 placeholdlight margin-bottom-20px">
       <input type="text" class="vivo-input" data-ng-model="address.state" style="width:150px;" required>
       <span class="highlight"></span>
       <span class="bar"></span>
       <label>State
        <sup class="required-star">*</sup>
       </label>
      </div>

     </div>

       </div>
       </div>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-lg-2"></div>

     <div class="col-lg-6">
      <input class="btn btn-vivo-checkout proceedbutton" type="submit" style="left:14%;margin-bottom: 50px;" name="Submit" data-ng-click="addAddress()" value="PROCEED TO PRODUCT SUMMARY">
     </div>

     <div class="col-lg-4"></div>
    </div>

      </div>
   </div>

   <div class="tab-pane row padding-top-0px" id="d">
    <div class="panel-body normal-text" style="font-size:15px;">

     <div class="row padding-bottom-10px">
      <div class="col-lg-2 grey-colour bold-font">
          Name
      </div>

      <div class="col-lg-1 no-padding-left-right">
          :
      </div>

      <div class="col-lg-8 grey-colour no-padding-left-right">
          {{order.name}}
      </div>
     </div>

     <div class="row padding-bottom-10px">
      <div class="col-lg-2 grey-colour bold-font">
          Email
      </div>

      <div class="col-lg-1 no-padding-left-right">
          :
      </div>

      <div class="col-lg-8 grey-colour no-padding-left-right">
          {{order.email}}
      </div>
     </div>

     <div class="row padding-bottom-10px">
      <div class="col-lg-2 grey-colour bold-font">
          Phone
      </div>

      <div class="col-lg-1 no-padding-left-right">
          :
      </div>

      <div class="col-lg-8 grey-colour no-padding-left-right">
          {{order.phone}}
      </div>
     </div>

     <div class="row padding-bottom-10px">
      <div class="col-lg-2 grey-colour bold-font">
          Billing Address
      </div>

      <div class="col-lg-1 no-padding-left-right">
          :
      </div>

      <div class="col-lg-8 grey-colour no-padding-left-right">
          {{order.first_line}}
          {{order.city}}
          {{order.state}} - {{order.pin}}
          {{order.country}}
      </div>
     </div>

     <div class="row padding-bottom-10px">
      <div class="col-lg-2 grey-colour bold-font">
          Shipping Address
      </div>

      <div class="col-lg-1 no-padding-left-right">
          :
      </div>

      <div class="col-lg-8 grey-colour no-padding-left-right">
          {{ship.first_line}}
          {{ship.city}}
          {{ship.state}} - {{ship.pin}}
          {{ship.country}}
      </div>
     </div>


      <div class="col-lg-12 text-center padding-top-20px no-padding-left-right product-summary-structure" data-ng-repeat="c in products">

       <div class="row">

        <div class="col-lg-3 no-padding-left-right">
         <img class="product-summary-image" data-ng-src="images/products-v2/{{c.item.VC_SKU}}-1.jpg" alt="{{c.item.title}}">
        </div>

        <div class="col-lg-5 padding-top-20px">

         <div class="row">
          <div class="col-lg-12 text-left padding-top-0px padding-bottom-20px">
           <span class="bold-font normal-text" style="font-size: 20px;">
           <a class="grey-colour" data-ng-href="p-product.php?id={{c.item.id}}">
                   {{c.item.title}}
           </a>
           </span>
          </div>
        </div>

        <div class="row">
         <div class="col-lg-12">
          <img data-ng-src="images/header/brands logos dropdown/{{c.item.supplier_name}} hover.png" style="float: left;width: 50px;" alt="{{c.item.supplier_name}}">
         </div>
        </div>

        </div>

        <div class="col-lg-4 pad-left-right padding-top-20px">

         <div class="row">
          <div class="col-lg-7 rightfont pad-left-right" style="padding-right: 23px !important;">
              Price
          </div>

          <div class="col-lg-5 rightfont pad-left-right" align="right">
              RS.&nbsp;{{c.item.price_before_discount | INR}}
          </div>
         </div>

         <div class="row">
          <div class="col-lg-7 rightfont pad-left-right">
              Discount
          </div>

          <div class="col-lg-5 rightfont pad-left-right" align="right">
              {{c.item.discount}} %
          </div>
         </div>

        <div class="row">
         <div class="col-lg-7 rightfont pad-left-right" style="padding-bottom: 0px !important;">
             Quantity
         </div>

         <div class="col-lg-5 rightfont pad-left-right" align="right" style="padding-bottom: 0px !important;">
             {{c.quantity}}
         </div>
        </div>

        <hr style="color:#e8e8e8;margin-top:0px;margin-bottom: 10px;">

        <div class="row">
         <div class="col-lg-7 rightfont pad-left-right">
             Subtotal
         </div>

         <div class="col-lg-5 rightfont pad-left-right" align="right">
             RS.&nbsp;{{c.item.price_after_discount*c.quantity | INR}}
         </div>
        </div>

       </div>

      </div>

     </div>
    </div>

    <div class="row" style="padding-top: 10px;padding-bottom: 15px;">

     <div class="col-lg-3 col-lg-offset-3 text-left grey-colour normal-text" style="font-size: 15px;">
      <img src="images/checkout/icons/gift wrap.png" class="gift-wrap-position" alt="gitft wrap">
         I want to gift wrap
     </div>

     <div class="col-lg-1 no-padding-left-right">

      <input name="radioGroup1" id="radio3" data-ng-model="isGift" value="1" type="radio" style="cursor:pointer;width: 50%;">

      <label class="normal-text grey-colour" style="font-size:15px;top: 1px;padding-left: 30px;">
          Yes
      </label>

     </div>

    <div class="col-lg-1 no-padding-left-right">

     <input name="radioGroup1" id="radio4" data-ng-model="isGift" value="0" checked="" type="radio" style="cursor:pointer;width: 40%;">

     <label class="normal-text grey-colour" style="font-size:15px;top: 1px;padding-left: 20px;">No</label>

    </div>


<!--
    <h6 class="normal-text no-padding-left">
        Any particular instruction?
    </h6>

    <textarea data-ng-model="comment" rows="7" class="form-control" style="background-color: #eee;border-radius: 0px; width:293px !important;resize: none;padding-top: 10px;"> </textarea>
-->


 </div>

    <div class="row" style="background-color:#e8e8e8;padding-top: 25px;padding-bottom: 25px;border-bottom: #a6a6a6 2px solid;">

     <div class="col-lg-3 no-padding-right padding-top-10px">
      <h class="normal-text grey-colour" style="font-size:20px;padding-left: 20px;">PAYMENT MODE : </h>
     </div>

     <div class="col-lg-6">
      <div class="row">
       <div class="col-lg-2 no-padding-right text-left">
        <div class="row">
         <div class="col-lg-3 no-padding-left-right" style="padding-top: 3px;">
          <input class="pointer" type="radio" data-ng-model="isCod" name="cod2" value="1" style="cursor:pointer;width: 32px;">
         </div>

         <div class="col-lg-6">
          <h class="normal-text grey-colour" style="font-size:20px;">
              COD
          </h>
         </div>
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-lg-6 no-padding-right text-left">
        <div class="row">
         <div class="col-lg-1 no-padding-left-right" style="top: 3px;left: 4px;">
          <input class="pointer width-100-percent" type="radio" data-ng-model="isCod" name="cod2" value="0">
         </div>

         <div class="col-lg-9">
          <h class="normal-text grey-colour" style="font-size:20px;">
              Online Payment
          </h>
         </div>
        </div>
       </div>
      </div>

     </div>

      <div class="col-lg-3 text-right padding-top-10px">
       <span class="grand-total-text">
           Grand Total : {{total | INR}}/-
       </span>
      </div>

    </div>

    <div class="row padding-top-30px">
     <div class="col-lg-4 col-lg-offset-4">
      <a class="btn btn-vivo proceedbutton" data-ng-click="finalConfirmation(total)">
         Confirm Order
      </a>
     </div>
    </div>
   </div>
  </div>

  </div>
 </div>
</div>
       
</div>

</div>
    
<vivo-footer></vivo-footer>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoCheckout.js"></script>
<!-- end angularjs modules -->

<script src="app/data.js"></script>
<script src="app/directives.js"></script>

<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->

<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>
