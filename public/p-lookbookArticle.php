<?php
$desk = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if(isset($_GET['id']) && $_GET['id'] != '')
{
    $id = $_GET['id'];
    $mob = 'https://' . $_SERVER['HTTP_HOST'] . "/m-index.html#/i/lookbookArticle/".$id;
    
    $SITE_ROOT = "https://www.vivocarat.com/";
    
    $lData = getLData($SITE_ROOT,$id);
    $lData = json_decode($lData,true);
    
    $title = "VivoCarat | ".$lData[0]['title'];
    $meta_keywords = $lData[0]['category'];
    $meta_description = $lData[0]['lb_short_description'];
    $meta_robots = "index,follow";
    $og_type = "website";
    $og_title = "VivoCarat | ".$lData[0]['title'];
    //$og_image = 'https://' . $_SERVER['HTTP_HOST']."/".$img;
    $img = "images/lookbook/blogs/".$lData[0]['banner_img'].".jpg";
    $og_image = $SITE_ROOT.$img;
    $og_description = $lData[0]['lb_short_description'];
}

function getLData($siteRoot,$id) {

    $rawData = file_get_contents($siteRoot.'api/v1/getLookbookDetail?id='.$id);
    return $rawData;
}
?>
<!DOCTYPE html>
<html lang="en" data-ng-app="vivoLookbook">

<head>
    
    <title><?php echo $title; ?></title>
    <meta name="keywords" content="<?php echo $meta_keywords; ?>" >
    <meta name="description" content="<?php echo $meta_description; ?>" />
    <meta name="robots" content="<?php echo $meta_robots; ?>" >
    <meta property="og:description" content="<?php echo $og_description; ?>" />
    <meta property="og:image" content="<?php echo $og_image; ?>" />
    <meta property="og:title"         content="<?php echo $og_title; ?>" />
    <meta property="og:type"          content="<?php echo $og_type; ?>" />
    <meta property="og:url" content="<?php echo $desk; ?>" />

    <meta http-equiv="Content-Language" content="en" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />

    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    
    <link rel="canonical" href="<?php echo $desk; ?>">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo $mob; ?>">
    <link rel="alternate" media="handheld" href="<?php echo $mob; ?>" />

    <link href="css/style.css" rel="stylesheet" media="all">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/i/lookbookArticle/"+getUrlParameter('id');
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
          appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
          autoRegister: true,
          notifyButton: {
            enable: false /* Set to false to hide */
          }
        }]);
    </script>
    <!-- onesignal end   -->        
</head>
    
<style>
.bottom:after{
    background: -moz-linear-gradient(left, rgba(208, 208, 208, 1) 1%, rgba(240, 240, 240, 1) 90%, rgba(255, 255, 255, 0) 100%);
    background: -webkit-linear-gradient(left, rgba(208, 208, 208, 1) 1%, rgba(240, 240, 240, 1) 90%, rgba(255, 255, 255, 0) 100%);
    background: linear-gradient(to right, rgba(208, 208, 208, 1) 1%, rgba(240, 240, 240, 1) 90%, rgba(255, 255, 255, 0) 100%);
    filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#d0d0d0 ', endColorstr='#00ffffff', GradientType=1);
    height: 1px;
    width: 100%;
}
</style>
    
<body data-ng-controller="lookbookArticleCtrl" ng-cloak>

<vivo-header></vivo-header>

<div class="container padding-bottom-60px">

 <div class="row normal-text" style="padding:50px 0px 0px 0px;">
  <div class="col-md-9 no-padding-left">

   <div class="row">

    <div class="col-md-2">
     {{list.category}}
    </div>

    <div class="col-md-2 no-padding-left-right">
      {{list.created_at | dateToISO | date:'dd MMMM'}}
    </div>

   </div>

  </div>

  <div class="col-md-3 text-right">
   <div class="row">
    <div class="col-md-5 pull-right no-padding-left-right">
     <p>
      <a data-ng-href="p-lookbookArticle.php?id={{id-1}}">Previous</a>&nbsp;&nbsp;|&nbsp;
      <a data-ng-href="p-lookbookArticle.php?id={{id-1+2}}">Next</a>
     </p>
    </div>
   </div>
  </div>

 </div>

 <div class="row" style="padding-bottom:12px;">
  <div class="col-md-12">
   <p class="article-title">{{list.title}}</p>
  </div>
 </div>

 <div class="row padding-bottom-30px">
  <div class="col-md-12 normal-text">
   <span data-ng-bind-html='list.lb_short_description' data-ng-if="!readMore">
                
   </span>
                
   <span data-ng-if="readMore" data-ng-bind-html='list.lb_long_description' class="normal-text"> </span>&nbsp;<span data-ng-click='toggleReadMore()'>
     <a class="vivocarat-theme-colour bold-font italic">
        {{readMore?'read less':'..read more'}}
     </a>
   </span>

  </div>
 </div>

 <div class="row">

  <div class="col-md-6">
   <img data-ng-src="images/lookbook/blogs/{{list.banner_img}}.jpg" alt="{{list.banner_img}}">
  </div>

  <div class="col-md-6">

    <div class="row">

        <div class="col-md-4 no-padding-right">
            <p class="similar-looks-heading">SIMILAR LOOKS</p>
        </div>

        <div class="col-md-7 categoryGrad" style="margin-top: 9px;"></div>

    </div>

                <div class="row padding-top-30px padding-bottom-30px bottom">

                    <div class="col-md-5 padding-bottom-30px">
                     <a data-ng-href="p-product.php?id={{slist[0].id}}&title={{slist[0].slug}}">
                        <div class="product_image text-center padding-bottom-30px">
                            <img data-ng-src="images/products-v2/{{slist[0].VC_SKU}}-1.jpg" alt="{{slist[0].title}}">
                        </div>

                        <div class="text-center">
                            <a class="btn btn-book" data-ng-click="buyNow(slist[0])"> Buy Now </a>
                        </div>
                     </a>
                    </div>

                    <div class="col-md-5 padding-bottom-30px">
                     <a data-ng-href="p-product.php?id={{slist[1].id}}&title={{slist[1].slug}}">
                        <div class="text-center padding-bottom-30px">
                            <img data-ng-src="images/products-v2/{{slist[1].VC_SKU}}-1.jpg" alt="{{slist[1].title}}">
                        </div>

                        <div class="text-center">
                            <a class="btn btn-book" data-ng-click="buyNow(slist[1])"> Buy Now </a>
                        </div>
                     </a>
                    </div>

                </div>

                <div class="row" style="padding-top: 15px;">

                    <div class="col-md-5" style="padding-top: 36px;">
                     <a data-ng-href="p-product.php?id={{slist[2].id}}&title={{slist[2].slug}}">
                        <div class="product_image text-center padding-bottom-30px">
                            <img data-ng-src="images/products-v2/{{slist[2].VC_SKU}}-1.jpg" alt="{{slist[2].title}}">
                        </div>

                        <div class="text-center">
                            <a class="btn btn-book" data-ng-click="buyNow(slist[2])"> Buy Now </a>
                        </div>
                     </a>
                    </div>

                    <div class="col-md-5" style="padding-top: 36px;">
                     <a data-ng-href="p-product.php?id={{slist[3].id}}&title={{slist[3].slug}}">
                        <div class="text-center padding-bottom-30px">
                            <img data-ng-src="images/products-v2/{{slist[3].VC_SKU}}-1.jpg" alt="{{slist[3].title}}">
                        </div>

                        <div class="text-center">
                            <a class="btn btn-book" data-ng-click="buyNow(slist[3])"> Buy Now </a>
                        </div>
                     </a>
                    </div>

                </div>

            </div>

        </div>

 <div class="row padding-bottom-20px">
            <div class="col-lg-4 vivocarat-heading">RELATED STORIES</div>

            <div class="col-lg-8 categoryGrad margin-top-68px no-padding-left-right">

            </div>
        </div>

 <div class="row">
<a data-ng-href="p-lookbookArticle.php?id={{related[0].id}}">
         <div class="col-md-4">

                <div class="row" style="border:1px solid #d5d5d5;">

                    <div class="col-md-12 no-padding-left-right">
                        <p class="title-background">
                         {{related[0].title}}
                        </p>

                    </div>

                </div>

                <div class="row" style="border:1px solid #d5d5d5;border-top: transparent;">
                    <div class="col-md-12 no-padding-left-right">
                        <img data-ng-src="images/lookbook/blogs/{{related[0].banner_img}}.jpg" alt="{{related[0].banner_img}}">
                    </div>
                </div>

                <div class="row related-footer">

                    <div class="col-md-8">
                        {{related[0].category}}
                    </div>

                    <div class="col-md-4 text-right">
                        {{related[0].created_at | dateToISO | date:'dd MMMM'}}
                    </div>
                </div>

            </div>
            </a>
            
  <a data-ng-href="p-lookbookArticle.php?id={{related[1].id}}">          
         <div class="col-md-4">

                <div class="row" style="border:1px solid #d5d5d5;">

                    <div class="col-md-12 no-padding-left-right">
                        <p class="title-background">
                         {{related[1].title}}
                        </p>

                    </div>

                </div>

                <div class="row" style="border:1px solid #d5d5d5;border-top: transparent;">
                    <div class="col-md-12 no-padding-left-right">
                        <img data-ng-src="images/lookbook/blogs/{{related[1].banner_img}}.jpg" alt="{{related[1].banner_img}}">
                    </div>
                </div>

                <div class="row related-footer">

                    <div class="col-md-8">
                        {{related[1].category}}
                    </div>

                    <div class="col-md-4 text-right">
                        {{related[1].created_at | dateToISO | date:'dd MMMM'}}
                    </div>
                </div>

            </div>
            </a>
     
        <a data-ng-href="p-lookbookArticle.php?id={{related[2].id}}">
         <div class="col-md-4">

                <div class="row" style="border:1px solid #d5d5d5;">

                    <div class="col-md-12 no-padding-left-right">
                        <p class="title-background">
                         {{related[2].title}}
                        </p>

                    </div>

                </div>

                <div class="row" style="border:1px solid #d5d5d5;border-top: transparent;">
                    <div class="col-md-12 no-padding-left-right">
                        <img data-ng-src="images/lookbook/blogs/{{related[2].banner_img}}.jpg" alt="{{related[2].banner_img}}">
                    </div>
                </div>

                <div class="row related-footer">

                    <div class="col-md-8">
                        {{related[2].category}}
                    </div>

                    <div class="col-md-4 text-right">
                        {{related[2].created_at | dateToISO | date:'dd MMMM'}}
                    </div>
                </div>

            </div>
            </a>


        </div>

</div>
    
<vivo-footer></vivo-footer>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.simplyscroll.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoLookbook.js"></script>
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>
    
 </body>
</html>